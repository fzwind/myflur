<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SystemSettingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('system_settings')->insert([
            'email_activation' => 0,
            'notification' => 'none',
            'network_type' => 'binary',
            'logging' => 1,
            'proof_bank_transfer_upload' => 0,
            'rewards' => 1,
            'bonus_sponsor' => 1,
            'bonus_pairing' => 1,
            'bonus_level' => 0,
            'bonus_pairing_level' => 0,
            'memdata_name' => 1,
            'memdata_email' => 1,
            'memdata_address' => 1,
            'memdata_province' => 1,
            'memdata_regency' => 1,
            'memdata_district' => 0,
            'memdata_village' => 0,
            'memdata_zip' => 0,
            'memdata_hp' => 1,
            'memdata_id_number' => 0,
            'memdata_tax_id_number' => 0,
            'memdata_sex' => 0,
            'memdata_birth_date' => 0,
            'member_max_bank_account' => 15,
            'member_bank_name_match' => 0,
            'member_bank_unique' => 0,
        ]);

        DB::table('withdrawal_settings')->insert([
            'minimum_wd' => 30000,
            'wd_admin' => 6500,
            'wd_index_deduction' => 10,
            'wd_mode' => 1,
            'wd_days' => '["0","1","2","3","4","5","6"]',
            'active_at' => \Carbon\Carbon::now(),
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
        ]);

        DB::table('plans')->insert([
            'code' => 'MF',
            'name' => 'STANDARD PLAN',
            'is_active' => 1,
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
        ]);

        DB::table('bonus_settings')->insert([
            'plan_id' => 1,
            'bonus_sponsor_amount' => 30000,
            'max_pairing_by' => 3,
            'max_pairing_per_day' => 50,
            'max_pairing_amount_per_day' => 500000,
            'over_pairing_handle' => 2,
            'bonus_pairing_amount' => 10000,
            'pair_per_level' => 1,
            'bonus_pairing_level_amount' => 0,
            'max_level_bonus_level' => 0,
            'active_at' => \Carbon\Carbon::now(),
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
        ]);

        DB::table('stockist_settings')->insert([
            'plan_id' => 1,
            'min_order_stockist' => 1,
            'min_order_master_stockist' => 1,
            'member_buy_crossline' => 1,
            'active_at' => \Carbon\Carbon::now(),
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
        ]);

        DB::table('plan_price_histories')->insert([
            'plan_id' => 1,
            'price' => 139000,
            'stockist_price' => 134000,
            'master_stockist_price' => 129000,
            'active_at' => \Carbon\Carbon::now(),
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
        ]);

        DB::table('reward_settings')->insert([
            'plan_id' => 1,
            'bonus_setting_id' => 1,
            'name' => 'Premium',
            'reward_type' => 3,
            'deducted_bonuses' => '["bonus pairing"]',
            'deduction_type' => 2,
            'deduction_value' => 10000,
            'deduction_target' => 10000,
            'active_at' => \Carbon\Carbon::now(),
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
        ]);
    }
}
