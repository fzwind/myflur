<?php

use Illuminate\Database\Seeder;
use App\Models\Memberstructure;
use App\Models\User;

class MemberstructuresTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*
    	 * Place top member user as root at members structure
    	 */
        $clsUser = new User();
        $user = $clsUser->where('username', '=', 'myflur')->first();

        $top = Memberstructure::create([
            'user_id' => $user->id,
            'parent_id' => null,
            'code' => '1.1',
            'position' => null,
            'level' => 1,
            'pin_code' => null,
            'is_company' => 1
        ]);

        /*
         * Place company accounts at members structure
         */
        for ($i = 2; $i <= 7; $i++) {
            $pos = 2;
            $level = 2;
            if ($i % 2 == 0) {
                $pos = 1;
            }

            if ($i == 2 || $i == 3) {
                ${'parent'. $i} = Memberstructure::create([
                    'user_id' => User::where('username', 'myflur'. ($i -1))->first()->id,
                    'parent_id' => $top->id,
                    'code' => $top->code .'-'. $level .'.'. $pos,
                    'position' => $pos,
                    'level' => $level,
                    'pin_code' => null,
                    'is_company' => 1
                ]);
            } else {
                $level = count(explode('-', ${'parent'. (floor($i/2))}->code)) + 1;
                ${'parent'. $i} = Memberstructure::create([
                    'user_id' => User::where('username', 'myflur'. ($i -1))->first()->id,
                    'parent_id' => ${'parent'. (floor($i/2))}->id,
                    'code' => ${'parent'. (floor($i/2))}->code .'-'. $level .'.'. $pos,
                    'position' => $pos,
                    'level' => $level,
                    'pin_code' => null,
                    'is_company' => 1
                ]);
            }
        }

        // $company = $clsUser->where('username', '=', 'myflur7')->first();
        // $parent = Memberstructure::where('user_id', $company->id)->first();
        // Memberstructure::create([
        //     'user_id' => User::where('username', 'topmember')->first()->id,
        //     'parent_id' => $parent->id,
        //     'code' => $parent->code .'-'. ($parent->level + 1) .'.1',
        //     'position' => 1,
        //     'level' => $parent->level + 1,
        //     'pin_code' => null,
        //     'is_company' => 0
        // ]);
    }
}
