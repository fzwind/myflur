<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RolePermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    /*
    	 * Create roles
    	 */
	    $roles = [
		    'super-administrator',
		    'administrators',
		    'pin-admin',
		    'wd-admin',
		    'member-admin',
		    'stockist-admin',
		    'product-admin',
		    'stock-admin',
		    'delivery-admin',
		    'cs',
		    'members'
	    ];

	    foreach ($roles as $role) {
		    Role::create(['name' => $role]);
	    }

	    /*
	     * Create permissions
	     */
	    $permissions = [
		    'System Area Access',
		    'Admin Area Access',
		    'Member Area Access',
		    'admin pin list',
		    'admin pin approve',
		    'admin pin delete',
		    'admin wd list ',
		    'admin wd approve',
		    'admin wd delete',
		    'admin member list',
		    'admin member edit',
		    'admin member change password',
		    'admin member block',
		    'admin member upgrade stockist',
		    'admin stockist upgrade',
		    'admin stockist delete',
		    'admin product list',
		    'admin product create',
		    'admin product edit',
		    'admin product delete',
		    'admin product bundled list',
		    'admin product bundled create',
		    'admin product bundled edit',
		    'admin product bundled delete',
		    'admin stock list',
		    'admin stock adjustment',
		    'admin stock receive',
		    'admin stock opname',
		    'admin stock warehouse list',
		    'admin stock warehouse create',
		    'admin stock warehouse edit',
		    'admin stock warehouse delete',
		    'admin delivery carrier list',
		    'admin delivery carrier create',
		    'admin delivery carrier edit',
		    'admin delivery carrier delete',
		    'admin delivery carrier tariff list',
		    'admin delivery carrier tariff create',
		    'admin delivery carrier tariff edit',
		    'admin delivery carrier tariff delete',
		    'admin delivery print label',
		    'admin delivery picking',
		    'admin delivery delivery',
		    'admin cs ticket list',
		    'admin cs ticket update',
		    'admin cs ticket delete',
		    'admin plan setting list',
		    'admin plan setting create',
		    'admin plan setting edit',
		    'admin plan setting delete',
		    'admin stockist setting list',
		    'admin stockist setting create',
		    'admin stockist setting edit',
		    'admin stockist setting delete',
		    'admin bonus setting list',
		    'admin bonus setting create',
		    'admin bonus setting edit',
		    'admin bonus setting delete',
		    'admin reward setting list',
		    'admin reward setting create',
		    'admin reward setting edit',
		    'admin reward setting delete',
		    'admin general setting list',
		    'admin general setting create',
		    'admin general setting edit',
		    'admin general setting delete',
		    'admin bank setting list',
		    'admin bank setting create',
		    'admin bank setting edit',
		    'admin bank setting delete',
	    ];

	    foreach ($permissions as $permission) {
		    Permission::create(['name' => $permission]);
	    }

	    /*
	     * Assign all permissions to super-administrator.
	     */
	    $role = Role::findByName('super-administrator');
	    $role->givePermissionTo($permissions);

	    /*
	     * Define administrators  permissions & assign
	     * those permissions to administrators role.
	     */
	    $adminsPermissions = [
		    'Admin Area Access',
		    'admin pin list',
		    'admin pin approve',
		    'admin pin delete',
		    'admin wd list ',
		    'admin wd approve',
		    'admin wd delete',
		    'admin member list',
		    'admin member edit',
		    'admin member change password',
		    'admin member block',
		    'admin member upgrade stockist',
		    'admin stockist upgrade',
		    'admin stockist delete',
		    'admin product list',
		    'admin product create',
		    'admin product edit',
		    'admin product delete',
		    'admin product bundled list',
		    'admin product bundled create',
		    'admin product bundled edit',
		    'admin product bundled delete',
		    'admin stock list',
		    'admin stock adjustment',
		    'admin stock receive',
		    'admin stock opname',
		    'admin stock warehouse list',
		    'admin stock warehouse create',
		    'admin stock warehouse edit',
		    'admin stock warehouse delete',
		    'admin delivery carrier list',
		    'admin delivery carrier create',
		    'admin delivery carrier edit',
		    'admin delivery carrier delete',
		    'admin delivery carrier tariff list',
		    'admin delivery carrier tariff create',
		    'admin delivery carrier tariff edit',
		    'admin delivery carrier tariff delete',
		    'admin delivery print label',
		    'admin delivery picking',
		    'admin delivery delivery',
		    'admin cs ticket list',
		    'admin cs ticket update',
		    'admin cs ticket delete',
		    'admin plan setting list',
		    'admin plan setting create',
		    'admin plan setting edit',
		    'admin plan setting delete',
		    'admin stockist setting list',
		    'admin stockist setting create',
		    'admin stockist setting edit',
		    'admin stockist setting delete',
		    'admin bonus setting list',
		    'admin bonus setting create',
		    'admin bonus setting edit',
		    'admin bonus setting delete',
		    'admin reward setting list',
		    'admin reward setting create',
		    'admin reward setting edit',
		    'admin reward setting delete',
		    'admin general setting list',
		    'admin general setting create',
		    'admin general setting edit',
		    'admin general setting delete',
		    'admin bank setting list',
		    'admin bank setting create',
		    'admin bank setting edit',
		    'admin bank setting delete',
	    ];

	    $role = Role::findByName('administrators');
	    $role->givePermissionTo($adminsPermissions);

	    /*
		 * Define pin-admin  permissions & assign
		 * those permissions to pin-admin role.
		 */
	    $pinAdminPermissions = [
		    'Admin Area Access',
		    'admin pin list',
		    'admin pin approve',
	    ];

	    $role = Role::findByName('pin-admin');
	    $role->givePermissionTo($pinAdminPermissions);

	    /*
		 * Define wd-admin  permissions & assign
		 * those permissions to wd-admin role.
		 */
	    $wdAdminPermissions = [
		    'Admin Area Access',
		    'admin wd list ',
		    'admin wd approve',
	    ];

	    $role = Role::findByName('wd-admin');
	    $role->givePermissionTo($wdAdminPermissions);

	    /*
		 * Define member-admin  permissions & assign
		 * those permissions to member-admin role.
		 */
	    $memberAdminPermissions = [
		    'Admin Area Access',
		    'admin member list',
		    'admin member edit',
		    'admin member change password',
		    'admin member block',
		    'admin member upgrade stockist',
	    ];

	    $role = Role::findByName('member-admin');
	    $role->givePermissionTo($memberAdminPermissions);

	    /*
		 * Define stockist-admin  permissions & assign
		 * those permissions to stockist-admin role.
		 */
	    $stockistAdminPermissions = [
		    'Admin Area Access',
		    'admin member list',
		    'admin member upgrade stockist',
		    'admin stockist upgrade',
	    ];

	    $role = Role::findByName('stockist-admin');
	    $role->givePermissionTo($stockistAdminPermissions);

	    /*
		 * Define product-admin  permissions & assign
		 * those permissions to product-admin role.
		 */
	    $productAdminPermissions = [
		    'Admin Area Access',
		    'admin product list',
		    'admin product create',
		    'admin product edit',
		    'admin product bundled list',
		    'admin product bundled create',
		    'admin product bundled edit',
	    ];

	    $role = Role::findByName('product-admin');
	    $role->givePermissionTo($productAdminPermissions);

	    /*
		 * Define stock-admin  permissions & assign
		 * those permissions to stock-admin role.
		 */
	    $stockAdminPermissions = [
		    'Admin Area Access',
		    'admin stock list',
		    'admin stock adjustment',
		    'admin stock receive',
		    'admin stock opname',
		    'admin stock warehouse list',
		    'admin stock warehouse create',
		    'admin stock warehouse edit',
	    ];

	    $role = Role::findByName('stock-admin');
	    $role->givePermissionTo($stockAdminPermissions);

	    /*
		 * Define delivery-admin  permissions & assign
		 * those permissions to delivery-admin role.
		 */
	    $deliveryAdminPermissions = [
		    'Admin Area Access',
		    'admin delivery carrier list',
		    'admin delivery carrier create',
		    'admin delivery carrier edit',
		    'admin delivery carrier tariff list',
		    'admin delivery carrier tariff create',
		    'admin delivery carrier tariff edit',
		    'admin delivery print label',
		    'admin delivery picking',
		    'admin delivery delivery',
	    ];

	    $role = Role::findByName('delivery-admin');
	    $role->givePermissionTo($deliveryAdminPermissions);

	    /*
		 * Define cs  permissions & assign
		 * those permissions to cs role.
		 */
	    $csPermissions = [
		    'Admin Area Access',
		    'admin cs ticket list',
		    'admin cs ticket update',
	    ];

	    $role = Role::findByName('cs');
	    $role->givePermissionTo($csPermissions);

	    /*
	     * Define members  permissions & assign
	     * those permissions to members role.
	     */
	    $membersPermissions = [
		    'Member Area Access'
	    ];

	    $role = Role::findByName('members');
	    $role->givePermissionTo($membersPermissions);

    }
}
