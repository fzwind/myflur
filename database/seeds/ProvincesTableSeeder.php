<?php

use Illuminate\Database\Seeder;
use App\Models\Province;

class ProvincesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $provinces = [
            ['id' => '11', 'name' => 'ACEH', 'timezone' => 'Asia/Jakarta'],
            ['id' => '12', 'name' => 'SUMATERA UTARA', 'timezone' => 'Asia/Jakarta'],
            ['id' => '13', 'name' => 'SUMATERA BARAT', 'timezone' => 'Asia/Jakarta'],
            ['id' => '14', 'name' => 'RIAU', 'timezone' => 'Asia/Jakarta'],
            ['id' => '15', 'name' => 'JAMBI', 'timezone' => 'Asia/Jakarta'],
            ['id' => '16', 'name' => 'SUMATERA SELATAN', 'timezone' => 'Asia/Jakarta'],
            ['id' => '17', 'name' => 'BENGKULU', 'timezone' => 'Asia/Jakarta'],
            ['id' => '18', 'name' => 'LAMPUNG', 'timezone' => 'Asia/Jakarta'],
            ['id' => '19', 'name' => 'KEPULAUAN BANGKA BELITUNG', 'timezone' => 'Asia/Jakarta'],
            ['id' => '21', 'name' => 'KEPULAUAN RIAU', 'timezone' => 'Asia/Jakarta'],
            ['id' => '31', 'name' => 'DKI JAKARTA', 'timezone' => 'Asia/Jakarta'],
            ['id' => '32', 'name' => 'JAWA BARAT', 'timezone' => 'Asia/Jakarta'],
            ['id' => '33', 'name' => 'JAWA TENGAH', 'timezone' => 'Asia/Jakarta'],
            ['id' => '34', 'name' => 'DI YOGYAKARTA', 'timezone' => 'Asia/Jakarta'],
            ['id' => '35', 'name' => 'JAWA TIMUR', 'timezone' => 'Asia/Jakarta'],
            ['id' => '36', 'name' => 'BANTEN', 'timezone' => 'Asia/Jakarta'],
            ['id' => '51', 'name' => 'BALI', 'timezone' => 'Asia/Makassar'],
            ['id' => '52', 'name' => 'NUSA TENGGARA BARAT', 'timezone' => 'Asia/Makassar'],
            ['id' => '53', 'name' => 'NUSA TENGGARA TIMUR', 'timezone' => 'Asia/Makassar'],
            ['id' => '61', 'name' => 'KALIMANTAN BARAT', 'timezone' => 'Asia/Jakarta'],
            ['id' => '62', 'name' => 'KALIMANTAN TENGAH', 'timezone' => 'Asia/Jakarta'],
            ['id' => '63', 'name' => 'KALIMANTAN SELATAN', 'timezone' => 'Asia/Makassar'],
            ['id' => '64', 'name' => 'KALIMANTAN TIMUR', 'timezone' => 'Asia/Makassar'],
            ['id' => '65', 'name' => 'KALIMANTAN UTARA', 'timezone' => 'Asia/Makassar'],
            ['id' => '71', 'name' => 'SULAWESI UTARA', 'timezone' => 'Asia/Makassar'],
            ['id' => '72', 'name' => 'SULAWESI TENGAH', 'timezone' => 'Asia/Makassar'],
            ['id' => '73', 'name' => 'SULAWESI SELATAN', 'timezone' => 'Asia/Makassar'],
            ['id' => '74', 'name' => 'SULAWESI TENGGARA', 'timezone' => 'Asia/Makassar'],
            ['id' => '75', 'name' => 'GORONTALO', 'timezone' => 'Asia/Makassar'],
            ['id' => '76', 'name' => 'SULAWESI BARAT', 'timezone' => 'Asia/Makassar'],
            ['id' => '81', 'name' => 'MALUKU', 'timezone' => 'Asia/Jayapura'],
            ['id' => '82', 'name' => 'MALUKU UTARA', 'timezone' => 'Asia/Jayapura'],
            ['id' => '91', 'name' => 'PAPUA BARAT', 'timezone' => 'Asia/Jayapura'],
            ['id' => '94', 'name' => 'PAPUA', 'timezone' => 'Asia/Jayapura']
        ];

        Province::insert($provinces);
    }
}
