<?php

use Illuminate\Database\Seeder;
use App\Models\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*
         * Create super-administrator user
         */
        $clsUser = new User;
        $clsUser->name = 'Super Administrator';
        $clsUser->email = 'superadmin@myflur.com';
        $clsUser->username = 'superadmin';
        $clsUser->province_id = 35;
        $clsUser->regency_id = 3578;
        $clsUser->password = bcrypt('asyik1357');
        $clsUser->is_active = 1;
        $clsUser->save();

        /*
         * Assign super-administrator role to super-administrator user
         */
        $clsUser->assignRole('super-administrator');

        /*
         * Create administrators user
         */
        $clsUser = new User;
        $clsUser->name = 'Administrator';
        $clsUser->email = 'admin@myflur.com';
        $clsUser->username = 'admin';
        $clsUser->province_id = 35;
        $clsUser->regency_id = 3578;
        $clsUser->password = bcrypt('asyik1357');
        $clsUser->is_active = 1;
        $clsUser->save();

        /*
         * Assign administrators role to administrators user
         */
        $clsUser->assignRole('administrators');

        /*
         * Create pin-admin user
         */
        $clsUser = new User;
        $clsUser->name = 'Pin Administrator';
        $clsUser->email = 'pin-admin@myflur.com';
        $clsUser->username = 'pin-admin';
        $clsUser->province_id = 35;
        $clsUser->regency_id = 3578;
        $clsUser->password = bcrypt('asyik1357');
        $clsUser->is_active = 1;
        $clsUser->save();

        /*
         * Assign administrators role to administrators user
         */
        $clsUser->assignRole('pin-admin');

        /*
         * Create wd-admin user
         */
        $clsUser = new User;
        $clsUser->name = 'WD Administrator';
        $clsUser->email = 'wd-admin@myflur.com';
        $clsUser->username = 'wd-admin';
        $clsUser->province_id = 35;
        $clsUser->regency_id = 3578;
        $clsUser->password = bcrypt('asyik1357');
        $clsUser->is_active = 1;
        $clsUser->save();

        /*
         * Assign administrators role to administrators user
         */
        $clsUser->assignRole('wd-admin');

        /*
         * Create member-admin user
         */
        $clsUser = new User;
        $clsUser->name = 'Member Administrator';
        $clsUser->email = 'member-admin@myflur.com';
        $clsUser->username = 'member-admin';
        $clsUser->province_id = 35;
        $clsUser->regency_id = 3578;
        $clsUser->password = bcrypt('asyik1357');
        $clsUser->is_active = 1;
        $clsUser->save();

        /*
         * Assign administrators role to administrators user
         */
        $clsUser->assignRole('member-admin');

        /*
         * Create stockist-admin user
         */
        $clsUser = new User;
        $clsUser->name = 'Stockist Administrator';
        $clsUser->email = 'stockist-admin@myflur.com';
        $clsUser->username = 'stockist-admin';
        $clsUser->province_id = 35;
        $clsUser->regency_id = 3578;
        $clsUser->password = bcrypt('asyik1357');
        $clsUser->is_active = 1;
        $clsUser->save();

        /*
         * Assign administrators role to administrators user
         */
        $clsUser->assignRole('stockist-admin');

        /*
         * Create product-admin user
         */
        $clsUser = new User;
        $clsUser->name = 'Product Administrator';
        $clsUser->email = 'product-admin@myflur.com';
        $clsUser->username = 'product-admin';
        $clsUser->province_id = 35;
        $clsUser->regency_id = 3578;
        $clsUser->password = bcrypt('asyik1357');
        $clsUser->is_active = 1;
        $clsUser->save();

        /*
         * Assign administrators role to administrators user
         */
        $clsUser->assignRole('product-admin');

        /*
         * Create stock-admin user
         */
        $clsUser = new User;
        $clsUser->name = 'Stock Administrator';
        $clsUser->email = 'stock-admin@myflur.com';
        $clsUser->username = 'stock-admin';
        $clsUser->province_id = 35;
        $clsUser->regency_id = 3578;
        $clsUser->password = bcrypt('asyik1357');
        $clsUser->is_active = 1;
        $clsUser->save();

        /*
         * Assign administrators role to administrators user
         */
        $clsUser->assignRole('stock-admin');

        /*
         * Create delivery-admin user
         */
        $clsUser = new User;
        $clsUser->name = 'Delivery Administrator';
        $clsUser->email = 'delivery-admin@myflur.com';
        $clsUser->username = 'delivery-admin';
        $clsUser->province_id = 35;
        $clsUser->regency_id = 3578;
        $clsUser->password = bcrypt('asyik1357');
        $clsUser->is_active = 1;
        $clsUser->save();

        /*
         * Assign administrators role to administrators user
         */
        $clsUser->assignRole('delivery-admin');

        /*
         * Create cs user
         */
        $clsUser = new User;
        $clsUser->name = 'Customer Service';
        $clsUser->email = 'cs@myflur.com';
        $clsUser->username = 'cs';
        $clsUser->province_id = 35;
        $clsUser->regency_id = 3578;
        $clsUser->password = bcrypt('asyik1357');
        $clsUser->is_active = 1;
        $clsUser->save();

        /*
         * Assign administrators role to administrators user
         */
        $clsUser->assignRole('cs');

        /*
         * Create top company user
         */
        $clsUser = new User;
        $clsUser->name = 'Myflur';
        $clsUser->email = 'myflur1@myflur.com';
        $clsUser->username = 'myflur';
        $clsUser->province_id = 35;
        $clsUser->regency_id = 3578;
        $clsUser->password = bcrypt('asyik1357');
        $clsUser->is_stockist = 1;
        $clsUser->stockist_type = 0;
        $clsUser->stockist_at = \Carbon\Carbon::now();
        $clsUser->is_active = 1;
        $clsUser->save();

        /*
         * Assign members role to top company user
         */
        $clsUser->assignRole('members');

        /*
         * Create company account user &
         * assign member role to them.
         */
        $clsUser = new User;
        $top = $clsUser->where('username', '=', 'myflur')->first();

        for ($i = 1; $i <= 6; $i++) {
            $clsUser = new User();
            $clsUser->name = 'Myflur '. $i;
            $clsUser->email = 'myflur'. $i .'@myflur.com';
            $clsUser->username = 'myflur'. $i;
            $clsUser->province_id = 35;
            $clsUser->regency_id = 3578;
            $clsUser->password = bcrypt('asyik1357');
            $clsUser->sponsor_id = $top->id;
            $clsUser->is_active = 1;
            // if ($i >= 7) {
            //     $clsUser->is_stockist = 1;
            //     $clsUser->stockist_type = 0;
            //     $clsUser->stockist_at = \Carbon\Carbon::now('Asia/Jakarta');
            // }
            $clsUser->save();
            $clsUser->assignRole('members');
        }

        for ($i = 1; $i <= 100; $i++) {
            $clsUser = new User();
            $clsUser->name = 'User '. $i;
            $clsUser->email = 'user'. $i .'@myflur.com';
            $clsUser->username = 'user'. $i;
            $clsUser->province_id = 35;
            $clsUser->regency_id = 3578;
            $clsUser->password = bcrypt('asyik1357');
            $clsUser->sponsor_id = $top->id;
            $clsUser->is_active = 0;
            $clsUser->join_plan = 1;
            $clsUser->current_plan = 1;
            $clsUser->current_plan_at = \Carbon\Carbon::now('Asia/Jakarta');
            // if ($i >= 7) {
            //     $clsUser->is_stockist = 1;
            //     $clsUser->stockist_type = 0;
            //     $clsUser->stockist_at = \Carbon\Carbon::now('Asia/Jakarta');
            // }
            $clsUser->save();
            $clsUser->assignRole('members');
        }
    }
}
