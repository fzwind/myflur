<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(ProvincesTableSeeder::class);
        $this->call(RegenciesTableSeeder::class);
        $this->call(DistrictsTableSeeder::class);
        $this->call(VillagesTableSeeder::class);
        $this->call(VillagesTableSeeder2::class);
        $this->call(VillagesTableSeeder3::class);
        $this->call(VillagesTableSeeder4::class);
        $this->call(RolePermissionTableSeeder::class);
        $this->call(SystemSettingsSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(MemberstructuresTableSeeder::class);
    }
}
