<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRewardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::create('reward_settings', function (Blueprint $table) {
		    $table->increments('id');
		    $table->unsignedInteger('plan_id');
		    $table->unsignedInteger('bonus_setting_id');
		    $table->string('name',50);
		    $table->tinyInteger('reward_type')->default(1)->comment('1: murni titik; 2= Q (potong bonus)');
		    $table->string('deducted_bonuses',255)->nullable();
		    $table->integer('deduction_type')->nullable()->comment('1:percentage ; 2:amount');
		    $table->integer('deduction_value')->nullable();
		    $table->integer('deduction_target')->nullable();
		    $table->unsignedInteger('reward_prerequisite')->nullable();
		    $table->timestamp('active_at')->nullable();
		    $table->timestamp('inactive_at')->nullable();
		    $table->timestamps();

		    $table->foreign('plan_id')->references('id')->on('plans');
		    $table->foreign('bonus_setting_id')->references('id')->on('bonus_settings');
		    $table->foreign('reward_prerequisite')->references('id')->on('reward_settings');
	    });

        Schema::create('rewards', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('reward_setting_id');
	        $table->tinyInteger('reward_type')->default(1)->comment('1: tunai; 2: nontunai');
	        $table->boolean('can_claim_as_value')->nullable()->comment('1: Ya; 0: Tidak');
            $table->integer('left');
            $table->integer('right');
            $table->string('reward',255);
            $table->integer('reward_value');
            $table->tinyInteger('reward_deduction_type')->default(1)->comment('0: none; 1: percentage; 2: value');
            $table->integer('reward_deduction_on_claim');
            $table->timestamp('active_at');
            $table->timestamp('inactive_at')->nullable();
            $table->timestamps();

            $table->foreign('reward_setting_id')->references('id')->on('reward_settings');
        });

	    Schema::create('member_reward_savings', function (Blueprint $table) {
		    $table->increments('id');
		    $table->unsignedInteger('reward_setting_id');
		    $table->unsignedInteger('withdrawal_id');
		    $table->unsignedInteger('user_id');
		    $table->integer('saving_amount');
		    $table->timestamps();

		    $table->foreign('reward_setting_id')->references('id')->on('reward_settings');
		    $table->foreign('withdrawal_id')->references('id')->on('withdrawals');
		    $table->foreign('user_id')->references('id')->on('users');
	    });

	    Schema::create('member_rewards', function (Blueprint $table) {
		    $table->increments('id');
		    $table->unsignedInteger('reward_id');
		    $table->unsignedInteger('user_id');
		    $table->smallInteger('left');
		    $table->smallInteger('right');
		    $table->tinyInteger('is_achieved')->default(0);
		    $table->timestamp('achieved_at')->nullable();
		    $table->tinyInteger('is_claimed')->default(0);
		    $table->timestamp('claimed_at')->nullable();
		    $table->tinyInteger('is_received')->default(0);
		    $table->timestamp('received_at')->nullable();
		    $table->timestamps();

		    $table->foreign('reward_id')->references('id')->on('rewards');
		    $table->foreign('user_id')->references('id')->on('users');
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::disableForeignKeyConstraints();
	    Schema::dropIfExists('reward_settings');
        Schema::dropIfExists('rewards');
	    Schema::dropIfExists('member_reward_savings');
	    Schema::dropIfExists('member_rewards');
	    Schema::enableForeignKeyConstraints();
    }
}
