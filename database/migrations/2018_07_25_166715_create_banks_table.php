<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBanksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('master_banks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',50);
            $table->tinyInteger('is_active')->default(1);
            $table->timestamps();
        });

	    Schema::create('user_banks', function (Blueprint $table) {
		    $table->increments('id');
		    $table->unsignedInteger('user_id');
		    $table->unsignedInteger('bank_id');
		    $table->string('account_number',50);
		    $table->string('account_name',50);
		    $table->tinyInteger('is_active')->default(1);
		    $table->timestamps();

		    $table->foreign('user_id')->references('id')->on('users');
		    $table->foreign('bank_id')->references('id')->on('master_banks');
	    });

	    Schema::create('company_banks', function (Blueprint $table) {
		    $table->increments('id');
		    $table->unsignedInteger('bank_id');
		    $table->string('account_number',50);
		    $table->string('account_name',50);
		    $table->tinyInteger('is_active')->default(1);
		    $table->timestamps();

		    $table->foreign('bank_id')->references('id')->on('master_banks');
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('master_banks');
	    Schema::dropIfExists('user_banks');
	    Schema::dropIfExists('company_banks');
	    Schema::enableForeignKeyConstraints();
    }
}
