<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSystemSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('system_settings', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('email_activation')->default(0);
			$table->string('notification')->default('none')->comment('available options: 1. none, 2: email, 3: app, 4: email-app');
			$table->string('network_type',10)->default('binary')->comment('available options: 1. binary, 2. matrix');
			$table->tinyInteger('logging')->default(0);
	        $table->tinyInteger('proof_bank_transfer_upload')->default(0);
			$table->tinyInteger('rewards')->default(1);
			$table->tinyInteger('bonus_sponsor')->default(1);
			$table->tinyInteger('bonus_pairing')->default(1);
			$table->tinyInteger('bonus_level')->default(0);
			$table->tinyInteger('bonus_pairing_level')->default(1);
			$table->tinyInteger('memdata_name')->default(1);
			$table->tinyInteger('memdata_email')->default(1);
			$table->tinyInteger('memdata_address')->default(0);
			$table->tinyInteger('memdata_province')->default(0);
			$table->tinyInteger('memdata_regency')->default(0);
			$table->tinyInteger('memdata_district')->default(0);
			$table->tinyInteger('memdata_village')->default(0);
			$table->tinyInteger('memdata_zip')->default(0);
			$table->tinyInteger('memdata_hp')->default(0);
			$table->tinyInteger('memdata_id_number')->default(0);
			$table->tinyInteger('memdata_tax_id_number')->default(0);
			$table->tinyInteger('memdata_sex')->default(0);
			$table->tinyInteger('memdata_birth_date')->default(0);
			$table->smallInteger('member_max_bank_account')->default(3);
			$table->tinyInteger('member_bank_name_match')->default(1);
			$table->tinyInteger('member_bank_unique')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('system_settings');
    }
}
