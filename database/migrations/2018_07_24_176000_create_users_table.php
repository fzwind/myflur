<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email', 100)->unique();
            $table->string('username', 30)->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->unsignedInteger('sponsor_id')->nullable();
            $table->string('timezone', 50)->default('Asia/Jakarta');
            $table->string('address', 255)->nullable();
            $table->char('province_id', 2)->nullable();
            $table->char('regency_id', 4)->nullable();
            $table->char('district_id', 7)->nullable();
            $table->char('village_id', 10)->nullable();
            $table->string('postal_code', 5)->nullable();
            $table->string('no_hp', 20)->nullable();
            $table->string('no_id', 50)->nullable();
            $table->string('no_tax_id', 50)->nullable();
            $table->string('sex', 6)->nullable();
            $table->timestamp('birth_date')->nullable();
            $table->tinyInteger('is_stockist')->default(0);
            $table->smallInteger('stockist_type')->default(0)->comment('0=stockist; 1=master stockist-setting');
            $table->timestamp('stockist_at')->nullable();
            $table->tinyInteger('is_active')->default(0);
            $table->timestamp('active_at')->nullable();
            $table->timestamp('inactive_at')->nullable();
            $table->unsignedInteger('join_plan')->nullable();
            $table->unsignedInteger('current_plan')->nullable();
            $table->timestamp('current_plan_at')->nullable();
            $table->tinyInteger('is_qualified')->default(0);
            $table->timestamp('qualified_at')->nullable();
            $table->rememberToken();
            $table->timestamps();

            $table->foreign('province_id')->references('id')->on('provinces');
            $table->foreign('regency_id')->references('id')->on('regencies');
            $table->foreign('district_id')->references('id')->on('districts');
            $table->foreign('village_id')->references('id')->on('villages');
            $table->foreign('join_plan')->references('id')->on('plans');
            $table->foreign('current_plan')->references('id')->on('plans');
            $table->foreign('sponsor_id')->references('id')->on('users');
        });

        Schema::create('password_resets', function (Blueprint $table) {
            $table->string('email')->index();
            $table->string('token');
            $table->timestamp('created_at')->nullable();
        });

        Schema::create('member_plan_histories', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('plan_id');
            $table->timestamps();

            $table->unique(['user_id','plan_id','created_at']);
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('plan_id')->references('id')->on('plans');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('users');
        Schema::dropIfExists('password_resets');
        Schema::dropIfExists('member_plan_histories');
        Schema::enableForeignKeyConstraints();
    }
}
