<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUniqueColumnToUserPinsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::disableForeignKeyConstraints();
        Schema::table('user_pins', function (Blueprint $table) {
            $table->dropUnique('user_pins_user_id_pin_code_unique');
            $table->unique(['user_id','pin_code','pin_transaction_id']);
        });
	    Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::disableForeignKeyConstraints();
        Schema::table('user_pins', function (Blueprint $table) {
            $table->dropUnique('user_pins_user_id_pin_code_pin_transaction_id_unique');
            $table->unique(['user_id','pin_code']);
        });
	    Schema::enableForeignKeyConstraints();
    }
}
