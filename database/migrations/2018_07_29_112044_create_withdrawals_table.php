<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWithdrawalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::create('withdrawal_settings', function (Blueprint $table) {
		    $table->increments('id');
		    $table->integer('wd_admin')->default(0);
		    $table->integer('wd_index_deduction')->default(0)->comment('in percent');
		    $table->tinyInteger('wd_mode')->default(1)->comment('1: auto; 2: manual');
		    $table->string('wd_days',255)->nullable();
		    $table->timestamp('active_at')->nullable();
		    $table->timestamps();
	    });

	    Schema::create('withdrawals', function (Blueprint $table) {
            $table->increments('id');
            $table->char('code',16);
            $table->unsignedInteger('user_id');
            $table->integer('wd_amount');
            $table->integer('wd_index_deduction')->default(0);
            $table->integer('wd_reward_deduction')->default(0);
            $table->integer('wd_admin');
            $table->tinyInteger('status')->default(0)->comment('0: created; 1: transfered');
            $table->string('notes',255)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::disableForeignKeyConstraints();
	    Schema::dropIfExists('withdrawal_settings');
        Schema::dropIfExists('withdrawals');
	    Schema::enableForeignKeyConstraints();
    }
}
