<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMemberstructuresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('memberstructures', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
	        $table->unsignedInteger('parent_id')->nullable();
            $table->text('code');
            $table->tinyInteger('position')->nullable()->default(1);
            $table->tinyInteger('level')->nullable();
            $table->char('pin_code',12)->nullable();
            $table->tinyInteger('is_company')->default(0);
            $table->timestamps();

	        $table->unique(['parent_id','position']);
	        $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('parent_id')->references('id')->on('memberstructures');
            $table->foreign('pin_code')->references('code')->on('pins');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('memberstructures');
	    Schema::enableForeignKeyConstraints();
    }
}
