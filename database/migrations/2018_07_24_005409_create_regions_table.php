<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('provinces', function (Blueprint $table) {
            $table->char('id',2)->primary();
            $table->string('name',100)->unique();
            $table->string('timezone',50);
            $table->timestamps();
        });

	    Schema::create('regencies', function (Blueprint $table) {
		    $table->char('id',4)->primary();
		    $table->char('province_id',2);
		    $table->string('name',100)->unique();
		    $table->timestamps();

		    $table->foreign('province_id')->references('id')->on('provinces');
	    });

	    Schema::create('districts', function (Blueprint $table) {
		    $table->char('id',7)->primary();
		    $table->char('regency_id',4);
		    $table->string('name',100);
		    $table->timestamps();

		    $table->foreign('regency_id')->references('id')->on('regencies');
	    });

	    Schema::create('villages', function (Blueprint $table) {
		    $table->char('id',10)->primary();
		    $table->char('district_id',7);
		    $table->string('name',100);
		    $table->timestamps();

		    $table->foreign('district_id')->references('id')->on('districts');
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('provinces');
	    Schema::dropIfExists('regencies');
	    Schema::dropIfExists('districts');
	    Schema::dropIfExists('villages');
	    Schema::enableForeignKeyConstraints();
    }
}
