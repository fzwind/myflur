<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plans', function (Blueprint $table) {
            $table->increments('id');
	        $table->char('code',5);
            $table->string('name',100);
            $table->tinyInteger('is_active')->default(0);
            $table->timestamps();
        });

	    Schema::create('plan_price_histories', function (Blueprint $table) {
		    $table->increments('id');
		    $table->unsignedInteger('plan_id');
		    $table->integer('price')->default(0);
		    $table->integer('stockist_price')->default(0);
		    $table->integer('master_stockist_price')->default(0);
		    $table->timestamp('active_at')->nullable();
		    $table->timestamps();

		    $table->foreign('plan_id')->references('id')->on('plans');
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('plans');
	    Schema::dropIfExists('plan_price_histories');
	    Schema::enableForeignKeyConstraints();
    }
}
