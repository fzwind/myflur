<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBonusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bonus_settings', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('plan_id');
            $table->integer('bonus_sponsor_amount')->nullable();
            $table->tinyInteger('max_pairing_by')->default(1)->comment('1=pairs; 2=pair amount; 3=both, which one achieved');
            $table->smallInteger('max_pairing_per_day')->default(0);
            $table->integer('max_pairing_amount_per_day')->default(0);
            $table->tinyInteger('over_pairing_handle')->default(1)->comment('1=carry forward; 2=flush');
            $table->integer('bonus_pairing_amount')->nullable();
            $table->tinyInteger('pair_per_level')->nullable();
            $table->integer('bonus_pairing_level_amount')->nullable();
            $table->smallInteger('max_level_bonus_level')->default(0);
            $table->timestamp('active_at')->nullable();
            $table->timestamps();

            $table->unique(['plan_id','active_at']);
            $table->foreign('plan_id')->references('id')->on('plans');
        });

        Schema::create('bonus_level_settings', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('bonus_setting_id');
            $table->smallInteger('level');
            $table->integer('bonus_level_amount');
            $table->timestamps();

            $table->foreign('bonus_setting_id')->references('id')->on('bonus_settings');
        });

        Schema::create('bonuses', function (Blueprint $table) {
            $table->increments('id');
            $table->char('code', 16);
            $table->unsignedInteger('plan_id');
            $table->unsignedInteger('bonus_setting_id');
            $table->unsignedInteger('user_id');
            $table->integer('bonus_sponsor')->nullable();
            $table->integer('bonus_pairing')->nullable();
            $table->integer('bonus_pairing_level')->nullable();
            $table->integer('bonus_level')->nullable();
            $table->tinyInteger('is_bonus_sponsor')->default(0);
            $table->tinyInteger('is_bonus_pairing')->default(0);
            $table->unsignedInteger('bonus_from');
            $table->tinyInteger('level');
            $table->timestamps();

            $table->foreign('plan_id')->references('id')->on('plans');
            $table->foreign('bonus_setting_id')->references('id')->on('bonus_settings');
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('bonus_from')->references('id')->on('users');
        });

        Schema::create('flushed_pairings', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->integer('flushed_left');
            $table->integer('flushed_right');
            $table->unsignedInteger('placement_id');
            $table->timestamp('flushed_at');
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('placement_id')->references('id')->on('memberstructures');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('bonus_settings');
        Schema::dropIfExists('bonus_level_settings');
        Schema::dropIfExists('bonuses');
        Schema::dropIfExists('flushed_pairings');
        Schema::enableForeignKeyConstraints();
    }
}
