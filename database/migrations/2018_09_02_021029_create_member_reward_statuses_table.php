<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMemberRewardStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('member_reward_statuses', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('reward_setting_id');
            $table->timestamps();

	        $table->unique(['user_id','reward_setting_id']);
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('reward_setting_id')->references('id')->on('reward_settings');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('member_reward_statuses');
	    Schema::enableForeignKeyConstraints();
    }
}
