<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDetailWithdrawalDataToWithdrawalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('withdrawals', function (Blueprint $table) {
	        $table->integer('bonus_level')->after('user_id')->default(0);
	        $table->integer('bonus_pairing_level')->after('user_id')->default(0);
	        $table->integer('bonus_pairing')->after('user_id')->default(0);
            $table->integer('bonus_sponsor')->after('user_id')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('withdrawals', function (Blueprint $table) {
            $table->dropColumn('bonus_level');
	        $table->dropColumn('bonus_pairing_level');
	        $table->dropColumn('bonus_pairing');
	        $table->dropColumn('bonus_sponsor');
        });
    }
}
