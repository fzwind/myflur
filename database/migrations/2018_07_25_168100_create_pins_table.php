<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePinsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pins', function (Blueprint $table) {
            $table->char('code',12)->unique()->index()->key();
            $table->unsignedInteger('plan_id');
            $table->tinyInteger('status')->default(0)->comment('0=available, 1=sold, 2=for testing');
            $table->timestamps();

            $table->foreign('plan_id')->references('id')->on('plans');
        });

	    Schema::create('pin_transactions', function (Blueprint $table) {
		    $table->increments('id');
		    $table->char('transaction_code',14);
		    $table->unsignedInteger('plan_id');
		    $table->unsignedInteger('from')->nullable();
		    $table->unsignedInteger('to');
		    $table->string('transaction_type',20)->comment('buy, transfer');
		    $table->smallInteger('amount');
		    $table->integer('price');
		    $table->smallInteger('unique_digit')->nullable();
		    $table->string('status',20)->comment('ordered, paid, confirmed, rejected');
		    $table->string('status_note',255)->nullable();
		    $table->timestamp('ordered_at')->nullable();
		    $table->unsignedInteger('paid_company_to')->nullable();
		    $table->unsignedInteger('paid_to')->nullable();
		    $table->string('paid_file',100)->nullable();
		    $table->timestamp('paid_at')->nullable();
		    $table->timestamp('confirmed_at')->nullable();
		    $table->timestamp('rejected_at')->nullable();
		    $table->timestamps();

		    $table->unique(['transaction_code']);
		    $table->foreign('from')->references('id')->on('users');
		    $table->foreign('to')->references('id')->on('users');
		    $table->foreign('plan_id')->references('id')->on('plans');
		    $table->foreign('paid_company_to')->references('id')->on('company_banks');
		    $table->foreign('paid_to')->references('id')->on('user_banks');
	    });

	    Schema::create('user_pins', function (Blueprint $table) {
		    $table->increments('id');
		    $table->unsignedInteger('user_id');
		    $table->unsignedInteger('pin_transaction_id');
		    $table->char('pin_code',12);
		    $table->tinyInteger('status')->default(0)->comment('0=available; 1=sold; 2=transfered; 3=used');
		    $table->timestamps();

		    $table->unique(['user_id', 'pin_code']);
		    $table->foreign('user_id')->references('id')->on('users');
		    $table->foreign('pin_transaction_id')->references('id')->on('pin_transactions');
		    $table->foreign('pin_code')->references('code')->on('pins');
	    });

	    Schema::create('stockist_settings', function (Blueprint $table) {
		    $table->increments('id');
		    $table->unsignedInteger('plan_id');
		    $table->integer('min_order_stockist')->default(0);
		    $table->integer('min_order_master_stockist')->default(0);
		    $table->tinyInteger('member_buy_crossline')->default(0)->comment('0=disallowed; 1=allowed');
		    $table->timestamp('active_at')->nullable();
		    $table->timestamps();

		    $table->foreign('plan_id')->references('id')->on('plans');
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('pins');
	    Schema::dropIfExists('pin_transactions');
	    Schema::dropIfExists('user_pins');
	    Schema::dropIfExists('stockist_settings');
	    Schema::enableForeignKeyConstraints();
    }
}
