@extends('layouts.member')

@section('content')
<div class="container-fluid">
  <div class="row">
    <div class="col-lg-4 col-md-12 col-sm-12">
      <div class="card card-stats">
        <div class="card-header card-header-warning card-header-icon">
          <div class="card-icon">
            <i class="material-icons">star</i>
          </div>
          <p class="card-category">Total Bonus</p>
          <h3 class="card-title">Rp {{ number_format($mySumBonus,0,',','.') }}
          </h3>
        </div>
      </div>
    </div>
    <div class="col-lg-4 col-md-6 col-sm-6">
      <div class="card card-stats">
        <div class="card-header card-header-success card-header-icon">
          <div class="card-icon">
            <i class="material-icons">supervisor_account</i>
          </div>
          <p class="card-category">Bonus Sponsor</p>
          <h3 class="card-title">{{ number_format($myBonusSponsor,0,',','.') }}</h3>
        </div>
      </div>
    </div>
    <div class="col-lg-4 col-md-6 col-sm-6">
      <div class="card card-stats">
        <div class="card-header card-header-danger card-header-icon">
          <div class="card-icon">
            <i class="material-icons">supervisor_account</i>
          </div>
          <p class="card-category">Bonus Pairing</p>
          <h3 class="card-title">{{ number_format($myBonusPairing,0,',','.') }}</h3>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title">Berita</h4>
                </div>
                <div class="card-body">
                  <div id="accordion" role="tablist">
                    @forelse($news as $article)
                    <div class="card-collapse">
                      <div class="card-header" role="tab" id="headingOne">
                        <h5 class="mb-0">
                          <a data-toggle="collapse" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne" class="collapsed" style="">
                            {{$article->title}}
                            <i class="material-icons">keyboard_arrow_down</i>
                          </a>
                        </h5>
                      </div>
                      <div id="collapseOne" class="collapse" role="tabpanel" aria-labelledby="headingOne" data-parent="#accordion" style="">
                        <div class="card-body">
                          {!! $article->description !!}
                      </div>
                    </div>
                    @empty
                        <p>Belum ada berita dari perusahaan</p>
                    @endforelse

                  </div>
                </div>
              </div>
    </div>
  </div>
</div>
@stop

@section('page_script')
    @if (session('UnauthorizedException'))
        <div id="modalerror" class="modal fade">
            <div class="modal-dialog" role="document">
                <div class="modal-content tx-size-sm">
                    <div class="modal-body tx-center pd-y-20 pd-x-20">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <i class="icon ion-ios-close-outline tx-100 tx-danger lh-1 mg-t-20 d-inline-block"></i>
                        <h4 class="tx-danger tx-semibold mg-b-20">Error!</h4>
                        <p class="mg-b-20 mg-x-20"><strong>{{ session('UnauthorizedException') }}</strong></p>
                        <button type="button" class="btn btn-danger tx-11 tx-uppercase pd-y-12 pd-x-25 tx-mont tx-medium mg-b-20" data-dismiss="modal" aria-label="Close"> OK</button>
                    </div><!-- modal-body -->
                </div><!-- modal-content -->
            </div><!-- modal-dialog -->
        </div><!-- modal -->
        <script>
            $('#modalerror').modal('show');
        </script>
    @endif
@endsection
