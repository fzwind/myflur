@extends('layouts.member')

@section('styles')
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="#">evolve.co.id</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('home') }}">member area</a></li>
                        <li class="breadcrumb-item">user</li>
                        <li class="breadcrumb-item active">akun saya</li>
                    </ol>
                </div>
                <h4 class="page-title">Akun Saya</h4>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-3">
                            <ul class="nav flex-column">
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('my.account') }}"><i class="fe-chevron-right"></i> Profil</a>
                                </li>
                                @if(system_settings()->memdata_address || system_settings()->memdata_province || system_settings()->memdata_regency || system_settings()->memdata_district || system_settings()->memdata_village || system_settings()->memdata_zip)
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('my.account.address') }}"><i class="fe-chevron-right"></i> Alamat</a>
                                </li>
                                @endif
                                <li class="nav-item">
                                    <a class="nav-link active"><i class="fe-chevron-right"></i> Identitas</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('my.account.bank') }}"><i class="fe-chevron-right"></i> Bank</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('my.account.changepw') }}"><i class="fe-chevron-right"></i> Ganti Password</a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-9">
                            <h3 class="header-title">Identitas</h3>
                            <p class="text-muted font-14">Data kartu identitas Anda.</p>
                            <form class="form-horizontal" method="post" action="{{ route('my.account.store',['user' => auth()->user()]) }}">
                                <input type="hidden" name="_method" value="PUT">
                                @csrf
                                @system('memdata_id_number')
                                <div class="form-group row">
                                    <label for="no_id" class="col-2 col-form-label">No. KTP:</label>
                                    <div class="col-10">
                                        <input class="form-control col-6" type="text" id="no_id" name="no_id" value="{{ auth()->user()->no_id }}">
                                        @if($errors->has('no_id'))
                                            <small class="form-text text-danger">{{ $errors->first('no_id') }}</small>
                                        @endif
                                    </div>
                                </div>
                                @endsystem
                                @system('memdata_tax_id_number')
                                <div class="form-group row">
                                    <label for="no_tax_id" class="col-2 col-form-label">NPWP:</label>
                                    <div class="col-10">
                                        <input class="form-control col-6" type="text" id="no_tax_id" name="no_tax_id" value="{{ auth()->user()->no_tax_id }}">
                                        @if($errors->has('no_tax_id'))
                                            <small class="form-text text-danger">{{ $errors->first('no_tax_id') }}</small>
                                        @endif
                                    </div>
                                </div>
                                @endsystem
                                <button type="submit" class="btn btn-info mt-3">Save</button>
                                <a class="btn btn-secondary mt-3" href="{{ route('bonus-settings.index') }}">Cancel</a>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
@endsection

@section('page_script')
    @if (session('success'))
        <script>
            toastr.options = {
                "closeButton": true,
                "debug": false,
                "newestOnTop": false,
                "progressBar": true,
                "positionClass": "toast-bottom-right",
                "preventDuplicates": false,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            }
            toastr["success"]("{{ session('success') }}", "Success!");
        </script>
    @endif

    @if (session('error'))
        <script>
            toastr.options = {
                "closeButton": true,
                "debug": false,
                "newestOnTop": false,
                "progressBar": true,
                "positionClass": "toast-bottom-right",
                "preventDuplicates": false,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            }
            toastr["error"]("{{ session('error') }}", "Error!");
        </script>
    @endif
@endsection