@extends('layouts.member')

@section('styles')
    <link href="{{ asset('admin/lib/select2/css/select2.min.css') }}" rel="stylesheet">
@endsection

@section('content')
    <div class="br-pageheader pd-y-15 pd-l-20">
    </div><!-- br-pageheader -->
    <div class="pd-x-20 pd-sm-x-30 pd-t-20 pd-sm-t-30">
        <div class="pull-left">
            <h4 class="tx-gray-800 mg-b-5">My Profile</h4>
        </div>
    </div>

    <div class="br-pagebody mg-t-70">
        <div class="br-section-wrapper pd-20">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-3">
                                    <ul class="nav flex-column">
                                        <li class="nav-item mb-1">
                                            <a class="btn btn-sm btn-teal text-white col-md-7" href="{{ route('my.account') }}"><i class="fe-chevron-right"></i> Profil</a>
                                        </li>
                                        @if(system_settings()->memdata_address || system_settings()->memdata_province || system_settings()->memdata_regency || system_settings()->memdata_district || system_settings()->memdata_village || system_settings()->memdata_zip)
                                        <li class="nav-item mb-1">
                                            <a class="btn btn-sm btn-teal text-white col-md-7" href="{{ route('my.account.address') }}"><i class="fe-chevron-right"></i> Alamat</a>
                                        </li>
                                        @endif
                                        @if(system_settings()->memdata_id_number || system_settings()->memdata_tax_id_number)
                                        <li class="nav-item mb-1">
                                            <a class="btn btn-sm btn-teal text-white col-md-7" href="{{ route('my.account.id') }}"><i class="fe-chevron-right"></i> Identitas</a>
                                        </li>
                                        @endif
                                        <li class="nav-item mb-1">
                                            <a class="btn btn-sm btn-secondary text-white col-md-7"><i class="fe-chevron-right"></i> Bank</a>
                                        </li>
                                        <li class="nav-item mb-1">
                                            <a class="btn btn-sm btn-teal text-white col-md-7" href="{{ route('my.account.changepw') }}"><i class="fe-chevron-right"></i> Ganti Password</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-md-9">
                                    <h3 class="header-title">Bank</h3>
                                    <div class="bd bd-gray-300 rounded table-responsive">
                                        <table class="table">
                                            <thead>
                                            <tr>
                                                <th>Bank</th>
                                                <th>No. Rekening</th>
                                                <th>Rek. Atas Nama</th>
                                                <th>Status</th>
                                                <th></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($banks as $bank)
                                                <tr>
                                                    <form method="post" action="{{ route('my.account.bank.update',['bank' => $bank]) }}">
                                                        <input type="hidden" name="_method" value="PUT">
                                                        @csrf
                                                        <td>
                                                            <select name="bank_id" class="form-control select2" data-toggle="select2">
                                                                <option value="" selected disabled>--- Pilih Bank ---</option>
                                                                @foreach($bank_list as $key => $lbank)
                                                                    @if($bank->bank_id==$key)
                                                                        <option value="{{ $key }}" selected>{{ $lbank }}</option>
                                                                    @else
                                                                        <option value="{{ $key }}">{{ $lbank }}</option>
                                                                    @endif
                                                                @endforeach
                                                            </select>
                                                        </td>
                                                        <td><input type="text" class="form-control" id="account_number" name="account_number" value="{{ $bank->account_number }}"></td>
                                                        <td><input type="text" class="form-control" id="account_name" name="account_name" value="{{ $bank->account_name }}"></td>
                                                        <td>
                                                            @if($bank->is_active == 1)
                                                                <a href="{{ route('my.account.bank.status',['bank' => $bank]) }}" class="btn btn-sm btn-outline-success"><i class="fe-check-circle"></i></a>
                                                            @else
                                                                <a href="{{ route('my.account.bank.status',['bank' => $bank]) }}" class="btn btn-sm btn-outline-danger"><i class="fe-x-circle"></i></a>
                                                            @endif
                                                        </td>
                                                        <td><button type="submit" class="btn btn-info btn-sm">update</button></td>
                                                    </form>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>

                                    <h5 class="mg-t-30">Tambah Bank</h5>
                                    <form class="form-horizontal" method="post" action="{{ route('my.account.bank.store') }}">
                                        @csrf
                                        <div class="form-group row">
                                            <label for="no_id" class="col-3 col-form-label">Bank:</label>
                                            <div class="col-9">
                                                <select name="bank_id" class="form-control select2 col-6" data-toggle="select2">
                                                    <option value="" selected disabled>--- Pilih Bank ---</option>
                                                    @foreach($bank_list as $key => $lbank)
                                                        <option value="{{ $key }}">{{ $lbank }}</option>
                                                    @endforeach
                                                </select>
                                                @if($errors->has('bank_id'))
                                                    <small class="form-text text-danger">{{ $errors->first('bank_id') }}</small>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="account_number" class="col-3 col-form-label">No. Rekening:</label>
                                            <div class="col-9">
                                                <input class="form-control col-6" type="text" id="account_number" name="account_number" placeholder="Input Nomor Rekening">
                                                @if($errors->has('account_number'))
                                                    <small class="form-text text-danger">{{ $errors->first('account_number') }}</small>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="account_name" class="col-3 col-form-label">Rek. Atas Nama:</label>
                                            <div class="col-9">
                                                <input class="form-control col-6" type="text" id="account_name" name="account_name" placeholder="Input Rekening Atas Nama">
                                                @if($errors->has('account_name'))
                                                    <small class="form-text text-danger">{{ $errors->first('account_name') }}</small>
                                                @endif
                                            </div>
                                        </div>
                                        <button type="submit" class="btn btn-info mt-3">Save</button>
                                        <a class="btn btn-secondary mt-3" href="{{ route('my.account') }}">Cancel</a>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('admin/lib/select2/js/select2.min.js') }}"></script>
@endsection

@section('page_script')
    @if (session('success'))
        <script>
            toastr.options = {
                "closeButton": true,
                "debug": false,
                "newestOnTop": false,
                "progressBar": true,
                "positionClass": "toast-bottom-right",
                "preventDuplicates": false,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            }
            toastr["success"]("{{ session('success') }}", "Success!");
        </script>
    @endif

    @if (session('error'))
        <script>
            toastr.options = {
                "closeButton": true,
                "debug": false,
                "newestOnTop": false,
                "progressBar": true,
                "positionClass": "toast-bottom-right",
                "preventDuplicates": false,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            }
            toastr["error"]("{{ session('error') }}", "Error!");
        </script>
    @endif
@endsection