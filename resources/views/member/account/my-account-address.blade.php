@extends('layouts.member')

@section('styles')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
@endsection

@section('content')
    <div class="br-pageheader pd-y-15 pd-l-20">
    </div><!-- br-pageheader -->
    <div class="pd-x-20 pd-sm-x-30 pd-t-20 pd-sm-t-30">
        <div class="pull-left">
            <h4 class="tx-gray-800 mg-b-5">My Profile</h4>
        </div>
    </div>

    <div class="br-pagebody mg-t-70">
        <div class="br-section-wrapper pd-20">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-3">
                                    <ul class="nav flex-column">
                                        <li class="nav-item mb-1">
                                            <a class="btn btn-sm btn-teal text-white col-md-7" href="{{ route('my.account') }}">Profil</a>
                                        </li>
                                        <li class="nav-item mb-1">
                                            <a class="btn btn-sm btn-secondary text-white col-md-7">Alamat</a>
                                        </li>
                                        @if(system_settings()->memdata_id_number || system_settings()->memdata_tax_id_number)
                                        <li class="nav-item mb-1">
                                            <a class="btn btn-sm btn-teal text-white col-md-7" href="{{ route('my.account.id') }}">Identitas</a>
                                        </li>
                                        @endif
                                        <li class="nav-item mb-1">
                                            <a class="btn btn-sm btn-teal text-white col-md-7" href="{{ route('my.account.bank') }}">Bank</a>
                                        </li>
                                        <li class="nav-item mb-1">
                                            <a class="btn btn-sm btn-teal text-white col-md-7" href="{{ route('my.account.changepw') }}">Ganti Password</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-md-9">
                                    <h3 class="header-title">Alamat</h3>
                                    <form class="form-horizontal" method="post" action="{{ route('my.account.store',['user' => auth()->user()]) }}">
                                        <input type="hidden" name="_method" value="PUT">
                                        @csrf
                                        @system('memdata_address')
                                        <div class="form-group row">
                                            <label for="address" class="col-3 col-form-label">Alamat:</label>
                                            <div class="col-9">
                                                <textarea class="form-control col-8" type="text" id="address" name="address" rows="5">{{ auth()->user()->address }}</textarea>
                                                @if($errors->has('address'))
                                                    <small class="form-text text-danger">{{ $errors->first('address') }}</small>
                                                @endif
                                            </div>
                                        </div>
                                        @endsystem
                                        @system('memdata_province')
                                        <div class="form-group row">
                                            <label for="province_id" class="col-3 col-form-label">Provinsi:</label>
                                            <div class="col-9">
                                                <select id="province_id" name="province_id" class="form-control select2 col-8" data-toggle="select2">
                                                    <option value="">-- Pilih Propinsi --</option>
                                                    @foreach($provinces as $key => $province)
                                                        @if(auth()->user()->province_id==$key)
                                                            <option value="{{ $key }}" selected>{{ $province }}</option>
                                                        @else
                                                            <option value="{{ $key }}">{{ $province }}</option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                                @if($errors->has('province_id'))
                                                    <small class="form-text text-danger">{{ $errors->first('province_id') }}</small>
                                                @endif
                                            </div>
                                        </div>
                                        @endsystem
                                        @system('memdata_regency')
                                        <div class="form-group row">
                                            <label for="regency_id" class="col-3 col-form-label">Kota / Kabupaten:</label>
                                            <div class="col-9">
                                                <select id="regency_id" name="regency_id" class="form-control select2 col-8" data-toggle="select2">
                                                    <option value="">-- Pilih Kota / Kabupaten --</option>
                                                    @foreach($regencies as $key => $regency)
                                                        @if(auth()->user()->regency_id==$key)
                                                            <option value="{{ $key }}" selected>{{ $regency }}</option>
                                                        @else
                                                            <option value="{{ $key }}">{{ $regency }}</option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                                @if($errors->has('regency_id'))
                                                    <small class="form-text text-danger">{{ $errors->first('regency_id') }}</small>
                                                @endif
                                            </div>
                                        </div>
                                        @endsystem
                                        @system('memdata_district')
                                        <div class="form-group row">
                                            <label for="district_id" class="col-3 col-form-label">Kecamatan:</label>
                                            <div class="col-9">
                                                <select id="district_id" name="district_id" class="form-control select2 col-8" data-toggle="select2">
                                                    <option value="">-- Pilih Kecamatan --</option>
                                                    @foreach($districts as $key => $district)
                                                        @if(auth()->user()->district_id==$key)
                                                            <option value="{{ $key }}" selected>{{ $district }}</option>
                                                        @else
                                                            <option value="{{ $key }}">{{ $district }}</option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                                @if($errors->has('district_id'))
                                                    <small class="form-text text-danger">{{ $errors->first('district_id') }}</small>
                                                @endif
                                            </div>
                                        </div>
                                        @endsystem
                                        @system('memdata_village')
                                        <div class="form-group row">
                                            <label for="village_id" class="col-3 col-form-label">Kelurahan / Desa:</label>
                                            <div class="col-9">
                                                <select id="village_id" name="village_id" class="form-control select2 col-8" data-toggle="select2">
                                                    <option value="">-- Pilih Kelurahan / Desa --</option>
                                                    @foreach($villages as $key => $village)
                                                        @if(auth()->user()->village_id==$key)
                                                            <option value="{{ $key }}" selected>{{ $village }}</option>
                                                        @else
                                                            <option value="{{ $key }}">{{ $village }}</option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                                @if($errors->has('village_id'))
                                                    <small class="form-text text-danger">{{ $errors->first('village_id') }}</small>
                                                @endif
                                            </div>
                                        </div>
                                        @endsystem
                                        @system('memdata_zip')
                                        <div class="form-group row">
                                            <label for="postal_code" class="col-3 col-form-label">Kode Pos:</label>
                                            <div class="col-9">
                                                <input class="form-control col-2" type="text" id="postal_code" name="postal_code" value="{{ auth()->user()->postal_code }}">
                                                @if($errors->has('postal_code'))
                                                    <small class="form-text text-danger">{{ $errors->first('postal_code') }}</small>
                                                @endif
                                            </div>
                                        </div>
                                        @endsystem
                                        <button type="submit" class="btn btn-info mt-3">Save</button>
                                        <a class="btn btn-secondary mt-3" href="{{ route('my.account') }}">Cancel</a>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
@endsection

@section('page_script')
<script type="text/javascript">
$(document).ready(function() {
  $('.select2').select2();
});
</script>
    @system('memdata_province')
        @system('memdata_regency')
        <script>
            $('#province_id').on('select2:select', function (e) {
                var data = e.params.data;
                //console.log(data);

                $('#regency_id').empty();
                $.ajax({
                    method: "POST",
                    url: '{{ route('get.regency') }}',
                    data: {
                        "_token": "{{ csrf_token() }}",
                        "id": data.id
                    },
                    success: data => {
                        //console.log(data);
                        $('#regency_id').append('<option value="">-- Pilih Kota / Kabupaten --</option>');
                        for (var opt in data) {
                            $('#regency_id').append('<option value="'+ opt +'">'+ data[opt] +'</option>');
                        }
                    }
                })
            });
        </script>
        @system('memdata_district')
        <script>
            $('#regency_id').on('select2:select', function (e) {
                var data = e.params.data;
                // console.log(data.id);

                $('#district_id').empty();
                $.ajax({
                    method: "POST",
                    url: '{{ route('get.district') }}',
                    data: {
                        "_token": "{{ csrf_token() }}",
                        "id": data.id
                    },
                    success: data => {
                        //console.log(data);
                        $('#district_id').append('<option value="">-- Pilih Kecamatan --</option>');
                        for (var opt in data) {
                            $('#district_id').append('<option value="'+ opt +'">'+ data[opt] +'</option>');
                        }
                    }
                })
            });
        </script>
        @system('memdata_village')
        <script>
            $('#district_id').on('select2:select', function (e) {
                var data = e.params.data;
                // console.log(data.id);

                $('#village_id').empty();
                $.ajax({
                    method: "POST",
                    url: '{{ route('get.village') }}',
                    data: {
                        "_token": "{{ csrf_token() }}",
                        "id": data.id
                    },
                    success: data => {
                        //console.log(data);
                        $('#village_id').append('<option value="">-- Pilih Kelurahan / Desa --</option>');
                        for (var opt in data) {
                            $('#village_id').append('<option value="'+ opt +'">'+ data[opt] +'</option>');
                        }
                    }
                })
            });
        </script>
        @endsystem
        @endsystem
        @endsystem
    @endsystem

    @if (session('success'))
        <script>
            toastr.options = {
                "closeButton": true,
                "debug": false,
                "newestOnTop": false,
                "progressBar": true,
                "positionClass": "toast-bottom-right",
                "preventDuplicates": false,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            }
            toastr["success"]("{{ session('success') }}", "Success!");
        </script>
    @endif

    @if (session('error'))
        <script>
            toastr.options = {
                "closeButton": true,
                "debug": false,
                "newestOnTop": false,
                "progressBar": true,
                "positionClass": "toast-bottom-right",
                "preventDuplicates": false,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            }
            toastr["error"]("{{ session('error') }}", "Error!");
        </script>
    @endif

@endsection
