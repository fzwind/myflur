@extends('layouts.member')

@section('styles')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
@endsection

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header card-header-primary">
					<h4 class="card-title">My Profile</h4>
				</div>
				<div class="card-body">
					<div class="row">
						<div class="col-md-3">
							<ul class="nav nav-pills nav-pills-rose flex-column" role="tablist">
								<li class="nav-item">
									<a class="nav-link {{ in_array(Route::current()->getName(), ['my.account']) ? 'show active' : '' }}" data-toggle="tab" href="#profil" role="tablist" style="">
									Profil
									</a>
								</li>
                @if(system_settings()->memdata_address || system_settings()->memdata_province || system_settings()->memdata_regency || system_settings()->memdata_district || system_settings()->memdata_village || system_settings()->memdata_zip)
								<li class="nav-item">
									<a class="nav-link {{ in_array(Route::current()->getName(), ['my.account.address']) ? 'show active' : '' }}" data-toggle="tab" href="#alamat" role="tablist" style="">
									Alamat
									</a>
								</li>
                @endif
                @if(system_settings()->memdata_id_number || system_settings()->memdata_tax_id_number)
                <li class="nav-item">
									<a class="nav-link" data-toggle="tab" href="#identitas" role="tablist" style="">
									Identitas
									</a>
								</li>
                @endif
								<li class="nav-item">
									<a class="nav-link tab-pane {{ in_array(Route::current()->getName(), ['my.account.bank']) ? 'show active' : '' }}" data-toggle="tab" href="#bank" role="tablist" style="">
									Bank
									</a>
								</li>
                <li class="nav-item">
									<a class="nav-link {{ in_array(Route::current()->getName(), ['my.account.changepw']) ? 'show active' : '' }}" data-toggle="tab" href="#password" role="tablist" style="">
									Ganti Password
									</a>
								</li>
							</ul>
						</div>
						<div class="col-md-9">
							<div class="tab-content">
								<div class="tab-pane {{ in_array(Route::current()->getName(), ['my.account']) ? 'show active' : '' }}" id="profil">
                  <form class="form-horizontal" method="post" action="{{ route('my.account.store',['user' => auth()->user()]) }}">
                    @csrf
                      <input type="hidden" name="_method" value="PUT">
                      <div class="row">
                        <div class="col-md-4">
                          <label class="bmd-label-floating">Username</label>
                        </div>
                        <div class="col-md-8">
                          <div class="form-group bmd-form-group {{$errors->has('amount') ? 'danger' : ''}}">
                            <input type="text" class="form-control" disabled value="{{ auth()->user()->username }}">
                          </div>
                        </div>
                      </div>
                      @system('memdata_name')
                      <div class="row">
                        <div class="col-md-4">
                          <label class="bmd-label-floating">Nama</label>
                        </div>
                        <div class="col-md-8">
                          <div class="form-group bmd-form-group {{$errors->has('name') ? 'danger' : ''}}">
                            <input type="text" class="form-control" name="name" value="{{ auth()->user()->name }}">
                            @if($errors->has('name'))
                                <label id="exampleEmails-error" class="error" for="exampleEmails">{{ $errors->first('name') }}</label>
                            @endif
                          </div>
                        </div>
                      </div>
                      @endsystem
                      @system('memdata_email')
                      <div class="row">
                        <div class="col-md-4">
                          <label class="bmd-label-floating">Email</label>
                        </div>
                        <div class="col-md-8">
                          <div class="form-group bmd-form-group {{$errors->has('name') ? 'danger' : ''}}">
                            <input type="email" class="form-control" name="email" value="{{ auth()->user()->email }}">
                            @if($errors->has('email'))
                                <label id="exampleEmails-error" class="error" for="exampleEmails">{{ $errors->first('email') }}</label>
                            @endif
                          </div>
                        </div>
                      </div>
                      @endsystem
                      @system('memdata_hp')
                      <div class="row">
                        <div class="col-md-4">
                          <label class="bmd-label-floating">No HP</label>
                        </div>
                        <div class="col-md-8">
                          <div class="form-group bmd-form-group {{$errors->has('no_hp') ? 'danger' : ''}}">
                            <input type="text" class="form-control" name="no_hp" value="{{ auth()->user()->no_hp }}">
                            @if($errors->has('no_hp'))
                                <label id="exampleEmails-error" class="error" for="exampleEmails">{{ $errors->first('no_hp') }}</label>
                            @endif
                          </div>
                        </div>
                      </div>
                      @endsystem
                      @system('memdata_sex')
                      <div class="row">
                        <div class="col-md-4">
                          <label class="bmd-label-floating">Jenis Kelamin</label>
                        </div>
                        <div class="col-md-8">
                          <div class="form-group bmd-form-group {{$errors->has('sex') ? 'danger' : ''}}">
                            <select class="form-control mdb-select" name="sex">
                              <option value="">-- Pilih --</option>
                              <option value="male" {{ (auth()->user()->sex == 'male') ? 'selected' : '' }}>Laki-Laki</option>
                              <option value="Female" {{ (auth()->user()->sex == 'female') ? 'selected' : '' }}>Perempuan</option>
                            </select>
                            @if($errors->has('sex'))
                                <label id="exampleEmails-error" class="error" for="exampleEmails">{{ $errors->first('sex') }}</label>
                            @endif
                          </div>
                        </div>
                      </div>
                      @endsystem
                      @system('memdata_birth_date')
                      <div class="row">
                        <div class="col-md-4">
                          <label class="bmd-label-floating">No HP</label>
                        </div>
                        <div class="col-md-8">
                          <div class="form-group bmd-form-group {{$errors->has('birth_date_at') ? 'danger' : ''}}">
                            <input type="text" class="form-control datepicker" name="birth_date_at" value="{{ to_utz(auth()->user()->birth_date,'Y-m-d') }}">
                            @if($errors->has('birth_date_at'))
                                <label id="exampleEmails-error" class="error" for="exampleEmails">{{ $errors->first('birth_date_at') }}</label>
                            @endif
                          </div>
                        </div>
                      </div>
                      @endsystem
                        <button type="submit" class="btn btn-primary pull-right">Save<div class="ripple-container"></div></button>
                        <a class="btn btn-secondary pull-right" href="{{ route('home') }}">Cancel</a>
                    </form>
								</div>
                <div class="tab-pane {{ in_array(Route::current()->getName(), ['my.account.address']) ? 'show active' : '' }}" id="alamat">
                  <form class="form-horizontal" method="post" action="{{ route('my.account.store',['user' => auth()->user()]) }}">
                    @csrf
                      <input type="hidden" name="_method" value="PUT">
                      @system('memdata_address')
                      <div class="row">
                        <div class="col-md-4">
                          <label class="bmd-label-floating">Alamat</label>
                        </div>
                        <div class="col-md-8">
                          <div class="form-group bmd-form-group {{$errors->has('address') ? 'danger' : ''}}">
                            <textarea name="address" class="form-control" required>{{ auth()->user()->address }}</textarea>
                          </div>
													@if($errors->has('address'))
															<label id="exampleEmails-error" class="error" for="exampleEmails">{{ $errors->first('address') }}</label>
													@endif
                        </div>
                      </div>
                      @endsystem
                      @system('memdata_province')
                      <div class="row">
                        <div class="col-md-4">
                          <label class="bmd-label-floating">Provinsi</label>
                        </div>
                        <div class="col-md-8">
                          <div class="form-group bmd-form-group {{$errors->has('province_id') ? 'danger' : ''}}">
                            <select id="province_id" name="province_id" class="form-control select2" data-toggle="select2">
                                <option value="">-- Pilih Propinsi --</option>
                                @foreach($provinces as $key => $province)
                                    @if(auth()->user()->province_id==$key)
                                        <option value="{{ $key }}" selected>{{ $province }}</option>
                                    @else
                                        <option value="{{ $key }}">{{ $province }}</option>
                                    @endif
                                @endforeach
                            </select>
                            @if($errors->has('province_id'))
                                <label id="exampleEmails-error" class="error" for="exampleEmails">{{ $errors->first('province_id') }}</label>
                            @endif
                          </div>
                        </div>
                      </div>
                      @endsystem
                      @system('memdata_regency')
                      <div class="row">
                        <div class="col-md-4">
                          <label class="bmd-label-floating">Kota / Kabupaten</label>
                        </div>
                        <div class="col-md-8">
                          <div class="form-group bmd-form-group {{$errors->has('regency_id') ? 'danger' : ''}}">
                            <select id="regency_id" name="regency_id" class="form-control select2 col-8" data-toggle="select2">
                                <option value="">-- Pilih Kota / Kabupaten --</option>
                                @foreach($regencies as $key => $regency)
                                    @if(auth()->user()->regency_id==$key)
                                        <option value="{{ $key }}" selected>{{ $regency }}</option>
                                    @else
                                        <option value="{{ $key }}">{{ $regency }}</option>
                                    @endif
                                @endforeach
                            </select>
                            @if($errors->has('regency_id'))
                                <label id="exampleEmails-error" class="error" for="exampleEmails">{{ $errors->first('regency_id') }}</label>
                            @endif
                          </div>
                        </div>
                      </div>
                      @endsystem
                      @system('memdata_district')
                      <div class="row">
                        <div class="col-md-4">
                          <label class="bmd-label-floating">Kecamatan</label>
                        </div>
                        <div class="col-md-8">
                          <div class="form-group bmd-form-group {{$errors->has('district_id') ? 'danger' : ''}}">
                            <select id="district_id" name="district_id" class="form-control select2 col-8" data-toggle="select2">
                                <option value="">-- Pilih Kecamatan --</option>
                                @foreach($districts as $key => $district)
                                    @if(auth()->user()->district_id==$key)
                                        <option value="{{ $key }}" selected>{{ $district }}</option>
                                    @else
                                        <option value="{{ $key }}">{{ $district }}</option>
                                    @endif
                                @endforeach
                            </select>
                            @if($errors->has('district_id'))
                                <label id="exampleEmails-error" class="error" for="exampleEmails">{{ $errors->first('district_id') }}</label>
                            @endif
                          </div>
                        </div>
                      </div>
                      @endsystem
                      @system('memdata_village')
                      <div class="row">
                        <div class="col-md-4">
                          <label class="bmd-label-floating">Kelurahan / Desa</label>
                        </div>
                        <div class="col-md-8">
                          <div class="form-group bmd-form-group {{$errors->has('village_id') ? 'danger' : ''}}">
                            <select id="village_id" name="village_id" class="form-control select2 col-8" data-toggle="select2">
                                <option value="">-- Pilih Kelurahan / Desa --</option>
                                @foreach($villages as $key => $village)
                                    @if(auth()->user()->village_id==$key)
                                        <option value="{{ $key }}" selected>{{ $village }}</option>
                                    @else
                                        <option value="{{ $key }}">{{ $village }}</option>
                                    @endif
                                @endforeach
                            </select>
                            @if($errors->has('village_id'))
                                <label id="exampleEmails-error" class="error" for="exampleEmails">{{ $errors->first('village_id') }}</label>
                            @endif
                          </div>
                        </div>
                      </div>
                      @endsystem
                      @system('memdata_zip')
                      <div class="row">
                        <div class="col-md-4">
                          <label class="bmd-label-floating">Kode POS</label>
                        </div>
                        <div class="col-md-8">
                          <div class="form-group bmd-form-group {{$errors->has('postal_code') ? 'danger' : ''}}">
                            <input type="text" class="form-control" name="postal_code" value="{{ auth()->user()->postal_code }}">
                            @if($errors->has('postal_code'))
                                <label id="exampleEmails-error" class="error" for="exampleEmails">{{ $errors->first('postal_code') }}</label>
                            @endif
                          </div>
                        </div>
                      </div>
                      @endsystem
                        <button type="submit" class="btn btn-primary pull-right">Save<div class="ripple-container"></div></button>
                        <a class="btn btn-secondary pull-right" href="{{ route('home') }}">Cancel</a>
                    </form>
								</div>
                <div class="tab-pane {{ in_array(Route::current()->getName(), ['my.account.bank']) ? 'show active' : '' }}" id="bank">
                  <table class="table">
                      <thead>
                      <tr>
                          <th>Bank</th>
                          <th>No. Rekening</th>
                          <th>Rek. Atas Nama</th>
                          <th>Status</th>
                          <th></th>
                      </tr>
                      </thead>
                      <tbody>
                      @foreach($banks as $bank)
                          <tr>
                              <form method="post" action="{{ route('my.account.bank.update',['bank' => $bank]) }}">
                                  <input type="hidden" name="_method" value="PUT">
                                  @csrf
                                  <td>
                                      <select name="bank_id" class="form-control select2" data-toggle="select2">
                                          <option value="" selected disabled>--- Pilih Bank ---</option>
                                          @foreach($bank_list as $key => $lbank)
                                              @if($bank->bank_id==$key)
                                                  <option value="{{ $key }}" selected>{{ $lbank }}</option>
                                              @else
                                                  <option value="{{ $key }}">{{ $lbank }}</option>
                                              @endif
                                          @endforeach
                                      </select>
                                  </td>
                                  <td><input type="text" class="form-control"  name="account_number" value="{{ $bank->account_number }}"></td>
                                  <td><input type="text" class="form-control"  name="account_name" value="{{ $bank->account_name }}"></td>
                                  <td>
                                      @if($bank->is_active == 1)
                                          <a href="{{ route('my.account.bank.status',['bank' => $bank]) }}" class="btn btn-sm btn-outline-success"><i class="material-icons">check_circle</i></a>
                                      @else
                                          <a href="{{ route('my.account.bank.status',['bank' => $bank]) }}" class="btn btn-sm btn-outline-danger"><i class="material-icons">cancel</i></a>
                                      @endif
                                  </td>
                                  <td><button type="submit" class="btn btn-info btn-sm">update</button></td>
                              </form>
                          </tr>
                      @endforeach
                      </tbody>
                  </table>
                  <br>
                  <h5>Tambah Bank</h5>
                  <form class="form-horizontal" method="post" action="{{ route('my.account.bank.store') }}">
                    @csrf
                      <div class="row">
                        <div class="col-md-4">
                          <label class="bmd-label-floating">Bank</label>
                        </div>
                        <div class="col-md-8">
                          <div class="form-group bmd-form-group {{$errors->has('bank_id') ? 'danger' : ''}}">
                            <select name="bank_id" class="form-control select2 col-6" data-toggle="select2">
                                <option value="" selected disabled>--- Pilih Bank ---</option>
                                @foreach($bank_list as $key => $lbank)
                                    <option value="{{ $key }}">{{ $lbank }}</option>
                                @endforeach
                            </select>
                            @if($errors->has('bank_id'))
                                <label id="exampleEmails-error" class="error" for="exampleEmails">{{ $errors->first('bank_id') }}</label>
                            @endif
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-4">
                          <label class="bmd-label-floating">No Rekening</label>
                        </div>
                        <div class="col-md-8">
                          <div class="form-group bmd-form-group {{$errors->has('account_number') ? 'danger' : ''}}">
                            <input type="text" class="form-control" name="account_number" value="">
                            @if($errors->has('account_number'))
                                <label id="exampleEmails-error" class="error" for="exampleEmails">{{ $errors->first('account_number') }}</label>
                            @endif
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-4">
                          <label class="bmd-label-floating">Rek. Atas Nama</label>
                        </div>
                        <div class="col-md-8">
                          <div class="form-group bmd-form-group {{$errors->has('account_name') ? 'danger' : ''}}">
                            <input type="text" class="form-control" name="account_name" value="">
                            @if($errors->has('account_name'))
                                <label id="exampleEmails-error" class="error" for="exampleEmails">{{ $errors->first('account_name') }}</label>
                            @endif
                          </div>
                        </div>
                      </div>

                        <button type="submit" class="btn btn-primary pull-right">Add<div class="ripple-container"></div></button>
                        <a class="btn btn-secondary pull-right" href="{{ route('home') }}">Cancel</a>
                    </form>
								</div>
                <div class="tab-pane {{ in_array(Route::current()->getName(), ['my.account.changepw']) ? 'show active' : '' }}" id="password">
                  <form class="form-horizontal" method="post" action="{{ route('my.account.store',['user' => auth()->user()]) }}">
                    @csrf
                      <input type="hidden" name="_method" value="PUT">

                      <div class="row">
                        <div class="col-md-4">
                          <label class="bmd-label-floating">Password Baru</label>
                        </div>
                        <div class="col-md-8">
                          <div class="form-group bmd-form-group {{$errors->has('password') ? 'danger' : ''}}">
                            <input type="password" class="form-control" name="password" value="">
                            @if($errors->has('password'))
                                <label id="exampleEmails-error" class="error" for="exampleEmails">{{ $errors->first('password') }}</label>
                            @endif
                          </div>
                        </div>
                      </div>

                      <div class="row">
                        <div class="col-md-4">
                          <label class="bmd-label-floating">Konfirmasi Password Baru</label>
                        </div>
                        <div class="col-md-8">
                          <div class="form-group bmd-form-group {{$errors->has('password_confirmation') ? 'danger' : ''}}">
                            <input type="password" class="form-control" name="password_confirmation" value="">
                            @if($errors->has('password_confirmation'))
                                <label id="exampleEmails-error" class="error" for="exampleEmails">{{ $errors->first('password_confirmation') }}</label>
                            @endif
                          </div>
                        </div>
                      </div>

                        <button type="submit" class="btn btn-primary pull-right">Add<div class="ripple-container"></div></button>
                        <a class="btn btn-secondary pull-right" href="{{ route('home') }}">Cancel</a>
                    </form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection

@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
@endsection

@section('page_script')
<script type="text/javascript">
$(document).ready(function() {
  $('.select2').select2();
});
</script>
@system('memdata_province')
    @system('memdata_regency')
    <script>
        $('#province_id').change(function(e){
          var id = $(this).val();
            //console.log(data);

            $('#regency_id').empty();
            $.ajax({
                method: "POST",
                url: '{{ route('get.regency') }}',
                data: {
                    "_token": "{{ csrf_token() }}",
                    "id": id
                },
                success: data => {
                    //console.log(data);
                    $('#regency_id').append('<option value="">-- Pilih Kota / Kabupaten --</option>');
                    for (var opt in data) {
                        $('#regency_id').append('<option value="'+ opt +'">'+ data[opt] +'</option>');
                    }
                }
            })
        });
    </script>
    @system('memdata_district')
    <script>
        $('#regency_id')..change(function(e){
          var id = $(this).val();
            // console.log(data.id);

            $('#district_id').empty();
            $.ajax({
                method: "POST",
                url: '{{ route('get.district') }}',
                data: {
                    "_token": "{{ csrf_token() }}",
                    "id": id
                },
                success: data => {
                    //console.log(data);
                    $('#district_id').append('<option value="">-- Pilih Kecamatan --</option>');
                    for (var opt in data) {
                        $('#district_id').append('<option value="'+ opt +'">'+ data[opt] +'</option>');
                    }
                }
            })
        });
    </script>
    @system('memdata_village')
    <script>
        $('#district_id')..change(function(e){
          var id = $(this).val();
            // console.log(data.id);

            $('#village_id').empty();
            $.ajax({
                method: "POST",
                url: '{{ route('get.village') }}',
                data: {
                    "_token": "{{ csrf_token() }}",
                    "id": id
                },
                success: data => {
                    //console.log(data);
                    $('#village_id').append('<option value="">-- Pilih Kelurahan / Desa --</option>');
                    for (var opt in data) {
                        $('#village_id').append('<option value="'+ opt +'">'+ data[opt] +'</option>');
                    }
                }
            })
        });
    </script>
    @endsystem
    @endsystem
    @endsystem
@endsystem

    @if (session('success'))
        <script>
            toastr.options = {
                "closeButton": true,
                "debug": false,
                "newestOnTop": false,
                "progressBar": true,
                "positionClass": "toast-bottom-right",
                "preventDuplicates": false,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            }
            toastr["success"]("{{ session('success') }}", "Success!");
        </script>
    @endif

    @if (session('error'))
        <script>
            toastr.options = {
                "closeButton": true,
                "debug": false,
                "newestOnTop": false,
                "progressBar": true,
                "positionClass": "toast-bottom-right",
                "preventDuplicates": false,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            }
            toastr["error"]("{{ session('error') }}", "Error!");
        </script>
    @endif
@endsection
