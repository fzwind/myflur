@extends('layouts.member')

@section('styles')
@endsection

@section('content')
    <div class="br-pageheader pd-y-15 pd-l-20">
    </div><!-- br-pageheader -->
    <div class="pd-x-20 pd-sm-x-30 pd-t-20 pd-sm-t-30">
        <div class="pull-left">
            <h4 class="tx-gray-800 mg-b-5">My Profile</h4>
        </div>
    </div>

    <div class="br-pagebody mg-t-70">
        <div class="br-section-wrapper pd-20">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-3">
                                    <ul class="nav flex-column">
                                        <li class="nav-item mb-1">
                                            <a class="btn btn-sm btn-teal text-white col-md-7" href="{{ route('my.account') }}"><i class="fe-chevron-right"></i> Profil</a>
                                        </li>
                                        @if(system_settings()->memdata_address || system_settings()->memdata_province || system_settings()->memdata_regency || system_settings()->memdata_district || system_settings()->memdata_village || system_settings()->memdata_zip)
                                        <li class="nav-item mb-1">
                                            <a class="btn btn-sm btn-teal text-white col-md-7" href="{{ route('my.account.address') }}"><i class="fe-chevron-right"></i> Alamat</a>
                                        </li>
                                        @endif
                                        @if(system_settings()->memdata_id_number || system_settings()->memdata_tax_id_number)
                                        <li class="nav-item mb-1">
                                            <a class="btn btn-sm btn-teal text-white col-md-7" href="{{ route('my.account.id') }}"><i class="fe-chevron-right"></i> Identitas</a>
                                        </li>
                                        @endif
                                        <li class="nav-item mb-1">
                                            <a class="btn btn-sm btn-teal text-white col-md-7" href="{{ route('my.account.bank') }}"><i class="fe-chevron-right"></i> Bank</a>
                                        </li>
                                        <li class="nav-item mb-1">
                                            <a class="btn btn-sm btn-secondary text-white col-md-7"><i class="fe-chevron-right"></i> Ganti Password</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-md-9">
                                    <h3 class="header-title">Ganti Password</h3>
                                    <form class="form-horizontal" method="post" action="{{ route('my.account.store',['user' => auth()->user()]) }}">
                                        <input type="hidden" name="_method" value="PUT">
                                        @csrf
                                        <div class="form-group row">
                                            <label for="password" class="col-3 col-form-label">Password Baru:</label>
                                            <div class="col-9">
                                                <input class="form-control col-6" type="password" id="password" name="password" value="">
                                                @if($errors->has('password'))
                                                    <small class="form-text text-danger">{{ $errors->first('password') }}</small>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="password_confirmation" class="col-3 col-form-label">Konfirmasi Password Baru:</label>
                                            <div class="col-9">
                                                <input class="form-control col-6" type="password" id="password_confirmation" name="password_confirmation" value="">
                                                @if($errors->has('password_confirmation'))
                                                    <small class="form-text text-danger">{{ $errors->first('password_confirmation') }}</small>
                                                @endif
                                            </div>
                                        </div>
                                        <button type="submit" class="btn btn-info mt-3">Save</button>
                                        <a class="btn btn-secondary mt-3" href="{{ route('my.account') }}">Cancel</a>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
@endsection

@section('page_script')
    @if (session('success'))
        <script>
            toastr.options = {
                "closeButton": true,
                "debug": false,
                "newestOnTop": false,
                "progressBar": true,
                "positionClass": "toast-bottom-right",
                "preventDuplicates": false,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            }
            toastr["success"]("{{ session('success') }}", "Success!");
        </script>
    @endif

    @if (session('error'))
        <script>
            toastr.options = {
                "closeButton": true,
                "debug": false,
                "newestOnTop": false,
                "progressBar": true,
                "positionClass": "toast-bottom-right",
                "preventDuplicates": false,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            }
            toastr["error"]("{{ session('error') }}", "Error!");
        </script>
    @endif
@endsection
