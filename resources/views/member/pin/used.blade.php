@extends('layouts.member')

@section('styles')
    <link href="{{ asset('admin/lib/select2/css/select2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('admin/lib/datatables/jquery.dataTables.css') }}" rel="stylesheet">
@endsection

@section('content')
    <div class="br-pageheader pd-y-15 pd-l-20">
    </div><!-- br-pageheader -->
    <div class="pd-x-20 pd-sm-x-30 pd-t-20 pd-sm-t-30">
        <div class="pull-left">
            <h4 class="tx-gray-800 mg-b-5">PIN</h4>
        </div>
    </div>

    <div class="br-pagebody mg-t-35 pd-x-20 pd-sm-x-30">
        <div class="row no-gutters widget-1 shadow-base">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h6 class="card-title">PIN Terpakai</h6>
                        <a href=""><i class="icon ion-android-more-horizontal"></i></a>
                    </div><!-- card-header -->
                    <div class="card-body">
                        <span id="spark1"><canvas style="display: inline-block; width: 89px; height: 30px; vertical-align: top;" width="89" height="30"></canvas></span>
                        <span>{{ count_pin_used(auth()->id()) }}</span>
                    </div><!-- card-body -->
                    <div class="card-footer">
                        <div>
                            <a href="{{ route('user-pin.index') }}">
                                <span class="tx-11">Tersedia</span>
                                <h6 class="tx-inverse">{{ count_pin_available(auth()->id()) }}</h6>
                            </a>
                        </div>
                        <div>
                            <a href="{{ route('user.pin.sold') }}">
                                <span class="tx-11">Terjual</span>
                                <h6 class="tx-inverse">{{ count_pin_sold(auth()->id()) }}</h6>
                            </a>
                        </div>
                    </div><!-- card-footer -->
                </div>
            </div>
        </div>
    </div>
    <div class="br-pagebody mg-t-15">
        <div class="br-section-wrapper pd-20">
            <div class="table-wrapper">
                <table id="plan-setting-data" class="table dt-responsive nowrap">
                    <thead>
                    <tr>
                        <th class="wd-25p">Kode Pin</th>
                        <th class="wd-30p">Status</th>
                        <th></th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('admin/lib/select2/js/select2.min.js') }}"></script>
    <script src="{{ asset('admin/lib/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('admin/lib/datatables-responsive/dataTables.responsive.js') }}"></script>
@endsection

@section('page_script')
    <script>
        $(function(){
            'use strict';

            $('#plan-setting-data').DataTable({
                responsive: false,
                language: {
                    searchPlaceholder: 'Cari...',
                    sSearch: '',
                    lengthMenu: '_MENU_ data/halaman',
                    zeroRecords: ' Tidak ada data yang dapat ditampilkan.',
                    info: 'Menampilkan halaman _PAGE_ dari _PAGES_ (Total Data : _MAX_)',
                    infoEmpty: '',
                    infoFiltered: "(disaring dari total _MAX_ data)"
                },
                processing: true,
                serverSide: true,
                ajax: '{{ route('user.pin.used.data') }}',
                columns: [
                    {data: 'pin_code', name: 'pin_code'},
                    {data: 'status', name: 'status',
                        render: function(data, type, row, meta){
                            if(type === 'display'){
                                if(data == 0){
                                    data = '<span class="text-success">tersedia</span>';
                                } else if(data == 1){
                                    data = '<span class="text-warning">terjual</span>';
                                } else if(data == 2){
                                    data = '<span class="text-purple">ditransfer</span>';
                                } else if(data == 3){
                                    data = '<span class="text-info">digunakan</span>';
                                }
                            }
                            return data;
                        }
                    },
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ]
            });

            $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });

        });
    </script>
@endsection