@extends('layouts.member')

@section('styles')
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">

    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
@endsection

@section('content')
<div class="container-fluid">
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header card-header-primary">
          <h4 class="card-title">PIN</h4>
        </div>
        <div class="card-body">
          <div class="row">
            <div class="col-lg-4 col-md-12 col-sm-12">
              <div class="card card-stats">
                <div class="card-header card-header-warning">
                  <h4 class="card-title">PIN Tersedia</h4>
                  <h3 class="card-title">{{ count_pin_available(auth()->id()) }}</h3>
                </div>
                <div class="card-body">

                </div>
              </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6">
              <div class="card card-stats">
                <div class="card-header card-header-success">
                  <h4 class="category">Terpakai</h4>
                  <h3 class="card-title">{{ count_pin_used(auth()->id()) }}</h3>
                </div>
                <div class="card-body">

                </div>
              </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6">
              <div class="card card-stats">
                <div class="card-header card-header-danger">
                  <h4 class="category">Terjual</h4>
                  <h3 class="card-title">{{ count_pin_sold(auth()->id()) }}</h3>
                </div>
                <div class="card-body">

                </div>
              </div>
            </div>
          </div>
          <div class="material-datatables">
            <div id="datatables_wrapper" class="dataTables_wrapper dt-bootstrap4 table-responsive" data-vivaldi-spatnav-clickable="1">
              <table id="plan-setting-data" class="table table-striped table-no-bordered table-hover dataTable dtr-inline" cellspacing="0" width="100%" style="width: 100%;" role="grid" aria-describedby="datatables_info">
                <thead>
                <tr>
                    <th class="wd-25p">Kode Pin</th>
                    <th class="wd-30p">Status</th>
                    <th></th>
                </tr>
                </thead>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection

@section('scripts')
    <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
@endsection

@section('page_script')
    <script>
        $(function(){
            'use strict';

            $('#plan-setting-data').DataTable({
                responsive: false,
                language: {
                    searchPlaceholder: 'Cari...',
                    sSearch: '',
                    lengthMenu: '_MENU_ data/halaman',
                    zeroRecords: ' Tidak ada data yang dapat ditampilkan.',
                    info: 'Menampilkan halaman _PAGE_ dari _PAGES_ (Total Data : _MAX_)',
                    infoEmpty: '',
                    infoFiltered: "(disaring dari total _MAX_ data)"
                },
                processing: true,
                serverSide: true,
                ajax: '{{ route('user.pin.data') }}',
                columns: [
                    {data: 'pin_code', name: 'pin_code'},
                    {data: 'status', name: 'status',
                        render: function(data, type, row, meta){
                            if(type === 'display'){
                                if(data == 0){
                                    data = '<span class="text-success">tersedia</span>';
                                } else if(data == 1){
                                    data = '<span class="text-warning">terjual</span>';
                                } else if(data == 2){
                                    data = '<span class="text-purple">ditransfer</span>';
                                } else if(data == 3){
                                    data = '<span class="text-info">digunakan</span>';
                                }
                            }
                            return data;
                        }
                    },
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ]
            });

            $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });

        });
    </script>
@endsection
