@extends('layouts.member')

@section('styles')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
@endsection

@section('content')
<div class="container-fluid">
  <div class="row">
    <div class="col-lg-12">
      <div class="card">
        <div class="card-header card-header-primary">
          <h4 class="card-title">Beli Pin</h4>
        </div>
        <div class="card-body">
          <form method="post" class="form-horizontal" action="{{ route('transaction-pin.store') }}">
            @csrf
            <input type="hidden" name="type" value="buy">
            <div class="row">
              <div class="col-md-3">
                <label class="bmd-label-floating">Beli PIN dari</label>
              </div>
              <div class="col-md-6">
                <div class="form-group bmd-form-group {{$errors->has('stockist_id') ? 'danger' : ''}}">
                  @if( $plans->count() > 1 )
                  <select name="stockist_id" class="form-control select2" data-toggle="select2">
                      <option value="" selected disabled>--- Pilih Stockist ---</option>
                      @foreach($stockist_list as $key => $plan)
                          @if(old('stockist_id')==$key)
                              <option value="{{ $key }}" selected>{{ $plan }}</option>
                          @else
                              <option value="{{ $key }}">{{ $plan }}</option>
                          @endif
                      @endforeach
                  </select>
                  @else
                  <select name="stockist_id" class="form-control select2" data-toggle="select2">
                      <option value="" selected disabled>--- Pilih Stockist ---</option>
                      @foreach($stockist_list as $key => $plan)
                          @if(old('stockist_id')==$key)
                              <option value="{{ $key }}" selected>{{ $plan }}</option>
                          @else
                              <option value="{{ $key }}">{{ $plan }}</option>
                          @endif
                      @endforeach
                  </select>
                  @endif
                  @if($errors->has('stockist_id'))
                      <label id="exampleEmails-error" class="error" for="exampleEmails">{{ $errors->first('stockist_id') }}</label>
                  @endif

                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-3">
                <label class="bmd-label-floating">Jumlah Pin</label>
              </div>
              <div class="col-md-6">
                <div class="form-group bmd-form-group {{$errors->has('amount') ? 'danger' : ''}}">
                  <input type="number" class="form-control" name="amount" value="{{ old('amount') }}">
                  @if($errors->has('amount'))
                      <label id="exampleEmails-error" class="error" for="exampleEmails">{{ $errors->first('amount') }}</label>
                  @endif

                </div>
              </div>
            </div>
              <div class="col-md-6">
                @if( $plans->count() > 1 )
                  <div class="form-group bmd-form-group {{$errors->has('plan_id') ? 'danger' : ''}}">
                    <select name="plan_id" class="form-control select2" data-toggle="select2">
                        <option value="" selected disabled>--- Pilih Paket ---</option>
                        @foreach($plans as $key => $plan)
                            @if(old('plan_id')==$key)
                                <option value="{{ $key }}" selected>{{ $plan }}</option>
                            @else
                                <option value="{{ $key }}">{{ $plan }}</option>
                            @endif
                        @endforeach
                    </select>
                    @if($errors->has('plan_id'))
                        <label id="exampleEmails-error" class="error" for="exampleEmails">{{ $errors->first('plan_id') }}</label>
                    @endif
                  </div>
                @else
                  @foreach($plans as $key => $plan)
                      <input type="hidden" name="plan_id" value="{{ $key }}">
                  @endforeach
                @endif
              </div>
              <div class="col-md-9">
                <button type="submit" class="btn btn-primary pull-right">Order<div class="ripple-container"></div></button>
                <a class="btn btn-secondary pull-right" href="{{ route('user-pin.index') }}">Cancel</a>
              </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection

@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
@endsection

@section('page_script')
<script type="text/javascript">
$(document).ready(function() {
  $('.select2').select2();
});
</script>
    @if( $plans->count() > 1 )
        <script>
            $('#plan_id').on('select2:select', function (e) {
                var data = e.params.data;
                //console.log(data);

                $('#stockist_id').empty();
                $.ajax({
                    method: "POST",
                    url: '{{ route('transaction.get.stockist') }}',
                    data: {
                        "_token": "{{ csrf_token() }}",
                        "id": data.id
                    },
                    success: data => {
                        //console.log(data);
                        for (var opt in data) {
                            $('#stockist_id').append('<option value="'+ opt +'">'+ data[opt] +'</option>');
                        }
                    }
                })
            });
        </script>
    @endif

    <script>
        $(function(){
            'use strict'

            $('.form-layout .form-control').on('focusin', function(){
                $(this).closest('.form-group').addClass('form-group-active');
            });

            $('.form-layout .form-control').on('focusout', function(){
                $(this).closest('.form-group').removeClass('form-group-active');
            });

            $('.select2').on('select2:opening', function (e) {
                $(this).closest('.form-group').addClass('form-group-active');
            });

            $('.select2').on('select2:closing', function (e) {
                $(this).closest('.form-group').removeClass('form-group-active');
            });

        });
    </script>

    @if (session('success'))
        <div id="modalsukses" class="modal fade">
            <div class="modal-dialog" role="document">
                <div class="modal-content tx-size-sm">
                    <div class="modal-body tx-center pd-y-20 pd-x-20">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <i class="icon ion-ios-checkmark-outline tx-100 tx-success lh-1 mg-t-20 d-inline-block"></i>
                        <h4 class="tx-success tx-semibold mg-b-20">Sukses!</h4>
                        <p class="mg-b-20 mg-x-20">{{ session('success') }}</p>
                        <button type="button" class="btn btn-success tx-11 tx-uppercase pd-y-12 pd-x-25 tx-mont tx-medium mg-b-20" data-dismiss="modal" aria-label="Close"> OK</button>
                    </div><!-- modal-body -->
                </div><!-- modal-content -->
            </div><!-- modal-dialog -->
        </div><!-- modal -->
        <script>
            $('#modalsukses').modal('show');
        </script>
    @endif

    @if (session('error'))
        <div id="modalerrordelete" class="modal fade">
            <div class="modal-dialog" role="document">
                <div class="modal-content tx-size-sm">
                    <div class="modal-body tx-center pd-y-20 pd-x-20">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <i class="icon ion-ios-close-outline tx-100 tx-danger lh-1 mg-t-20 d-inline-block"></i>
                        <h4 class="tx-danger tx-semibold mg-b-20">Error!</h4>
                        <p class="mg-b-20 mg-x-20">{{ session('error') }}</p>
                        <button type="button" class="btn btn-danger tx-11 tx-uppercase pd-y-12 pd-x-25 tx-mont tx-medium mg-b-20" data-dismiss="modal" aria-label="Close"> OK</button>
                    </div><!-- modal-body -->
                </div><!-- modal-content -->
            </div><!-- modal-dialog -->
        </div><!-- modal -->
        <script>
            $('#modalerrordelete').modal('show');
        </script>
    @endif

@endsection
