@extends('layouts.app-plain')

@section('styles')
@endsection

@section('content')
    <div id="printThis">
        <div class="row mb-1">
            <div class="col-8 pt-1">
                <div class="row mb-1">
                    <div class="col-3">Kode Transaksi</div>
                    <div class="col-8">{{ $transaction->transaction_code }}</div>
                </div>
                <div class="row">
                    <div class="col-3">Tanggal Order</div>
                    <div class="col-8">{{ from_utc($transaction->ordered_at,'d-m-Y') }}</div>
                </div>
            </div>
            @if($transaction->touser->is_stockist == 1)
                @if($transaction->touser->stockist_type == 0)
                    <?php
                    $price = $transaction->plan->activeprice->stockist_price;
                    ?>
                @else
                    <?php
                    $price = $transaction->plan->activeprice->master_stockist_price;
                    ?>
                @endif
            @else
                <?php
                $price = $transaction->plan->activeprice->price;
                ?>
            @endif
            <div class="col-4">
                <div class="row">
                    <div class="col-12">
                        <div class="float-right text-right">
                            <h3 class="float-right text-purple">Rp {{ number_format(($transaction->amount * $price) + $transaction->unique_digit,0,',','.') }}</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-12">
                <img src="{{ asset('storage/bukti-transfer/'. $transaction->paid_file) }}">
            </div>
        </div>
    </div>
@endsection

@section('scripts')
@endsection

@section('page_script')
@endsection