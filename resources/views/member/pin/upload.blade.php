@extends('layouts.app-plain')

@section('styles')
@endsection

@section('content')
    <form method="post" id="frmUpload" action="{{ route('transaction.pin.doupload', ['pin_transaction' => $transaction]) }}" enctype="multipart/form-data">
    @csrf
    <div id="printThis">
        <div class="row mb-1">
            <div class="col-8 pt-1">
                <div class="row mb-1">
                    <div class="col-3">Kode Transaksi</div>
                    <div class="col-8">{{ $transaction->transaction_code }}</div>
                </div>
                <div class="row">
                    <div class="col-3">Tanggal Order</div>
                    <div class="col-8">{{ from_utc($transaction->ordered_at,'d-m-Y') }}</div>
                </div>
            </div>
            @if($transaction->touser->is_stockist == 1)
                @if($transaction->touser->stockist_type == 0)
			        <?php
			        $price = $transaction->plan->activeprice->stockist_price;
			        ?>
                @else
			        <?php
			        $price = $transaction->plan->activeprice->master_stockist_price;
			        ?>
                @endif
            @else
		        <?php
		        $price = $transaction->plan->activeprice->price;
		        ?>
            @endif
            <div class="col-4">
                <div class="row">
                    <div class="col-12">
                        <div class="float-right text-right">
                            <h3 class="float-right text-purple">Rp {{ number_format(($transaction->amount * $price) + $transaction->unique_digit,0,',','.') }}</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <ul class="list-group list-group-flush mt-2">
            <li class="list-group-item text-danger">Lakukan transfer sesuai dengan total invoice.
                Jumlah transfer yang tidak sesuai akan mengakibatkan system tidak dapat mendeteksi transfer Anda.</li>
        </ul>
        @if(! is_null($company_banks))
            <div class="form-group row mt-3">
                <label for="paid_company_to" class="col-2 col-form-label">Bank:</label>
                <div class="col-10">
                    <select name="paid_company_to" class="form-control select2 col-12" data-toggle="select2">
                        <option value="" selected disabled>--- Pilih Bank ---</option>
                        @foreach($company_banks as $key => $bank)
                            <option value="{{ $key }}">{{ $bank }}</option>
                        @endforeach
                    </select>
                    <small id="paid_company_to_error" class="form-text text-danger"></small>
                </div>
            </div>
        @endif
        @if(! is_null($user_banks))
            <div class="form-group row mt-3">
                <label for="paid_to" class="col-2 col-form-label">Bank:</label>
                <div class="col-10">
                    <select name="paid_to" class="form-control select2 col-12" data-toggle="select2">
                        <option value="" selected disabled>--- Pilih Bank ---</option>
                        @foreach($user_banks as $key => $bank)
                            <option value="{{ $key }}">{{ $bank }}</option>
                        @endforeach
                    </select>
                    <small id="paid_to_error" class="form-text text-danger"></small>
                </div>
            </div>
        @endif
        <div class="form-group row mt-3">
            <label for="paid_file" class="col-2 col-form-label">Bukti Transfer:</label>
            <div class="col-10">
                <input class="form-control" type="file" id="paid_file" name="paid_file">
                <code>File yang didukung : jpg, jpeg, bmp, png. Maksimum : 1MB</code>
                <br /><small id="paid_file_error" class="form-text text-danger"></small>
            </div>
        </div>
    </div>
    <div class="text-right">
        <button type="submit" id="btnUpload" class="btn btn-primary waves-effect waves-light"><i class="fe-upload-cloud"></i> Upload</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    </div>
    </form>
@endsection

@section('scripts')
@endsection

@section('page_script')
    <script>
        $('#frmUpload').submit(function(event) {
            var postData = new FormData(this);
            event.preventDefault();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: "{{ route('transaction.pin.doupload', ['pin_transaction' => $transaction]) }}",
                type: "POST",
                data: postData,
                contentType: false,
                cache: false,
                processData: false,
                success: function(result) {
                    // console.log(result);
                    $('#upload').modal('toggle');
                    location.reload();
                },
                error: function(data) {
                    $.each(data.responseJSON.errors, function( index, value ) {
                        $('#'+ index +'_error').text(value[0]);
                    });
                }
            });
        });
    </script>
@endsection