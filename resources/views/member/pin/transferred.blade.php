@extends('layouts.member')

@section('styles')
    <link href="{{ asset('admin/css/vendor/dataTables.bootstrap4.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('admin/css/vendor/responsive.bootstrap4.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="#">evolve.co.id</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('home') }}">member area</a></li>
                        <li class="breadcrumb-item">pin</li>
                        <li class="breadcrumb-item active">pin saya</li>
                    </ol>
                </div>
                <h4 class="page-title">PIN</h4>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xl-3 col-lg-6">
            <a href="{{ route('user.pin.available') }}">
                <div class="card widget-flat">
                    <div class="card-body p-0 bg-light">
                        <div class="p-3 pb-0">
                            <div class="float-right">
                                <i class="mdi mdi-apple-keyboard-command text-success widget-icon"></i>
                            </div>
                            <h5 class="text-muted font-weight-normal mt-0">Tersedia</h5>
                            <h3 class="mt-2">{{ count_pin_available(auth()->id()) }}</h3>
                        </div>
                    </div> <!-- end card-body-->
                </div> <!-- end card-->
            </a>
        </div> <!-- end col-->

        <div class="col-xl-3 col-lg-6">
            <a href="{{ route('user.pin.used') }}">
                <div class="card widget-flat">
                    <div class="card-body p-0 bg-light">
                        <div class="p-3 pb-0">
                            <div class="float-right">
                                <i class="mdi mdi-account-multiple-plus text-primary widget-icon"></i>
                            </div>
                            <h5 class="text-muted font-weight-normal mt-0">Digunakan</h5>
                            <h3 class="mt-2">{{ count_pin_used(auth()->id()) }}</h3>
                        </div>
                    </div> <!-- end card-body-->
                </div> <!-- end card-->
            </a>
        </div> <!-- end col-->

        <div class="col-xl-3 col-lg-6">
            <a href="{{ route('user.pin.sold') }}">
                <div class="card widget-flat">
                    <div class="card-body p-0 bg-light">
                        <div class="p-3 pb-0">
                            <div class="float-right">
                                <i class="mdi mdi-cart text-info widget-icon"></i>
                            </div>
                            <h5 class="text-muted font-weight-normal mt-0">Terjual</h5>
                            <h3 class="mt-2">{{ count_pin_sold(auth()->id()) }}</h3>
                        </div>
                    </div> <!-- end card-body-->
                </div> <!-- end card-->
            </a>
        </div> <!-- end col-->

        <div class="col-xl-3 col-lg-6">
            <a href="{{ route('user.pin.transferred') }}">
                <div class="card widget-flat border-purple">
                    <div class="card-body p-0 bg-light">
                        <div class="p-3 pb-0">
                            <div class="float-right">
                                <i class="mdi mdi-cart-off text-purple widget-icon"></i>
                            </div>
                            <h5 class="text-muted font-weight-normal mt-0">Ditransfer</h5>
                            <h3 class="mt-2">{{ count_pin_transferred(auth()->id()) }}</h3>
                        </div>
                    </div> <!-- end card-body-->
                </div> <!-- end card-->
            </a>
        </div> <!-- end col-->
    </div>
    <!-- end row -->

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <table id="plan-setting-data" class="table dt-responsive nowrap">
                                <thead>
                                <tr>
                                    <th class="wd-25p">Kode Pin</th>
                                    <th class="wd-30p">Status</th>
                                    <th></th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('js/vendor/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('js/vendor/dataTables.bootstrap4.js') }}"></script>
    <script src="{{ asset('js/vendor/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('js/vendor/responsive.bootstrap4.min.js') }}"></script>
@endsection

@section('page_script')
    <script>
        $(function(){
            'use strict';

            $('#plan-setting-data').DataTable({
                responsive: false,
                language: {
                    searchPlaceholder: 'Cari...',
                    sSearch: '',
                    lengthMenu: '_MENU_ data/halaman',
                    zeroRecords: ' Tidak ada data yang dapat ditampilkan.',
                    info: 'Menampilkan halaman _PAGE_ dari _PAGES_ (Total Data : _MAX_)',
                    infoEmpty: '',
                    infoFiltered: "(disaring dari total _MAX_ data)",
                    paginate: {
                        previous: "<i class='mdi mdi-chevron-left'>",
                        next: "<i class='mdi mdi-chevron-right'>"
                    }
                },
                drawCallback: function() {
                    $(".dataTables_paginate > .pagination").addClass("pagination-rounded")
                },
                processing: true,
                serverSide: true,
                ajax: '{{ route('user.pin.transferred.data') }}',
                columns: [
                    {data: 'pin_code', name: 'pin_code'},
                    {data: 'status', name: 'status',
                        render: function(data, type, row, meta){
                            if(type === 'display'){
                                if(data == 0){
                                    data = '<span class="text-success">tersedia</span>';
                                } else if(data == 1){
                                    data = '<span class="text-warning">terjual</span>';
                                } else if(data == 2){
                                    data = '<span class="text-purple">ditransfer</span>';
                                } else if(data == 3){
                                    data = '<span class="text-info">digunakan</span>';
                                }
                            }
                            return data;
                        }
                    },
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ]
            });
        });
    </script>
@endsection