@extends('layouts.app-plain')

@section('styles')
@endsection

@section('content')
    <div class="card" id="printThis" style="margin-top: -70px; border: 0;">
        <div class="card-body">
            <h5 class="card-title tx-dark tx-medium mg-b-10">#{{ $transaction->transaction_code }}</h5>
            <p class="card-subtitle tx-normal mg-b-15">Pemesanan pada {{ from_utc($transaction->ordered_at,'d-m-Y') }}</p>
            <p class="card-text">
                <span class="row">
                    <span class="col-md-4">
                        Jumlah Pin
                    </span>
                    <span class="col-md-8">
                        : {{ $transaction->amount }}
                    </span>
                </span>
                <span class="row">
                    <span class="col-md-4">
                        Harga Pin
                    </span>
                    <span class="col-md-8">
                                @if($transaction->touser->is_stockist == 1)
                            @if($transaction->touser->stockist_type == 0)
			                    <?php
			                    $price = $transaction->plan->activeprice->stockist_price;
			                    ?>
                                : Rp {{ number_format($price,0,',','.') }}
                            @else
			                    <?php
			                    $price = $transaction->plan->activeprice->master_stockist_price;
			                    ?>
                                : Rp {{ number_format($price,0,',','.') }}
                            @endif
                        @else
		                    <?php
		                    $price = $transaction->plan->activeprice->price;
		                    ?>
                            : Rp {{ number_format($price,0,',','.') }}
                        @endif
                    </span>
                </span>
                <span class="row">
                    <span class="col-md-4">
                        Total Harga
                    </span>
                    <span class="col-md-8">
                        : Rp {{ number_format($transaction->amount * $price,0,',','.') }}
                    </span>
                </span>
                <span class="row">
                    <span class="col-md-4">
                        Kode Unik
                    </span>
                    <span class="col-md-8">
                        : {{ number_format($transaction->unique_digit,0,',','.') }}
                    </span>
                </span>
                <span class="row mg-t-5">
                    <span class="col-md-4 font-weight-bold text-danger" style="font-size: 18px;">
                        Jumlah Transfer
                    </span>
                    <span class="col-md-8 font-weight-bold text-danger" style="font-size: 18px;">
                        : Rp {{ number_format(($transaction->amount * $price) + $transaction->unique_digit,0,',','.') }}
                    </span>
                </span>
            </p>
            <div class="alert alert-danger">
            Transfer sejumlah angka yang tertera sebagai "Jumlah Transfer", ke rekening berikut :
            <ol>
                @foreach($banks as $bank)
                    <li>{{ $bank->bank->name }}, No. Rek {{ $bank->account_number }}, Atas Nama {{ $bank->account_name }}</li>
                @endforeach
            </ol>
            </div>
        </div>
    </div>
@endsection