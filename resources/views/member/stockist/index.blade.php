@extends('layouts.member')

@section('styles')
    <link href="{{ asset('admin/lib/select2/css/select2.min.css') }}" rel="stylesheet">
@endsection

@section('content')
    <div class="br-pageheader pd-y-15 pd-l-20">
    </div><!-- br-pageheader -->
    <div class="pd-x-20 pd-sm-x-30 pd-t-20 pd-sm-t-30">
        <div class="pull-left">
            <h4 class="tx-gray-800 mg-b-5">Request Stockist</h4>
        </div>
    </div>

    <div class="br-pagebody mg-t-70">
        <div class="br-section-wrapper pd-20">
            <div class="row">
                <div class="col-12">
                    @if(! empty($my_request))
                            <div class="card d-block mb-4">
                                <div class="card-header">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <h4>
                                                @if($my_request->status == 0)
                                                    Menunggu Persetujuan Perusahaan
                                                @elseif($my_request->status == 2)
                                                    Permohonan Ditolak
                                                @endif
                                            </h4>
                                        </div>
                                        <div class="col-md-6 text-right">
                                            @if($my_request->status == 2)
                                                <a href="#" class="btn btn-outline-primary page-title-rightt">Request Ulang</a>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body">
                                    @if($my_request->status == 0)
                                        <p>Permohonan Anda sedang dalam proses.</p>
                                    @elseif($my_request->status == 2)
                                        <p>Permohonan Anda Ditolak, dengan alasan / keterangan sebagai berikut : <pre>{{ $my_request->status_note }}</pre></p>
                                    @endif
                                </div>
                            </div>
                    @else
                        <p>Dengan mengklik tombol "Ajukan Permohonan" di bawah ini, Anda menyatakan memahami dan menyetujui syarat dan ketentuan yang berlaku terkait dengan syarat, hak, dan kewajiban sebagai stockist.</p>
                    <form method="post" action="{{ route('stockist.request.post') }}">
                        @csrf
                        <input type="hidden" name="user_id" value="{{ auth()->id() }}">
                        <select id="request_type" name="request_type" class="form-control select2-show-search col-3" data-toggle="select2">
                            <option value="0">Stockist</option>
                            <option value="1">Master Stockist</option>
                        </select>
                        <button type="submit" class="btn btn-teal">Ajukan Permohonan</button>
                    </form>
                    @endif

                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('admin/lib/select2/js/select2.min.js') }}"></script>
@endsection

@section('page_script')
    @if (session('success'))
        <div id="modalsukses" class="modal fade">
            <div class="modal-dialog" role="document">
                <div class="modal-content tx-size-sm">
                    <div class="modal-body tx-center pd-y-20 pd-x-20">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <i class="icon ion-ios-checkmark-outline tx-100 tx-success lh-1 mg-t-20 d-inline-block"></i>
                        <h4 class="tx-success tx-semibold mg-b-20">Sukses!</h4>
                        <p class="mg-b-20 mg-x-20">{{ session('success') }}</p>
                        <button type="button" class="btn btn-success tx-11 tx-uppercase pd-y-12 pd-x-25 tx-mont tx-medium mg-b-20" data-dismiss="modal" aria-label="Close"> OK</button>
                    </div><!-- modal-body -->
                </div><!-- modal-content -->
            </div><!-- modal-dialog -->
        </div><!-- modal -->
        <script>
            $('#modalsukses').modal('show');
        </script>
    @endif

    @if (session('error'))
        <div id="modalerrordelete" class="modal fade">
            <div class="modal-dialog" role="document">
                <div class="modal-content tx-size-sm">
                    <div class="modal-body tx-center pd-y-20 pd-x-20">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <i class="icon ion-ios-close-outline tx-100 tx-danger lh-1 mg-t-20 d-inline-block"></i>
                        <h4 class="tx-danger tx-semibold mg-b-20">Error!</h4>
                        <p class="mg-b-20 mg-x-20">{{ session('error') }}</p>
                        <button type="button" class="btn btn-danger tx-11 tx-uppercase pd-y-12 pd-x-25 tx-mont tx-medium mg-b-20" data-dismiss="modal" aria-label="Close"> OK</button>
                    </div><!-- modal-body -->
                </div><!-- modal-content -->
            </div><!-- modal-dialog -->
        </div><!-- modal -->
        <script>
            $('#modalerrordelete').modal('show');
        </script>
    @endif
@endsection