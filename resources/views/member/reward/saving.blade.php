@extends('layouts.member')

@section('styles')
    <link href="{{ asset('admin/lib/select2/css/select2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('admin/lib/datatables/jquery.dataTables.css') }}" rel="stylesheet">
@endsection

@section('content')
    <div class="br-pageheader pd-y-15 pd-l-20">
    </div><!-- br-pageheader -->
    <div class="pd-x-20 pd-sm-x-30 pd-t-20 pd-sm-t-30">
        <div class="pull-left">
            <h4 class="tx-gray-800 mg-b-5">{{ $title }}</h4>
        </div>
    </div>

    <div class="br-pagebody mg-t-70">
        <div class="br-section-wrapper pd-20">
            <div class="table-wrapper">
                <table id="wd-saving-data" class="table dt-responsive nowrap">
                    <thead>
                    <tr>
                        <th>Kode WD</th>
                        <th>Jumlah Saving (Rp)</th>
                        <th>Tanggal</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('admin/lib/select2/js/select2.min.js') }}"></script>
    <script src="{{ asset('admin/lib/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('admin/lib/datatables-responsive/dataTables.responsive.js') }}"></script>
@endsection

@section('page_script')
    <script>

        $(function(){
            'use strict';

            $('#wd-saving-data').DataTable({
                responsive: false,
                language: {
                    searchPlaceholder: 'Cari...',
                    sSearch: '',
                    lengthMenu: '_MENU_ data/halaman',
                    zeroRecords: ' Tidak ada data yang dapat ditampilkan.',
                    info: 'Menampilkan halaman _PAGE_ dari _PAGES_ (Total Data : _MAX_)',
                    infoEmpty: '',
                    infoFiltered: "(disaring dari total _MAX_ data)"
                },
                processing: true,
                serverSide: true,
                ajax: '{{ route('reward.saving.data', ['reward_setting' => $reward_setting_id]) }}',
                columns: [
                    {data: 'withdrawal_code', name: 'withdrawal_code'},
                    {data: 'formatted_saving_amount', name: 'formatted_saving_amount'},
                    {data: 'saving_date', name: 'saving_date'},
                ],
                order: [[2, 'desc']]
            });

            $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });

        });

    </script>

@endsection