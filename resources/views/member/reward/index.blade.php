@extends('layouts.member')

@section('styles')
@endsection

@section('content')
<div class="container-fluid">
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header card-header-primary">
          <h4 class="card-title">Rewards</h4>
        </div>
        <div class="card-body">
          @foreach($rewards as $key => $reward)
          <div class="card d-block mb-4">
              <div class="card-header">
                  <div class="row">
                      <div class="col-md-6">
                          <h4>{{ $reward->first()->rewardsetting->name }}</h4>
                      </div>
                      <div class="col-md-6 text-right">
                          @if($reward->first()->rewardsetting->reward_type == 2)
                              <a href="{{ route('reward.saving',['reward_setting' => $reward->first()->rewardsetting->id]) }}" class="btn btn-outline-primary page-title-rightt">Lihat Saving</a>
                          @endif
                      </div>
                  </div>
              </div>
              <div class="card-body">
                  <div class="bd bd-gray-300 rounded table-responsive">
                      <table class="table dt-responsive nowrap">
                          <thead>
                          <tr>
                              <th rowspan="2">Reward</th>
                              <th colspan="2" class="text-center">Target</th>
                              <th colspan="2" class="text-center">Pencapaian</th>
                              <th rowspan="2"></th>
                          </tr>
                          <tr>
                              <th class="text-center">Kiri</th>
                              <th class="text-center">Kanan</th>
                              <th class="text-center">Kiri</th>
                              <th class="text-center">Kanan</th>
                          </tr>
                          </thead>
                          <tbody>
                          @foreach( $reward as $i => $item)
                          <tr>
                              <td>{{ $item->reward }}</td>
                              <td class="text-center">{{ $item->left }}</td>
                              <td class="text-center">{{ $item->right }}</td>
                              <td class="text-center">{{ optional($item->authmemberreward->first())->left }}</td>
                              <td class="text-center">{{ optional($item->authmemberreward->first())->right }}</td>
                              <td class="text-center">
                                  @if( optional($item->authmemberreward->first())->is_achieved == 1 && optional($item->authmemberreward->first())->is_claimed == 0 )
                                      <a href="{{ route('reward.claim',['member_reward_id' => optional($item->authmemberreward->first())->id]) }}" class="btn btn-outline-primary btn-sm">Klaim</a>
                                  @endif
                                  @if( optional($item->authmemberreward->first())->is_achieved == 0 && optional($item->authmemberreward->first())->is_claimed == 0 )
                                      <a href="#" class="btn btn-outline-light btn-sm disabled">Klaim</a>
                                  @endif
                                  @if( optional($item->authmemberreward->first())->is_achieved == 1 && optional($item->authmemberreward->first())->is_claimed == 1 && optional($item->authmemberreward->first())->is_received == 0 )
                                      <a href="#" class="btn btn-outline-warning btn-sm">Dalam Proses</a>
                                  @endif
                                  @if( optional($item->authmemberreward->first())->is_achieved == 1 && optional($item->authmemberreward->first())->is_claimed == 1 && optional($item->authmemberreward->first())->is_received == 1 )
                                      <a href="#" class="btn btn-outline-success btn-sm">Selesai</a>
                                  @endif
                              </td>
                          </tr>
                          @endforeach
                          </tbody>
                      </table>
                  </div>
              </div>
          </div>
          @endforeach
        </div>
      </div>
    </div>
  </div>
</div>
    
@endsection

@section('scripts')
@endsection

@section('page_script')
    @if (session('success'))
        <div id="modalsukses" class="modal fade">
            <div class="modal-dialog" role="document">
                <div class="modal-content tx-size-sm">
                    <div class="modal-body tx-center pd-y-20 pd-x-20">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <i class="icon ion-ios-checkmark-outline tx-100 tx-success lh-1 mg-t-20 d-inline-block"></i>
                        <h4 class="tx-success tx-semibold mg-b-20">Sukses!</h4>
                        <p class="mg-b-20 mg-x-20">{{ session('success') }}</p>
                        <button type="button" class="btn btn-success tx-11 tx-uppercase pd-y-12 pd-x-25 tx-mont tx-medium mg-b-20" data-dismiss="modal" aria-label="Close"> OK</button>
                    </div><!-- modal-body -->
                </div><!-- modal-content -->
            </div><!-- modal-dialog -->
        </div><!-- modal -->
        <script>
            $('#modalsukses').modal('show');
        </script>
    @endif

    @if (session('error'))
        <div id="modalerrordelete" class="modal fade">
            <div class="modal-dialog" role="document">
                <div class="modal-content tx-size-sm">
                    <div class="modal-body tx-center pd-y-20 pd-x-20">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <i class="icon ion-ios-close-outline tx-100 tx-danger lh-1 mg-t-20 d-inline-block"></i>
                        <h4 class="tx-danger tx-semibold mg-b-20">Error!</h4>
                        <p class="mg-b-20 mg-x-20">{{ session('error') }}</p>
                        <button type="button" class="btn btn-danger tx-11 tx-uppercase pd-y-12 pd-x-25 tx-mont tx-medium mg-b-20" data-dismiss="modal" aria-label="Close"> OK</button>
                    </div><!-- modal-body -->
                </div><!-- modal-content -->
            </div><!-- modal-dialog -->
        </div><!-- modal -->
        <script>
            $('#modalerrordelete').modal('show');
        </script>
    @endif
@endsection
