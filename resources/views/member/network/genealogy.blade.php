@extends('layouts.member')

@section('styles')

<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
@endsection

@section('header_scripts')
    @if($ok)
        <script type='text/javascript' src='https://www.google.com/jsapi'></script>
        <script type='text/javascript'>
            google.load('visualization', '1', {packages:['orgchart']});
            google.setOnLoadCallback(drawChart);

            function drawChart() {
                var data = new google.visualization.DataTable();
                data.addColumn('string', 'Username');
                data.addColumn('string', 'Upline');
                data.addColumn('string', 'ToolTip');
                data.addRows([
                    @foreach($structure as $node)
                        @if($loop->iteration == 1)
                            [{v: '{{ $node->id }}', f:'<img src="{{ asset('img/logo-genealogy.png') }}"><a href="{{ route('genealogy',['top_id' => $node->user_id]) }}"><strong>{{ optional($node->user)->username }}</strong></a><br /><div class="premium {{$node->user->premium ? 'active' : ''}}"><strong>Premium</strong></div><div class="count-normal">{{ $node->left_count }} - {{ $node->right_count }}</div><div class="count-premium">{{ $node->vleft_count }} - {{ $node->vright_count }}</div>'}, '', '{{ optional($node->user)->name }}'],
                        @else
                            @if($node->user_id == 0)
                                [{v: '{{ $node->id }}', f:'<img src="{{ asset('img/logo-genealogy-disabled.png') }}"><a href="{{ route('placement.list', ['parent_id' => $node->parent_id, 'position' => $node->position]) }}" class="btn btn-sm btn-warning text-white pl-2 pr-2" data-toggle="modal" data-target="#placement" aria-labelledby="placementModalLabel"><i class="fa fa-user"></i> Di Sini</a><br>&nbsp;<br>&nbsp;'}, '{{ $node->parent_id }}', 'Tempatkan Member Baru'],
                            @else
                                [{v: '{{ $node->id }}', f:'<img src="{{ asset('img/logo-genealogy.png') }}"><a href="{{ route('genealogy',['top_id' => $node->user_id]) }}"><strong>{{ optional($node->user)->username }}</strong></a><br /><div class="premium {{$node->user->premium ? 'active' : ''}}"><strong>Premium</strong></div><div class="count-normal">{{ $node->left_count }} - {{ $node->right_count }}</div><div class="count-premium">{{ $node->vleft_count }} - {{ $node->vright_count }}</div>'}, '{{ $node->parent_id }}', '{{ optional($node->user)->name }}'],
                            @endif
                        @endif
                    @endforeach
                ]);

                var chart = new google.visualization.OrgChart(document.getElementById('genealogy'));

                // set selection on 'ready' and 'select'
                google.visualization.events.addListener(chart, 'ready', setSelection);
                google.visualization.events.addListener(chart, 'select', setSelection);

                function setSelection() {
                    chart.setSelection([]);
                }

                chart.draw(data, {allowHtml:true, nodeClass:'myflur'});
            }
        </script>
    @endif
    <style>
        .myflur {
            color: green;
            width: 55px;
            border: 0;
            text-align: center;
        }
        .google-visualization-orgchart-lineleft {
            border-left: 1px solid #ccc!important;
        }
        .google-visualization-orgchart-linebottom {
            border-bottom: 1px solid #ccc!important;
        }
        .google-visualization-orgchart-lineright {
            border-right: 1px solid #ccc!important;
        }
        .google-visualization-orgchart-node {
            border: 0 !important;
            color: #373b44 !important;
            -moz-border-radius: 5px !important;
            -webkit-border-radius: 5px !important;
            -webkit-box-shadow: rgba(0, 0, 0, 0.5) 3px 3px 3px !important;
            -moz-box-shadow: rgba(0, 0, 0, 0.5) 3px 3px 3px !important;
            background-color: #8DC26F !important;
            background: -webkit-gradient(linear, left top, left bottom, from(#00b297), to(#11998e))!important;
            min-width: 120px !important;
            max-width: 120px !important;
            height: 60px !important;
        }
        .google-visualization-orgchart-node a {
            color: #ffffff !important;
        }
        .google-visualization-orgchart-node a:hover {
            color: #fffc00 !important;
        }
        .google-visualization-orgchart-node-medium {
            font-size: 1em !important;
        }
        .myflur img {
            clear: both;
            display: block;
            margin: 0 auto 3px;
            width: 50px;
        }

        .modal-dialog,
        .modal-content {
            /* 80% of window height */
            height: 80vh;
        }

        .modal-body {
            /* 100% = dialog height, 120px = header + footer */
            max-height: calc(100vh - 210px);
            overflow-y: auto;
        }
        .premium{
          color: gray;
          font-size: 0.8em;
          font-weight: 400;
        }
        .active{
          color: green;
        }
        .count-normal{
          color: black;
          margin: 0;
          padding: 0;
        }
        .count-premium{
          color: green;
          margin: 0;
          padding: 0;
        }
    </style>
@endsection

@section('content')
<div class="container-fluid">
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header card-header-primary">
          <h4 class="card-title">Network Tree</h4>
        </div>
        <div class="card-body">
          <div class="row">
              <div class="col-md-6">
                  <div class="form-group">
                      <select id="top_id" name="top_id" class="form-control select2-show-search" data-toggle="select2">
                          <option value="" selected disabled>Cari...</option>
                          @foreach($my_tree_list as $key => $tree)
                              <option value="{{ $key }}">{{ $tree }}</option>
                          @endforeach
                      </select>
                  </div>
              </div>
          </div>
          @if($ok)
              <div class="row">
                  <div class="col-md-12">
                      <div id="genealogy"></div>
                  </div>
              </div>
          @else
              <div class="row">
                  <div class="col-md-12">
                      <div id="genealogy">
                          <div class="alert alert-danger" role="alert">
                              Anda tidak berhak mengakses struktur yang bukan milik Anda!
                          </div>
                      </div>
                  </div>
              </div>
          @endif
        </div>
      </div>
    </div>
  </div>
</div>

    <div class="modal fade" id="placement" tabindex="-2" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="placementModalLabel">Tempatkan Member Baru</h4>
                </div>
                <div class="modal-body">
                </div>
                <div class="modal-footer">
                    <div class="text-right d-print-none">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
@endsection

@section('page_script')
<script type="text/javascript">
$(document).ready(function() {
  $('.select2-show-search').select2();
});
</script>
    <script>
        $('#placement').on('show.bs.modal', function (e) {
            e.stopImmediatePropagation();

            var button = $(e.relatedTarget);
            var modal = $(this);

            $.ajax({
                type: "GET",
                url: button.attr('href'),
                contentType: false,
                cache: false,
                processData: false,
                success: function (data) {
                    modal.find('.modal-body').html(data);
                },
                error: function (err) {
                }
            })
        });

        // $('#top_id').on('select2:select', function (e) {
        //     var data = e.params.data;
        //     var rawurl = '{{ route('genealogy', ['top_id' => 'data.id']) }}';
        //     var url = rawurl.replace('data.id', data.id);
        //     window.location = url;
        // });
        $('#top_id').change(function(e){
          var data = $(this).val();
          var rawurl = '{{ route('genealogy', ['top_id' => 'data']) }}';
          var url = rawurl.replace('data', data);
          window.location = url;
        });

    </script>

    @if (session('success'))
        <div id="modalsukses" class="modal fade">
            <div class="modal-dialog" role="document">
                <div class="modal-content tx-size-sm">
                    <div class="modal-body tx-center pd-y-20 pd-x-20">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <i class="icon ion-ios-checkmark-outline tx-100 tx-success lh-1 mg-t-20 d-inline-block"></i>
                        <h4 class="tx-success tx-semibold mg-b-20">Sukses!</h4>
                        <p class="mg-b-20 mg-x-20">{{ session('success') }}</p>
                        <button type="button" class="btn btn-success tx-11 tx-uppercase pd-y-12 pd-x-25 tx-mont tx-medium mg-b-20" data-dismiss="modal" aria-label="Close"> OK</button>
                    </div><!-- modal-body -->
                </div><!-- modal-content -->
            </div><!-- modal-dialog -->
        </div><!-- modal -->
        <script>
            $('#modalsukses').modal('show');
        </script>
    @endif

    @if (session('error'))
        <div id="modalerrordelete" class="modal fade">
            <div class="modal-dialog" role="document">
                <div class="modal-content tx-size-sm">
                    <div class="modal-body tx-center pd-y-20 pd-x-20">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <i class="icon ion-ios-close-outline tx-100 tx-danger lh-1 mg-t-20 d-inline-block"></i>
                        <h4 class="tx-danger tx-semibold mg-b-20">Error!</h4>
                        <p class="mg-b-20 mg-x-20">{{ session('error') }}</p>
                        <button type="button" class="btn btn-danger tx-11 tx-uppercase pd-y-12 pd-x-25 tx-mont tx-medium mg-b-20" data-dismiss="modal" aria-label="Close"> OK</button>
                    </div><!-- modal-body -->
                </div><!-- modal-content -->
            </div><!-- modal-dialog -->
        </div><!-- modal -->
        <script>
            $('#modalerrordelete').modal('show');
        </script>
    @endif
@endsection
