@extends('layouts.member')

@section('styles')
    <link href="{{ asset('admin/lib/select2/css/select2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('admin/lib/jquery-toggles/toggles-full.css') }}" rel="stylesheet">
@endsection

@section('content')
<div class="container-fluid">
  <div class="row">
    <div class="col-lg-12">
      <div class="card">
        <div class="card-header card-header-primary">
          <h4 class="card-title">Registrasi Member</h4>
        </div>
        <div class="card-body">
          <form method="post" action="{{ route('register.store') }}">
            @csrf
              <div class="col-md-6">
                @if( $plans->count() > 1 )
                  <div class="form-group bmd-form-group {{$errors->has('plan_id') ? 'danger' : ''}}">
                    <select name="plan_id" class="form-control select2" data-toggle="select2">
                        <option value="" selected disabled>--- Pilih Paket ---</option>
                        @foreach($plans as $key => $plan)
                            @if(old('plan_id')==$key)
                                <option value="{{ $key }}" selected>{{ $plan }}</option>
                            @else
                                <option value="{{ $key }}">{{ $plan }}</option>
                            @endif
                        @endforeach
                    </select>
                    @if($errors->has('plan_id'))
                        <label id="exampleEmails-error" class="error" for="exampleEmails">{{ $errors->first('plan_id') }}</label>
                    @endif
                  </div>
                @else
                  @foreach($plans as $key => $plan)
                      <input type="hidden" name="plan_id" value="{{ $key }}">
                  @endforeach
                @endif
              </div>
              <div class="col-md-6">
                <div class="form-group bmd-form-group {{$errors->has('name') ? 'danger' : ''}}">
                  <label class="bmd-label-floating">Nama Lengkap</label>
                  <input type="text" class="form-control" name="name" value="{{ old('name') }}">
                  @if($errors->has('name'))
                      <label id="exampleEmails-error" class="error" for="exampleEmails">{{ $errors->first('name') }}</label>
                  @endif

                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group bmd-form-group {{$errors->has('email') ? 'danger' : ''}}">
                  <label class="bmd-label-floating">Email</label>
                  <input type="email" class="form-control" name="email" value="{{ old('email') }}">
                  @if($errors->has('email'))
                      <label id="exampleEmails-error" class="error" for="exampleEmails">{{ $errors->first('email') }}</label>
                  @endif

                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group bmd-form-group {{$errors->has('username') ? 'danger' : ''}}">
                  <label class="bmd-label-floating">Username</label>
                  <input type="text" class="form-control" name="username" value="{{ old('username') }}">
                  @if($errors->has('username'))
                      <label id="exampleEmails-error" class="error" for="exampleEmails">{{ $errors->first('username') }}</label>
                  @endif

                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group bmd-form-group {{$errors->has('password') ? 'danger' : ''}}">
                  <label class="bmd-label-floating">Password</label>
                  <input type="password" class="form-control" name="password" value="">
                  @if($errors->has('password'))
                      <label id="exampleEmails-error" class="error" for="exampleEmails">{{ $errors->first('password') }}</label>
                  @endif

                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group bmd-form-group ">
                  <label class="bmd-label-floating">Konfirmasi Password</label>
                  <input type="password" class="form-control" name="password_confirmation" value="">
                </div>
              </div>
              <div class="col-md-6">
                <button type="submit" class="btn btn-primary pull-right">Save<div class="ripple-container"></div></button>
                <a class="btn btn-secondary pull-right" href="{{ route('home') }}">Cancel</a>
              </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection

@section('scripts')
    <script src="{{ asset('admin/lib/select2/js/select2.min.js') }}"></script>
    <script src="{{ asset('admin/lib/jquery-toggles/toggles.min.js') }}"></script>
@endsection

@section('page_script')
    @if (session('success'))
        <div id="modalsukses" class="modal fade">
            <div class="modal-dialog" role="document">
                <div class="modal-content tx-size-sm">
                    <div class="modal-body tx-center pd-y-20 pd-x-20">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <i class="icon ion-ios-checkmark-outline tx-100 tx-success lh-1 mg-t-20 d-inline-block"></i>
                        <h4 class="tx-success tx-semibold mg-b-20">Sukses!</h4>
                        <p class="mg-b-20 mg-x-20">{{ session('success') }}</p>
                        <button type="button" class="btn btn-success tx-11 tx-uppercase pd-y-12 pd-x-25 tx-mont tx-medium mg-b-20" data-dismiss="modal" aria-label="Close"> OK</button>
                    </div><!-- modal-body -->
                </div><!-- modal-content -->
            </div><!-- modal-dialog -->
        </div><!-- modal -->
        <script>
            $('#modalsukses').modal('show');
        </script>
    @endif

    @if (session('error'))
        <div id="modalerrordelete" class="modal fade">
            <div class="modal-dialog" role="document">
                <div class="modal-content tx-size-sm">
                    <div class="modal-body tx-center pd-y-20 pd-x-20">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <i class="icon ion-ios-close-outline tx-100 tx-danger lh-1 mg-t-20 d-inline-block"></i>
                        <h4 class="tx-danger tx-semibold mg-b-20">Error!</h4>
                        <p class="mg-b-20 mg-x-20">{{ session('error') }}</p>
                        <button type="button" class="btn btn-danger tx-11 tx-uppercase pd-y-12 pd-x-25 tx-mont tx-medium mg-b-20" data-dismiss="modal" aria-label="Close"> OK</button>
                    </div><!-- modal-body -->
                </div><!-- modal-content -->
            </div><!-- modal-dialog -->
        </div><!-- modal -->
        <script>
            $('#modalerrordelete').modal('show');
        </script>
    @endif

@endsection
