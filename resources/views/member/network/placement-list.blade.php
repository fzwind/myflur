@extends('layouts.app-plain')

@section('styles')
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <small id="error" class="text-danger"></small>
            <table class="table table-striped table-bordered">
                <thead class="thead-colored thead-teal">
                    <tr>
                        <th>Username</th>
                        <th>Nama</th>
                        <th class="w-15"></th>
                    </tr>
                </thead>
                <tbody>
                @foreach($placement_list as $user)
                    <tr>
                        <td>{{ $user->username }}</td>
                        <td>{{ $user->name }}</td>
                        <td class="text-center">
                            <form id="frmPlacement" method="post" action="{{ route('placement') }}">
                                @csrf
                                <input type="hidden" name="user_id" value="{{ $user->id }}">
                                <input type="hidden" name="parent_id" value="{{ $parentId }}">
                                <input type="hidden" name="position" value="{{ $position }}">
                                <button type="submit" class="btn btn-sm btn-success">tempatkan</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('page_script')
    <script>
        $('#frmPlacement').submit(function(event) {
            var postData = new FormData(this);
            event.preventDefault();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: "{{ route('placement') }}",
                type: "POST",
                data: postData,
                contentType: false,
                cache: false,
                processData: false,
                success: function(result) {
                    // console.log(result);
                    $('#placement').modal('toggle');
                    location.reload();
                },
                error: function(data) {
                    // console.log(data.responseJSON);
                    $.each(data.responseJSON.errors, function( index, value ) {
                         $('#error').text(value[0]);
                    });
                }
            });
        });
    </script>
@endsection