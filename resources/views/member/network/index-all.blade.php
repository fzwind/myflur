@extends('layouts.member')

@section('styles')
    <link href="{{ asset('admin/css/vendor/dataTables.bootstrap4.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('admin/css/vendor/responsive.bootstrap4.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="#">evolve.co.id</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('home') }}">member area</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('user-pin.index') }}">jaringan</a></li>
                        <li class="breadcrumb-item active">seluruh jaringan</li>
                    </ol>
                </div>
                <h4 class="page-title">Daftar Seluruh Jaringan</h4>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <table id="all-member-data" class="table dt-responsive nowrap">
                                <thead>
                                <tr>
                                    <th>Nama (username)</th>
                                    <th></th>
                                    <th>Email</th>
                                    <th>Status</th>
                                    <th></th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('js/vendor/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('js/vendor/dataTables.bootstrap4.js') }}"></script>
    <script src="{{ asset('js/vendor/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('js/vendor/responsive.bootstrap4.min.js') }}"></script>
@endsection

@section('page_script')
    <script>

        $(function(){
            'use strict';

            $('#all-member-data').DataTable({
                responsive: false,
                language: {
                    searchPlaceholder: 'Cari...',
                    sSearch: '',
                    lengthMenu: '_MENU_ data/halaman',
                    zeroRecords: ' Tidak ada data yang dapat ditampilkan.',
                    info: 'Menampilkan halaman _PAGE_ dari _PAGES_ (Total Data : _MAX_)',
                    infoEmpty: '',
                    infoFiltered: "(disaring dari total _MAX_ data)",
                    paginate: {
                        previous: "<i class='mdi mdi-chevron-left'>",
                        next: "<i class='mdi mdi-chevron-right'>"
                    }
                },
                drawCallback: function() {
                    $(".dataTables_paginate > .pagination").addClass("pagination-rounded")
                },
                processing: true,
                serverSide: true,
                ajax: '{{ route('networks.all.data') }}',
                columns: [
                    {data: 'user.name', name: 'user.name'},
                    {data: 'user.username', name: 'user.username'},
                    {data: 'user.email', name: 'user.email'},
                    {data: 'status', name: 'status'},
                    {data: 'status_date', name: 'status_date'},
                ],
                columnDefs: [
                    {
                        render: function (data, type, row) {
                            return data +' ('+ row.user.username +')';
                        },
                        targets: 0
                    },
                    {visible: false, targets: [1]}
                ],
                order: [[4, 'desc']]
            });
        });

        @if (session('success'))
            toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": false,
            "progressBar": true,
            "positionClass": "toast-bottom-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }
        toastr["success"]("{{ session('success') }}", "Success!");
        @endif

                @if (session('error'))
            toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": false,
            "progressBar": true,
            "positionClass": "toast-bottom-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }
        toastr["error"]("{{ session('error') }}", "Error!");
        @endif
    </script>

@endsection