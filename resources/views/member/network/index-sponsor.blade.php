@extends('layouts.member')

@section('styles')
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
@endsection

@section('content')
<div class="container-fluid">
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header card-header-primary">
          <h4 class="card-title">Sponsoring</h4>
        </div>
        <div class="card-body">
          <div class="material-datatables">
            <div id="datatables_wrapper" class="dataTables_wrapper dt-bootstrap4 table-responsive" data-vivaldi-spatnav-clickable="1">
              <table id="sponsored-member-data" class="table table-striped table-no-bordered table-hover dataTable dtr-inline" cellspacing="0" width="100%" style="width: 100%;" role="grid" aria-describedby="datatables_info">
                  <thead>
                  <tr>
                      <th>Nama (username)</th>
                      <th></th>
                      <th>Email</th>
                      <th>Status</th>
                      <th></th>
                  </tr>
                  </thead>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('scripts')
    <script src="//code.jquery.com/jquery.js"></script>
    <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
@endsection

@section('page_script')
    <script>

        $(function(){
            'use strict';

            $('#sponsored-member-data').DataTable({
                responsive: false,
                language: {
                    searchPlaceholder: 'Cari...',
                    sSearch: '',
                    lengthMenu: '_MENU_ data/halaman',
                    zeroRecords: ' Tidak ada data yang dapat ditampilkan.',
                    info: 'Menampilkan halaman _PAGE_ dari _PAGES_ (Total Data : _MAX_)',
                    infoEmpty: '',
                    infoFiltered: "(disaring dari total _MAX_ data)"
                },
                processing: true,
                serverSide: true,
                ajax: '{{ route('networks.sponsor.data') }}',
                columns: [
                    {data: 'name', name: 'name'},
                    {data: 'username', name: 'username'},
                    {data: 'email', name: 'email'},
                    {data: 'status', name: 'status'},
                    {data: 'status_date', name: 'status_date'},
                ],
                columnDefs: [
                    {
                        render: function (data, type, row) {
                            return data +' ('+ row.username +')';
                        },
                        targets: 0
                    },
                    {visible: false, targets: [1]}
                ],
                order: [[4, 'desc']]
            });

            $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });

        });

    </script>

@endsection
