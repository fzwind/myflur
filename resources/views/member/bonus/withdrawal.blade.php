@extends('layouts.member')

@section('styles')
    <link href="{{ asset('admin/lib/select2/css/select2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('admin/lib/datatables/jquery.dataTables.css') }}" rel="stylesheet">
@endsection

@section('content')
<div class="container-fluid">
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header card-header-primary">
          <h4 class="card-title">Withdrawal</h4>
        </div>
        <div class="card-body">
          <div class="material-datatables">
            <div id="datatables_wrapper" class="dataTables_wrapper dt-bootstrap4 table-responsive" data-vivaldi-spatnav-clickable="1">
              <table id="bonus-sponsor-data" class="table table-striped table-no-bordered table-hover dataTable dtr-inline" cellspacing="0" width="100%" style="width: 100%;" role="grid" aria-describedby="datatables_info">
                <thead>
                <tr>
                    <th>Kode</th>
                    <th>WD (Rp)</th>
                    <th>Reward Saving (Rp)</th>
                    <th>Biaya Admin (Rp)</th>
                    <th>Ditransfer (Rp)</th>
                    <th>Tanggal</th>
                    <th></th>
                    <th>Status</th>
                </tr>
                </thead>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('scripts')
<script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
@endsection

@section('page_script')
    <script>

        $(function(){
            'use strict';

            $('#bonus-sponsor-data').DataTable({
                responsive: false,
                language: {
                    searchPlaceholder: 'Cari...',
                    sSearch: '',
                    lengthMenu: '_MENU_ data/halaman',
                    zeroRecords: ' Tidak ada data yang dapat ditampilkan.',
                    info: 'Menampilkan halaman _PAGE_ dari _PAGES_ (Total Data : _MAX_)',
                    infoEmpty: '',
                    infoFiltered: "(disaring dari total _MAX_ data)"
                },
                processing: true,
                serverSide: true,
                ajax: '{{ route('withdrawal.data') }}',
                columns: [
                    {data: 'code', name: 'code'},
                    {data: 'formatted_wd_amount', name: 'formatted_wd_amount'},
                    {data: 'formatted_reward_savings', name: 'formatted_reward_savings'},
                    {data: 'formatted_wd_admin', name: 'formatted_wd_admin'},
                    {data: 'formatted_transferred', name: 'formatted_transferred'},
                    {data: 'wd_date', name: 'wd_date'},
                    {data: 'updated_at', name: 'updated_at'},
                    {data: 'formatted_status', name: 'formatted_status'},
                ],
                order: [[6, 'desc']],
                "columnDefs":[
                    {"visible": false, "targets":6}
                ]
            });

            $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });

        });

    </script>

@endsection
