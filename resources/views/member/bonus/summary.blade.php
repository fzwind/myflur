@extends('layouts.member')

@section('styles')
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
@endsection

@section('content')
<div class="container-fluid">
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header card-header-primary">
          <h4 class="card-title">Ringkasan Bonus</h4>
        </div>
        <div class="card-body">
          <div class="row">
            <div class="col-lg-4 col-md-12 col-sm-12">
              <div class="card card-stats">
                <div class="card-header card-header-warning">
                  <h4 class="card-title">Bonus Sponsor</h4>
                </div>
                <div class="card-body">
                  <h3>Rp {{ number_format($sponsor_total,0,',','.')}}</h3>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6">
              <div class="card card-stats">
                <div class="card-header card-header-success">
                  <h4 class="card-title">Bonus Pairing</h4>
                </div>
                <div class="card-body">
                  <h3>Rp {{ number_format($pairing_total,0,',','.')}}</h3>
                </div>
              </div>
            </div>
            <!-- <div class="col-lg-4 col-md-6 col-sm-6">
              <div class="card card-stats">
                <div class="card-header card-header-danger">
                  <h4 class="card-title">Bonus Pairing Level</h4>
                </div>
                <div class="card-body">
                  <h3 class="card-title">Rp {{ number_format($pairing_level_total,0,',','.') }}</h3>
                </div>
              </div>
            </div> -->
          </div>
          <div class="material-datatables">
            <div id="datatables_wrapper" class="dataTables_wrapper dt-bootstrap4 table-responsive" data-vivaldi-spatnav-clickable="1">
              <table id="bonus-data" class="table table-striped table-no-bordered table-hover dataTable dtr-inline" cellspacing="0" width="100%" style="width: 100%;" role="grid" aria-describedby="datatables_info">
                <thead>
                <tr>
                    <th rowspan="2">Kode</th>
                    <th rowspan="2">Dari</th>
                    <th colspan="{{ $i }}" class="text-center">Bonus (Rp)</th>
                    <th rowspan="2">Tanggal</th>
                    <th rowspan="2"></th>
                </tr>
                <tr>
                    @system('bonus_sponsor')
                    <th>Sponsor</th>
                    @endsystem
                    @system('bonus_pairing')
                    <th>Pairing</th>
                    @endsystem
                    @system('bonus_pairing_level')
                    <th>Pairing Level</th>
                    @endsystem
                    @system('bonus_level')
                    <th>Level</th>
                    @endsystem
                </tr>
                </thead>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection

@section('scripts')
    <!-- <script src="//code.jquery.com/jquery.js"></script> -->
    <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
@endsection

@section('page_script')
    <script>

        $(function(){
            'use strict';

            $('#bonus-data').DataTable({
                responsive: false,
                language: {
                    searchPlaceholder: 'Cari...',
                    sSearch: '',
                    lengthMenu: '_MENU_ data/halaman',
                    zeroRecords: ' Tidak ada data yang dapat ditampilkan.',
                    info: 'Menampilkan halaman _PAGE_ dari _PAGES_ (Total Data : _MAX_)',
                    infoEmpty: '',
                    infoFiltered: "(disaring dari total _MAX_ data)"
                },
                processing: true,
                serverSide: true,
                ajax: '{{ route('bonus.summary.data') }}',
                columns: [
                    {data: 'code', name: 'code'},
                    {data: 'from_user', name: 'from_user'},
                    @system('bonus_sponsor')
                    {data: 'bonus_sponsor', name: 'bonus_sponsor'},
                    @endsystem
                    @system('bonus_pairing')
                    {data: 'bonus_pairing', name: 'bonus_pairing'},
                    @endsystem
                    @system('bonus_pairing_level')
                    {data: 'bonus_pairing_level', name: 'bonus_pairing_level'},
                    @endsystem
                    @system('bonus_level')
                    {data: 'bonus_level', name: 'bonus_level'},
                    @endsystem
                    {data: 'bonus_date', name: 'bonus_date'},
                    {data: 'created_at', name: 'created_at'},
                ],
                order: [[{{ $i + 3 }}, 'desc']],
                "columnDefs":[
                    {"visible": false, "targets":{{ $i + 3 }}}
                ]
            });
        });
    </script>

@endsection
