<!DOCTYPE html>
<html lang="en-us" class="no-js">

	<head>
		<meta charset="utf-8">
        <title>Flur Indonesia</title>
        <meta name="description" content="The description should optimally be between 150-160 characters.">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="author" content="Flur Indonesia">

        <!-- ================= Favicon ================== -->
        <!-- Standard -->
        <link rel="shortcut icon" href="img/favicon.png">
        <!-- Retina iPad Touch Icon-->
        <!-- <link rel="apple-touch-icon" sizes="144x144" href="img/favicon-retina-ipad.png"> -->
        <!-- Retina iPhone Touch Icon-->
        <!-- <link rel="apple-touch-icon" sizes="114x114" href="img/favicon-retina-iphone.png"> -->
        <!-- Standard iPad Touch Icon-->
        <!-- <link rel="apple-touch-icon" sizes="72x72" href="img/favicon-standard-ipad.png"> -->
        <!-- Standard iPhone Touch Icon-->
        <!-- <link rel="apple-touch-icon" sizes="57x57" href="img/favicon-standard-iphone.png"> -->

        <!-- ============== Resources style ============== -->
        <link rel="stylesheet" href="{{asset('front')}}/css/style.css" />

		<!-- Modernizr runs quickly on page load to detect features -->
		<script src="{{asset('front')}}/js/modernizr.custom.js"></script>
	</head>

	<body>

		<!-- Page preloader -->
		<div id="loading">
			<div id="preloader">
				<span></span>
				<span></span>
			</div>
		</div>

		<!-- START - Home part -->
		<div class="outer-home">

			<!-- START - Home section -->
			<section id="home">

				<!-- Background picture(s) -->
				<div id="vegas-background"></div>

				<!-- Overlay -->
				<div class="global-overlay"></div>

				<!-- START - Content -->
				<div class="content">

					<h1 class="text-intro opacity-0">
						<span>Flur Indonesia</span><br>
						We're Coming Soon...
					</h1>

					<p class="text-intro opacity-0">Bisnis Terbaik Bagi Anda akan hadir dalam waktu yang tidak lama lagi. Segera Kunci posisi anda.<br>
						Dapatkan Info Lebih banyak lagi dengan mengklik tombol + dibawah ini.</p>

					<a href="#" id="open-more-info" data-target="main-content" class="action-btn trigger text-intro opacity-0"><i class="icon ion-ios-plus-empty"></i></a>

				</div>
				<!-- END - Content -->

				<!-- START - Bottom home -->
				<div class="bottom-home">

					<!-- Social icons, replace # by your links -->
					<!-- <div class="social-icons">

						<a href="https://www.facebook.com/themehelite/" target="_blank"><i class="fa fa-facebook"></i></a>
						<a href="https://twitter.com/themehelite" target="_blank"><i class="fa fa-twitter"></i></a>
						<a href="#" target="_blank"><i class="fa fa-google-plus"></i></a>
						<a href="#" target="_blank"><i class="fa fa-linkedin"></i></a>
						<a href="#" target="_blank"><i class="fa fa-youtube"></i></a>

					</div>

					<p class="copyright">
						© Flur Indonesia
					</p> -->

				</div>
				<!-- END - Bottom home -->

			</section>
			<!-- END - Home section -->

		</div>
		<!-- END - Home part -->

		<!-- Transparent div used when more info part is opening, allow the user to click on the home to get back on it -->
		<div class="close-right-part layer-left hide-layer-left"></div>

		<!-- START - More Informations part -->
		<section id="main-content" class="hide-main-content">

			<div class="header-right">

				<!-- <div class="hero-info">

					<img src="img/logo.png" alt="" class="brand-logo" />

					<h2>IQON Inc. London<br>
						<small>Since 1929</small>
					</h2>

				</div> -->

			</div>

			<div class="content">

				<h2>Tentang Perusahaan Kita</h2>

				<p>Kami adalah perusahaan startup dari kota Blitar, Jawa Timur yang bervisi "Mengangkat taraf hidup Masyarakat Indonesia dengan bisnis yang terjangkau namun memiliki kualitas produk dan rencana pemasaran yang Baik". </p>
				<br>
				<p>Didukung oleh manajemen yang berpengalaman dalam bidang Network Marketing selama puluhan tahun kami yakin akan dapat membantu para mitra kami untuk mewujudkan apa rencana masa depan mereka semua</p>
				<br>
				<p>Semoga Flur Indonesia menjadi sarana bagi rekan-rekan kami untuk menyongsong masa depan yang lebih baik.</p>

			</div>

		</section>
		<!-- END - More Informations part -->

		<!-- Arrow going down -->
		<div class="to-scroll info-close" data-toggle="tooltip" data-placement="left" title="Scroll to see more...">
			<i class="icon ion-arrow-down-c scroll-down"></i>
		</div>

		<!-- Cross to close the More Informations part -->
		<button id="close-more-info" class="close-right-part hide-close"><i class="icon ion-android-close"></i></button>

		<!-- START - Root element of PhotoSwipe, the gallery. Must have class pswp. -->
		<div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">

		    <!-- Background of PhotoSwipe.
	        	It's a separate element as animating opacity is faster than rgba(). -->
		    <div class="pswp__bg"></div>

		    <!-- Slides wrapper with overflow:hidden. -->
		    <div class="pswp__scroll-wrap">

		        <!-- Container that holds slides.
		            PhotoSwipe keeps only 3 of them in the DOM to save memory.
		            Don't modify these 3 pswp__item elements, data is added later on. -->
		        <div class="pswp__container">
		            <div class="pswp__item"></div>
		            <div class="pswp__item"></div>
		            <div class="pswp__item"></div>
		        </div>

		        <!-- Default (PhotoSwipeUI_Default) interface on top of sliding area. Can be changed. -->
		        <div class="pswp__ui pswp__ui--hidden">

		            <div class="pswp__top-bar">

		                <!--  Controls are self-explanatory. Order can be changed. -->

		                <div class="pswp__counter"></div>

		                <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>

		                <button class="pswp__button pswp__button--share" title="Share"></button>

		                <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>

		                <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>

		                <!-- Preloader demo http://codepen.io/dimsemenov/pen/yyBWoR -->
		                <!-- element will get class pswp__preloader - - active when preloader is running -->
		                <div class="pswp__preloader">
		                    <div class="pswp__preloader__icn">
		                      <div class="pswp__preloader__cut">
		                        <div class="pswp__preloader__donut"></div>
		                      </div>
		                    </div>
		                </div>

		            </div>

		            <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
		                <div class="pswp__share-tooltip"></div>
		            </div>

		            <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)">
		            </button>

		            <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)">
		            </button>

		            <div class="pswp__caption">
		                <div class="pswp__caption__center"></div>
		            </div>

		        </div>

		    </div>

		</div>
		<!-- END - Root element of PhotoSwipe, the gallery. Must have class pswp. -->

		<!-- ///////////////////\\\\\\\\\\\\\\\\\\\ -->
	    <!-- ********** Resources jQuery ********** -->
	    <!-- \\\\\\\\\\\\\\\\\\\/////////////////// -->

		<!-- * Libraries jQuery, Easing and Bootstrap - Be careful to not remove them * -->
		<script src="{{asset('front')}}/js/jquery.min.js"></script>
		<script src="{{asset('front')}}/js/jquery.easings.min.js"></script>
		<script src="{{asset('front')}}/js/bootstrap.min.js"></script>

		<!-- Google Maps API -->
		<!-- <script src="http://maps.google.com/maps/api/js"></script> -->

		<!-- Newsletter plugin -->
		<script src="{{asset('front')}}/js/notifyMe.js"></script>

		<!-- Contact form plugin -->
		<script src="{{asset('front')}}/js/contact-me.js"></script>

		<!-- Slideshow/Image plugin -->
    <script type="text/javascript">
    $(document).ready(function(){
        "use strict";

        // Main slideshow displayed on the home
        $('#vegas-background').vegas({
            slides: [
                { src: '{{asset('front')}}/img/slide-1.png' },
                { src: '{{asset('front')}}/img/slide-2.png' }, // Remove this line and the next one if you want only one static picture
                { src: '{{asset('front')}}/img/slide-3.png' }, // ...
            ],

            // Delay beetween slides in milliseconds.
            delay: 5000,

            // Chose your transition effect (See the documentation provided in your download pack)
            transition: 'fade'
        });

        // Second slideshow displayed in the right panel
        $('.header-right').vegas({
            slides: [
                { src: '{{asset('front')}}/img/right-slide-1.png' },
                { src: '{{asset('front')}}/img/right-slide-2.png' }, // Remove this line and the next one if you want only one static picture
                // { src: 'img/right-slide-3.jpg' }, // ...
            ],

            // Delay beetween slides in milliseconds.
            delay: 4000,

            // Chose your transition effect (See the documentation provided in your download pack)
            transition: 'fade',
        });

    });
    </script>
		<script src="{{asset('front')}}/js/vegas.js"></script>

		<!-- Scroll plugin -->
		<script src="{{asset('front')}}/js/jquery.mousewheel.js"></script>

		<!-- Custom Scrollbar plugin -->
		<script src="{{asset('front')}}/js/jquery.mCustomScrollbar.js"></script>

		<!-- PhotoSwipe Core JS file -->
		<script src="{{asset('front')}}/js/photoswipe.js"></script>

		<!-- PhotoSwipe UI JS file -->
		<script src="{{asset('front')}}/js/photoswipe-ui-default.js"></script>

		<!-- Main JS File -->
		<script src="{{asset('front')}}/js/main.js"></script>

		<!--[if lte IE 9]><script src="//cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js"></script><![endif]-->

	</body>

</html>
