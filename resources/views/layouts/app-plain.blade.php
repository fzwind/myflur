<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Meta -->

    <link href="{{ asset('img/favicon.ico') }}" rel="icon" type="image/x-icon" />

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- vendor css -->
    <link href="{{ asset('admin/lib/font-awesome/css/font-awesome.css') }}" rel="stylesheet">
    <link href="{{ asset('admin/lib/Ionicons/css/ionicons.css') }}" rel="stylesheet">
    <link href="{{ asset('admin/lib/perfect-scrollbar/css/perfect-scrollbar.css') }}" rel="stylesheet">
    <link href="{{ asset('admin/lib/jquery-switchbutton/jquery.switchButton.css') }}" rel="stylesheet">
    <link href="{{ asset('admin/lib/highlightjs/github.css') }}" rel="stylesheet">

@yield('styles')


    @yield('header_scripts')
</head>

<body>

<!-- ########## START: MAIN PANEL ########## -->
<div class="br-mainpanel" style="margin-left: 0;">

    @yield('content')

</div><!-- br-mainpanel -->
<!-- ########## END: MAIN PANEL ########## -->

<script src="{{ asset('admin/lib/jquery/jquery.js') }}"></script>
<script src="{{ asset('admin/lib/bootstrap/bootstrap.js') }}"></script>

@yield('scripts')


@yield('page_script')

</body>
</html>
