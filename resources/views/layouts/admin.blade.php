<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Meta -->

    <link href="{{ asset('admin')}}/img/favicon.ico" rel="icon" type="image/x-icon" />

    <title>{{ config('app.name', 'Laravel')}}</title>

    <!-- vendor css -->
    <link href="{{ asset('admin')}}/lib/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="{{ asset('admin')}}/lib/Ionicons/css/ionicons.css" rel="stylesheet">
    <link href="{{ asset('admin')}}/lib/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet">
    <link href="{{ asset('admin')}}/lib/jquery-switchbutton/jquery.switchButton.css" rel="stylesheet">
    <link href="{{ asset('admin')}}/lib/highlightjs/github.css" rel="stylesheet">

@yield('styles')

<!-- Bracket CSS -->
    <link rel="stylesheet" href="{{ asset('admin')}}/css/myflur.css">

    @yield('header_scripts')
</head>

<body @role('super-administrator|administrators') class="collapsed-menu" @endrole>

<!-- ########## START: LEFT PANEL ########## -->
<div class="br-logo"><a href=""><img src="{{ asset('admin')}}/img/logo.png" style="height: 50px;"></a></div>
<div class="br-sideleft overflow-y-auto">
    <label class="sidebar-label pd-x-15 mg-t-20">&nbsp;</label>
    <div class="br-sideleft-menu">
        @role('members')
        @include('partials.member-menu')
        @endrole
        @role('super-administrator|administrators')
        @include('partials.admin-menu')
        @endrole
        @role('super-administrator')
        @include('partials.superadmin-menu')
        @endrole
    </div><!-- br-sideleft-menu -->
    <br>
</div><!-- br-sideleft -->
<!-- ########## END: LEFT PANEL ########## -->

<!-- ########## START: HEAD PANEL ########## -->
<div class="br-header">
    <div class="br-header-left">
        <div class="navicon-left hidden-md-down"><a id="btnLeftMenu" href=""><i class="icon ion-navicon-round"></i></a></div>
        <div class="navicon-left hidden-lg-up"><a id="btnLeftMenuMobile" href=""><i class="icon ion-navicon-round"></i></a></div>
    </div><!-- br-header-left -->
    <div class="br-header-right">
        <nav class="nav">
            @include('partials.notifications')
            @include('partials.user-dropdown')
        </nav>
        <div class="navicon-right">
            <a id="btnRightMenu" href="" class="pos-relative">
                <i class="icon ion-more"></i>
            </a>
        </div><!-- navicon-right -->
    </div><!-- br-header-right -->
</div><!-- br-header -->
<!-- ########## END: HEAD PANEL ########## -->

<!-- ########## START: RIGHT PANEL ########## -->
@include('partials.right-panel')
<!-- ########## END: RIGHT PANEL ########## --->

<!-- ########## START: MAIN PANEL ########## -->
<div class="br-mainpanel">

    @yield('content')

    <footer class="br-footer">
        <div class="footer-left">
            <div class="mg-b-2">Copyright &copy; 2018. <a href="http://myflur.id">myflur.id</a>. All Rights Reserved.</div>
        </div>
    </footer>

</div><!-- br-mainpanel -->
<!-- ########## END: MAIN PANEL ########## -->

<script src="{{ asset('admin')}}/lib/jquery/jquery.js"></script>
<script src="{{ asset('admin')}}/lib/popper.js/popper.js"></script>
<script src="{{ asset('admin')}}/lib/bootstrap/bootstrap.js"></script>
<script src="{{ asset('admin')}}/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.js"></script>
<script src="{{ asset('admin')}}/lib/moment/moment.js"></script>
<script src="{{ asset('admin')}}/lib/jquery-ui/jquery-ui.js"></script>
<script src="{{ asset('admin')}}/lib/jquery-switchbutton/jquery.switchButton.js"></script>
<script src="{{ asset('admin')}}/lib/peity/jquery.peity.js"></script>
<script src="{{ asset('admin')}}/lib/highlightjs/highlight.pack.js"></script>

@yield('scripts')

<script src="{{ asset('admin')}}/js/myflur.js"></script>

@yield('page_script')

</body>
</html>
