<div class="collapse navbar-collapse justify-content-end">
  <form class="navbar-form" style="display:none">
    <div class="input-group no-border">
      <input type="text" value="" class="form-control" placeholder="Search...">
      <button type="submit" class="btn btn-white btn-round btn-just-icon">
        <i class="material-icons">search</i>
        <div class="ripple-container"></div>
      </button>
    </div>
  </form>
  <ul class="navbar-nav">
    <li class="nav-item dropdown">
      <a class="nav-link" href="#pablo" id="navbarDropdownProfile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <i class="material-icons">person</i>
        <p class="d-lg-none d-md-block">
          Account
        </p>
      </a>
      <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
        <a class="dropdown-item" href="{{ route('my.account') }}">Profile</a>
        <!-- <a class="dropdown-item" href="#">Settings</a> -->
        <div class="dropdown-divider"></div>
        <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                         document.getElementById('logout-form').submit();"> Sign Out</a>
      </div>
    </li>
  </ul>
</div>
