<ul class="nav">
  <li class="nav-item {{ in_array(Route::current()->getName(), ['home']) ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('home') }}">
      <i class="material-icons">dashboard</i>
      <p>Dashboard</p>
    </a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="#network" data-toggle="collapse">
      <i class="material-icons">supervisor_account</i>
      <p>Network<b class="caret"></b></p>
    </a>
    <div class="collapse {{ in_array(Route::current()->getName(), ['register', 'genealogy', 'networks.all', 'networks.sponsor']) ? 'show' : '' }}" id="network" style="">
      <ul class="nav">
        <li class="nav-item {{ in_array(Route::current()->getName(), ['register']) ? 'active' : '' }}">
          <a class="nav-link" href="{{ route('register') }}">
            <!-- <span class="sidebar-mini"> P </span> -->
            <span class="sidebar-normal"> Registrasi Member baru </span>
          </a>
        </li>
        <li class="nav-item {{ in_array(Route::current()->getName(), ['networks.sponsor']) ? 'active' : '' }}">
          <a class="nav-link" href="{{ route('networks.sponsor') }}">
            <!-- <span class="sidebar-mini"> P </span> -->
            <span class="sidebar-normal"> Sponsoring </span>
          </a>
        </li>
        <li class="nav-item {{ in_array(Route::current()->getName(), ['genealogy']) ? 'active' : '' }}">
          <a class="nav-link" href="{{ route('genealogy', ['top_id' => auth()->id()]) }}">
            <!-- <span class="sidebar-mini"> P </span> -->
            <span class="sidebar-normal"> Network Tree </span>
          </a>
        </li>
      </ul>
    </div>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="#pin" data-toggle="collapse">
      <i class="material-icons">lock</i>
      <p>PIN<b class="caret"></b></p>
    </a>
    <div class="collapse {{ in_array(Route::current()->getName(), ['user-pin.index','user-pin.show','transaction-pin.create', 'transaction-pin.index', 'transaction.pin.running']) ? ' show' : '' }}" id="pin" style="">
      <ul class="nav">
        <li class="nav-item {{ in_array(Route::current()->getName(), ['transaction-pin.create']) ? 'active' : '' }}">
          <a class="nav-link" href="{{ route('transaction-pin.create') }}">
            <!-- <span class="sidebar-mini"> P </span> -->
            <span class="sidebar-normal"> Beli PIN </span>
          </a>
        </li>
        <li class="nav-item {{ in_array(Route::current()->getName(), ['user-pin.index','user-pin.show']) ? 'active' : '' }}">
          <a class="nav-link" href="{{ route('user-pin.index') }}">
            <!-- <span class="sidebar-mini"> P </span> -->
            <span class="sidebar-normal"> PIN Saya </span>
          </a>
        </li>
        <li class="nav-item {{ in_array(Route::current()->getName(), ['transaction.pin.running']) ? 'active' : '' }}">
          <a class="nav-link" href="{{ route('transaction.pin.running') }}">
            <!-- <span class="sidebar-mini"> P </span> -->
            <span class="sidebar-normal"> Transaksi </span>
          </a>
        </li>
        <li class="nav-item {{ in_array(Route::current()->getName(), ['transaction-pin.index']) ? 'active' : '' }}">
          <a class="nav-link" href="{{ route('transaction-pin.index') }}">
            <!-- <span class="sidebar-mini"> P </span> -->
            <span class="sidebar-normal"> Riwayat Transaksi </span>
          </a>
        </li>
      </ul>
    </div>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="#bonus" data-toggle="collapse">
      <i class="material-icons">card_giftcard</i>
      <p>Bonus / Reward<b class="caret"></b></p>
    </a>
    <div class="collapse {{ in_array(Route::current()->getName(), ['reward.achievement','bonus.summary','bonus.sponsor','bonus.pairing','bonus.pairing-level','withdrawal.index','reward.saving']) ? 'show' : '' }}" id="bonus" style="">
      <ul class="nav">
        @system('rewards')
        <li class="nav-item {{ in_array(Route::current()->getName(), ['reward.achievement','reward.saving']) ? 'active' : '' }}">
          <a class="nav-link" href="{{ route('reward.achievement') }}">
            <!-- <span class="sidebar-mini"> P </span> -->
            <span class="sidebar-normal"> Reward </span>
          </a>
        </li>
        @endsystem

        <li class="nav-item {{ in_array(Route::current()->getName(), ['bonus.summary']) ? 'active' : '' }}">
          <a class="nav-link" href="{{ route('bonus.summary') }}">
            <!-- <span class="sidebar-mini"> P </span> -->
            <span class="sidebar-normal"> Bonus Summary </span>
          </a>
        </li>

        @system('bonus_sponsor')
        <li class="nav-item {{ in_array(Route::current()->getName(), ['bonus.sponsor']) ? 'active' : '' }}">
          <a class="nav-link" href="{{ route('bonus.sponsor') }}">
            <!-- <span class="sidebar-mini"> P </span> -->
            <span class="sidebar-normal"> Bonus Sponsor </span>
          </a>
        </li>
        @endsystem

        @system('bonus_pairing')
        <li class="nav-item {{ in_array(Route::current()->getName(), ['bonus.pairing']) ? 'active' : '' }}">
          <a class="nav-link" href="{{ route('bonus.pairing') }}">
            <!-- <span class="sidebar-mini"> P </span> -->
            <span class="sidebar-normal"> Bonus Pairing </span>
          </a>
        </li>
        @endsystem

        @system('bonus_pairing_level')
        <li class="nav-item {{ in_array(Route::current()->getName(), ['bonus.pairing-level']) ? 'active' : '' }}">
          <a class="nav-link" href="{{ route('bonus.pairing-level') }}">
            <!-- <span class="sidebar-mini"> P </span> -->
            <span class="sidebar-normal"> Bonus Pairing Level</span>
          </a>
        </li>
        @endsystem

        <li class="nav-item {{ in_array(Route::current()->getName(), ['withdrawal.index']) ? 'active' : '' }}">
          <a class="nav-link" href="{{ route('withdrawal.index') }}">
            <!-- <span class="sidebar-mini"> P </span> -->
            <span class="sidebar-normal"> Withdrawal </span>
          </a>
        </li>
      </ul>
    </div>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="#profile" data-toggle="collapse">
      <i class="material-icons">person</i>
      <p>Account<b class="caret"></b></p>
    </a>
    <div class="collapse {{ in_array(Route::current()->getName(), ['my.account','my.account.address','my.account.id','my.account.bank','my.account.changepw']) ? 'show' : '' }}" id="profile" style="">
      <ul class="nav">

        <li class="nav-item {{ in_array(Route::current()->getName(), ['my.account','my.account.address','my.account.id','my.account.bank','my.account.changepw']) ? 'active' : '' }}">
          <a class="nav-link" href="{{ route('my.account') }}">
            <!-- <span class="sidebar-mini"> P </span> -->
            <span class="sidebar-normal"> Profile </span>
          </a>
        </li>

        <li class="nav-item">
          <a class="nav-link" href="{{ route('logout') }}" onclick="event.preventDefault();
                           document.getElementById('logout-form').submit();">
            <!-- <span class="sidebar-mini"> P </span> -->
            <span class="sidebar-normal"> Logout </span>
          </a>
          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
              @csrf
          </form>
        </li>
      </ul>
    </div>
  </li>
  @nonstockist
  <li class="nav-item {{ in_array(Route::current()->getName(), ['stockist.request']) ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('stockist.request') }}">
      <i class="material-icons">lock</i>
      <p>Request Stockist</p>
    </a>
  </li>
  @endnonstockist
</ul>
