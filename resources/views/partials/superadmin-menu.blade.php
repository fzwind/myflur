<label class="sidebar-label pd-x-15 mg-t-20">&nbsp;System Settings</label>
<a href="{{ route('system.general.settings') }}" class="br-menu-link {{ in_array(Route::current()->getName(), ['system.general.settings']) ? 'active' : '' }}">
    <div class="br-menu-item">
        <i class="menu-item-icon fa fa-angle-right tx-18"></i>
        <span class="menu-item-label">General</span>
    </div><!-- menu-item -->
</a><!-- br-menu-link -->
<a href="{{ route('system.member-meta.settings') }}" class="br-menu-link {{ in_array(Route::current()->getName(), ['system.member-meta.settings']) ? 'active' : '' }}">
    <div class="br-menu-item">
        <i class="menu-item-icon fa fa-angle-right tx-18"></i>
        <span class="menu-item-label">Member Meta-Data</span>
    </div><!-- menu-item -->
</a><!-- br-menu-link -->
<a href="{{ route('system.bonus.settings') }}" class="br-menu-link {{ in_array(Route::current()->getName(), ['system.bonus.settings']) ? 'active' : '' }}">
    <div class="br-menu-item">
        <i class="menu-item-icon fa fa-angle-right tx-18"></i>
        <span class="menu-item-label">Bonuses &amp; Rewards</span>
    </div><!-- menu-item -->
</a><!-- br-menu-link -->
<a href="{{ route('system-bank.index') }}" class="br-menu-link {{ in_array(Route::current()->getName(), ['system-bank.index']) ? 'active' : '' }}">
    <div class="br-menu-item">
        <i class="menu-item-icon fa fa-angle-right tx-18"></i>
        <span class="menu-item-label">Master Bank</span>
    </div><!-- menu-item -->
</a><!-- br-menu-link -->
<a href="{{ route('system.cache.clear') }}" class="br-menu-link {{ in_array(Route::current()->getName(), ['system.cache.clear']) ? 'active' : '' }}">
    <div class="br-menu-item">
        <i class="menu-item-icon fa fa-angle-right tx-18"></i>
        <span class="menu-item-label">Clear Cache</span>
    </div><!-- menu-item -->
</a><!-- br-menu-link -->