<div class="dropdown">
    <a href="" class="nav-link pd-x-7 pos-relative" data-toggle="dropdown">
        <i class="icon ion-ios-bell-outline tx-24"></i>
    </a>
    <div class="dropdown-menu dropdown-menu-header wd-300 pd-0-force">
        <div class="d-flex align-items-center justify-content-between pd-y-10 pd-x-20 bd-b bd-gray-200">
            <label class="tx-12 tx-info tx-uppercase tx-semibold tx-spacing-2 mg-b-0">Notifications</label>
            <a href="" class="tx-11">Mark All as Read</a>
        </div><!-- d-flex -->


    </div><!-- dropdown-menu -->
</div><!-- dropdown -->