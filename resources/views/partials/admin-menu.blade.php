<a href="{{ route('home') }}" class="br-menu-link {{ in_array(Route::current()->getName(), ['home']) ? 'active' : '' }}">
    <div class="br-menu-item">
        <i class="menu-item-icon fa fa-tachometer tx-18"></i>
        <span class="menu-item-label">Admin Dashboard</span>
    </div><!-- menu-item -->
</a><!-- br-menu-link -->

{{-- Transaksi PIN --}}
<a href="#" class="br-menu-link {{ in_array(Route::current()->getName(), ['pin.index','pin.history']) ? 'active show-sub' : '' }}">
    <div class="br-menu-item">
        <i class="menu-item-icon fa fa-ticket tx-18"></i>
        <span class="menu-item-label">Transaksi PIN</span>
        <i class="menu-item-arrow fa fa-angle-down"></i>
    </div><!-- menu-item -->
</a><!-- br-menu-link -->
<ul class="br-menu-sub nav flex-column">
    <li class="nav-item"><a href="{{ route('pin.index') }}" class="nav-link {{ in_array(Route::current()->getName(), ['pin.index']) ? 'active' : '' }}">Order PIN</a></li>
    <li class="nav-item"><a href="{{ route('pin.history') }}" class="nav-link {{ in_array(Route::current()->getName(), ['pin.history']) ? 'active' : '' }}">History</a></li>
</ul>

{{-- Withdrawal --}}
<a href="#" class="br-menu-link {{ in_array(Route::current()->getName(), ['admin.withdrawal.index','admin.withdrawal.history']) ? 'active show-sub' : '' }}">
    <div class="br-menu-item">
        <i class="menu-item-icon fa fa-money tx-18"></i>
        <span class="menu-item-label">Withdrawal</span>
        <i class="menu-item-arrow fa fa-angle-down"></i>
    </div><!-- menu-item -->
</a><!-- br-menu-link -->
<ul class="br-menu-sub nav flex-column">
    <li class="nav-item"><a href="{{ route('admin.withdrawal.index') }}" class="nav-link {{ in_array(Route::current()->getName(), ['admin.withdrawal.index']) ? 'active' : '' }}">Belum Transfer</a></li>
    <li class="nav-item"><a href="{{ route('admin.withdrawal.history') }}" class="nav-link {{ in_array(Route::current()->getName(), ['admin.withdrawal.history']) ? 'active' : '' }}">Sudah Transfer</a></li>
</ul>

{{-- Reward --}}
<a href="#" class="br-menu-link {{ in_array(Route::current()->getName(), ['admin.member-reward.index','admin.member-reward.history']) ? 'active show-sub' : '' }}">
    <div class="br-menu-item">
        <i class="menu-item-icon fa fa-trophy tx-18"></i>
        <span class="menu-item-label">Klaim Reward</span>
        <i class="menu-item-arrow fa fa-angle-down"></i>
    </div><!-- menu-item -->
</a><!-- br-menu-link -->
<ul class="br-menu-sub nav flex-column">
    <li class="nav-item"><a href="{{ route('admin.member-reward.index') }}" class="nav-link {{ in_array(Route::current()->getName(), ['admin.member-reward.index']) ? 'active' : '' }}">Belum Diproses</a></li>
    <li class="nav-item"><a href="{{ route('admin.member-reward.history') }}" class="nav-link {{ in_array(Route::current()->getName(), ['admin.member-reward.history']) ? 'active' : '' }}">Selesai Diproses</a></li>
</ul>

{{-- Reward --}}
<a href="#" class="br-menu-link {{ in_array(Route::current()->getName(), ['admin.request-stockist.index','admin.request-stockist.history']) ? 'active show-sub' : '' }}">
    <div class="br-menu-item">
        <i class="menu-item-icon fa fa-user-secret tx-18"></i>
        <span class="menu-item-label">Request Stockist</span>
        <i class="menu-item-arrow fa fa-angle-down"></i>
    </div><!-- menu-item -->
</a><!-- br-menu-link -->
<ul class="br-menu-sub nav flex-column">
    <li class="nav-item"><a href="{{ route('admin.request-stockist.index') }}" class="nav-link {{ in_array(Route::current()->getName(), ['admin.request-stockist.index']) ? 'active' : '' }}">Belum Diproses</a></li>
    <li class="nav-item"><a href="{{ route('admin.request-stockist.history') }}" class="nav-link {{ in_array(Route::current()->getName(), ['admin.request-stockist.history']) ? 'active' : '' }}">Selesai Diproses</a></li>
</ul>

<label class="sidebar-label pd-x-15 mg-t-20">&nbsp;Settings</label>
<a href="{{ route('plan-settings.index') }}" class="br-menu-link {{ in_array(Route::current()->getName(), ['plan-settings.index','plan-settings.edit','plan-settings.create']) ? 'active' : '' }}">
    <div class="br-menu-item">
        <i class="menu-item-icon fa fa-angle-right tx-18"></i>
        <span class="menu-item-label">Plan</span>
    </div><!-- menu-item -->
</a><!-- br-menu-link -->
<a href="{{ route('bonus-settings.index') }}" class="br-menu-link {{ in_array(Route::current()->getName(), ['bonus-settings.index','bonus-settings.create','bonus-settings.edit','bonus-settings.show']) ? 'active' : '' }}">
    <div class="br-menu-item">
        <i class="menu-item-icon fa fa-angle-right tx-18"></i>
        <span class="menu-item-label">Bonus</span>
    </div><!-- menu-item -->
</a><!-- br-menu-link -->
@system('rewards')
<a href="#" class="br-menu-link {{ in_array(Route::current()->getName(), ['reward-settings.index', 'reward-settings.create', 'reward-settings.edit', 'rewards.index', 'rewards.create', 'rewards.edit']) ? 'active show-sub' : '' }}">
    <div class="br-menu-item">
        <i class="menu-item-icon fa fa-angle-right tx-18"></i>
        <span class="menu-item-label">Reward</span>
        <i class="menu-item-arrow fa fa-angle-down"></i>
    </div><!-- menu-item -->
</a><!-- br-menu-link -->
<ul class="br-menu-sub nav flex-column">
    <li class="nav-item"><a href="{{ route('reward-settings.index') }}" class="nav-link {{ in_array(Route::current()->getName(), ['reward-settings.index', 'reward-settings.create', 'reward-settings.edit']) ? 'active' : '' }}">Reward Setting</a></li>
    <li class="nav-item"><a href="{{ route('rewards.index') }}" class="nav-link {{ in_array(Route::current()->getName(), ['rewards.index', 'rewards.create', 'rewards.edit']) ? 'active' : '' }}">Rewards</a></li>
</ul>
@endsystem
<a href="{{ route('pin-settings.index') }}" class="br-menu-link {{ in_array(Route::current()->getName(), ['pin-settings.index','pin.generate']) ? 'active' : '' }}">
    <div class="br-menu-item">
        <i class="menu-item-icon fa fa-angle-right tx-18"></i>
        <span class="menu-item-label">PIN</span>
    </div><!-- menu-item -->
</a><!-- br-menu-link -->
<a href="{{ route('stockist-settings.index') }}" class="br-menu-link {{ in_array(Route::current()->getName(), ['stockist-settings.index','stockist-settings.create','stockist-settings.edit','stockist-settings.show']) ? 'active' : '' }}">
    <div class="br-menu-item">
        <i class="menu-item-icon fa fa-angle-right tx-18"></i>
        <span class="menu-item-label">Stockist</span>
    </div><!-- menu-item -->
</a><!-- br-menu-link -->
<a href="{{ route('withdrawal-settings.index') }}" class="br-menu-link {{ in_array(Route::current()->getName(), ['withdrawal-settings.index','withdrawal-settings.create','withdrawal-settings.edit']) ? 'active' : '' }}">
    <div class="br-menu-item">
        <i class="menu-item-icon fa fa-angle-right tx-18"></i>
        <span class="menu-item-label">Withdrawal</span>
    </div><!-- menu-item -->
</a><!-- br-menu-link -->
<a href="{{ route('company-bank.index') }}" class="br-menu-link {{ in_array(Route::current()->getName(), ['company-bank.index','company-bank.create','company-bank.edit','stockist-settings.show']) ? 'active' : '' }}">
    <div class="br-menu-item">
        <i class="menu-item-icon fa fa-angle-right tx-18"></i>
        <span class="menu-item-label">Bank Perusahaan</span>
    </div><!-- menu-item -->
</a><!-- br-menu-link -->
