<div class="dropdown">
    <a href="" class="nav-link nav-link-profile" data-toggle="dropdown">
        <span class="logged-name hidden-md-down">{{ Auth::user()->name }}</span>
        <img src="{{ asset('img/user.png') }}" class="wd-32 rounded-circle" alt="">
        <span class="square-10 bg-success"></span>
    </a>
    <div class="dropdown-menu dropdown-menu-header wd-200">
        <ul class="list-unstyled user-profile-nav">
            <li><a href="#"><i class="icon ion-ios-person"></i> My Profile</a></li>
            <li><a href="{{ route('logout') }}" onclick="event.preventDefault();
                             document.getElementById('logout-form').submit();">
                    <i class="icon ion-power"></i> Sign Out</a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </li>
        </ul>
    </div><!-- dropdown-menu -->
</div><!-- dropdown -->
