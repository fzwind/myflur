@extends('layouts.admin')

@section('styles')
    <link href="{{ asset('admin/lib/select2/css/select2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('admin/lib/jquery-toggles/toggles-full.css') }}" rel="stylesheet">
@endsection

@section('content')
    <div class="br-pageheader pd-y-15 pd-l-20">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
            <a class="breadcrumb-item" href="{{ route('home') }}">myflur</a>
            <a class="breadcrumb-item" href="#">admin</a>
            <a class="breadcrumb-item" href="#">settings</a>
            <a class="breadcrumb-item" href="{{ route('plan-settings.index') }}">plan setting</a>
            <span class="breadcrumb-item active">tambah plan baru</span>
        </nav>
    </div><!-- br-pageheader -->
    <div class="pd-x-20 pd-sm-x-30 pd-t-20 pd-sm-t-30">
        <div class="pull-left">
            <h4 class="tx-gray-800 mg-b-5">PLAN SETTINGS</h4>
            <p class="mg-b-0">Tambah Plan Baru</p>
        </div>
    </div>

    <div class="br-pagebody mg-t-70">
        <div class="br-section-wrapper pd-20">
            <form method="post" action="{{ route('plan-settings.store') }}" data-parsley-validate>
                @csrf
                <div class="form-layout form-layout-2">
                    <div class="row no-gutters">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="form-control-label"><h6>Nama Plan: <span class="tx-danger">*</span></h6></label>
                                <input class="form-control parsley-error" type="text" id="name" name="name" value="{{ old('name') }}" placeholder="Input Nama Plan">
                                @if($errors->has('name'))
                                    <ul class="parsley-errors-list filled">
                                        <li class="parsley-required">{{ $errors->first('name') }}</li>
                                    </ul>
                                @endif
                            </div>
                        </div><!-- col-12 -->
                        <div class="col-md-6 mg-t--1">
                            <div class="form-group">
                                <label class="form-control-label"><h6>Kode: <span class="tx-danger">*</span></h6></label>
                                <input class="form-control parsley-error" type="text" id="code" name="code" value="{{ old('code') }}" placeholder="Input Kode Plan">
                                @if($errors->has('code'))
                                    <ul class="parsley-errors-list filled">
                                        <li class="parsley-required">{{ $errors->first('code') }}</li>
                                    </ul>
                                @endif
                            </div>
                        </div><!-- col-6 -->
                        <div class="col-md-6 mg-t--1">
                            <div class="form-group mg-md-l--1">
                                <label class="form-control-label"><h6>Aktif: <span class="tx-danger">*</span></h6></label>
                                <div class="toggle-wrapper">
                                    <div class="toggle toggle-light success" id="toggle-aktif"></div>
                                    <input type="hidden" name="is_active" value="0">
                                    <input type="checkbox" id="is_active" name="is_active" value="1" style="display: none">
                                </div>
                                @if($errors->has('is_active'))
                                    <ul class="parsley-errors-list filled">
                                        <li class="parsley-required">{{ $errors->first('is_active') }}</li>
                                    </ul>
                                @endif
                            </div>
                        </div><!-- col-6 -->
                        <div class="col-md-4 mg-t--1">
                            <div class="form-group">
                                <label class="form-control-label"><h6>Harga: <span class="tx-danger">*</span></h6></label>
                                <input class="form-control parsley-error" type="text" id="price" name="price" value="{{ old('price') }}" placeholder="Input Harga Plan">
                                @if($errors->has('price'))
                                    <ul class="parsley-errors-list filled">
                                        <li class="parsley-required">{{ $errors->first('price') }}</li>
                                    </ul>
                                @endif
                            </div>
                        </div><!-- col-4 -->
                        <div class="col-md-4 mg-t--1">
                            <div class="form-group mg-md-l--1">
                                <label class="form-control-label"><h6>Harga Stockist: <span class="tx-danger">*</span></h6></label>
                                <input class="form-control parsley-error" type="text" id="stockist_price" name="stockist_price" value="{{ old('stockist_price') }}" placeholder="Input Harga Stockist">
                                @if($errors->has('stockist_price'))
                                    <ul class="parsley-errors-list filled">
                                        <li class="parsley-required">{{ $errors->first('stockist_price') }}</li>
                                    </ul>
                                @endif
                            </div>
                        </div><!-- col-4 -->
                        <div class="col-md-4 mg-t--1">
                            <div class="form-group mg-md-l--1">
                                <label class="form-control-label"><h6>Harga Master Stockist: <span class="tx-danger">*</span></h6></label>
                                <input class="form-control parsley-error" type="text" id="master_stockist_price" name="master_stockist_price" value="{{ old('master_stockist_price') }}" placeholder="Input Harga Master Stockist">
                                @if($errors->has('master_stockist_price'))
                                    <ul class="parsley-errors-list filled">
                                        <li class="parsley-required">{{ $errors->first('master_stockist_price') }}</li>
                                    </ul>
                                @endif
                            </div>
                        </div><!-- col-4 -->
                    </div><!-- row -->
                    <div class="form-layout-footer bd pd-20 bd-t-0">
                        <button type="submit" class="btn btn-info">Save</button>
                        <a class="btn btn-secondary" href="{{ route('plan-settings.index') }}">Cancel</a>
                        <button type="button" class="btn btn-outline-primary btn-icon rounded-circle tx-34-force pd-0-force bd-0 pull-right"
                                data-container="body"
                                data-toggle="popover"
                                data-popover-color="default"
                                data-placement="top"
                                data-html="true"
                                title="Help"
                                data-content="Under Construction"><div><i class="fa fa-question-circle-o"></i></div></button>
                    </div><!-- form-group -->
                </div><!-- form-layout -->
            </form>
        </div><!-- section-wrapper -->
    </div><!-- br-pagebody -->
@endsection

@section('scripts')
    <script src="{{ asset('admin/lib/select2/js/select2.min.js') }}"></script>
    <script src="{{ asset('admin/lib/jquery-toggles/toggles.min.js') }}"></script>
@endsection

@section('page_script')
    <script>
        $(function(){
            'use strict'

            $('.form-layout .form-control').on('focusin', function(){
                $(this).closest('.form-group').addClass('form-group-active');
            });

            $('.form-layout .form-control').on('focusout', function(){
                $(this).closest('.form-group').removeClass('form-group-active');
            });

            $('.select2').on('select2:opening', function (e) {
                $(this).closest('.form-group').addClass('form-group-active');
            });

            $('.select2').on('select2:closing', function (e) {
                $(this).closest('.form-group').removeClass('form-group-active');
            });

            // Toggles
            $('#toggle-aktif').toggles({
                on: @if(old('is_active') == 1)
                    true
                @else
                    false
                @endif,
                height: 26,
                checkbox: $('#is_active'),
                text: {on:'AKTIF', off:'TIDAK'}
            });

        });
    </script>
@endsection