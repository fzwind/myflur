@extends('layouts.admin')

@section('styles')
    <link href="{{ asset('admin/lib/datatables/jquery.dataTables.css') }}" rel="stylesheet">
    <link href="{{ asset('admin/lib/select2/css/select2.min.css') }}" rel="stylesheet">
@endsection

@section('content')
    <div class="br-pageheader pd-y-15 pd-l-20">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
            <a class="breadcrumb-item" href="#">myflur</a>
            <a class="breadcrumb-item" href="#">admin</a>
            <a class="breadcrumb-item" href="#">settings</a>
            <span class="breadcrumb-item active">plan setting</span>
        </nav>
    </div><!-- br-pageheader -->
    <div class="pd-x-20 pd-sm-x-30 pd-t-20 pd-sm-t-30">
        <div class="pull-left">
            <h4 class="tx-gray-800 mg-b-5">Plan</h4>
            <p class="mg-b-0">Daftar Plan.</p>
        </div>
{{--
        @if(Auth::User()->hasPermissionTo('Add New Plan'))
--}}
            <div class="pull-right">
                <a href="{{ route('plan-settings.create') }}" class="btn btn-primary btn-with-icon mg-b-auto">
                    <div class="ht-40 justify-content-between">
                        <span class="pd-x-15">Tambah Plan Baru</span>
                        <span class="icon wd-40"><i class="fa fa-plus"></i></span>
                    </div>
                </a>
            </div>
{{--
        @endif
--}}
    </div>
    <div class="br-pagebody mg-t-70">
        <div class="br-section-wrapper pd-20">
            <div class="table-wrapper">
                <table id="plan-setting-data" class="table display responsive nowrap">
                    <thead>
                    <tr>
                        <th class="wd-10p text-center" rowspan="2">Kode</th>
                        <th class="wd-15p text-center" rowspan="2">Nama Plan</th>
                        <th class="wd-45p text-center" colspan="3">Harga</th>
                        <th class="wd-15p text-center" rowspan="2">Tgl. Aktif</th>
                        <th class="wd-10p text-center" rowspan="2">Status</th>
                        <th rowspan="2"></th>
                    </tr>
                    <tr>
                        <th class="wd-15p text-center">Member</th>
                        <th class="wd-15p text-center">Stockist</th>
                        <th class="wd-15p text-center">Master Stockist</th>
                    </tr>
                    </thead>
                </table>
            </div><!-- table-wrapper -->
        </div><!-- section-wrapper -->
    </div><!-- br-pagebody -->
@endsection

@section('scripts')
    <script src="{{ asset('admin/lib/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('admin/lib/datatables-responsive/dataTables.responsive.js') }}"></script>
    <script src="{{ asset('admin/lib/select2/js/select2.min.js') }}"></script>
@endsection

@section('page_script')
    <script>
        $(function(){
            'use strict';

            $('#plan-setting-data').DataTable({
                responsive: true,
                language: {
                    searchPlaceholder: 'Cari...',
                    sSearch: '',
                    lengthMenu: '_MENU_ data/halaman',
                    zeroRecords: 'Oyoyoyoy.... Tidak ada data yang dapat ditampilkan.',
                    info: 'Menampilkan halaman _PAGE_ dari _PAGES_ (Total Data : _MAX_)',
                    infoEmpty: '',
                    infoFiltered: "(disaring dari total _MAX_ data)"
                },
                processing: true,
                serverSide: true,
                ajax: '{{ route('plan.setting.data') }}',
                columns: [
                    {data: 'code', name: 'code'},
                    {data: 'name', name: 'name'},
                    {data: 'price', name: 'price', orderable: false, searchable: false},
                    {data: 'stockist_price', name: 'stockist_price', orderable: false, searchable: false},
                    {data: 'master_stockist_price', name: 'master_stockist_price', orderable: false, searchable: false},
                    {data: 'active_at', name: 'active_at', orderable: false, searchable: false},
                    {data: 'is_active', name: 'is_active',
                        render: function(data, type, row, meta){
                            if(type === 'display'){
                                if(data == 1){
                                    data = '<h3 class="valign-middle text-center text-success"><i class="icon ion-ios-checkmark-outline"></i></h3>';
                                } else {
                                    data = '<h3 class="valign-middle text-center text-danger"><i class="icon ion-ios-close-outline"></i></h3>';
                                }
                            }
                            return data;
                        }
                    },
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ]
            });

            // Select2
            $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });

        });

        function confirmSubmit(form) {
            return confirm('Yakin akan menghapus data PLAN '+ form[2].value +' ('+ form[1].value +') ?');
        }
    </script>

    @if (session('success'))
        <div id="modalsukses" class="modal fade">
            <div class="modal-dialog" role="document">
                <div class="modal-content tx-size-sm">
                    <div class="modal-body tx-center pd-y-20 pd-x-20">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <i class="icon ion-ios-checkmark-outline tx-100 tx-success lh-1 mg-t-20 d-inline-block"></i>
                        <h4 class="tx-success tx-semibold mg-b-20">Sukses!</h4>
                        <p class="mg-b-20 mg-x-20">{{ session('success') }}</p>
                        <button type="button" class="btn btn-success tx-11 tx-uppercase pd-y-12 pd-x-25 tx-mont tx-medium mg-b-20" data-dismiss="modal" aria-label="Close"> OK</button>
                    </div><!-- modal-body -->
                </div><!-- modal-content -->
            </div><!-- modal-dialog -->
        </div><!-- modal -->
        <script>
            $('#modalsukses').modal('show');
        </script>
    @endif

    @if (session('error'))
        <div id="modalerrordelete" class="modal fade">
            <div class="modal-dialog" role="document">
                <div class="modal-content tx-size-sm">
                    <div class="modal-body tx-center pd-y-20 pd-x-20">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <i class="icon ion-ios-close-outline tx-100 tx-danger lh-1 mg-t-20 d-inline-block"></i>
                        <h4 class="tx-danger tx-semibold mg-b-20">Error!</h4>
                        <p class="mg-b-20 mg-x-20">{{ session('error') }}</p>
                        <button type="button" class="btn btn-danger tx-11 tx-uppercase pd-y-12 pd-x-25 tx-mont tx-medium mg-b-20" data-dismiss="modal" aria-label="Close"> OK</button>
                    </div><!-- modal-body -->
                </div><!-- modal-content -->
            </div><!-- modal-dialog -->
        </div><!-- modal -->
        <script>
            $('#modalerrordelete').modal('show');
        </script>
    @endif

@endsection