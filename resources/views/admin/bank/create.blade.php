@extends('layouts.admin')

@section('styles')
    <link href="{{ asset('admin/css/vendor/switchery.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <h4 class="page-title">Tambah Bank</h4>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <form method="post" action="{{ route('company-bank.store') }}">
                @csrf
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="bank_id" class="font-weight-bold font-15">Bank:</label>
                                    <div>
                                        <select id="bank_id" name="bank_id" class="form-control select2" data-toggle="select2">
                                            <option value="" selected disabled>--- Pilih Bank ---</option>
                                            @foreach($banks as $key => $bank)
                                                @if(old('plan_id')==$key)
                                                    <option value="{{ $key }}" selected>{{ $bank }}</option>
                                                @else
                                                    <option value="{{ $key }}">{{ $bank }}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                        @if($errors->has('bank_id'))
                                            <small class="form-text text-danger">{{ $errors->first('bank_id') }}</small>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="account_number" class="font-weight-bold font-15">Account No.:</label>
                                    <div>
                                        <input class="form-control" type="text" id="account_number" name="account_number" value="{{ old('code') }}" placeholder="Input Nomor Rekening">
                                        @if($errors->has('account_number'))
                                            <small class="form-text text-danger">{{ $errors->first('account_number') }}</small>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="account_name" class="font-weight-bold font-15">Account Name:</label>
                                    <div>
                                        <input class="form-control" type="text" id="account_name" name="account_name" value="{{ old('account_name') }}" placeholder="Input Rekening Atas Nama">
                                        @if($errors->has('account_name'))
                                            <small class="form-text text-danger">{{ $errors->first('account_name') }}</small>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="is_active" class="font-weight-bold font-15">Active:</label>
                                    <div>
                                        <input type="hidden" name="is_active" value="0">
                                        <input type="checkbox" {{ (old('is_active') == 1) ? 'checked' : '' }} id="is_active" name="is_active" value="1" data-plugin="switchery" data-color="#3db9dc"/>
                                        @if($errors->has('is_active'))
                                            <small class="form-text text-danger">{{ $errors->first('is_active') }}</small>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-info">Save</button>
                        <a class="btn btn-secondary" href="{{ route('company-bank.index') }}">Cancel</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('admin/js/vendor/switchery.min.js') }}"></script>
@endsection

@section('page_script')
    <script>
        !function($) {
            "use strict";

            var Components = function() {};

            //switch
            Components.prototype.initSwitchery = function() {
                $('[data-plugin="switchery"]').each(function (idx, obj) {
                    new Switchery($(this)[0], $(this).data());
                });
            },

                //initilizing
                Components.prototype.init = function() {
                    var $this = this;
                    this.initSwitchery()
                },

                $.Components = new Components, $.Components.Constructor = Components

        }(window.jQuery),

            //initializing main application module
            function($) {
                "use strict";
                $.Components.init();
            }(window.jQuery);
    </script>
@endsection