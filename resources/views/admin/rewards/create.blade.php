@extends('layouts.admin')

@section('styles')
    <link href="{{ asset('admin/css/vendor/switchery.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <h4 class="page-title">Tambah Reward</h4>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <form method="post" action="{{ route('rewards.store') }}">
                @csrf
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="reward_setting_id" class="font-weight-bold font-15">Reward Setting:</label>
                                    <select id="reward_setting_id" name="reward_setting_id" class="form-control select2" data-toggle="select2">
                                        <option value="" selected disabled>--- Pilih Setting ---</option>
                                        @foreach($reward_settings as $key => $reward_setting)
                                            @if(old('reward_setting_id')==$key)
                                                <option value="{{ $key }}" selected>{{ $reward_setting }}</option>
                                            @else
                                                <option value="{{ $key }}">{{ $reward_setting }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                    @if($errors->has('reward_setting_id'))
                                        <small class="form-text text-danger">{{ $errors->first('reward_setting_id') }}</small>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="reward_type" class="font-weight-bold font-15">Bentuk Reward:</label>
                                    <select id="reward_type" name="reward_type" class="form-control select2" data-toggle="select2">
                                        <option value="" selected disabled>--- Pilih Bentuk Reward ---</option>
                                        <option value="1" {{ (old('reward_type') == 1) ? 'selected' : '' }}>Tunai</option>
                                        <option value="2" {{ (old('reward_type') == 2) ? 'selected' : '' }}>Non-Tunai</option>
                                    </select>
                                    @if($errors->has('reward_type'))
                                        <small class="form-text text-danger">{{ $errors->first('reward_type') }}</small>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-2" id="claim_as_value">
                                <div class="form-group">
                                    <label for="can_claim_as_value" class="font-weight-bold font-15">Dapat Di-uangkan:</label>
                                    <div>
                                        <input type="hidden" name="can_claim_as_value" value="0">
                                        <input type="checkbox" {{ (old('can_claim_as_value') == 1) ? 'checked' : '' }} id="can_claim_as_value" name="can_claim_as_value" value="1" data-plugin="switchery" data-color="#3db9dc"/>
                                        @if($errors->has('can_claim_as_value'))
                                            <small class="form-text text-danger">{{ $errors->first('can_claim_as_value') }}</small>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="left" class="font-weight-bold font-15">Target Kiri:</label>
                                    <div>
                                        <input class="form-control" type="text" id="left" name="left" value="{{ old('left') }}" placeholder="Input target kiri">
                                        @if($errors->has('left'))
                                            <small class="form-text text-danger">{{ $errors->first('left') }}</small>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="right" class="font-weight-bold font-15">Target Kanan:</label>
                                    <div>
                                        <input class="form-control" type="text" id="right" name="right" value="{{ old('right') }}" placeholder="Input target kanan">
                                        @if($errors->has('right'))
                                            <small class="form-text text-danger">{{ $errors->first('right') }}</small>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="reward" class="font-weight-bold font-15">Reward:</label>
                                    <div>
                                        <input class="form-control" type="text" id="reward" name="reward" value="{{ old('reward') }}" placeholder="Input reward">
                                        @if($errors->has('reward'))
                                            <small class="form-text text-danger">{{ $errors->first('reward') }}</small>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="reward_value" class="font-weight-bold font-15">Nilai Reward (Rp):</label>
                                    <div>
                                        <input class="form-control" type="text" id="reward_value" name="reward_value" value="{{ old('reward_value') }}" placeholder="Input nilai reward">
                                        @if($errors->has('reward_value'))
                                            <small class="form-text text-danger">{{ $errors->first('reward_value') }}</small>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="reward_deduction_type" class="font-weight-bold font-15">Potongan Saat Claim:</label>
                                    <select id="reward_deduction_type" name="reward_deduction_type" class="form-control select2" data-toggle="select2">
                                        <option value="" selected disabled>--- Pilih Bentuk Reward ---</option>
                                        <option value="0" {{ (old('reward_deduction_type') == 0) ? 'selected' : '' }}>Tidak Ada Potongan</option>
                                        <option value="1" {{ (old('reward_deduction_type') == 1) ? 'selected' : '' }}>Persentase</option>
                                        <option value="2" {{ (old('reward_deduction_type') == 2) ? 'selected' : '' }}>Nominal</option>
                                    </select>
                                    @if($errors->has('reward_deduction_type'))
                                        <small class="form-text text-danger">{{ $errors->first('reward_deduction_type') }}</small>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="reward_deduction_on_claim" class="font-weight-bold font-15">Potongan:</label>
                                    <div class="input-group mb-2">
                                        <div class="input-group-prepend" id="deduction_rp" style="display: none">
                                            <div class="input-group-text">Rp</div>
                                        </div>
                                        <input class="form-control" type="text" id="reward_deduction_on_claim" name="reward_deduction_on_claim" value="{{ old('reward_deduction_on_claim') }}" placeholder="Input besaran potongan">
                                        <div class="input-group-append" id="deduction_prctg" style="display: none">
                                            <div class="input-group-text">%</div>
                                        </div>
                                        @if($errors->has('reward_deduction_on_claim'))
                                            <small class="form-text text-danger">{{ $errors->first('reward_deduction_on_claim') }}</small>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="active_at" class="font-weight-bold font-15">Tanggal Aktif:</label>
                                    <input class="form-control date" type="text" id="active_at" name="active_at" value="{{ old('active_at') }}" placeholder="Input Tanggal Aktif" data-toggle="date-picker" data-single-date-picker="true">
                                    @if($errors->has('active_at'))
                                        <small class="form-text text-danger">{{ $errors->first('active_at') }}</small>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="inactive_at" class="font-weight-bold font-15">Tanggal Non-Aktif:</label>
                                    <input class="form-control date" type="text" id="inactive_at" name="inactive_at" value="{{ old('inactive_at') }}" placeholder="Input Tanggal Non-Aktif" data-toggle="date-picker" data-single-date-picker="true">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="inactive_at_null" onclick="setNull()">
                                        <label class="custom-control-label" for="inactive_at_null">Null</label>
                                    </div>
                                    @if($errors->has('inactive_at'))
                                        <small class="form-text text-danger">{{ $errors->first('inactive_at') }}</small>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-info">Save</button>
                        <a class="btn btn-secondary" href="{{ route('reward-settings.index') }}">Cancel</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('admin/js/vendor/switchery.min.js') }}"></script>
@endsection

@section('page_script')
    <script>
        !function($) {
            "use strict";

            var Components = function() {};

            //switch
            Components.prototype.initSwitchery = function() {
                $('[data-plugin="switchery"]').each(function (idx, obj) {
                    new Switchery($(this)[0], $(this).data());
                });
            },

                //initilizing
                Components.prototype.init = function() {
                    var $this = this;
                    this.initSwitchery()
                },

                $.Components = new Components, $.Components.Constructor = Components

        }(window.jQuery),

        //initializing main application module
        function($) {
            "use strict";
            $.Components.init();
        }(window.jQuery);

        function setNull() {
            if ($("#inactive_at_null").prop("checked", true)) {
                $('#inactive_at').val(null);
            }
        }

        $('#inactive_at').on('change', function () {
            $("#inactive_at_null").prop("checked", false);
        });

        $('#reward_deduction_type').on('select2:select', function (e) {
            var data = e.params.data;
            console.log(data.id);
            if (data.id == 0) {
                $('#deduction_rp').hide();
                $('#deduction_prctg').hide();
            } else if (data.id == 1) {
                $('#deduction_rp').hide();
                $('#deduction_prctg').show();
            } else {
                $('#deduction_rp').show();
                $('#deduction_prctg').hide();
            }
        });
    </script>
@endsection