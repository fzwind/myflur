@extends('layouts.admin')

@section('styles')
    <link href="{{ asset('admin/css/vendor/switchery.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <h4 class="page-title">Tambah Stockist Setting</h4>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <form method="post" action="{{ route('withdrawal-settings.store') }}">
                @csrf
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="active_at" class="font-weight-bold font-15">Tanggal Aktif:</label>
                                    <input class="form-control date" type="text" id="active_at" name="active_at" value="{{ old('active_at') }}" placeholder="Input Tanggal Aktif" data-toggle="date-picker" data-single-date-picker="true">
                                    @if($errors->has('active_at'))
                                        <small class="form-text text-danger">{{ $errors->first('active_at') }}</small>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="wd_admin" class="font-weight-bold font-15">Biaya Admin:</label>
                                    <input class="form-control" type="text" id="wd_admin" name="wd_admin" value="{{ old('wd_admin') }}" placeholder="Input biaya admin WD">
                                    @if($errors->has('wd_admin'))
                                        <small class="form-text text-danger">{{ $errors->first('wd_admin') }}</small>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="wd_index_deduction" class="font-weight-bold font-15">Index:</label>
                                    <div class="input-group mb-2">
                                        <input class="form-control" type="text" id="wd_index_deduction" name="wd_index_deduction" value="{{ old('wd_index_deduction') }}" placeholder="Input besaran index">
                                        <div class="input-group-append">
                                            <div class="input-group-text">%</div>
                                        </div>
                                        @if($errors->has('wd_index_deduction'))
                                            <small class="form-text text-danger">{{ $errors->first('wd_index_deduction') }}</small>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="wd_mode" class="font-weight-bold font-15">Mode WD:</label>
                                    <select id="wd_mode" name="wd_mode" class="form-control select2" data-toggle="select2">
                                        <option value="" selected disabled>--- mode WD ---</option>
                                        <option value="1" {{ (old('wd_mode') == 1) ? 'selected' : '' }}>Auto</option>
                                        <option value="2" {{ (old('wd_mode') == 2) ? 'selected' : '' }}>Manual</option>
                                    </select>
                                    @if($errors->has('wd_mode'))
                                        <small class="form-text text-danger">{{ $errors->first('wd_mode') }}</small>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="wd_days" class="font-weight-bold font-15">Hari WD:</label>
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="wd_days_1" name="wd_days[]" value="1">
                                        <label class="custom-control-label" for="wd_days_1">Senin</label>
                                    </div>
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="wd_days_2" name="wd_days[]" value="2">
                                        <label class="custom-control-label" for="wd_days_2">Selasa</label>
                                    </div>
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="wd_days_3" name="wd_days[]" value="3">
                                        <label class="custom-control-label" for="wd_days_3">Rabu</label>
                                    </div>
                                </div>
                                @if($errors->has('wd_days'))
                                    <small class="form-text text-danger">{{ $errors->first('wd_days') }}</small>
                                @endif
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="wd_days" class="font-weight-bold font-15">&nbsp;</label>
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="wd_days_4" name="wd_days[]" value="4">
                                        <label class="custom-control-label" for="wd_days_4">Kamis</label>
                                    </div>
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="wd_days_5" name="wd_days[]" value="5">
                                        <label class="custom-control-label" for="wd_days_5">Jumat</label>
                                    </div>
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="wd_days_6" name="wd_days[]" value="6">
                                        <label class="custom-control-label" for="wd_days_6">Sabtu</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <label for="wd_days" class="font-weight-bold font-15">&nbsp;</label>
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="wd_days_7" name="wd_days[]" value="7">
                                    <label class="custom-control-label" for="wd_days_7">Minggu</label>
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-info">Save</button>
                        <a class="btn btn-secondary" href="{{ route('withdrawal-settings.index') }}">Cancel</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('admin/js/vendor/switchery.min.js') }}"></script>
@endsection

@section('page_script')
    <script>
        !function($) {
            "use strict";

            var Components = function() {};

            //switch
            Components.prototype.initSwitchery = function() {
                $('[data-plugin="switchery"]').each(function (idx, obj) {
                    new Switchery($(this)[0], $(this).data());
                });
            },

                //initilizing
                Components.prototype.init = function() {
                    var $this = this;
                    this.initSwitchery()
                },

                $.Components = new Components, $.Components.Constructor = Components

        }(window.jQuery),

            //initializing main application module
            function($) {
                "use strict";
                $.Components.init();
            }(window.jQuery);
    </script>
@endsection