@extends('layouts.admin')

@section('styles')
    <link href="{{ asset('admin/lib/select2/css/select2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('admin/lib/datatables/jquery.dataTables.css') }}" rel="stylesheet">
    <link href="{{ asset('admin/lib/datatables-responsive/css/responsive.dataTables.css') }}" rel="stylesheet">
@endsection

@section('content')
    <div class="br-pageheader pd-y-15 pd-l-20">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
            <a class="breadcrumb-item" href="#">myflur</a>
            <a class="breadcrumb-item" href="#">admin</a>
            <a class="breadcrumb-item" href="#">settings</a>
            <span class="breadcrumb-item active">pin setting</span>
        </nav>
    </div><!-- br-pageheader -->
    <div class="pd-x-20 pd-sm-x-30 pd-t-20 pd-sm-t-30">
        <div class="pull-left">
            <h4 class="tx-gray-800 mg-b-5">Order PIN</h4>
            <p class="mg-b-0">Order pembelian PIN oleh stockist.</p>
        </div>
    </div>
    <div class="br-pagebody mg-t-70">
        <div class="br-section-wrapper pd-20">
            <div class="table-wrapper">
                <table id="plan-setting-data" class="table display responsive nowrap">
                    <thead>
                    <tr>
                        <th>Kode Transaksi</th>
                        <th>Transaksi</th>
                        <th>Jumlah</th>
                        <th></th>
                        <th>Status</th>
                        <th></th>
                        <th></th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

    <div class="modal fade" id="invoice" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content" id="printSection">
                <div class="modal-header">
                    <h3 class="modal-title" id="exampleModalLabel">Invoice</h3>
                </div>
                <div class="modal-body">
                </div>
                <div class="modal-footer">
                    <div class="text-right d-print-none">
                        <a href="javascript:window.print()" id="btnPrint" class="btn btn-primary waves-effect waves-light"><i class="mdi mdi-printer mr-1"></i> Print</a>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="image" tabindex="-2" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title" id="imageModalLabel">Bukti Transfer</h3>
                </div>
                <div class="modal-body">
                </div>
                <div class="modal-footer">
                    <div class="text-right">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="reject" tabindex="-2" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <form method="post" class="form-layout form-layout-2" action="{{ route('pin.reject') }}">
                @csrf
                <input type="hidden" id="reject_id" name="id" value="">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3 class="modal-title" id="imageModalLabel">Tolak Transaksi</h3>
                    </div>
                    <div class="modal-body">
                        <div class="row no-gutters">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="form-control-label"><h6>Alasan penolakan: <span class="tx-danger">*</span></h6></label>
                                    <textarea id="status_note" name="status_note" class="form-control parsley-error" cols="40" rows="5"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="text-right">
                            <button type="submit" class="btn btn-info">Save</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

@endsection

@section('scripts')
    <script src="{{ asset('admin/lib/select2/js/select2.min.js') }}"></script>
    <script src="{{ asset('admin/lib/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('admin/lib/datatables-responsive/dataTables.responsive.js') }}"></script>
@endsection

@section('page_script')
    <script>
        $('#invoice').on('show.bs.modal', function (e) {
            e.stopImmediatePropagation();

            var button = $(e.relatedTarget);
            var modal = $(this);

            // console.log(button.attr('href'));
            //modal.find('.modal-body').load('aaa');
            $.ajax({
                type: "GET",
                url: button.attr('href'),
                cache: false,
                success: function (data) {
                    //console.log(data);
                    modal.find('.modal-body').html(data);
                },
                error: function (err) {
                    //console.log(err);
                }
            })
        });

        $('#image').on('show.bs.modal', function (e) {
            e.stopImmediatePropagation();

            var button = $(e.relatedTarget);
            var modal = $(this);

            $.ajax({
                type: "GET",
                url: button.attr('href'),
                contentType: false,
                cache: false,
                processData: false,
                success: function (data) {
                    modal.find('.modal-body').html(data);
                },
                error: function (err) {
                }
            })
        });

        $('#reject').on('show.bs.modal', function (e) {
            e.stopImmediatePropagation();

            var button = $(e.relatedTarget);
            $('#reject_id').val(button.attr('href'));
        });

        $(function(){
            'use strict';

            $('#plan-setting-data').DataTable({
                responsive: true,
                language: {
                    searchPlaceholder: 'Cari...',
                    sSearch: '',
                    lengthMenu: '_MENU_ data/halaman',
                    zeroRecords: ' Tidak ada data yang dapat ditampilkan.',
                    info: 'Menampilkan halaman _PAGE_ dari _PAGES_ (Total Data : _MAX_)',
                    infoEmpty: '',
                    infoFiltered: "(disaring dari total _MAX_ data)"
                },
                processing: true,
                serverSide: true,
                ajax: '{{ route('pin.data') }}',
                columns: [
                    {data: 'transaction_code', name: 'transaction_code'},
                    {data: 'transaction', name: 'transaction'},
                    {data: 'total_amount', name: 'total_amount'},
                    {data: 'amount', name: 'amount'},
                    {data: 'status', name: 'status'},
                    {data: 'status_date', name: 'status_date'},
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ],
                columnDefs: [
                    {
                        render: function (data, type, row) {
                            return data +' ('+ row.amount +')';
                        },
                        targets: 2
                    },
                    {
                        render: function (data, type, row) {
                            return data +' <pre class="text-muted">'+ row.status_date +'</pre>';
                        },
                        targets: 4
                    },
                    {visible: false, targets: [3,5]},
                    {className: 'mono', targets: [0]}
                ],
                order: [[5, 'desc']]
            });

            $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });

        });
    </script>

    @if (session('success'))
        <div id="modalsukses" class="modal fade">
            <div class="modal-dialog" role="document">
                <div class="modal-content tx-size-sm">
                    <div class="modal-body tx-center pd-y-20 pd-x-20">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <i class="icon ion-ios-checkmark-outline tx-100 tx-success lh-1 mg-t-20 d-inline-block"></i>
                        <h4 class="tx-success tx-semibold mg-b-20">Sukses!</h4>
                        <p class="mg-b-20 mg-x-20">{{ session('success') }}</p>
                        <button type="button" class="btn btn-success tx-11 tx-uppercase pd-y-12 pd-x-25 tx-mont tx-medium mg-b-20" data-dismiss="modal" aria-label="Close"> OK</button>
                    </div><!-- modal-body -->
                </div><!-- modal-content -->
            </div><!-- modal-dialog -->
        </div><!-- modal -->
        <script>
            $('#modalsukses').modal('show');
        </script>
    @endif

    @if (session('error'))
        <div id="modalerrordelete" class="modal fade">
            <div class="modal-dialog" role="document">
                <div class="modal-content tx-size-sm">
                    <div class="modal-body tx-center pd-y-20 pd-x-20">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <i class="icon ion-ios-close-outline tx-100 tx-danger lh-1 mg-t-20 d-inline-block"></i>
                        <h4 class="tx-danger tx-semibold mg-b-20">Error!</h4>
                        <p class="mg-b-20 mg-x-20">{{ session('error') }}</p>
                        <button type="button" class="btn btn-danger tx-11 tx-uppercase pd-y-12 pd-x-25 tx-mont tx-medium mg-b-20" data-dismiss="modal" aria-label="Close"> OK</button>
                    </div><!-- modal-body -->
                </div><!-- modal-content -->
            </div><!-- modal-dialog -->
        </div><!-- modal -->
        <script>
            $('#modalerrordelete').modal('show');
        </script>
    @endif

@endsection
