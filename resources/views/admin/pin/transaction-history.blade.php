@extends('layouts.admin')

@section('styles')
    <link href="{{ asset('admin/lib/select2/css/select2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('admin/lib/datatables/jquery.dataTables.css') }}" rel="stylesheet">
@endsection

@section('content')
    <div class="br-pageheader pd-y-15 pd-l-20">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
            <a class="breadcrumb-item" href="#">myflur</a>
            <a class="breadcrumb-item" href="#">admin</a>
            <a class="breadcrumb-item" href="#">settings</a>
            <span class="breadcrumb-item active">pin setting</span>
        </nav>
    </div><!-- br-pageheader -->
    <div class="pd-x-20 pd-sm-x-30 pd-t-20 pd-sm-t-30">
        <div class="pull-left">
            <h4 class="tx-gray-800 mg-b-5">History Order PIN</h4>
            <p class="mg-b-0">History order pembelian PIN oleh stockist.</p>
        </div>
    </div>
    <div class="br-pagebody mg-t-70">
        <div class="br-section-wrapper pd-20">
            <div class="table-wrapper">
                <table id="plan-setting-data" class="table display responsive nowrap">
                    <thead>
                    <tr>
                        <th>Kode Transaksi</th>
                        <th>Transaksi</th>
                        <th>Jumlah</th>
                        <th></th>
                        <th>Status</th>
                        <th></th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

    <div class="modal fade" id="invoice" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content" id="printSection">
                <div class="modal-header">
                    <h3 class="modal-title" id="exampleModalLabel">Invoice</h3>
                </div>
                <div class="modal-body">
                </div>
                <div class="modal-footer">
                    <div class="text-right d-print-none">
                        <a href="javascript:window.print()" id="btnPrint" class="btn btn-primary waves-effect waves-light"><i class="mdi mdi-printer mr-1"></i> Print</a>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="image" tabindex="-2" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title" id="imageModalLabel">Bukti Transfer</h3>
                </div>
                <div class="modal-body">
                </div>
                <div class="modal-footer">
                    <div class="text-right">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script src="{{ asset('admin/lib/select2/js/select2.min.js') }}"></script>
    <script src="{{ asset('admin/lib/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('admin/lib/datatables-responsive/dataTables.responsive.js') }}"></script>
@endsection

@section('page_script')
    <script>
        $('#invoice').on('show.bs.modal', function (e) {
            e.stopImmediatePropagation();

            var button = $(e.relatedTarget);
            var modal = $(this);

            // console.log(button.attr('href'));
            //modal.find('.modal-body').load('aaa');
            $.ajax({
                type: "GET",
                url: button.attr('href'),
                cache: false,
                success: function (data) {
                    //console.log(data);
                    modal.find('.modal-body').html(data);
                },
                error: function (err) {
                    //console.log(err);
                }
            })
        });

        $('#image').on('show.bs.modal', function (e) {
            e.stopImmediatePropagation();

            var button = $(e.relatedTarget);
            var modal = $(this);

            $.ajax({
                type: "GET",
                url: button.attr('href'),
                contentType: false,
                cache: false,
                processData: false,
                success: function (data) {
                    modal.find('.modal-body').html(data);
                },
                error: function (err) {
                }
            })
        });

        $(function(){
            'use strict';

            $('#plan-setting-data').DataTable({
                responsive: false,
                language: {
                    searchPlaceholder: 'Cari...',
                    sSearch: '',
                    lengthMenu: '_MENU_ data/halaman',
                    zeroRecords: ' Tidak ada data yang dapat ditampilkan.',
                    info: 'Menampilkan halaman _PAGE_ dari _PAGES_ (Total Data : _MAX_)',
                    infoEmpty: '',
                    infoFiltered: "(disaring dari total _MAX_ data)"
                },
                processing: true,
                serverSide: true,
                ajax: '{{ route('pin.history.data') }}',
                columns: [
                    {data: 'transaction_code', name: 'transaction_code'},
                    {data: 'transaction', name: 'transaction'},
                    {data: 'total_amount', name: 'total_amount'},
                    {data: 'amount', name: 'amount'},
                    {data: 'status', name: 'status'},
                    {data: 'status_date', name: 'status_date'},
                ],
                columnDefs: [
                    {
                        render: function (data, type, row) {
                            return data +' ('+ row.amount +')';
                        },
                        targets: 2
                    },
                    {
                        render: function (data, type, row) {
                            return data +' <pre class="text-muted">'+ row.status_date +'</pre>';
                        },
                        targets: 4
                    },
                    {visible: false, targets: [3,5]},
                    {className: 'mono', targets: [0]}
                ],
                order: [[5, 'desc']]
            });

            $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });

        });
    </script>
@endsection