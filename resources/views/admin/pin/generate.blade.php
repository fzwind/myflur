@extends('layouts.admin')

@section('styles')
    <link href="{{ asset('admin/lib/select2/css/select2.min.css') }}" rel="stylesheet">
@endsection

@section('content')
    <div class="br-pageheader pd-y-15 pd-l-20">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
            <a class="breadcrumb-item" href="{{ route('home') }}">myflur</a>
            <a class="breadcrumb-item" href="#">admin</a>
            <a class="breadcrumb-item" href="#">settings</a>
            <a class="breadcrumb-item" href="{{ route('pin-settings.index') }}">pin setting</a>
            <span class="breadcrumb-item active">generate pin baru</span>
        </nav>
    </div><!-- br-pageheader -->
    <div class="pd-x-20 pd-sm-x-30 pd-t-20 pd-sm-t-30">
        <div class="pull-left">
            <h4 class="tx-gray-800 mg-b-5">PIN SETTINGS</h4>
            <p class="mg-b-0">Generate Pin Baru</p>
        </div>
    </div>

    <div class="br-pagebody mg-t-70">
        <div class="br-section-wrapper pd-20">
            <form method="post" action="{{ route('pin.generate.post') }}" data-parsley-validate>
                @csrf
                <div class="form-layout form-layout-2">
                    <div class="row no-gutters">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="form-control-label"><h6>Plan: <span class="tx-danger">*</span></h6></label>
                                <select id="plan_id" name="plan_id" class="form-control select2-show-search" data-placeholder="--- Pilih PLAN ---">
                                    <option label="--- Pilih PLAN ---"></option>
                                    @foreach($plans as $key => $plan)
                                        @if(old('plan_id')==$key)
                                            <option value="{{ $key }}" selected>{{ $plan }}</option>
                                        @else
                                            <option value="{{ $key }}">{{ $plan }}</option>
                                        @endif
                                    @endforeach
                                </select>
                                @if($errors->has('plan_id'))
                                    <ul class="parsley-errors-list filled">
                                        <li class="parsley-required">{{ $errors->first('plan_id') }}</li>
                                    </ul>
                                @endif
                            </div>
                        </div><!-- col-4 -->
                        <div class="col-md-8">
                            <div class="form-group mg-l--1">
                                <label class="form-control-label"><h6>Jumlah Pin: <span class="tx-danger">*</span></h6></label>
                                <input class="form-control parsley-error" type="text" id="amount" name="amount" value="{{ old('amount') }}" placeholder="Input jumlah Pin yang akan digenerate">
                                @if($errors->has('amount'))
                                    <ul class="parsley-errors-list filled">
                                        <li class="parsley-required">{{ $errors->first('amount') }}</li>
                                    </ul>
                                @endif
                            </div>
                        </div>
                    </div><!-- row -->
                    <div class="form-layout-footer bd pd-20 bd-t-0">
                        <button type="submit" class="btn btn-info">Save</button>
                        <a class="btn btn-secondary" href="{{ route('pin-settings.index') }}">Cancel</a>
                        <button type="button" class="btn btn-outline-primary btn-icon rounded-circle tx-34-force pd-0-force bd-0 pull-right"
                                data-container="body"
                                data-toggle="popover"
                                data-popover-color="default"
                                data-placement="top"
                                data-html="true"
                                title="Help"
                                data-content="Under Construction"><div><i class="fa fa-question-circle-o"></i></div></button>
                    </div><!-- form-group -->
                </div><!-- form-layout -->
            </form>
        </div><!-- section-wrapper -->
    </div><!-- br-pagebody -->
@endsection

@section('scripts')
    <script src="{{ asset('admin/lib/select2/js/select2.min.js') }}"></script>
@endsection

@section('page_script')
    <script>
        $(function(){
            'use strict'

            $('.form-layout .form-control').on('focusin', function(){
                $(this).closest('.form-group').addClass('form-group-active');
            });

            $('.form-layout .form-control').on('focusout', function(){
                $(this).closest('.form-group').removeClass('form-group-active');
            });

            $('.select2').on('select2:opening', function (e) {
                $(this).closest('.form-group').addClass('form-group-active');
            });

            $('.select2').on('select2:closing', function (e) {
                $(this).closest('.form-group').removeClass('form-group-active');
            });

        });
    </script>
@endsection