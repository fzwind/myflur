@extends('layouts.admin')

@section('styles')
    <link href="{{ asset('admin/lib/select2/css/select2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('admin/lib/jquery-toggles/toggles-full.css') }}" rel="stylesheet">
@endsection

@section('content')
    <div class="br-pageheader pd-y-15 pd-l-20">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
            <a class="breadcrumb-item" href="{{ route('home') }}">myflur</a>
            <a class="breadcrumb-item" href="#">administrator</a>
            <a class="breadcrumb-item" href="#">system settings</a>
            <span class="breadcrumb-item active">member meta-data settings</span>
        </nav>
    </div><!-- br-pageheader -->
    <div class="pd-x-20 pd-sm-x-30 pd-t-20 pd-sm-t-30">
        <div class="pull-left">
            <h4 class="tx-gray-800 mg-b-5">MEMBER META-DATA SYSTEM SETTINGS</h4>
        </div>
    </div>

    <div class="br-pagebody mg-t-70">
        <div class="br-section-wrapper pd-20">
            <form method="post" action="{{ route('system.member-meta.settings.post') }}" data-parsley-validate>
                @csrf
                <div class="form-layout form-layout-2">
                    <div class="row no-gutters">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="form-control-label"><h6>Name: <span class="tx-danger">*</span></h6></label>
                                <div class="toggle-wrapper">
                                    <div class="toggle toggle-modern success" id="toggle-memdata_name"></div>
                                    <input type="hidden" name="memdata_name" value="0">
                                    <input type="checkbox" id="memdata_name" name="memdata_name" value="1" style="display: none">
                                </div>
                                @if($errors->has('memdata_name'))
                                    <ul class="parsley-errors-list filled">
                                        <li class="parsley-required">{{ $errors->first('memdata_name') }}</li>
                                    </ul>
                                @endif
                            </div>
                        </div><!-- col-4 -->
                        <div class="col-md-4">
                            <div class="form-group mg-md-l--1">
                                <label class="form-control-label"><h6>Email: <span class="tx-danger">*</span></h6></label>
                                <div class="toggle-wrapper">
                                    <div class="toggle toggle-modern success" id="toggle-memdata_email"></div>
                                    <input type="hidden" name="memdata_email" value="0">
                                    <input type="checkbox" id="memdata_email" name="memdata_email" value="1" style="display: none">
                                </div>
                                @if($errors->has('memdata_email'))
                                    <ul class="parsley-errors-list filled">
                                        <li class="parsley-required">{{ $errors->first('memdata_email') }}</li>
                                    </ul>
                                @endif
                            </div>
                        </div><!-- col-4 -->
                        <div class="col-md-4">
                            <div class="form-group mg-md-l--1">
                                <label class="form-control-label"><h6>Address: <span class="tx-danger">*</span></h6></label>
                                <div class="toggle-wrapper">
                                    <div class="toggle toggle-modern success" id="toggle-memdata_address"></div>
                                    <input type="hidden" name="memdata_address" value="0">
                                    <input type="checkbox" id="memdata_address" name="memdata_address" value="1" style="display: none">
                                </div>
                                @if($errors->has('memdata_address'))
                                    <ul class="parsley-errors-list filled">
                                        <li class="parsley-required">{{ $errors->first('memdata_address') }}</li>
                                    </ul>
                                @endif
                            </div>
                        </div><!-- col-4 -->
                        <div class="col-md-4 mg-t--1">
                            <div class="form-group mg-md-l--1">
                                <label class="form-control-label"><h6>Province: <span class="tx-danger">*</span></h6></label>
                                <div class="toggle-wrapper">
                                    <div class="toggle toggle-modern success" id="toggle-memdata_province"></div>
                                    <input type="hidden" name="memdata_province" value="0">
                                    <input type="checkbox" id="memdata_province" name="memdata_province" value="1" style="display: none">
                                </div>
                                @if($errors->has('memdata_province'))
                                    <ul class="parsley-errors-list filled">
                                        <li class="parsley-required">{{ $errors->first('memdata_province') }}</li>
                                    </ul>
                                @endif
                            </div>
                        </div><!-- col-4 -->
                        <div class="col-md-4 mg-t--1">
                            <div class="form-group mg-md-l--1">
                                <label class="form-control-label"><h6>Regency/City: <span class="tx-danger">*</span></h6></label>
                                <div class="toggle-wrapper">
                                    <div class="toggle toggle-modern success" id="toggle-memdata_regency"></div>
                                    <input type="hidden" name="memdata_regency" value="0">
                                    <input type="checkbox" id="memdata_regency" name="memdata_regency" value="1" style="display: none">
                                </div>
                                @if($errors->has('memdata_regency'))
                                    <ul class="parsley-errors-list filled">
                                        <li class="parsley-required">{{ $errors->first('memdata_regency') }}</li>
                                    </ul>
                                @endif
                            </div>
                        </div><!-- col-4 -->
                        <div class="col-md-4 mg-t--1">
                            <div class="form-group mg-md-l--1">
                                <label class="form-control-label"><h6>District: <span class="tx-danger">*</span></h6></label>
                                <div class="toggle-wrapper">
                                    <div class="toggle toggle-modern success" id="toggle-memdata_district"></div>
                                    <input type="hidden" name="memdata_district" value="0">
                                    <input type="checkbox" id="memdata_district" name="memdata_district" value="1" style="display: none">
                                </div>
                                @if($errors->has('memdata_district'))
                                    <ul class="parsley-errors-list filled">
                                        <li class="parsley-required">{{ $errors->first('memdata_district') }}</li>
                                    </ul>
                                @endif
                            </div>
                        </div><!-- col-4 -->
                        <div class="col-md-4 mg-t--1">
                            <div class="form-group mg-md-l--1">
                                <label class="form-control-label"><h6>Village: <span class="tx-danger">*</span></h6></label>
                                <div class="toggle-wrapper">
                                    <div class="toggle toggle-modern success" id="toggle-memdata_village"></div>
                                    <input type="hidden" name="memdata_village" value="0">
                                    <input type="checkbox" id="memdata_village" name="memdata_village" value="1" style="display: none">
                                </div>
                                @if($errors->has('memdata_village'))
                                    <ul class="parsley-errors-list filled">
                                        <li class="parsley-required">{{ $errors->first('memdata_village') }}</li>
                                    </ul>
                                @endif
                            </div>
                        </div><!-- col-4 -->
                        <div class="col-md-4 mg-t--1">
                            <div class="form-group mg-md-l--1">
                                <label class="form-control-label"><h6>ZIP/Postal Code: <span class="tx-danger">*</span></h6></label>
                                <div class="toggle-wrapper">
                                    <div class="toggle toggle-modern success" id="toggle-memdata_zip"></div>
                                    <input type="hidden" name="memdata_zip" value="0">
                                    <input type="checkbox" id="memdata_zip" name="memdata_zip" value="1" style="display: none">
                                </div>
                                @if($errors->has('memdata_zip'))
                                    <ul class="parsley-errors-list filled">
                                        <li class="parsley-required">{{ $errors->first('memdata_zip') }}</li>
                                    </ul>
                                @endif
                            </div>
                        </div><!-- col-4 -->
                        <div class="col-md-4 mg-t--1">
                            <div class="form-group mg-md-l--1">
                                <label class="form-control-label"><h6>Mobile Phone: <span class="tx-danger">*</span></h6></label>
                                <div class="toggle-wrapper">
                                    <div class="toggle toggle-modern success" id="toggle-memdata_hp"></div>
                                    <input type="hidden" name="memdata_hp" value="0">
                                    <input type="checkbox" id="memdata_hp" name="memdata_hp" value="1" style="display: none">
                                </div>
                                @if($errors->has('memdata_hp'))
                                    <ul class="parsley-errors-list filled">
                                        <li class="parsley-required">{{ $errors->first('memdata_hp') }}</li>
                                    </ul>
                                @endif
                            </div>
                        </div><!-- col-4 -->
                        <div class="col-md-4 mg-t--1">
                            <div class="form-group mg-md-l--1">
                                <label class="form-control-label"><h6>ID Number: <span class="tx-danger">*</span></h6></label>
                                <div class="toggle-wrapper">
                                    <div class="toggle toggle-modern success" id="toggle-memdata_id_number"></div>
                                    <input type="hidden" name="memdata_id_number" value="0">
                                    <input type="checkbox" id="memdata_id_number" name="memdata_id_number" value="1" style="display: none">
                                </div>
                                @if($errors->has('memdata_id_number'))
                                    <ul class="parsley-errors-list filled">
                                        <li class="parsley-required">{{ $errors->first('memdata_id_number') }}</li>
                                    </ul>
                                @endif
                            </div>
                        </div><!-- col-4 -->
                        <div class="col-md-4 mg-t--1">
                            <div class="form-group mg-md-l--1">
                                <label class="form-control-label"><h6>Tax ID Number: <span class="tx-danger">*</span></h6></label>
                                <div class="toggle-wrapper">
                                    <div class="toggle toggle-modern success" id="toggle-memdata_tax_id_number"></div>
                                    <input type="hidden" name="memdata_tax_id_number" value="0">
                                    <input type="checkbox" id="memdata_tax_id_number" name="memdata_tax_id_number" value="1" style="display: none">
                                </div>
                                @if($errors->has('memdata_tax_id_number'))
                                    <ul class="parsley-errors-list filled">
                                        <li class="parsley-required">{{ $errors->first('memdata_tax_id_number') }}</li>
                                    </ul>
                                @endif
                            </div>
                        </div><!-- col-4 -->
                        <div class="col-md-4 mg-t--1">
                            <div class="form-group mg-md-l--1">
                                <label class="form-control-label"><h6>Sex/Gender: <span class="tx-danger">*</span></h6></label>
                                <div class="toggle-wrapper">
                                    <div class="toggle toggle-modern success" id="toggle-memdata_sex"></div>
                                    <input type="hidden" name="memdata_sex" value="0">
                                    <input type="checkbox" id="memdata_sex" name="memdata_sex" value="1" style="display: none">
                                </div>
                                @if($errors->has('memdata_sex'))
                                    <ul class="parsley-errors-list filled">
                                        <li class="parsley-required">{{ $errors->first('memdata_sex') }}</li>
                                    </ul>
                                @endif
                            </div>
                        </div><!-- col-4 -->
                        <div class="col-md-4 mg-t--1">
                            <div class="form-group mg-md-l--1">
                                <label class="form-control-label"><h6>Birth Date: <span class="tx-danger">*</span></h6></label>
                                <div class="toggle-wrapper">
                                    <div class="toggle toggle-modern success" id="toggle-memdata_birth_date"></div>
                                    <input type="hidden" name="memdata_birth_date" value="0">
                                    <input type="checkbox" id="memdata_birth_date" name="memdata_birth_date" value="1" style="display: none">
                                </div>
                                @if($errors->has('memdata_birth_date'))
                                    <ul class="parsley-errors-list filled">
                                        <li class="parsley-required">{{ $errors->first('memdata_birth_date') }}</li>
                                    </ul>
                                @endif
                            </div>
                        </div><!-- col-4 -->
                        <div class="col-md-4 mg-t--1">
                            <div class="form-group mg-md-l--1">
                                <label class="form-control-label"><h6>Max. Bank Account: <span class="tx-danger">*</span></h6></label>
                                <input class="form-control parsley-error" type="text" id="member_max_bank_account" name="member_max_bank_account" value="{{ $settings->member_max_bank_account }}" placeholder="max. bank account per-member">
                                @if($errors->has('member_max_bank_account'))
                                    <ul class="parsley-errors-list filled">
                                        <li class="parsley-required">{{ $errors->first('member_max_bank_account') }}</li>
                                    </ul>
                                @endif
                            </div>
                        </div><!-- col-4 -->
                        <div class="col-md-4 mg-t--1">
                            <div class="form-group mg-md-l--1">
                                <label class="form-control-label"><h6>Bank Acc. Name Must Match: <span class="tx-danger">*</span></h6></label>
                                <div class="toggle-wrapper">
                                    <div class="toggle toggle-modern success" id="toggle-member_bank_name_match"></div>
                                    <input type="hidden" name="member_bank_name_match" value="0">
                                    <input type="checkbox" id="member_bank_name_match" name="member_bank_name_match" value="1" style="display: none">
                                </div>
                                @if($errors->has('member_bank_name_match'))
                                    <ul class="parsley-errors-list filled">
                                        <li class="parsley-required">{{ $errors->first('member_bank_name_match') }}</li>
                                    </ul>
                                @endif
                            </div>
                        </div><!-- col-4 -->
                        <div class="col-md-4 mg-t--1">
                            <div class="form-group mg-md-l--1">
                                <label class="form-control-label"><h6>Bank Acc. Unique: <span class="tx-danger">*</span></h6></label>
                                <div class="toggle-wrapper">
                                    <div class="toggle toggle-modern success" id="toggle-member_bank_unique"></div>
                                    <input type="hidden" name="member_bank_unique" value="0">
                                    <input type="checkbox" id="member_bank_unique" name="member_bank_unique" value="1" style="display: none">
                                </div>
                                @if($errors->has('member_bank_unique'))
                                    <ul class="parsley-errors-list filled">
                                        <li class="parsley-required">{{ $errors->first('member_bank_unique') }}</li>
                                    </ul>
                                @endif
                            </div>
                        </div><!-- col-4 -->
                        <div class="col-md-8 mg-t--1">
                            <div class="form-group mg-md-l--1">
                            </div>
                        </div><!-- col-4 -->
                    </div><!-- row -->
                    <div class="form-layout-footer bd pd-20 bd-t-0">
                        <button type="submit" class="btn btn-info">Save</button>
                        <a class="btn btn-secondary" href="{{ route('home') }}">Cancel</a>
                        <button type="button" class="btn btn-outline-primary btn-icon rounded-circle tx-34-force pd-0-force bd-0 pull-right"
                                data-container="body"
                                data-toggle="popover"
                                data-popover-color="default"
                                data-placement="top"
                                data-html="true"
                                title="Help"
                                data-content="Under Construction"><div><i class="fa fa-question-circle-o"></i></div></button>
                    </div><!-- form-group -->
                </div><!-- form-layout -->
            </form>
        </div><!-- section-wrapper -->
    </div><!-- br-pagebody -->
@endsection

@section('scripts')
    <script src="{{ asset('admin/lib/select2/js/select2.min.js') }}"></script>
    <script src="{{ asset('admin/lib/jquery-toggles/toggles.min.js') }}"></script>
@endsection

@section('page_script')
    <script>
        $(function(){
            'use strict'

            $('.form-layout .form-control').on('focusin', function(){
                $(this).closest('.form-group').addClass('form-group-active');
            });

            $('.form-layout .form-control').on('focusout', function(){
                $(this).closest('.form-group').removeClass('form-group-active');
            });

            $('.select2').on('select2:opening', function (e) {
                $(this).closest('.form-group').addClass('form-group-active');
            });

            $('.select2').on('select2:closing', function (e) {
                $(this).closest('.form-group').removeClass('form-group-active');
            });

            // Toggles
            $('#toggle-memdata_name').toggles({
                on: @if ($settings->memdata_name == 1) true @else false @endif,
                height: 26,
                checkbox: $('#memdata_name'),
                text: {on:'YES', off:'NO'}
            });

            $('#toggle-memdata_email').toggles({
                on: @if($settings->memdata_email == 1) true @else false @endif,
                height: 26,
                checkbox: $('#memdata_email'),
                text: {on:'YES', off:'NO'}
            });

            $('#toggle-memdata_address').toggles({
                on: @if($settings->memdata_address == 1) true @else false @endif,
                height: 26,
                checkbox: $('#memdata_address'),
                text: {on:'YES', off:'NO'}
            });

            $('#toggle-memdata_province').toggles({
                on: @if($settings->memdata_province == 1) true @else false @endif,
                height: 26,
                checkbox: $('#memdata_province'),
                text: {on:'YES', off:'NO'}
            });

            $('#toggle-memdata_regency').toggles({
                on: @if($settings->memdata_regency == 1) true @else false @endif,
                height: 26,
                checkbox: $('#memdata_regency'),
                text: {on:'YES', off:'NO'}
            });

            $('#toggle-memdata_district').toggles({
                on: @if($settings->memdata_district == 1) true @else false @endif,
                height: 26,
                checkbox: $('#memdata_district'),
                text: {on:'YES', off:'NO'}
            });

            $('#toggle-memdata_village').toggles({
                on: @if($settings->memdata_village == 1) true @else false @endif,
                height: 26,
                checkbox: $('#memdata_village'),
                text: {on:'YES', off:'NO'}
            });

            $('#toggle-memdata_zip').toggles({
                on: @if($settings->memdata_zip == 1) true @else false @endif,
                height: 26,
                checkbox: $('#memdata_zip'),
                text: {on:'YES', off:'NO'}
            });

            $('#toggle-memdata_hp').toggles({
                on: @if($settings->memdata_hp == 1) true @else false @endif,
                height: 26,
                checkbox: $('#memdata_hp'),
                text: {on:'YES', off:'NO'}
            });

            $('#toggle-memdata_id_number').toggles({
                on: @if($settings->memdata_id_number == 1) true @else false @endif,
                height: 26,
                checkbox: $('#memdata_id_number'),
                text: {on:'YES', off:'NO'}
            });

            $('#toggle-memdata_tax_id_number').toggles({
                on: @if($settings->memdata_tax_id_number == 1) true @else false @endif,
                height: 26,
                checkbox: $('#memdata_tax_id_number'),
                text: {on:'YES', off:'NO'}
            });

            $('#toggle-memdata_sex').toggles({
                on: @if($settings->memdata_sex == 1) true @else false @endif,
                height: 26,
                checkbox: $('#memdata_sex'),
                text: {on:'YES', off:'NO'}
            });

            $('#toggle-memdata_birth_date').toggles({
                on: @if($settings->memdata_birth_date == 1) true @else false @endif,
                height: 26,
                checkbox: $('#memdata_birth_date'),
                text: {on:'YES', off:'NO'}
            });

            $('#toggle-member_bank_name_match').toggles({
                on: @if($settings->member_bank_name_match == 1) true @else false @endif,
                height: 26,
                checkbox: $('#member_bank_name_match'),
                text: {on:'YES', off:'NO'}
            });

            $('#toggle-member_bank_unique').toggles({
                on: @if($settings->member_bank_unique == 1) true @else false @endif,
                height: 26,
                checkbox: $('#member_bank_unique'),
                text: {on:'YES', off:'NO'}
            });

        });
    </script>

    @if (session('success'))
        <div id="modalsukses" class="modal fade">
            <div class="modal-dialog" role="document">
                <div class="modal-content tx-size-sm">
                    <div class="modal-body tx-center pd-y-20 pd-x-20">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <i class="icon ion-ios-checkmark-outline tx-100 tx-success lh-1 mg-t-20 d-inline-block"></i>
                        <h4 class="tx-success tx-semibold mg-b-20">Success!</h4>
                        <p class="mg-b-20 mg-x-20">{{ session('success') }}</p>
                        <button type="button" class="btn btn-success tx-11 tx-uppercase pd-y-12 pd-x-25 tx-mont tx-medium mg-b-20" data-dismiss="modal" aria-label="Close"> OK</button>
                    </div><!-- modal-body -->
                </div><!-- modal-content -->
            </div><!-- modal-dialog -->
        </div><!-- modal -->
        <script>
            $('#modalsukses').modal('show');
        </script>
    @endif

@endsection