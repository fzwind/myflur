@extends('layouts.admin')

@section('styles')
    <link href="{{ asset('admin/lib/select2/css/select2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('admin/lib/jquery-toggles/toggles-full.css') }}" rel="stylesheet">
@endsection

@section('content')
    <div class="pd-x-20 pd-sm-x-30 pd-t-20 pd-sm-t-30">
        <div class="pull-left">
            <h4 class="tx-gray-800 mg-b-5">GENERAL SYSTEM SETTINGS</h4>
        </div>
    </div>

    <div class="br-pagebody mg-t-70">
        <div class="br-section-wrapper pd-20">
            <form method="post" action="{{ route('system.general.settings.post') }}" data-parsley-validate>
                @csrf
                <div class="form-layout form-layout-2">
                    <div class="row no-gutters">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="form-control-label"><h6>Network Type: <span class="tx-danger">*</span></h6></label>
                                <select class="form-control select2" data-placeholder="Choose Network Type" id="network_type" name="network_type">
                                    <option label="Choose Network Type"></option>
                                    <option value="binary" {{ ($settings->network_type == 'binary') ? ' selected' : '' }}>Binary Network</option>
                                    <option value="matrix" {{ ($settings->network_type == 'matrix') ? ' selected' : '' }}>Matrix Network</option>
                                </select>
                                @if($errors->has('network_type'))
                                    <ul class="parsley-errors-list filled">
                                        <li class="parsley-required">{{ $errors->first('network_type') }}</li>
                                    </ul>
                                @endif
                            </div>
                        </div><!-- col-4 -->
                        <div class="col-md-4">
                            <div class="form-group mg-md-l--1">
                                <label class="form-control-label"><h6>Email Activation: <span class="tx-danger">*</span></h6></label>
                                <div class="toggle-wrapper">
                                    <div class="toggle toggle-modern success" id="toggle-email"></div>
                                    <input type="hidden" name="email_activation" value="0">
                                    <input type="checkbox" id="email_activation" name="email_activation" value="1" style="display: none">
                                </div>
                                @if($errors->has('email_activation'))
                                    <ul class="parsley-errors-list filled">
                                        <li class="parsley-required">{{ $errors->first('email_activation') }}</li>
                                    </ul>
                                @endif
                            </div>
                        </div><!-- col-4 -->
                        <div class="col-md-4">
                            <div class="form-group mg-md-l--1">
                                <label class="form-control-label"><h6>Notifications: <span class="tx-danger">*</span></h6></label>
                                <select class="form-control select2" data-placeholder="Choose Notification Method" id="notification" name="notification">
                                    <option label="Choose Notification Method"></option>
                                    <option value="none" {{ ($settings->notification == 'none') ? ' selected' : '' }}>None</option>
                                    <option value="email" {{ ($settings->notification == 'email') ? ' selected' : '' }}>Email</option>
                                    <option value="app" {{ ($settings->notification == 'app') ? ' selected' : '' }}>In-App</option>
                                    <option value="email-app" {{ ($settings->notification == 'email-app') ? ' selected' : '' }}>Email &amp; In-App</option>
                                </select>
                                @if($errors->has('notification'))
                                    <ul class="parsley-errors-list filled">
                                        <li class="parsley-required">{{ $errors->first('notification') }}</li>
                                    </ul>
                                @endif
                            </div>
                        </div><!-- col-4 -->
                    </div><!-- row -->
                    <div class="row no-gutters">
                        <div class="col-md-4">
                            <div class="form-group mg-md-t--1">
                                <label class="form-control-label"><h6>Logging: <span class="tx-danger">*</span></h6></label>
                                <div class="toggle-wrapper">
                                    <div class="toggle toggle-modern success" id="toggle-logging"></div>
                                    <input type="hidden" name="logging" value="0">
                                    <input type="checkbox" id="logging" name="logging" value="1" style="display: none">
                                </div>
                                @if($errors->has('logging'))
                                    <ul class="parsley-errors-list filled">
                                        <li class="parsley-required">{{ $errors->first('logging') }}</li>
                                    </ul>
                                @endif
                            </div>
                        </div><!-- col-4 -->
                        <div class="col-md-4">
                            <div class="form-group mg-md-t--1 mg-md-l--1">
                                <label class="form-control-label"><h6>Proof of Bank Transfer Upload: <span class="tx-danger">*</span></h6></label>
                                <div class="toggle-wrapper">
                                    <div class="toggle toggle-modern success" id="toggle-upload"></div>
                                    <input type="hidden" name="proof_bank_transfer_upload" value="0">
                                    <input type="checkbox" {{ ($settings->proof_bank_transfer_upload == 1) ? 'checked' : '' }} id="proof_bank_transfer_upload" name="proof_bank_transfer_upload" value="1" style="display: none"/>
                                </div>
                                @if($errors->has('proof_bank_transfer_upload'))
                                    <ul class="parsley-errors-list filled">
                                        <li class="parsley-required">{{ $errors->first('proof_bank_transfer_upload') }}</li>
                                    </ul>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group mg-md-t--1 mg-md-l--1">
                            </div>
                        </div>
                    </div>
                    <div class="form-layout-footer bd pd-20 bd-t-0">
                        <button type="submit" class="btn btn-info">Save</button>
                        <a class="btn btn-secondary" href="{{ route('home') }}">Cancel</a>
                        <button type="button" class="btn btn-outline-primary btn-icon rounded-circle tx-34-force pd-0-force bd-0 pull-right"
                                data-container="body"
                                data-toggle="popover"
                                data-popover-color="default"
                                data-placement="top"
                                data-html="true"
                                title="Help"
                                data-content="Under Construction"><div><i class="fa fa-question-circle-o"></i></div></button>
                    </div><!-- form-group -->
                </div><!-- form-layout -->
            </form>
        </div><!-- section-wrapper -->
    </div><!-- br-pagebody -->
@endsection

@section('scripts')
    <script src="{{ asset('admin/lib/select2/js/select2.min.js') }}"></script>
    <script src="{{ asset('admin/lib/jquery-toggles/toggles.min.js') }}"></script>
@endsection

@section('page_script')
    <script>
        $(function(){
            'use strict'

            $('.form-layout .form-control').on('focusin', function(){
                $(this).closest('.form-group').addClass('form-group-active');
            });

            $('.form-layout .form-control').on('focusout', function(){
                $(this).closest('.form-group').removeClass('form-group-active');
            });

            $('.select2').on('select2:opening', function (e) {
                $(this).closest('.form-group').addClass('form-group-active');
            });

            $('.select2').on('select2:closing', function (e) {
                $(this).closest('.form-group').removeClass('form-group-active');
            });

            // Toggles
            $('#toggle-email').toggles({
                on: @if($settings->email_activation == 1)
                        true
                    @else
                        false
                    @endif,
                height: 26,
                checkbox: $('#email_activation'),
                text: {on:'YES', off:'NO'}
            });

            $('#toggle-logging').toggles({
                on: @if($settings->logging == 1)
                    true
                @else
                    false
                @endif,
                height: 26,
                checkbox: $('#logging'),
                text: {on:'YES', off:'NO'}
            });

            $('#toggle-upload').toggles({
                on: @if($settings->proof_bank_transfer_upload == 1)
                    true
                @else
                    false
                @endif,
                height: 26,
                checkbox: $('#proof_bank_transfer_upload'),
                text: {on:'YES', off:'NO'}
            });

        });
    </script>

    @if (session('success'))
        <div id="modalsukses" class="modal fade">
            <div class="modal-dialog" role="document">
                <div class="modal-content tx-size-sm">
                    <div class="modal-body tx-center pd-y-20 pd-x-20">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <i class="icon ion-ios-checkmark-outline tx-100 tx-success lh-1 mg-t-20 d-inline-block"></i>
                        <h4 class="tx-success tx-semibold mg-b-20">Success!</h4>
                        <p class="mg-b-20 mg-x-20">{{ session('success') }}</p>
                        <button type="button" class="btn btn-success tx-11 tx-uppercase pd-y-12 pd-x-25 tx-mont tx-medium mg-b-20" data-dismiss="modal" aria-label="Close"> OK</button>
                    </div><!-- modal-body -->
                </div><!-- modal-content -->
            </div><!-- modal-dialog -->
        </div><!-- modal -->
        <script>
            $('#modalsukses').modal('show');
        </script>
    @endif

@endsection