@extends('layouts.admin')

@section('styles')
    <link href="{{ asset('admin/lib/select2/css/select2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('admin/lib/jquery-toggles/toggles-full.css') }}" rel="stylesheet">
@endsection

@section('content')
    <div class="br-pageheader pd-y-15 pd-l-20">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
            <a class="breadcrumb-item" href="{{ route('home') }}">myflur</a>
            <a class="breadcrumb-item" href="#">administrator</a>
            <a class="breadcrumb-item" href="#">system settings</a>
            <span class="breadcrumb-item active">clear cache</span>
        </nav>
    </div><!-- br-pageheader -->
    <div class="pd-x-20 pd-sm-x-30 pd-t-20 pd-sm-t-30">
        <div class="pull-left">
            <h4 class="tx-gray-800 mg-b-5">CLEAR CACHE</h4>
        </div>
    </div>

    <div class="br-pagebody mg-t-70">
        <div class="br-section-wrapper pd-20">
            <div class="row row-sm mg-b-20">
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="bg-indigo rounded overflow-hidden">
                        <div class="pd-25 d-flex align-items-center">
                            <i class="ion ion-earth tx-60 lh-0 tx-white op-7"></i>
                            <div class="mg-l-20">
                                <p class="tx-24 tx-white tx-lato tx-bold mg-b-10 lh-1">All Cache</p>
                                <form method="post" action="{{ route('system.cache.clear.post',['cache' => 'all']) }}">
                                    @csrf
                                    <input class="btn btn-purple" type="submit" value="Clear Cache">
                                </form>
                            </div>
                        </div>
                    </div>
                </div><!-- col-3 -->
            </div>
            <div class="row row-sm">
                <div class="col-3">
                    <div class="bg-secondary rounded overflow-hidden">
                        <div class="pd-25 d-flex align-items-center">
                            <i class="ion ion-clock tx-60 lh-0 tx-white op-7"></i>
                            <div class="mg-l-20">
                                <p class="tx-24 tx-white tx-lato tx-bold mg-b-10 lh-1">System Settings</p>
                                <form method="post" action="{{ route('system.cache.clear.post',['cache' => 'systemsettings']) }}">
                                    @csrf
                                    <input class="btn btn-dark" type="submit" value="Clear Cache">
                                </form>
                            </div>
                        </div>
                    </div>
                </div><!-- col-3 -->
                <div class="col-3">
                    <div class="bg-secondary rounded overflow-hidden">
                        <div class="pd-25 d-flex align-items-center">
                            <i class="ion ion-clock tx-60 lh-0 tx-white op-7"></i>
                            <div class="mg-l-20">
                                <p class="tx-24 tx-white tx-lato tx-bold mg-b-10 lh-1">Plan Settings</p>
                                <form method="post" action="{{ route('system.cache.clear.post',['cache' => 'plansettings']) }}">
                                    @csrf
                                    <input class="btn btn-dark" type="submit" value="Clear Cache">
                                </form>
                            </div>
                        </div>
                    </div>
                </div><!-- col-3 -->
                <div class="col-3">
                    <div class="bg-secondary rounded overflow-hidden">
                        <div class="pd-25 d-flex align-items-center">
                            <i class="ion ion-clock tx-60 lh-0 tx-white op-7"></i>
                            <div class="mg-l-20">
                                <p class="tx-24 tx-white tx-lato tx-bold mg-b-10 lh-1">Bonus Settings</p>
                                <form method="post" action="{{ route('system.cache.clear.post',['cache' => 'bonussettings']) }}">
                                    @csrf
                                    <input class="btn btn-dark" type="submit" value="Clear Cache">
                                </form>
                            </div>
                        </div>
                    </div>
                </div><!-- col-3 -->
                <div class="col-3">
                    <div class="bg-secondary rounded overflow-hidden">
                        <div class="pd-25 d-flex align-items-center">
                            <i class="ion ion-clock tx-60 lh-0 tx-white op-7"></i>
                            <div class="mg-l-20">
                                <p class="tx-24 tx-white tx-lato tx-bold mg-b-10 lh-1">Stockist Settings</p>
                                <form method="post" action="{{ route('system.cache.clear.post',['cache' => 'stockistsettings']) }}">
                                    @csrf
                                    <input class="btn btn-dark" type="submit" value="Clear Cache">
                                </form>
                            </div>
                        </div>
                    </div>
                </div><!-- col-3 -->
            </div><!-- row -->
        </div><!-- section-wrapper -->
    </div><!-- br-pagebody -->
@endsection

@section('scripts')
    <script src="{{ asset('admin/lib/select2/js/select2.min.js') }}"></script>
    <script src="{{ asset('admin/lib/jquery-toggles/toggles.min.js') }}"></script>
@endsection

@section('page_script')
    <script>
        $(function(){
            'use strict'

            $('.form-layout .form-control').on('focusin', function(){
                $(this).closest('.form-group').addClass('form-group-active');
            });

            $('.form-layout .form-control').on('focusout', function(){
                $(this).closest('.form-group').removeClass('form-group-active');
            });

            $('.select2').on('select2:opening', function (e) {
                $(this).closest('.form-group').addClass('form-group-active');
            });

            $('.select2').on('select2:closing', function (e) {
                $(this).closest('.form-group').removeClass('form-group-active');
            });

        });
    </script>

    @if (session('success'))
        <div id="modalsukses" class="modal fade">
            <div class="modal-dialog" role="document">
                <div class="modal-content tx-size-sm">
                    <div class="modal-body tx-center pd-y-20 pd-x-20">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <i class="icon ion-ios-checkmark-outline tx-100 tx-success lh-1 mg-t-20 d-inline-block"></i>
                        <h4 class="tx-success tx-semibold mg-b-20">Success!</h4>
                        <p class="mg-b-20 mg-x-20">{{ session('success') }}</p>
                        <button type="button" class="btn btn-success tx-11 tx-uppercase pd-y-12 pd-x-25 tx-mont tx-medium mg-b-20" data-dismiss="modal" aria-label="Close"> OK</button>
                    </div><!-- modal-body -->
                </div><!-- modal-content -->
            </div><!-- modal-dialog -->
        </div><!-- modal -->
        <script>
            $('#modalsukses').modal('show');
        </script>
    @endif

@endsection