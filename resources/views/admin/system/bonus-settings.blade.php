@extends('layouts.admin')

@section('styles')
    <link href="{{ asset('admin/lib/select2/css/select2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('admin/lib/jquery-toggles/toggles-full.css') }}" rel="stylesheet">
@endsection

@section('content')
    <div class="br-pageheader pd-y-15 pd-l-20">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
            <a class="breadcrumb-item" href="{{ route('home') }}">myflur</a>
            <a class="breadcrumb-item" href="#">administrator</a>
            <a class="breadcrumb-item" href="#">system settings</a>
            <span class="breadcrumb-item active">bonus &amp; reward settings</span>
        </nav>
    </div><!-- br-pageheader -->
    <div class="pd-x-20 pd-sm-x-30 pd-t-20 pd-sm-t-30">
        <div class="pull-left">
            <h4 class="tx-gray-800 mg-b-5">BONUSES &amp; REWARD SYSTEM SETTINGS</h4>
        </div>
    </div>

    <div class="br-pagebody mg-t-70">
        <div class="br-section-wrapper pd-20">
            <form method="post" action="{{ route('system.bonus.settings.post') }}" data-parsley-validate>
                @csrf
                <div class="form-layout form-layout-2">
                    <div class="row no-gutters">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="form-control-label"><h6>Reward: <span class="tx-danger">*</span></h6></label>
                                <div class="toggle-wrapper">
                                    <div class="toggle toggle-modern success" id="toggle-rewards"></div>
                                    <input type="hidden" name="rewards" value="0">
                                    <input type="checkbox" id="rewards" name="rewards" value="1" style="display: none">
                                </div>
                                @if($errors->has('rewards'))
                                    <ul class="parsley-errors-list filled">
                                        <li class="parsley-required">{{ $errors->first('rewards') }}</li>
                                    </ul>
                                @endif
                            </div>
                        </div><!-- col-4 -->
                        <div class="col-md-4">
                            <div class="form-group mg-md-l--1">
                                <label class="form-control-label"><h6>Sponsor Bonus: <span class="tx-danger">*</span></h6></label>
                                <div class="toggle-wrapper">
                                    <div class="toggle toggle-modern success" id="toggle-bonus-sponsor"></div>
                                    <input type="hidden" name="bonus_sponsor" value="0">
                                    <input type="checkbox" id="bonus_sponsor" name="bonus_sponsor" value="1" style="display: none">
                                </div>
                                @if($errors->has('bonus_sponsor'))
                                    <ul class="parsley-errors-list filled">
                                        <li class="parsley-required">{{ $errors->first('bonus_sponsor') }}</li>
                                    </ul>
                                @endif
                            </div>
                        </div><!-- col-4 -->
                        <div class="col-md-4">
                            <div class="form-group mg-md-l--1">
                                <label class="form-control-label"><h6>Pairing Bonus: <span class="tx-danger">*</span></h6></label>
                                <div class="toggle-wrapper">
                                    <div class="toggle toggle-modern success" id="toggle-bonus-pairing"></div>
                                    <input type="hidden" name="bonus_pairing" value="0">
                                    <input type="checkbox" id="bonus_pairing" name="bonus_pairing" value="1" style="display: none">
                                </div>
                                @if($errors->has('bonus_pairing'))
                                    <ul class="parsley-errors-list filled">
                                        <li class="parsley-required">{{ $errors->first('bonus_pairing') }}</li>
                                    </ul>
                                @endif
                            </div>
                        </div><!-- col-4 -->
                        <div class="col-md-4 mg-t--1">
                            <div class="form-group mg-md-l--1">
                                <label class="form-control-label"><h6>Pairing Level Bonus: <span class="tx-danger">*</span></h6></label>
                                <div class="toggle-wrapper">
                                    <div class="toggle toggle-modern success" id="toggle-bonus-pairing-level"></div>
                                    <input type="hidden" name="bonus_pairing_level" value="0">
                                    <input type="checkbox" id="bonus_pairing_level" name="bonus_pairing_level" value="1" style="display: none">
                                </div>
                                @if($errors->has('bonus_pairing_level'))
                                    <ul class="parsley-errors-list filled">
                                        <li class="parsley-required">{{ $errors->first('bonus_pairing_level') }}</li>
                                    </ul>
                                @endif
                            </div>
                        </div><!-- col-4 -->
                        <div class="col-md-4 mg-t--1">
                            <div class="form-group mg-md-l--1">
                                <label class="form-control-label"><h6>Level Bonus: <span class="tx-danger">*</span></h6></label>
                                <div class="toggle-wrapper">
                                    <div class="toggle toggle-modern success" id="toggle-bonus-level"></div>
                                    <input type="hidden" name="bonus_level" value="0">
                                    <input type="checkbox" id="bonus_level" name="bonus_level" value="1" style="display: none">
                                </div>
                                @if($errors->has('bonus_level'))
                                    <ul class="parsley-errors-list filled">
                                        <li class="parsley-required">{{ $errors->first('bonus_level') }}</li>
                                    </ul>
                                @endif
                            </div>
                        </div><!-- col-4 -->
                        <div class="col-md-4 mg-t--1">
                            <div class="form-group mg-md-l--1">

                            </div>
                        </div><!-- col-4 -->
                    </div><!-- row -->
                    <div class="form-layout-footer bd pd-20 bd-t-0">
                        <button type="submit" class="btn btn-info">Save</button>
                        <a class="btn btn-secondary" href="{{ route('home') }}">Cancel</a>
                        <button type="button" class="btn btn-outline-primary btn-icon rounded-circle tx-34-force pd-0-force bd-0 pull-right"
                                data-container="body"
                                data-toggle="popover"
                                data-popover-color="default"
                                data-placement="top"
                                data-html="true"
                                title="Help"
                                data-content="Under Construction"><div><i class="fa fa-question-circle-o"></i></div></button>
                    </div><!-- form-group -->
                </div><!-- form-layout -->
            </form>
        </div><!-- section-wrapper -->
    </div><!-- br-pagebody -->
@endsection

@section('scripts')
    <script src="{{ asset('admin/lib/select2/js/select2.min.js') }}"></script>
    <script src="{{ asset('admin/lib/jquery-toggles/toggles.min.js') }}"></script>
@endsection

@section('page_script')
    <script>
        $(function(){
            'use strict'

            $('.form-layout .form-control').on('focusin', function(){
                $(this).closest('.form-group').addClass('form-group-active');
            });

            $('.form-layout .form-control').on('focusout', function(){
                $(this).closest('.form-group').removeClass('form-group-active');
            });

            $('.select2').on('select2:opening', function (e) {
                $(this).closest('.form-group').addClass('form-group-active');
            });

            $('.select2').on('select2:closing', function (e) {
                $(this).closest('.form-group').removeClass('form-group-active');
            });

            // Toggles
            $('#toggle-rewards').toggles({
                on: @if($settings->rewards == 1)
                    true
                @else
                    false
                @endif,
                height: 26,
                checkbox: $('#rewards'),
                text: {on:'YES', off:'NO'}
            });

            $('#toggle-bonus-sponsor').toggles({
                on: @if($settings->bonus_sponsor == 1)
                    true
                @else
                    false
                @endif,
                height: 26,
                checkbox: $('#bonus_sponsor'),
                text: {on:'YES', off:'NO'}
            });

            $('#toggle-bonus-pairing').toggles({
                on: @if($settings->bonus_pairing == 1)
                    true
                @else
                    false
                @endif,
                height: 26,
                checkbox: $('#bonus_pairing'),
                text: {on:'YES', off:'NO'}
            });

            $('#toggle-bonus-pairing-level').toggles({
                on: @if($settings->bonus_pairing_level == 1)
                    true
                @else
                    false
                @endif,
                height: 26,
                checkbox: $('#bonus_pairing_level'),
                text: {on:'YES', off:'NO'}
            });

            $('#toggle-bonus-level').toggles({
                on: @if($settings->bonus_level == 1)
                    true
                @else
                    false
                @endif,
                height: 26,
                checkbox: $('#bonus_level'),
                text: {on:'YES', off:'NO'}
            });

        });
    </script>

    @if (session('success'))
        <div id="modalsukses" class="modal fade">
            <div class="modal-dialog" role="document">
                <div class="modal-content tx-size-sm">
                    <div class="modal-body tx-center pd-y-20 pd-x-20">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <i class="icon ion-ios-checkmark-outline tx-100 tx-success lh-1 mg-t-20 d-inline-block"></i>
                        <h4 class="tx-success tx-semibold mg-b-20">Success!</h4>
                        <p class="mg-b-20 mg-x-20">{{ session('success') }}</p>
                        <button type="button" class="btn btn-success tx-11 tx-uppercase pd-y-12 pd-x-25 tx-mont tx-medium mg-b-20" data-dismiss="modal" aria-label="Close"> OK</button>
                    </div><!-- modal-body -->
                </div><!-- modal-content -->
            </div><!-- modal-dialog -->
        </div><!-- modal -->
        <script>
            $('#modalsukses').modal('show');
        </script>
    @endif

@endsection