@extends('layouts.admin')

@section('styles')
    <link href="{{ asset('admin/lib/select2/css/select2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('admin/lib/datatables/jquery.dataTables.css') }}" rel="stylesheet">
    <link href="{{ asset('admin/lib/datatables-responsive/css/responsive.dataTables.css') }}" rel="stylesheet">
@endsection

@section('content')
    <div class="br-pageheader pd-y-15 pd-l-20">
    </div><!-- br-pageheader -->
    <div class="pd-x-20 pd-sm-x-30 pd-t-20 pd-sm-t-30">
        <div class="pull-left">
            <h4 class="tx-gray-800 mg-b-5">History Klaim Reward</h4>
        </div>
    </div>
    <div class="br-pagebody mg-t-70">
        <div class="br-section-wrapper pd-20">
            <div class="table-wrapper">
                <table id="member-reward-claim" class="table display responsive nowrap">
                    <thead>
                    <tr>
                        <th>Reward</th>
                        <th>Member</th>
                        <th>Kiri</th>
                        <th>Kanan</th>
                        <th>Bank</th>
                        <th>Tgl. Klaim</th>
                        <th>Tgl. Proses</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('admin/lib/select2/js/select2.min.js') }}"></script>
    <script src="{{ asset('admin/lib/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('admin/lib/datatables-responsive/dataTables.responsive.js') }}"></script>
@endsection

@section('page_script')
    <script>
        $(function(){
            'use strict';

            $('#member-reward-claim').DataTable({
                responsive: true,
                language: {
                    searchPlaceholder: 'Cari...',
                    sSearch: '',
                    lengthMenu: '_MENU_ data/halaman',
                    zeroRecords: 'Tidak ada data yang dapat ditampilkan.',
                    info: 'Menampilkan halaman _PAGE_ dari _PAGES_ (Total Data : _MAX_)',
                    infoEmpty: '',
                    infoFiltered: "(disaring dari total _MAX_ data)"
                },
                processing: true,
                serverSide: true,
                ajax: '{{ route('admin.member-reward.history.data') }}',
                columns: [
                    {data: 'str_reward', name: 'str_reward'},
                    {data: 'str_member', name: 'str_member'},
                    {data: 'str_left', name: 'str_left'},
                    {data: 'str_right', name: 'str_right'},
                    {data: 'member_bank', name: 'member_bank'},
                    {data: 'str_claimed_at', name: 'str_claimed_at'},
                    {data: 'str_process_at', name: 'str_process_at'},
                ]
            });

            $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });

        });

    </script>

@endsection
