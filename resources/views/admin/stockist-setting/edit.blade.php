@extends('layouts.admin')

@section('styles')
    <link href="{{ asset('admin/lib/select2/css/select2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('admin/lib/jquery-toggles/toggles-full.css') }}" rel="stylesheet">
@endsection

@section('content')
    <style>
        /*.ui-datepicker-div{ z-index:99999 !important; }*/
        .modal-open .ui-datepicker{ z-index:99999 !important; }
    </style>
    <div class="br-pageheader pd-y-15 pd-l-20">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
            <a class="breadcrumb-item" href="{{ route('home') }}">myflur</a>
            <a class="breadcrumb-item" href="#">admin</a>
            <a class="breadcrumb-item" href="#">settings</a>
            <a class="breadcrumb-item" href="{{ route('plan-settings.index') }}">plan setting</a>
            <span class="breadcrumb-item active">edit plan</span>
        </nav>
    </div><!-- br-pageheader -->
    <div class="pd-x-20 pd-sm-x-30 pd-t-20 pd-sm-t-30">
        <div class="pull-left">
            <h4 class="tx-gray-800 mg-b-5">PLAN SETTINGS</h4>
            <p class="mg-b-0">Edit Data Plan</p>
        </div>
    </div>

    <div class="br-pagebody mg-t-70">
        <div class="br-section-wrapper pd-20">
            <form method="post" action="{{ route('plan-settings.update', ['plan_setting' => $plan]) }}">
                @csrf
                <input type="hidden" name="_method" value="PUT">
                <div class="form-layout form-layout-2">
                    <div class="row no-gutters">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="form-control-label"><h6>Nama Plan: <span class="tx-danger">*</span></h6></label>
                                <input class="form-control parsley-error" type="text" id="name" name="name" value="{{ $plan->name ?? old('name')  }}" placeholder="Input Nama Plan">
                                @if($errors->has('name'))
                                    <ul class="parsley-errors-list filled">
                                        <li class="parsley-required">{{ $errors->first('name') }}</li>
                                    </ul>
                                @endif
                            </div>
                        </div><!-- col-6 -->
                        <div class="col-md-3">
                            <div class="form-group mg-l--1">
                                <label class="form-control-label"><h6>Kode: <span class="tx-danger">*</span></h6></label>
                                <input class="form-control parsley-error" type="text" id="code" name="code" value="{{ $plan->code ?? old('code') }}" placeholder="Input Kode Plan">
                                @if($errors->has('code'))
                                    <ul class="parsley-errors-list filled">
                                        <li class="parsley-required">{{ $errors->first('code') }}</li>
                                    </ul>
                                @endif
                            </div>
                        </div><!-- col-3 -->
                        <div class="col-md-3">
                            <div class="form-group mg-l--1">
                                <label class="form-control-label"><h6>Aktif: <span class="tx-danger">*</span></h6></label>
                                <div class="toggle-wrapper">
                                    <div class="toggle toggle-light success" id="toggle-aktif"></div>
                                    <input type="hidden" name="is_active" value="0">
                                    <input type="checkbox" id="is_active" name="is_active" value="1" style="display: none">
                                </div>
                                @if($errors->has('is_active'))
                                    <ul class="parsley-errors-list filled">
                                        <li class="parsley-required">{{ $errors->first('is_active') }}</li>
                                    </ul>
                                @endif
                            </div>
                        </div><!-- col-3 -->
                    </div><!-- row -->
                    <div class="form-layout-footer bd pd-20 bd-t-0">
                        <button type="submit" class="btn btn-info">Save</button>
                        <a class="btn btn-secondary" href="{{ route('plan-settings.index') }}">Cancel</a>
                        <button type="button" class="btn btn-outline-primary btn-icon rounded-circle tx-34-force pd-0-force bd-0 pull-right"
                                data-container="body"
                                data-toggle="popover"
                                data-popover-color="default"
                                data-placement="top"
                                data-html="true"
                                title="Help"
                                data-content="Under Construction"><div><i class="fa fa-question-circle-o"></i></div></button>
                    </div><!-- form-group -->
                </div><!-- form-layout -->
            </form>
            <div class="form-layout form-layout-2 mg-t-25">
                <div class="row no-gutters">
                    <div class="col-md-12">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th colspan="3" class="text-center">Harga</th>
                                    <th rowspan="2" class="text-center">Tanggal Aktif</th>
                                    <th rowspan="2" class="text-center">Status</th>
                                    <th rowspan="2" class="text-center"></th>
                                </tr>
                                <tr>
                                    <th class="text-center">Member</th>
                                    <th class="text-center">Stockist</th>
                                    <th class="text-center">Master Stockist</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($plan_prices as $row)
                                    <?php
                                    ($active_price_id == $row->id) ? $status = 'aktif' : $status = '';
                                    ($active_price_id == $row->id) ? $class = 'table-info' : $class = '';
                                    ($active_price_id == $row->id) ? $activeclass = 'tx-semibold tx-primary' : $activeclass = '';
                                    ?>
                                    <tr class="{{ $class }}">
                                        <td class="{{ $activeclass }}">Rp {{ number_format($row->price,0,',','.') }}</td>
                                        <td class="{{ $activeclass }}">Rp {{ number_format($row->stockist_price,0,',','.') }}</td>
                                        <td class="{{ $activeclass }}">Rp {{ number_format($row->master_stockist_price,0,',','.') }}</td>
                                        <td class="{{ $activeclass }}">{{ to_utz($row->active_at) }}</td>
                                        <td class="{{ $activeclass }}">{{ $status }}</td>
                                        @if($status != 'aktif' && $row->active_at > now('UTC'))
                                            <td class="text-center">
                                                <a href="#" class="text-primary tx-bold">edit</a>&nbsp;
                                                <a href="#" class="text-danger tx-bold">delete</a>
                                            </td>
                                        @else
                                            <td></td>
                                        @endif
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <a href="#" class="btn btn-sm btn-outline-info" data-toggle="modal" data-target="#price-form-modal">Tambah Harga Baru</a>
                    </div><!-- col-12 -->
                </div><!-- row -->
            </div><!-- form-layout -->
        </div><!-- section-wrapper -->
    </div><!-- br-pagebody -->
    <div id="price-form-modal" class="modal fade">
        <div class="modal-dialog modal-dialog-vertical-center" role="document">
            <div class="modal-content bd-0 tx-14">
                <div class="modal-header pd-y-20 pd-x-25">
                    <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">Tabah Harga Baru</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="post" action="{{ route('plan-price.store') }}" data-parsley-validate>
                    @csrf
                    <input type="hidden" id="plan_id" name="plan_id" value="{{ $plan->id }}">
                <div class="modal-body pd-25">
                    <h4 class="lh-3 mg-b-20"><a href="" class="tx-inverse hover-primary">{{ $plan->name }} ({{ $plan->code }})</a></h4>
                    <div class="form-layout form-layout-2">
                        <div class="row no-gutters">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-control-label"><h6>Tanggal Aktif: <span class="tx-danger">*</span></h6></label>
                                    <input class="form-control parsley-error fc-datepicker" type="text" id="active_at" name="active_at" value="{{ old('active_at') }}" placeholder="Input Tanggal Aktif">
                                    @if($errors->has('active_at'))
                                        <ul class="parsley-errors-list filled">
                                            <li class="parsley-required">{{ $errors->first('active_at') }}</li>
                                        </ul>
                                    @endif
                                </div>
                            </div><!-- col-6 -->
                            <div class="col-md-6">
                                <div class="form-group mg-l--1">
                                    <label class="form-control-label"><h6>Harga: <span class="tx-danger">*</span></h6></label>
                                    <input class="form-control parsley-error" type="text" id="price" name="price" value="{{ old('price') }}" placeholder="Input Harga Plan">
                                    @if($errors->has('price'))
                                        <ul class="parsley-errors-list filled">
                                            <li class="parsley-required">{{ $errors->first('price') }}</li>
                                        </ul>
                                    @endif
                                </div>
                            </div><!-- col-6 -->
                            <div class="col-md-6 mg-t--1">
                                <div class="form-group mg-l--1">
                                    <label class="form-control-label"><h6>Harga Stockist: <span class="tx-danger">*</span></h6></label>
                                    <input class="form-control parsley-error" type="text" id="stockist_price" name="stockist_price" value="{{ old('stockist_price') }}" placeholder="Input Harga Stockist">
                                    @if($errors->has('stockist_price'))
                                        <ul class="parsley-errors-list filled">
                                            <li class="parsley-required">{{ $errors->first('stockist_price') }}</li>
                                        </ul>
                                    @endif
                                </div>
                            </div><!-- col-6 -->
                            <div class="col-md-6 mg-t--1">
                                <div class="form-group mg-l--1">
                                    <label class="form-control-label"><h6>Harga Master Stockist: <span class="tx-danger">*</span></h6></label>
                                    <input class="form-control parsley-error" type="text" id="master_stockist_price" name="master_stockist_price" value="{{ old('master_stockist_price') }}" placeholder="Input Harga Master Stockist">
                                    @if($errors->has('master_stockist_price'))
                                        <ul class="parsley-errors-list filled">
                                            <li class="parsley-required">{{ $errors->first('master_stockist_price') }}</li>
                                        </ul>
                                    @endif
                                </div>
                            </div><!-- col-6 -->
                        </div><!-- row -->
                    </div><!-- form-layout -->
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary tx-11 tx-uppercase pd-y-12 pd-x-25 tx-mont tx-medium">Save changes</button>
                    <button type="button" class="btn btn-secondary tx-11 tx-uppercase pd-y-12 pd-x-25 tx-mont tx-medium" data-dismiss="modal">Close</button>
                </div>
                </form>
            </div>
        </div><!-- modal-dialog -->
    </div><!-- modal -->
@endsection

@section('scripts')
    <script src="{{ asset('admin/lib/select2/js/select2.min.js') }}"></script>
    <script src="{{ asset('admin/lib/jquery-toggles/toggles.min.js') }}"></script>
@endsection

@section('page_script')
    <script>
        $(function(){
            'use strict'

            @if($errors->has('active_at'))
                $('#price-form-modal').modal('show');
            @endif

            // Datepicker
            $('.fc-datepicker').datepicker({
                showOtherMonths: true,
                selectOtherMonths: true,
                dateFormat: "yy-mm-dd"
            });

            $('.form-layout .form-control').on('focusin', function(){
                $(this).closest('.form-group').addClass('form-group-active');
            });

            $('.form-layout .form-control').on('focusout', function(){
                $(this).closest('.form-group').removeClass('form-group-active');
            });

            $('.select2').on('select2:opening', function (e) {
                $(this).closest('.form-group').addClass('form-group-active');
            });

            $('.select2').on('select2:closing', function (e) {
                $(this).closest('.form-group').removeClass('form-group-active');
            });

            // Toggles
            $('#toggle-aktif').toggles({
                on: @if($plan->is_active == 1)
                    true
                @else
                    false
                @endif,
                height: 26,
                checkbox: $('#is_active'),
                text: {on:'AKTIF', off:'TIDAK'}
            });

        });
    </script>
@endsection