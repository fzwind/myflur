@extends('layouts.admin')

@section('styles')
    <link href="{{ asset('admin/lib/select2/css/select2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('admin/lib/jquery-toggles/toggles-full.css') }}" rel="stylesheet">
@endsection

@section('content')
    <div class="br-pageheader pd-y-15 pd-l-20">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
            <a class="breadcrumb-item" href="{{ route('home') }}">myflur</a>
            <a class="breadcrumb-item" href="#">admin</a>
            <a class="breadcrumb-item" href="#">settings</a>
            <a class="breadcrumb-item" href="{{ route('stockist-settings.index') }}">stockist setting</a>
            <span class="breadcrumb-item active">tambah setting baru</span>
        </nav>
    </div><!-- br-pageheader -->
    <div class="pd-x-20 pd-sm-x-30 pd-t-20 pd-sm-t-30">
        <div class="pull-left">
            <h4 class="tx-gray-800 mg-b-5">STOCKIST SETTINGS</h4>
            <p class="mg-b-0">Tambah Setting Baru</p>
        </div>
    </div>

    <div class="br-pagebody mg-t-70">
        <div class="br-section-wrapper pd-20">
            <form method="post" action="{{ route('stockist-settings.store') }}" data-parsley-validate>
                @csrf
                <div class="form-layout form-layout-2">
                    <div class="row no-gutters">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="form-control-label"><h6>Plan: <span class="tx-danger">*</span></h6></label>
                                <select id="plan_id" name="plan_id" class="form-control select2-show-search" data-placeholder="--- Pilih PLAN ---">
                                    <option label="--- Pilih PLAN ---"></option>
                                    @foreach($plans as $key => $plan)
                                        @if(old('plan_id')==$key)
                                            <option value="{{ $key }}" selected>{{ $plan }}</option>
                                        @else
                                            <option value="{{ $key }}">{{ $plan }}</option>
                                        @endif
                                    @endforeach
                                </select>
                                @if($errors->has('plan_id'))
                                    <ul class="parsley-errors-list filled">
                                        <li class="parsley-required">{{ $errors->first('plan_id') }}</li>
                                    </ul>
                                @endif
                            </div>
                        </div><!-- col-12 -->
                        <div class="col-md-6">
                            <div class="form-group mg-l--1">
                                <label class="form-control-label"><h6>Tanggal Aktif: <span class="tx-danger">*</span></h6></label>
                                <input class="form-control parsley-error fc-datepicker" type="text" id="active_at" name="active_at" value="{{ old('active_at') }}" placeholder="Input Tanggal Aktif">

                            @if($errors->has('active_at'))
                                    <ul class="parsley-errors-list filled">
                                        <li class="parsley-required">{{ $errors->first('active_at') }}</li>
                                    </ul>
                                @endif
                            </div>
                        </div><!-- col-6 -->
                        <div class="col-md-4 mg-t--1">
                            <div class="form-group mg-md-l--1">
                                <label class="form-control-label"><h6>Member Order Crossline: <span class="tx-danger">*</span></h6></label>
                                <div class="toggle-wrapper">
                                    <div class="toggle toggle-light success" id="toggle-aktif"></div>
                                    <input type="hidden" name="member_buy_crossline" value="0">
                                    <input type="checkbox" id="member_buy_crossline" name="member_buy_crossline" value="1" style="display: none">
                                </div>
                                @if($errors->has('member_buy_crossline'))
                                    <ul class="parsley-errors-list filled">
                                        <li class="parsley-required">{{ $errors->first('member_buy_crossline') }}</li>
                                    </ul>
                                @endif
                            </div>
                        </div><!-- col-4 -->
                        <div class="col-md-4 mg-t--1">
                            <div class="form-group mg-l--1">
                                <label class="form-control-label"><h6>Min. Order Stockist: <span class="tx-danger">*</span></h6></label>
                                <input class="form-control parsley-error" type="text" id="min_order_stockist" name="min_order_stockist" value="{{ old('min_order_stockist') }}" placeholder="Input Min. Order Stockist">
                                @if($errors->has('min_order_stockist'))
                                    <ul class="parsley-errors-list filled">
                                        <li class="parsley-required">{{ $errors->first('min_order_stockist') }}</li>
                                    </ul>
                                @endif
                            </div>
                        </div><!-- col-4 -->
                        <div class="col-md-4 mg-t--1">
                            <div class="form-group mg-l--1">
                                <label class="form-control-label"><h6>Min. Order Master Stockist: <span class="tx-danger">*</span></h6></label>
                                <input class="form-control parsley-error" type="text" id="min_order_master_stockist" name="min_order_master_stockist" value="{{ old('min_order_master_stockist') }}" placeholder="Input Min. Order Master Stockist">
                                @if($errors->has('min_order_master_stockist'))
                                    <ul class="parsley-errors-list filled">
                                        <li class="parsley-required">{{ $errors->first('min_order_master_stockist') }}</li>
                                    </ul>
                                @endif
                            </div>
                        </div><!-- col-4 -->
                    </div><!-- row -->
                    <div class="form-layout-footer bd pd-20 bd-t-0">
                        <button type="submit" class="btn btn-info">Save</button>
                        <a class="btn btn-secondary" href="{{ route('stockist-settings.index') }}">Cancel</a>
                        <button type="button" class="btn btn-outline-primary btn-icon rounded-circle tx-34-force pd-0-force bd-0 pull-right"
                                data-container="body"
                                data-toggle="popover"
                                data-popover-color="default"
                                data-placement="top"
                                data-html="true"
                                title="Help"
                                data-content="Under Construction"><div><i class="fa fa-question-circle-o"></i></div></button>
                    </div><!-- form-group -->
                </div><!-- form-layout -->
            </form>
        </div><!-- section-wrapper -->
    </div><!-- br-pagebody -->
@endsection

@section('scripts')
    <script src="{{ asset('admin/lib/select2/js/select2.min.js') }}"></script>
    <script src="{{ asset('admin/lib/jquery-toggles/toggles.min.js') }}"></script>
@endsection

@section('page_script')
    <script>
        $(function(){
            'use strict'

            $('.fc-datepicker').datepicker({
                showOtherMonths: true,
                selectOtherMonths: true,
                dateFormat: "yy-mm-dd"
            });

            $('.form-layout .form-control').on('focusin', function(){
                $(this).closest('.form-group').addClass('form-group-active');
            });

            $('.form-layout .form-control').on('focusout', function(){
                $(this).closest('.form-group').removeClass('form-group-active');
            });

            $('.select2').on('select2:opening', function (e) {
                $(this).closest('.form-group').addClass('form-group-active');
            });

            $('.select2').on('select2:closing', function (e) {
                $(this).closest('.form-group').removeClass('form-group-active');
            });

            // Toggles
            $('#toggle-aktif').toggles({
                on: @if(old('member_buy_crossline') == 1)
                    true
                @else
                    false
                @endif,
                height: 26,
                width: 120,
                checkbox: $('#member_buy_crossline'),
                text: {on:'ALLOWED', off:'NOT ALLOWED'}
            });

        });
    </script>
@endsection