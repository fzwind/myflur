@extends('layouts.admin')

@section('styles')
    <link href="{{ asset('admin/lib/select2/css/select2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('admin/lib/jquery-toggles/toggles-full.css') }}" rel="stylesheet">
    <style>
        .show {
            display: block !important;
        }
        .hidden {
            display: none !important;
            visibility: hidden !important;
        }
        .invisible {
            visibility: hidden;
        }    </style>
@endsection

@section('content')
    <div class="br-pageheader pd-y-15 pd-l-20">
        <nav class="breadcrumb pd-0 mg-0 tx-12">
            <a class="breadcrumb-item" href="{{ route('home') }}">myflur</a>
            <a class="breadcrumb-item" href="#">admin</a>
            <a class="breadcrumb-item" href="#">settings</a>
            <a class="breadcrumb-item" href="{{ route('bonus-settings.index') }}">bonus setting</a>
            <span class="breadcrumb-item active">view bonus setting</span>
        </nav>
    </div><!-- br-pageheader -->
    <div class="pd-x-20 pd-sm-x-30 pd-t-20 pd-sm-t-30">
        <div class="pull-left">
            <h4 class="tx-gray-800 mg-b-5">BONUS SETTINGS</h4>
            <p class="mg-b-0">View Bonus Setting</p>
        </div>
    </div>

    <div class="br-pagebody mg-t-70">
        <div class="br-section-wrapper pd-20">
            <div class="form-layout form-layout-2">
                <div class="row no-gutters">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="form-control-label"><h6>Plan:</h6></label>
                            <div class="tx-bold tx-black">{{ $bonus_setting->plan->code }} - {{ $bonus_setting->plan->name }}</div>
                        </div>
                    </div><!-- col-4 -->
                    <div class="col-md-8">
                        <div class="form-group mg-l--1">
                            <label class="form-control-label"><h6>Tanggal Aktif:</h6></label>
                            <div class="tx-bold tx-black">{{ to_utz($bonus_setting->active_at) }}</div>
                        </div>
                    </div>
                </div><!-- row -->

                @include('admin.bonus-setting.partials.bonus-nominal-show')

                @include('admin.bonus-setting.partials.bonus-pairing-show')

                @include('admin.bonus-setting.partials.bonus-pairing-level-show')

                @include('admin.bonus-setting.partials.bonus-level-show')

                <div class="form-layout-footer bd pd-20 bd-t-0">
                    <a class="btn btn-secondary" href="{{ route('bonus-settings.index') }}">Back</a>
                </div><!-- form-group -->
            </div><!-- form-layout -->
        </div><!-- section-wrapper -->
    </div><!-- br-pagebody -->
@endsection

@section('scripts')
    <script src="{{ asset('admin/lib/select2/js/select2.min.js') }}"></script>
    <script src="{{ asset('admin/lib/jquery-toggles/toggles.min.js') }}"></script>
@endsection

@section('page_script')
@endsection