@system('bonus_level')
<div class="row no-gutters">
    <div class="col-md-12 mg-t--1">
        <div class="form-group form-group-active"><h6 class="text-info">BONUS LEVEL</h6></div>
    </div>
</div><!-- row -->

<div class="row no-gutters">
    <div class="col-12 mg-t--1">
        <div class="form-group">
            <label class="form-control-label"><h6>Jumlah Level:<span class="tx-danger">*</span></h6></label>
            <input class="form-control parsley-error" type="text" id="max_level_bonus_level" name="max_level_bonus_level" value="{{ old('max_level_bonus_level') }}" placeholder="Input jumlah maks. level">
            @if($errors->has('max_level_bonus_level'))
                <ul class="parsley-errors-list filled">
                    <li class="parsley-required">{{ $errors->first('max_level_bonus_level') }}</li>
                </ul>
            @endif
        </div>
    </div><!-- col-12 -->
</div><!-- row -->
<div class="row no-gutters hidden" id="level-1">
    <div class="col-12 mg-t--1">
        <div class="form-group">
            <label class="form-control-label"><h6>Nominal Bonus Level 1:<span class="tx-danger">*</span></h6></label>
            <input class="form-control parsley-error" type="text" id="bonus_level_amount_1" name="bonus_level_amount_1" value="{{ old('bonus_level_amount_1') }}" placeholder="Input nominal bonus level 1" disabled>
            @if($errors->has('bonus_level_amount_1'))
                <ul class="parsley-errors-list filled">
                    <li class="parsley-required">{{ $errors->first('bonus_level_amount_1') }}</li>
                </ul>
            @endif
        </div>
    </div><!-- col-12 -->
</div><!-- row -->
<div class="row no-gutters hidden" id="level-2">
    <div class="col-12 mg-t--1">
        <div class="form-group">
            <label class="form-control-label"><h6>Nominal Bonus Level 2:<span class="tx-danger">*</span></h6></label>
            <input class="form-control parsley-error" type="text" id="bonus_level_amount_2" name="bonus_level_amount_2" value="{{ old('bonus_level_amount_2') }}" placeholder="Input nominal bonus level 2" disabled>
            @if($errors->has('bonus_level_amount_2'))
                <ul class="parsley-errors-list filled">
                    <li class="parsley-required">{{ $errors->first('bonus_level_amount_2') }}</li>
                </ul>
            @endif
        </div>
    </div><!-- col-12 -->
</div><!-- row -->
<div class="row no-gutters hidden" id="level-3">
    <div class="col-12 mg-t--1">
        <div class="form-group">
            <label class="form-control-label"><h6>Nominal Bonus Level 3:<span class="tx-danger">*</span></h6></label>
            <input class="form-control parsley-error" type="text" id="bonus_level_amount_3" name="bonus_level_amount_3" value="{{ old('bonus_level_amount_3') }}" placeholder="Input nominal bonus level 3" disabled>
            @if($errors->has('bonus_level_amount_3'))
                <ul class="parsley-errors-list filled">
                    <li class="parsley-required">{{ $errors->first('bonus_level_amount_3') }}</li>
                </ul>
            @endif
        </div>
    </div><!-- col-12 -->
</div><!-- row -->
<div class="row no-gutters hidden" id="level-4">
    <div class="col-12 mg-t--1">
        <div class="form-group">
            <label class="form-control-label"><h6>Nominal Bonus Level 4:<span class="tx-danger">*</span></h6></label>
            <input class="form-control parsley-error" type="text" id="bonus_level_amount_4" name="bonus_level_amount_4" value="{{ old('bonus_level_amount_4') }}" placeholder="Input nominal bonus level 4" disabled>
            @if($errors->has('bonus_level_amount_4'))
                <ul class="parsley-errors-list filled">
                    <li class="parsley-required">{{ $errors->first('bonus_level_amount_4') }}</li>
                </ul>
            @endif
        </div>
    </div><!-- col-12 -->
</div><!-- row -->
<div class="row no-gutters hidden" id="level-5">
    <div class="col-12 mg-t--1">
        <div class="form-group">
            <label class="form-control-label"><h6>Nominal Bonus Level 5:<span class="tx-danger">*</span></h6></label>
            <input class="form-control parsley-error" type="text" id="bonus_level_amount_5" name="bonus_level_amount_5" value="{{ old('bonus_level_amount_5') }}" placeholder="Input nominal bonus level 5" disabled>
            @if($errors->has('bonus_level_amount_5'))
                <ul class="parsley-errors-list filled">
                    <li class="parsley-required">{{ $errors->first('bonus_level_amount_5') }}</li>
                </ul>
            @endif
        </div>
    </div><!-- col-12 -->
</div><!-- row -->
<div class="row no-gutters hidden" id="level-6">
    <div class="col-12 mg-t--1">
        <div class="form-group">
            <label class="form-control-label"><h6>Nominal Bonus Level 6:<span class="tx-danger">*</span></h6></label>
            <input class="form-control parsley-error" type="text" id="bonus_level_amount_6" name="bonus_level_amount_6" value="{{ old('bonus_level_amount_6') }}" placeholder="Input nominal bonus level 6" disabled>
            @if($errors->has('bonus_level_amount_6'))
                <ul class="parsley-errors-list filled">
                    <li class="parsley-required">{{ $errors->first('bonus_level_amount_6') }}</li>
                </ul>
            @endif
        </div>
    </div><!-- col-12 -->
</div><!-- row -->
<div class="row no-gutters hidden" id="level-7">
    <div class="col-12 mg-t--1">
        <div class="form-group">
            <label class="form-control-label"><h6>Nominal Bonus Level 7:<span class="tx-danger">*</span></h6></label>
            <input class="form-control parsley-error" type="text" id="bonus_level_amount_7" name="bonus_level_amount_7" value="{{ old('bonus_level_amount_7') }}" placeholder="Input nominal bonus level 7" disabled>
            @if($errors->has('bonus_level_amount_7'))
                <ul class="parsley-errors-list filled">
                    <li class="parsley-required">{{ $errors->first('bonus_level_amount_7') }}</li>
                </ul>
            @endif
        </div>
    </div><!-- col-12 -->
</div><!-- row -->
<div class="row no-gutters hidden" id="level-8">
    <div class="col-12 mg-t--1">
        <div class="form-group">
            <label class="form-control-label"><h6>Nominal Bonus Level 8:<span class="tx-danger">*</span></h6></label>
            <input class="form-control parsley-error" type="text" id="bonus_level_amount_8" name="bonus_level_amount_8" value="{{ old('bonus_level_amount_8') }}" placeholder="Input nominal bonus level 8" disabled>
            @if($errors->has('bonus_level_amount_8'))
                <ul class="parsley-errors-list filled">
                    <li class="parsley-required">{{ $errors->first('bonus_level_amount_8') }}</li>
                </ul>
            @endif
        </div>
    </div><!-- col-12 -->
</div><!-- row -->
<div class="row no-gutters hidden" id="level-9">
    <div class="col-12 mg-t--1">
        <div class="form-group">
            <label class="form-control-label"><h6>Nominal Bonus Level 9:<span class="tx-danger">*</span></h6></label>
            <input class="form-control parsley-error" type="text" id="bonus_level_amount_9" name="bonus_level_amount_9" value="{{ old('bonus_level_amount_9') }}" placeholder="Input nominal bonus level 9" disabled>
            @if($errors->has('bonus_level_amount_9'))
                <ul class="parsley-errors-list filled">
                    <li class="parsley-required">{{ $errors->first('bonus_level_amount_9') }}</li>
                </ul>
            @endif
        </div>
    </div><!-- col-12 -->
</div><!-- row -->
<div class="row no-gutters hidden" id="level-10">
    <div class="col-12 mg-t--1">
        <div class="form-group">
            <label class="form-control-label"><h6>Nominal Bonus Level 10:<span class="tx-danger">*</span></h6></label>
            <input class="form-control parsley-error" type="text" id="bonus_level_amount_10" name="bonus_level_amount_10" value="{{ old('bonus_level_amount_10') }}" placeholder="Input nominal bonus level 10" disabled>
            @if($errors->has('bonus_level_amount_10'))
                <ul class="parsley-errors-list filled">
                    <li class="parsley-required">{{ $errors->first('bonus_level_amount_10') }}</li>
                </ul>
            @endif
        </div>
    </div><!-- col-12 -->
</div><!-- row -->
@endsystem
