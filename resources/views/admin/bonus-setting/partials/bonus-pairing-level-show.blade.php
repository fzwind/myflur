@system('bonus_pairing_level')
<div class="row no-gutters">
    <div class="col-md-12 mg-t--1">
        <div class="form-group form-group-active"><h6 class="text-info">BONUS PAIRING LEVEL</h6></div>
    </div>
</div><!-- row -->

<div class="row no-gutters">
    <div class="col-12 mg-t--1">
        <div class="form-group">
            <label class="form-control-label"><h6>Jumlah Pairing / Level:</h6></label>
            <div class="tx-bold tx-black">
                {{ $bonus_setting->pair_per_level }}
            </div>
        </div>
    </div><!-- col-12 -->
</div><!-- row -->
@endsystem
