@system('bonus_pairing')
<div class="row no-gutters">
    <div class="col-md-12 mg-t--1">
        <div class="form-group form-group-active"><h6 class="text-info">BONUS PAIRING</h6></div>
    </div>
</div><!-- row -->

<div class="row no-gutters">
    <div class="col-6 mg-t--1">
        <div class="form-group">
            <label class="form-control-label"><h6>Dasar Pairing:</h6></label>
            <div class="tx-bold tx-black">
                {{ $bonus_setting->max_pairing_by==1 ? 'Jumlah Pasangan' : $bonus_setting->max_pairing_by==2 ? 'Nilai Total Nominal Pasangan' : $bonus_setting->max_pairing_by==3 ? 'Jumlah Pasangan + Nilai Total Nominal Pasangan (Mana Tercapai Lebih Dulu)' : '' }}
            </div>
        </div>
    </div><!-- col-6 -->
    <div class="col-6 mg-t--1">
        <div class="form-group mg-l--1">
            <label class="form-control-label"><h6>Over Pairing:</h6></label>
            <div class="tx-bold tx-black">
                {{ $bonus_setting->over_pairing_handle==1 ? 'Carry Forward' : $bonus_setting->over_pairing_handle==2 ? 'Flush' : '' }}
            </div>
        </div>
    </div><!-- col-6 -->
    <div class="col-6 mg-t--1">
        <div class="form-group">
            <label class="form-control-label"><h6>Maks. Jumlah Pairing:</h6></label>
            <div class="tx-bold tx-black">
                {{ $bonus_setting->max_pairing_per_day }}
            </div>
        </div>
    </div><!-- col-6 -->
    <div class="col-6 mg-t--1">
        <div class="form-group mg-l--1">
            <label class="form-control-label"><h6>Maks. Total Nominal Pairing:</h6></label>
            <div class="tx-bold tx-black">
                Rp {{ number_format($bonus_setting->max_pairing_amount_per_day,0,',','.') }}
            </div>
        </div>
    </div><!-- col-6 -->
</div><!-- row -->
@endsystem
