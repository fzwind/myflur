<div class="row no-gutters">
    <div class="col-md-12 mg-t--1">
        <div class="form-group form-group-active"><h6 class="text-info">NOMINAL</h6></div>
    </div>
</div><!-- row -->

<div class="row no-gutters">
	<?php
	$column = 0;
	$maxcolumn = 3;
	?>
    @system('bonus_sponsor')
    <div class="col-md-{{ 12/$maxcolumn }} mg-t--1">
        <div class="form-group {{ ($column % $maxcolumn == 0) ? '' : 'mg-l--1' }}">
            <label class="form-control-label"><h6>Bonus Sponsor:<span class="tx-danger">*</span></h6></label>
            <input class="form-control parsley-error" type="text" id="bonus_sponsor_amount" name="bonus_sponsor_amount" value="{{ $bonus_setting->bonus_sponsor_amount ?? old('bonus_sponsor_amount') }}" placeholder="Input Nominal Bonus Sponsor">
            @if($errors->has('bonus_sponsor_amount'))
                <ul class="parsley-errors-list filled">
                    <li class="parsley-required">{{ $errors->first('bonus_sponsor_amount') }}</li>
                </ul>
            @endif
        </div>
    </div><!-- col-{{ 12/$maxcolumn }} -->
	<?php
	$column++;
	?>
    @endsystem

    @system('bonus_pairing')
    <div class="col-md-{{ 12/$maxcolumn }} mg-t--1">
        <div class="form-group {{ ($column % $maxcolumn == 0) ? '' : 'mg-l--1' }}">
            <label class="form-control-label"><h6>Bonus Pairing:<span class="tx-danger">*</span></h6></label>
            <input class="form-control parsley-error" type="text" id="bonus_pairing_amount" name="bonus_pairing_amount" value="{{ $bonus_setting->bonus_pairing_amount ?? old('bonus_pairing_amount') }}" placeholder="Input Nominal Bonus Pairing">
            @if($errors->has('bonus_pairing_amount'))
                <ul class="parsley-errors-list filled">
                    <li class="parsley-required">{{ $errors->first('bonus_pairing_amount') }}</li>
                </ul>
            @endif
        </div>
    </div><!-- col-{{ 12/$maxcolumn }} -->
	<?php
	$column++;
	?>
    @endsystem

    @system('bonus_pairing_level')
    <div class="col-md-{{ 12/$maxcolumn }} mg-t--1">
        <div class="form-group {{ ($column % $maxcolumn == 0) ? '' : 'mg-l--1' }}">
            <label class="form-control-label"><h6>Bonus Pairing Level:<span class="tx-danger">*</span></h6></label>
            <input class="form-control parsley-error" type="text" id="bonus_pairing_level_amount" name="bonus_pairing_level_amount" value="{{ $bonus_setting->bonus_pairing_level_amount ?? old('bonus_pairing_level_amount') }}" placeholder="Input Nominal Bonus Pairing Level">
            @if($errors->has('bonus_pairing_level_amount'))
                <ul class="parsley-errors-list filled">
                    <li class="parsley-required">{{ $errors->first('bonus_pairing_level_amount') }}</li>
                </ul>
            @endif
        </div>
    </div><!-- col-{{ 12/$maxcolumn }} -->
	<?php
	$column++;
	?>
    @endsystem

    @for($i = $column + 1; $i <= $maxcolumn; $i++)
        <div class="col-md-{{ 12/$maxcolumn }} mg-t--1">
            <div class="form-group  {{ ($column % $maxcolumn == 0) ? '' : 'mg-l--1' }}"></div>
        </div><!-- col-{{ 12/$maxcolumn }} -->
    @endfor
</div><!-- row -->
