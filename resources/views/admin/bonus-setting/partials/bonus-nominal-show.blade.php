<div class="row no-gutters">
    <div class="col-md-12 mg-t--1">
        <div class="form-group form-group-active"><h6 class="text-info">NOMINAL</h6></div>
    </div>
</div><!-- row -->

<div class="row no-gutters">
	<?php
	$column = 0;
	$maxcolumn = 3;
	?>
    @system('bonus_sponsor')
    <div class="col-md-{{ 12/$maxcolumn }} mg-t--1">
        <div class="form-group {{ ($column % $maxcolumn == 0) ? '' : 'mg-l--1' }}">
            <label class="form-control-label"><h6>Bonus Sponsor:</h6></label>
            <div class="tx-bold tx-black">Rp {{ number_format($bonus_setting->bonus_sponsor_amount,0,',','.') }}</div>
        </div>
    </div><!-- col-{{ 12/$maxcolumn }} -->
	<?php
	$column++;
	?>
    @endsystem

    @system('bonus_pairing')
    <div class="col-md-{{ 12/$maxcolumn }} mg-t--1">
        <div class="form-group {{ ($column % $maxcolumn == 0) ? '' : 'mg-l--1' }}">
            <label class="form-control-label"><h6>Bonus Pairing:</h6></label>
            <div class="tx-bold tx-black">Rp {{ number_format($bonus_setting->bonus_pairing_amount,0,',','.') }}</div>
        </div>
    </div><!-- col-{{ 12/$maxcolumn }} -->
	<?php
	$column++;
	?>
    @endsystem

    @system('bonus_pairing_level')
    <div class="col-md-{{ 12/$maxcolumn }} mg-t--1">
        <div class="form-group {{ ($column % $maxcolumn == 0) ? '' : 'mg-l--1' }}">
            <label class="form-control-label"><h6>Bonus Pairing Level:</h6></label>
            <div class="tx-bold tx-black">Rp {{ number_format($bonus_setting->bonus_pairing_level_amount,0,',','.') }}</div>
        </div>
    </div><!-- col-{{ 12/$maxcolumn }} -->
	<?php
	$column++;
	?>
    @endsystem

    @for($i = $column + 1; $i <= $maxcolumn; $i++)
        <div class="col-md-{{ 12/$maxcolumn }} mg-t--1">
            <div class="form-group  {{ ($column % $maxcolumn == 0) ? '' : 'mg-l--1' }}"></div>
        </div><!-- col-{{ 12/$maxcolumn }} -->
    @endfor
</div><!-- row -->
