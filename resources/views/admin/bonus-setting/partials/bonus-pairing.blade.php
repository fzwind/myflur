@system('bonus_pairing')
<div class="row no-gutters">
    <div class="col-md-12 mg-t--1">
        <div class="form-group form-group-active"><h6 class="text-info">BONUS PAIRING</h6></div>
    </div>
</div><!-- row -->

<div class="row no-gutters">
    <div class="col-6 mg-t--1">
        <div class="form-group">
            <label class="form-control-label"><h6>Dasar Pairing:<span class="tx-danger">*</span></h6></label>
            <select id="max_pairing_by" name="max_pairing_by" class="form-control select2" data-placeholder="--- Pilih Dasar Pairing ---">
                <option label="--- Pilih Dasar Pairing ---"></option>
                <option value="1" {{ (($bonus_setting->max_pairing_by ?? old('max_pairing_by'))==1) ? 'selected' : '' }}>Jumlah Pasangan</option>
                <option value="2" {{ (($bonus_setting->max_pairing_by ?? old('max_pairing_by'))==2) ? 'selected' : '' }}>Nilai Total Nominal Pasangan</option>
                <option value="3" {{ (($bonus_setting->max_pairing_by ?? old('max_pairing_by'))==3) ? 'selected' : '' }}>Keduanya (Mana Tercapai Lebih Dulu)</option>
            </select>
            @if($errors->has('max_pairing_by'))
                <ul class="parsley-errors-list filled">
                    <li class="parsley-required">{{ $errors->first('max_pairing_by') }}</li>
                </ul>
            @endif
        </div>
    </div><!-- col-6 -->
    <div class="col-6 mg-t--1">
        <div class="form-group mg-l--1">
            <label class="form-control-label"><h6>Over Pairing:<span class="tx-danger">*</span></h6></label>
            <select id="over_pairing_handle" name="over_pairing_handle" class="form-control select2" data-placeholder="--- Pilih ---">
                <option label="--- Pilih ---"></option>
                <option value="1" {{ (($bonus_setting->over_pairing_handle ?? old('over_pairing_handle'))==1) ? 'selected' : '' }}>Carry Forward</option>
                <option value="2" {{ (($bonus_setting->over_pairing_handle ?? old('over_pairing_handle'))==2) ? 'selected' : '' }}>Flush</option>
            </select>
            @if($errors->has('over_pairing_handle'))
                <ul class="parsley-errors-list filled">
                    <li class="parsley-required">{{ $errors->first('over_pairing_handle') }}</li>
                </ul>
            @endif
        </div>
    </div><!-- col-6 -->
    <div class="col-6 mg-t--1">
        <div class="form-group">
            <label class="form-control-label"><h6>Maks. Jumlah Pairing:<span class="tx-danger">*</span></h6></label>
            <input class="form-control parsley-error" type="text" id="max_pairing_per_day" name="max_pairing_per_day" value="{{ $bonus_setting->max_pairing_per_day ?? old('max_pairing_per_day') }}" placeholder="Input maks. jumlah pairing/hari">
            @if($errors->has('max_pairing_per_day'))
                <ul class="parsley-errors-list filled">
                    <li class="parsley-required">{{ $errors->first('max_pairing_per_day') }}</li>
                </ul>
            @endif
        </div>
    </div><!-- col-6 -->
    <div class="col-6 mg-t--1">
        <div class="form-group mg-l--1">
            <label class="form-control-label"><h6>Maks. Total Nominal Pairing:<span class="tx-danger">*</span></h6></label>
            <input class="form-control parsley-error" type="text" id="max_pairing_amount_per_day" name="max_pairing_amount_per_day" value="{{ $bonus_setting->max_pairing_amount_per_day ?? old('max_pairing_amount_per_day') }}" placeholder="Input maks. total nominal pairing/hari">
            @if($errors->has('max_pairing_amount_per_day'))
                <ul class="parsley-errors-list filled">
                    <li class="parsley-required">{{ $errors->first('max_pairing_amount_per_day') }}</li>
                </ul>
            @endif
        </div>
    </div><!-- col-6 -->
</div><!-- row -->
@endsystem
