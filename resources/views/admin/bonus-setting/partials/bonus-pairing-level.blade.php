@system('bonus_pairing_level')
<div class="row no-gutters">
    <div class="col-md-12 mg-t--1">
        <div class="form-group form-group-active"><h6 class="text-info">BONUS PAIRING LEVEL</h6></div>
    </div>
</div><!-- row -->

<div class="row no-gutters">
    <div class="col-12 mg-t--1">
        <div class="form-group">
            <label class="form-control-label"><h6>Jumlah Pairing / Level:<span class="tx-danger">*</span></h6></label>
            <input class="form-control parsley-error" type="text" id="pair_per_level" name="pair_per_level" value="{{ $bonus_setting->pair_per_level ?? old('pair_per_level') }}" placeholder="Input jumlah pairing/level">
            @if($errors->has('pair_per_level'))
                <ul class="parsley-errors-list filled">
                    <li class="parsley-required">{{ $errors->first('pair_per_level') }}</li>
                </ul>
            @endif
        </div>
    </div><!-- col-12 -->
</div><!-- row -->
@endsystem
