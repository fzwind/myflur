@system('bonus_level')
<div class="row no-gutters">
    <div class="col-md-12 mg-t--1">
        <div class="form-group form-group-active"><h6 class="text-info">BONUS LEVEL</h6></div>
    </div>
</div><!-- row -->

<div class="row no-gutters">
    <div class="col-12 mg-t--1">
        <div class="form-group">
            <label class="form-control-label"><h6>Jumlah Level:</h6></label>
            <div class="tx-bold tx-black">
                {{ $bonus_setting->max_level_bonus_level }}
            </div>
        </div>
    </div><!-- col-12 -->
</div><!-- row -->

{{--
@if(isset($bonus_level_setting))
@foreach($bonus_level_setting)
    <div class="row no-gutters hidden" id="level-1">
        <div class="col-12 mg-t--1">
            <div class="form-group">
                <label class="form-control-label"><h6>Nominal Bonus Level {{ $bonus_level_setting->level }}:</h6></label>
                <div class="tx-bold text-primary">
                    Rp {{ number_format($bonus_level_setting->bonus_level_amount,0,',','.') }}
                </div>
            </div>
        </div><!-- col-12 -->
    </div><!-- row -->
@endforeach
@endif
--}}
@endsystem
