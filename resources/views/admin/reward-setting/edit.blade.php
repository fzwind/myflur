@extends('layouts.admin')

@section('styles')
    <link href="{{ asset('admin/css/vendor/switchery.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
    <style>
        /*.ui-datepicker-div{ z-index:99999 !important; }*/
        .modal-open .ui-datepicker{ z-index:99999 !important; }
    </style>

    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <h4 class="page-title">Edit Plan</h4>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <form method="post" action="{{ route('plan-settings.update', ['plan_setting' => $plan]) }}">
                @csrf
                <div class="card">
                    <div class="card-body">
                        <div class="row mb-2">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="code" class="font-weight-bold font-15">Kode Plan:</label>
                                    <div>
                                        <input class="form-control" type="text" id="code" name="code" value="{{ $plan->code ?? old('code') }}" placeholder="Input Kode Plan">
                                        @if($errors->has('code'))
                                            <small class="form-text text-danger">{{ $errors->first('code') }}</small>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="name" class="font-weight-bold font-15">Nama Plan:</label>
                                    <div>
                                        <input class="form-control" type="text" id="name" name="name" value="{{ $plan->name ?? old('name')  }}" placeholder="Input Nama Plan">
                                        @if($errors->has('name'))
                                            <small class="form-text text-danger">{{ $errors->first('name') }}</small>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="network_type" class="font-weight-bold font-15">Aktif / Non-Aktif:</label>
                                    <div>
                                        <input type="hidden" name="is_active" value="0">
                                        <input type="checkbox" {{ ($plan->is_active == 1 || old('is_active')) ? 'checked' : '' }} id="is_active" name="is_active" value="1" data-plugin="switchery" data-color="#3db9dc"/>
                                        @if($errors->has('is_active'))
                                            <small class="form-text text-danger">{{ $errors->first('is_active') }}</small>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-info">Save</button>
                        <a class="btn btn-secondary" href="{{ route('plan-settings.index') }}">Cancel</a>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">History Harga</h5>
                    <div class="row mb-2">
                        <div class="col-md-12">
                            <table class="table dt-responsive nowrap">
                                <thead>
                                <tr>
                                    <th colspan="3" class="text-center">Harga</th>
                                    <th rowspan="2" class="text-center">Tanggal Aktif</th>
                                    <th rowspan="2" class="text-center">Status</th>
                                    <th rowspan="2" class="text-center"></th>
                                </tr>
                                <tr>
                                    <th class="text-center">Member</th>
                                    <th class="text-center">Stockist</th>
                                    <th class="text-center">Master Stockist</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($plan_prices as $row)
			                        <?php
			                        ($active_price_id == $row->id) ? $status = '<span class="badge badge-success">Aktif</span>' : $status = '<span class="badge badge-secondary">Non-Aktif</span>';
			                        ($active_price_id == $row->id) ? $class = 'table-info' : $class = '';
			                        ($active_price_id == $row->id) ? $activeclass = 'text-primary' : $activeclass = '';
			                        ?>
                                    <tr class="{{ $class }}">
                                        <td class="{{ $activeclass }}">Rp {{ number_format($row->price,0,',','.') }}</td>
                                        <td class="{{ $activeclass }}">Rp {{ number_format($row->stockist_price,0,',','.') }}</td>
                                        <td class="{{ $activeclass }}">Rp {{ number_format($row->master_stockist_price,0,',','.') }}</td>
                                        <td class="{{ $activeclass }}">{{ to_utz($row->active_at) }}</td>
                                        <td class="{{ $activeclass }}">{!! $status !!}</td>
                                        @if($status != 'aktif' && $row->active_at > now('UTC'))
                                            <td class="text-center">
                                                <form method="post" action="{{ route('plan-price.destroy',['plan_price' => $row]) }}" onsubmit="return confirmSubmit(this)">
                                                    @csrf
                                                    <input type="hidden" name="_method" value="DELETE">
                                                    <input type="submit" class="btn btn-sm btn-outline-danger" value="delete">
                                                </form>
                                            </td>
                                        @else
                                            <td></td>
                                        @endif
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <a href="#" class="btn btn-sm btn-outline-info" data-toggle="modal" data-target="#price-form-modal">Tambah Harga Baru</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="price-form-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Tambah Harga Baru</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <form method="post" action="{{ route('plan-price.store') }}" data-parsley-validate>
                    @csrf
                    <input type="hidden" id="plan_id" name="plan_id" value="{{ $plan->id }}">
                    <div class="modal-body">
                        <h4 class="lh-3 mg-b-20"><a href="" class="tx-inverse hover-primary">{{ $plan->name }} ({{ $plan->code }})</a></h4>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="active_at" class="font-weight-bold font-15">Tanggal Aktif:</label>
                                    <div>
                                        <input class="form-control date" type="text" id="active_at" name="active_at" value="{{ old('active_at') }}" placeholder="Input Tanggal Aktif" data-toggle="date-picker" data-single-date-picker="true">
                                        @if($errors->has('active_at'))
                                            <small class="form-text text-danger">{{ $errors->first('active_at') }}</small>
                                        @endif
                                    </div>
                                </div>
                            </div><!-- col-6 -->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="price" class="font-weight-bold font-15">Harga Member:</label>
                                    <div>
                                        <input class="form-control" type="text" id="price" name="price" value="{{ old('price') }}" placeholder="Input Harga Member">
                                        @if($errors->has('price'))
                                            <small class="form-text text-danger">{{ $errors->first('price') }}</small>
                                        @endif
                                    </div>
                                </div>
                            </div><!-- col-6 -->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="stockist_price" class="font-weight-bold font-15">Harga Stockist:</label>
                                    <div>
                                        <input class="form-control" type="text" id="stockist_price" name="stockist_price" value="{{ old('stockist_price') }}" placeholder="Input Harga Stockist">
                                        @if($errors->has('stockist_price'))
                                            <small class="form-text text-danger">{{ $errors->first('stockist_price') }}</small>
                                        @endif
                                    </div>
                                </div>
                            </div><!-- col-6 -->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="master_stockist_price" class="font-weight-bold font-15">Harga Master Stockist:</label>
                                    <div>
                                        <input class="form-control" type="text" id="master_stockist_price" name="master_stockist_price" value="{{ old('price') }}" placeholder="Input Harga Master Stockist">
                                        @if($errors->has('master_stockist_price'))
                                            <small class="form-text text-danger">{{ $errors->first('master_stockist_price') }}</small>
                                        @endif
                                    </div>
                                </div>
                            </div><!-- col-6 -->
                        </div><!-- row -->
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary tx-11 tx-uppercase pd-y-12 pd-x-25 tx-mont tx-medium">Save</button>
                        <button type="button" class="btn btn-secondary tx-11 tx-uppercase pd-y-12 pd-x-25 tx-mont tx-medium" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div><!-- modal-dialog -->
    </div><!-- modal -->
@endsection

@section('scripts')
    <script src="{{ asset('admin/js/vendor/switchery.min.js') }}"></script>
@endsection

@section('page_script')
    <script>
        !function($) {
            "use strict";

            var Components = function() {};

            //switch
            Components.prototype.initSwitchery = function() {
                $('[data-plugin="switchery"]').each(function (idx, obj) {
                    new Switchery($(this)[0], $(this).data());
                });
            },

                //initilizing
                Components.prototype.init = function() {
                    var $this = this;
                    this.initSwitchery()
                },

                $.Components = new Components, $.Components.Constructor = Components

        }(window.jQuery),

        //initializing main application module
        function($) {
            "use strict";
            $.Components.init();
        }(window.jQuery);

        function confirmSubmit(form) {
            return confirm('Yakin akan menghapus data harga Plan ?');
        }



        @if(count($errors) > 0)
            <?php
                $err = '';
                $i = 1;
	        ?>
            @foreach ($errors->all() as $error)
                <?php
                    $err .= $i. ') '. $error .' ';
                    $i++;
                ?>
            @endforeach
            toastr.options = {
                "closeButton": true,
                "debug": false,
                "newestOnTop": false,
                "progressBar": true,
                "positionClass": "toast-bottom-right",
                "preventDuplicates": false,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "7000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut",
                "allowHtml": true,
                "escapeHtml": false
            }
            toastr["error"]("{{ $err }}", "Error!");
        @endif

        @if (session('success'))
            toastr.options = {
                "closeButton": true,
                "debug": false,
                "newestOnTop": false,
                "progressBar": true,
                "positionClass": "toast-bottom-right",
                "preventDuplicates": false,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            }
            toastr["success"]("{{ session('success') }}", "Success!");
        @endif

        </script>
@endsection