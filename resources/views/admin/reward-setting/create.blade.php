@extends('layouts.admin')

@section('styles')
    <link href="{{ asset('admin/lib/select2/css/select2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('admin/lib/jquery-toggles/toggles-full.css') }}" rel="stylesheet">
@endsection

@section('content')
    <div class="br-pageheader pd-y-15 pd-l-20">
    </div><!-- br-pageheader -->
    <div class="pd-x-20 pd-sm-x-30 pd-t-20 pd-sm-t-30">
        <div class="pull-left">
            <h4 class="tx-gray-800 mg-b-5">Tambah Reward Setting</h4>
        </div>
    </div>

    <div class="br-pagebody mg-t-70">
        <div class="br-section-wrapper pd-20">
            <form method="post" action="{{ route('reward-settings.store') }}" data-parsley-validate>
                @csrf
                <div class="form-layout form-layout-2">
                    <div class="row no-gutters">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="plan_id" class="font-weight-bold font-15">Plan:</label>
                                <select id="plan_id" name="plan_id" class="form-control select2">
                                    <option value="" selected disabled>--- Pilih Plan ---</option>
                                    @foreach($plans as $key => $plan)
                                        @if(old('plan_id')==$key)
                                            <option value="{{ $key }}" selected>{{ $plan }}</option>
                                        @else
                                            <option value="{{ $key }}">{{ $plan }}</option>
                                        @endif
                                    @endforeach
                                </select>
                                @if($errors->has('plan_id'))
                                    <ul class="parsley-errors-list filled">
                                        <li class="parsley-required">{{ $errors->first('plan_id') }}</li>
                                    </ul>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group mg-md-l--1">
                                <label for="name" class="font-weight-bold font-15">Nama Reward:</label>
                                <div>
                                    <input class="form-control parsley-error" type="text" id="name" name="name" value="{{ old('name') }}" placeholder="Input Nama Plan">
                                    @if($errors->has('name'))
                                        <ul class="parsley-errors-list filled">
                                            <li class="parsley-required">{{ $errors->first('name') }}</li>
                                        </ul>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group mg-md-l--1">
                                <label for="reward_type" class="font-weight-bold font-15">Tipe Reward:</label>
                                <select id="reward_type" name="reward_type" class="form-control select2">
                                    <option value="" selected disabled>--- Pilih Tipe Reward ---</option>
                                    <option value="1" {{ (old('reward_type') == 1) ? 'selected' : '' }}>Murni Kiri-Kanan</option>
                                    <option value="2" {{ (old('reward_type') == 2) ? 'selected' : '' }}>Potong Bonus</option>
                                    <option value="3" {{ (old('reward_type') == 2) ? 'selected' : '' }}>Potong Bonus Langsung</option>
                                </select>
                                @if($errors->has('reward_type'))
                                    <ul class="parsley-errors-list filled">
                                        <li class="parsley-required">{{ $errors->first('reward_type') }}</li>
                                    </ul>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group mg-md-l--1">
                                <label for="reward_prerequisite" class="font-weight-bold font-15">Reward Syarat:</label>
                                <select id="reward_prerequisite" name="reward_prerequisite" class="form-control select2">
                                    <option value="" selected disabled>--- Pilih Reward Syarat ---</option>
                                    @foreach($rewards as $key => $reward)
                                        @if(old('reward_prerequisite')==$key)
                                            <option value="{{ $key }}" selected>{{ $reward }}</option>
                                        @else
                                            <option value="{{ $key }}">{{ $reward }}</option>
                                        @endif
                                    @endforeach
                                </select>
                                @if($errors->has('reward_prerequisite'))
                                    <ul class="parsley-errors-list filled">
                                        <li class="parsley-required">{{ $errors->first('reward_prerequisite') }}</li>
                                    </ul>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row no-gutters">
                        <div class="col-md-3 mg-md-t--1">
                            <div class="form-group">
                                <label for="deducted_bonuses" class="font-weight-bold font-15">Bonus Yang Dipotong:</label>
                                @system('bonus_sponsor')
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="deducted_bonus_sponsor" name="deducted_bonuses[]" value="bonus sponsor">
                                    <label class="custom-control-label" for="deducted_bonus_sponsor">Bonus Sponsor</label>
                                </div>
                                @endsystem
                                @system('bonus_pairing')
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="deducted_bonus_pairing" name="deducted_bonuses[]" value="bonus pairing">
                                    <label class="custom-control-label" for="deducted_bonus_pairing">Bonus Pairing</label>
                                </div>
                                @endsystem
                                @system('bonus_pairing_level')
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="deducted_bonus_pairing_level" name="deducted_bonuses[]" value="bonus pairing level">
                                    <label class="custom-control-label" for="deducted_bonus_pairing_level">Bonus Pairing Level</label>
                                </div>
                                @endsystem
                                @system('bonus_level')
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="deducted_bonus_level" name="deducted_bonuses[]" value="bonus level">
                                    <label class="custom-control-label" for="deducted_bonus_level">Bonus Level</label>
                                </div>
                                @endsystem
                                @if($errors->has('deducted_bonuses'))
                                    <ul class="parsley-errors-list filled">
                                        <li class="parsley-required">{{ $errors->first('deducted_bonuses') }}</li>
                                    </ul>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-3 mg-md-t--1">
                            <div class="form-group mg-md-l--1">
                                <label for="deduction_type" class="font-weight-bold font-15">Tipe Potongan:</label>
                                <select id="deduction_type" name="deduction_type" class="form-control select2" data-toggle="select2">
                                    <option value="" selected disabled>--- Tipe Potongan ---</option>
                                    <option value="1" {{ (old('deduction_type') == 1) ? 'selected' : '' }}>Persentase</option>
                                    <option value="2" {{ (old('deduction_type') == 2) ? 'selected' : '' }}>Nominal</option>
                                </select>
                                @if($errors->has('deduction_type'))
                                    <ul class="parsley-errors-list filled">
                                        <li class="parsley-required">{{ $errors->first('deducted_bonuses') }}</li>
                                    </ul>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-3 mg-md-t--1">
                            <div class="form-group mg-md-l--1">
                                <label for="deduction_value" class="font-weight-bold font-15">Potongan:</label>
                                <input class="form-control parsley-error" type="text" id="deduction_value" name="deduction_value" value="{{ old('deduction_value') }}" placeholder="Input besaran potongan">
                                @if($errors->has('deduction_value'))
                                    <ul class="parsley-errors-list filled">
                                        <li class="parsley-required">{{ $errors->first('deduction_value') }}</li>
                                    </ul>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-3 mg-md-t--1">
                            <div class="form-group mg-md-l--1">
                                <label for="deduction_target" class="font-weight-bold font-15">Target Potongan (Rp):</label>
                                <div>
                                    <input class="form-control parsley-error" type="text" id="deduction_target" name="deduction_target" value="{{ old('deduction_target') }}" placeholder="Input target potongan">
                                    @if($errors->has('deduction_target'))
                                        <ul class="parsley-errors-list filled">
                                            <li class="parsley-required">{{ $errors->first('deduction_target') }}</li>
                                        </ul>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row no-gutters">
                        <div class="col-md-3 mg-md-t--1">
                            <div class="form-group">
                                <label for="active_at" class="font-weight-bold font-15">Tanggal Aktif:</label>
                                <input class="form-control fc-datepicker parsley-error" type="text" id="active_at" name="active_at" value="{{ old('active_at') }}" placeholder="Input Tanggal Aktif" data-toggle="date-picker" data-single-date-picker="true">
                                @if($errors->has('active_at'))
                                    <small class="form-text text-danger">{{ $errors->first('active_at') }}</small>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-3 mg-md-t--1">
                            <div class="form-group mg-md-l--1">
                                <label for="inactive_at" class="font-weight-bold font-15">Tanggal Non-Aktif:</label>
                                <input class="form-control fc-datepicker parsley-error" type="text" id="inactive_at" name="inactive_at" value="{{ old('inactive_at') }}" placeholder="Input Tanggal Non-Aktif" data-toggle="date-picker" data-single-date-picker="true">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="inactive_at_null" onclick="setNull()">
                                    <label class="custom-control-label" for="inactive_at_null">Null</label>
                                </div>
                                @if($errors->has('inactive_at'))
                                    <small class="form-text text-danger">{{ $errors->first('inactive_at') }}</small>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-3 mg-md-t--1">
                            <div class="form-group mg-md-l--1">
                            </div>
                        </div>
                        <div class="col-md-3 mg-md-t--1">
                            <div class="form-group mg-md-l--1">
                            </div>
                        </div>
                    </div>
                    <div class="form-layout-footer bd pd-20 bd-t-0">
                        <button type="submit" class="btn btn-info">Save</button>
                        <a class="btn btn-secondary" href="{{ route('home') }}">Cancel</a>
                        <button type="button" class="btn btn-outline-primary btn-icon rounded-circle tx-34-force pd-0-force bd-0 pull-right"
                                data-container="body"
                                data-toggle="popover"
                                data-popover-color="default"
                                data-placement="top"
                                data-html="true"
                                title="Help"
                                data-content="Under Construction"><div><i class="fa fa-question-circle-o"></i></div></button>
                    </div><!-- form-group -->
                </div>
            </form>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('admin/lib/select2/js/select2.min.js') }}"></script>
    <script src="{{ asset('admin/lib/jquery-toggles/toggles.min.js') }}"></script>
@endsection

@section('page_script')
    <script>
        function setNull() {
            if ($("#inactive_at_null").prop("checked", true)) {
                $('#inactive_at').val(null);
            }
        }

        $('#inactive_at').on('change', function () {
            $("#inactive_at_null").prop("checked", false);
        });

        $('#deduction_type').on('select2:select', function (e) {
            var data = e.params.data;
            console.log(data.id);
            if (data.id == 1) {
                $('#deduction_rp').hide();
                $('#deduction_prctg').show();
            } else {
                $('#deduction_rp').show();
                $('#deduction_prctg').hide();
            }
        });

        $('.form-layout .form-control').on('focusin', function(){
            $(this).closest('.form-group').addClass('form-group-active');
        });

        $('.form-layout .form-control').on('focusout', function(){
            $(this).closest('.form-group').removeClass('form-group-active');
        });

        $('.select2').on('select2:opening', function (e) {
            $(this).closest('.form-group').addClass('form-group-active');
        });

        $('.select2').on('select2:closing', function (e) {
            $(this).closest('.form-group').removeClass('form-group-active');
        });

        $('.fc-datepicker').datepicker({
            showOtherMonths: true,
            selectOtherMonths: true,
            dateFormat: "yy-mm-dd"
        });

    </script>
@endsection