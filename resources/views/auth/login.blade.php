<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <title>Flur Indonesia</title>
    <meta name="description" content="Flur Indonesia" />

    <!-- Favicon -->
    <link rel="shortcut icon" href="favicon.ico">
    <link rel="icon" href="favicon.ico" type="image/x-icon">

    <!-- Custom CSS -->
    <link href="{{asset('auth')}}/dist/css/style.css" rel="stylesheet" type="text/css">
</head>

<body>
    <!-- Preloader -->
    <div class="preloader-it">
        <div class="loader-pendulums"></div>
    </div>
    <!-- /Preloader -->

	<!-- HK Wrapper -->
	<div class="hk-wrapper">

        <!-- Main Content -->
        <div class="hk-pg-wrapper hk-auth-wrapper">
            <header class="d-flex justify-content-between align-items-center">
                <a class="d-flex auth-brand" href="#"><!--
                    <img class="brand-img" src="dist/img/logo-dark.png" alt="brand" /> -->
                </a>
                <!-- <div class="button-list">
                    <a href="https://www.facebook.com/flurindonesia/" class=""><img class="" src="{{asset('auth')}}/images/fb.png" style="width:50px"></a>
                    <a href="https://www.instagram.com/flurindonesiaofficial" class=""><img class="" src="{{asset('auth')}}/images/ig.png" style="width:50px"></a>
                    <a href="https://wa.me/6281217879090" class=""><img class="" src="{{asset('auth')}}/images/msg.png" style="width:50px"></a>
                </div> -->
            </header>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xl-6 pa-0">
                        <div id="owl_demo_1" class="owl-carousel dots-on-item owl-theme">
                            <div class="fadeOut item auth-cover-img overlay-wrap " style="background-image:url({{asset('auth')}}/images/box-login.png);">
                                <div style="height: 50vh"></div>
                            </div>
                            <div class="fadeOut item auth-cover-img overlay-wrap " style="background-image:url({{asset('auth')}}/images/box-login2.png);">
                                <div style="height: 50vh"></div>
                            </div>
                            <div class="fadeOut item auth-cover-img overlay-wrap " style="background-image:url({{asset('auth')}}/images/box-login3.png);">
                                <div style="height: 50vh"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-6 pa-0">
                        <div class="auth-form-wrap py-xl-0 py-50">
                            <div class="auth-form w-xxl-55 w-xl-75 w-sm-90 w-xs-100">
                                <form method="POST" action="{{ route('login') }}" aria-label="{{ __('Login') }}">
                                  @csrf
                                  <div class="container-fluid">
                                    <div class="row">
                                      <div class="col-md-4 pa-0">
                                        <div class="button-list">
                                            <a href="https://www.facebook.com/flurindonesia/" class=""><img class="" src="{{asset('auth')}}/images/fb.png" style="width:50px"></a>
                                            <a href="https://www.instagram.com/flurindonesiaofficial" class=""><img class="" src="{{asset('auth')}}/images/ig.png" style="width:50px"></a>
                                            <a href="https://wa.me/6281217879090" class=""><img class="" src="{{asset('auth')}}/images/msg.png" style="width:50px"></a>
                                        </div>
                                      </div>
                                      <div class="col-md-8 pa-0 order-first">
                                        <h1 class="display-4 mb-10">Flur Indonesia</h1>
                                      </div>
                                    </div>
                                  </div>
                                    <p class="mb-30">Silahkan Login untuk masuk ke member Area</p>
                                    @if ($errors->has('username') || $errors->has('password'))
                                        <div class="alert alert-danger text-center" role="alert">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                            <p class="d-block d-sm-inline-block-force mg-t-15">
                                                <strong>Error!</strong><br />Username / Password tidak sesuai!
                                            </p>
                                        </div><!-- alert -->
                                    @endif
                                    <div class="form-group">
                                        <input class="form-control" placeholder="Username" type="text" name="username">
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input class="form-control" placeholder="Password" type="password" name="password">
                                            <div class="input-group-append">
                                                <span class="input-group-text"><span class="feather-icon"><i data-feather="eye-off"></i></span></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="custom-control custom-checkbox mb-25">
                                        <input class="custom-control-input" id="same-address" type="checkbox" checked>
                                        <label class="custom-control-label font-14" for="same-address">Tetap login</label>
                                    </div>
                                    <button class="btn btn-gradient-flur btn-block" type="submit">Masuk</button>
                                    <p class="font-16 text-center mt-15"><a href="#" >Lupa Password</a></p>

                                    <div class="option-sep"></div>
                                    <div class="form-row">
                                        <div class="col-sm-6 mb-20">
                                            <a class="btn btn-gradient-flur btn-block btn-wth-icon" href="{{asset('marketing/brochure.zip')}}">Download Brochure Product</a>
                                        </div>
                                        <div class="col-sm-6 mb-20">
                                            <a class="btn btn-gradient-flur btn-block btn-wth-icon" href="{{asset('marketing/flur-presentation.ppt')}}">Download PPT</a>
                                        </div>
                                    </div>
                                    <p class="text-center">Perjalanan Seribu Mil Selalu Dimulai Dengan Langkah Pertama</p>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /Main Content -->

    </div>
	<!-- /HK Wrapper -->

    <!-- jQuery -->
    <script src="{{asset('auth')}}/vendors/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="{{asset('auth')}}/vendors/popper.js/dist/umd/popper.min.js"></script>
    <script src="{{asset('auth')}}/vendors/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Slimscroll JavaScript -->
    <script src="{{asset('auth')}}/dist/js/jquery.slimscroll.js"></script>

    <!-- Fancy Dropdown JS -->
    <script src="{{asset('auth')}}/dist/js/dropdown-bootstrap-extended.js"></script>

    <!-- Owl JavaScript -->
    <script src="{{asset('auth')}}/vendors/owl.carousel/dist/owl.carousel.min.js"></script>

    <!-- FeatherIcons JavaScript -->
    <script src="{{asset('auth')}}/dist/js/feather.min.js"></script>

    <!-- Init JavaScript -->
    <script src="{{asset('auth')}}/dist/js/init.js"></script>
    <script src="{{asset('auth')}}/dist/js/login-data.js"></script>
</body>

</html>
