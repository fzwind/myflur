<?php

namespace App\Tools;

use App\Repositories\MemberRewardInterface;
use App\Repositories\MemberRewardSavingInterface;
use App\Repositories\MemberRewardStatusInterface;
use App\Repositories\MemberStructureInterface;
use App\Repositories\RewardInterface;
use App\Repositories\RewardSettingInterface;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class SetRewardStatusService
{
    use BonusTool;

    public $reward_setting;
    public $rewards;
    public $member_structure;
    public $member_reward_status;
    public $member_reward_saving;
    public $member_reward;

    public function __construct(
        RewardSettingInterface $reward_setting,
        RewardInterface $rewards,
        MemberStructureInterface $member_structure,
        MemberRewardStatusInterface $member_reward_status,
        MemberRewardSavingInterface $member_reward_saving,
        MemberRewardInterface $member_reward
    ) {
        $this->reward_setting = $reward_setting;
        $this->member_structure = $member_structure;
        $this->member_reward_status = $member_reward_status;
        $this->member_reward_saving = $member_reward_saving;
        $this->member_reward = $member_reward;
        $this->rewards = $rewards;
    }

    public function run($mode = null)
    {
        $structures = $this->member_structure->all();
        $reward_settings = $this->reward_setting->getAllActive();

        if ($mode == 'init') {
            $this->resetRewardStatus();
            $this->resetMemberReward();

            foreach ($structures as $structure) {
                foreach ($reward_settings as $reward_setting) {
                    $this->updateRewardStatus($structure, $reward_setting);

                    if ($reward_setting->reward_type == 1) {
                        $this->updateStructureReward($structure, $reward_setting);
                    }
                }
            }
        }

        foreach ($structures as $structure) {
            foreach ($reward_settings as $reward_setting) {
                if ($reward_setting->reward_type > 1) {
                    try {
                        $this->updateMemberReward($structure, $reward_setting);
                    } catch (\Exception $e) {
                        return false;
                    }
                }
            }
        }
    }

    public function updateMemberReward($structure, $reward_setting)
    {
        if ($reward_setting->reward_type > 1) {
            $rewards = $this->rewards->getBySetting($reward_setting->id);

            $qualified = $this->member_reward_status->getStatus($reward_setting->id, $structure->user_id);

            $left_reward = $this->member_structure->countMyLeftReward($structure->user_id, $reward_setting->id);
            $right_reward = $this->member_structure->countMyRightReward($structure->user_id, $reward_setting->id);

            foreach ($rewards as $reward) {
                $member_reward = $this->member_reward->getByAttributes(
                    [
                        'reward_id' => $reward->id,
                        'user_id' => $structure->user_id
                    ]
                );

                if ($left_reward > 0 || $right_reward > 0) {
                    if ($member_reward->isEmpty() || $member_reward['is_achieved'] != 1) {
                        DB::transaction(function () use ($reward, $structure, $qualified, $left_reward, $right_reward) {
                            $is_achieved = 0;
                            $achieved_at = null;
                            if ($qualified && $left_reward >= $reward->left && $right_reward >= $reward->right) {
                                $is_achieved = 1;
                                $achieved_at = Carbon::now('Asia/Jakarta');
                            }

                            $this->member_reward->updateOrCreate(
                                [
                                    'reward_id' => $reward->id,
                                    'user_id' => $structure->user_id,
                                ],
                                [
                                    'left' => $left_reward,
                                    'right' => $right_reward,
                                    'is_achieved' => $is_achieved,
                                    'achieved_at' => $achieved_at,
                                ]
                            );
                        }, 3);
                    }
                }
            }
        }
    }

    public function updateStructureReward($structure, $reward_setting)
    {
        if ($reward_setting->reward_type == 1) {
            $rewards = $this->rewards->getBySetting($reward_setting->id);

            foreach ($rewards as $reward) {
                $left_structure = $this->member_structure->countMyLeftStructure($structure, null, $reward->active_at, $reward->inactive_at);
                $right_structure = $this->member_structure->countMyRightStructure($structure, null, $reward->active_at, $reward->inactive_at);

                DB::transaction(function () use ($reward, $structure, $left_structure, $right_structure) {
                    $is_achieved = 0;
                    $achieved_at = null;
                    if ($left_structure >= $reward->left && $right_structure >= $reward->right) {
                        $is_achieved = 1;
                        $achieved_at = Carbon::now('Asia/Jakarta');
                    }

                    $this->member_reward->updateOrCreate(
                        [
                            'reward_id' => $reward->id,
                            'user_id' => $structure->user_id,
                        ],
                        [
                            'left' => $left_structure,
                            'right' => $right_structure,
                            'is_achieved' => $is_achieved,
                            'achieved_at' => $achieved_at,
                        ]
                    );
                }, 3);
            }
        }
    }

    public function updateRewardStatus($structure, $reward_setting)
    {
        if ($reward_setting->reward_type == 2) {
            if ($this->member_reward_saving->getSum($structure->user_id, $reward_setting->id)->sum_amount >= $reward_setting->deduction_target) {
                $this->member_reward_status->setStatus($structure->user_id, $reward_setting->id);
            }
        }

        if ($reward_setting->reward_type == 3) {
            $deducted_bonuses = json_decode($reward_setting->deducted_bonuses);

            if (in_array('bonus sponsor', $deducted_bonuses)) {
                $count = $this->countSponsor($structure->user_id);
                $amount_potongan = ($reward_setting->deduction_type == 1) ? bonus_setting($structure->user->join_plan)->bonus_sponsor_amount * $reward_setting->deduction_value / 100 : $reward_setting->deduction_value;
                $kali_potongan = ceil($reward_setting->deduction_target / $amount_potongan);

                if ($count >= $kali_potongan) {
                    $this->member_reward_status->setStatus($structure->user_id, $reward_setting->id);
                }
            }

            if (in_array('bonus pairing', $deducted_bonuses)) {
                $count = $this->countPairing($structure->user_id);
                $amount_potongan = ($reward_setting->deduction_type == 1) ? bonus_setting($structure->user->join_plan)->bonus_pairing_amount * $reward_setting->deduction_value / 100 : $reward_setting->deduction_value;
                $kali_potongan = ceil($reward_setting->deduction_target / $amount_potongan);
                
                if ($count >= $kali_potongan) {
                    $this->member_reward_status->setStatus($structure->user_id, $reward_setting->id);
                }
            }
        }
    }

    public function resetRewardStatus()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('member_reward_statuses')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }

    public function resetMemberReward()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('member_rewards')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
