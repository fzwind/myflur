<?php

namespace App\Tools;

use Carbon\Carbon;

trait PlanSettingTool
{
    public function filterPlanSettingData($request)
    {
        $plan_data['code'] = $request['code'];
        $plan_data['name'] = $request['name'];
        $plan_data['is_active'] = $request['is_active'];

        return $plan_data;
    }

    public function filterPlanPriceData($request, $planId)
    {
        $price_data['plan_id'] = $planId;
        $price_data['price'] = $request['price'];
        $price_data['stockist_price'] = $request['stockist_price'];
        $price_data['master_stockist_price'] = $request['master_stockist_price'];
        $price_data['active_at'] = Carbon::now('Asia/Jakarta');

        return $price_data;
    }
}
