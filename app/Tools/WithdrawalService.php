<?php

namespace App\Tools;

use App\Repositories\BonusInterface;
use App\Repositories\MemberRewardSavingInterface;
use App\Repositories\MemberRewardStatusInterface;
use App\Repositories\RewardInterface;
use App\Repositories\RewardSettingInterface;
use App\Repositories\WithdrawalInterface;
use App\Repositories\WithdrawalSettingInterface;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class WithdrawalService
{
    public $withdrawal;
    public $withdrawal_setting;
    public $bonus;
    public $reward_setting;
    public $member_reward_status;
    public $member_reward_saving;
    public $rewards;

    public function __construct(
        WithdrawalInterface $withdrawal,
        WithdrawalSettingInterface $withdrawal_setting,
        BonusInterface $bonus,
        RewardSettingInterface $reward_setting,
        MemberRewardStatusInterface $member_reward_status,
        MemberRewardSavingInterface $member_reward_saving,
        RewardInterface $rewards
    ) {
        $this->withdrawal = $withdrawal;
        $this->withdrawal_setting = $withdrawal_setting;
        $this->bonus = $bonus;
        $this->reward_setting = $reward_setting;
        $this->member_reward_status = $member_reward_status;
        $this->member_reward_saving = $member_reward_saving;
        $this->rewards = $rewards;
    }

    public function run($date = null)
    {
        $date = Carbon::parse($date, 'Asia/Jakarta');

        $withdrawal_setting = $this->withdrawal_setting->getActiveSetting();

        if ($withdrawal_setting->wd_mode == 1) {
            $reward_setting = $this->reward_setting->getRewardPotongBonus();

            foreach ($reward_setting as $reward) {
                $bonus = $this->calculate($withdrawal_setting, $date);

                if (! is_null($reward->reward_prerequisite)) {
                    $qualified = $this->member_reward_status->getStatus($reward->id);

                    if ($qualified) {
                        try {
                            DB::transaction(function () use ($withdrawal_setting, $bonus, $reward) {
                                $deducted_bonus = $this->deductBonus($bonus, $reward, $withdrawal_setting);
                                $this->createWithdrawalAndSavings($deducted_bonus, $reward);
                            }, 3);
                        } catch (\Exception $e) {
                            Log::error('Withdrawal Error : '. $e);
                            return false;
                        }
                    }
                } else {
                    try {
                        DB::transaction(function () use ($withdrawal_setting, $bonus, $reward) {
                            $deducted_bonus = $this->deductBonus($bonus, $reward, $withdrawal_setting);
                            $this->createWithdrawalAndSavings($deducted_bonus, $reward);
                        }, 3);
                    } catch (\Exception $e) {
                        Log::error('Withdrawal Error : '. $e);
                        return false;
                    }
                }
            }

            Log::info('Auto Withdrawal success');
        }
    }

    public function calculate($withdrawal_setting, $date = null)
    {
        $return_data = [];

        $user_bonus = $this->bonus->getAllBonusForWithdrawal($date);

        foreach ($user_bonus as $bonus) {
            $withdrawn = $this->withdrawal->getWithdrawnBonus($bonus->user_id);

            $withdrawn_bonus_sponsor = is_null($withdrawn->bonus_sponsor) ? 0 : $withdrawn->bonus_sponsor;
            $withdrawn_bonus_pairing = is_null($withdrawn->bonus_pairing) ? 0 : $withdrawn->bonus_pairing;
            $withdrawn_bonus_pairing_level = is_null($withdrawn->bonus_pairing_level) ? 0 : $withdrawn->bonus_pairing_level;
            $withdrawn_bonus_level = is_null($withdrawn->bonus_level) ? 0 : $withdrawn->bonus_level;

            $bonus_sponsor = $bonus->sponsor - $withdrawn_bonus_sponsor;
            $bonus_pairing = $bonus->pairing - $withdrawn_bonus_pairing;
            $bonus_pairing_level = $bonus->pairing_level - $withdrawn_bonus_pairing_level;
            $bonus_level = $bonus->level - $withdrawn_bonus_level;

            $total_bonus_available = $bonus_sponsor + $bonus_pairing + $bonus_pairing_level + $bonus_level;

            if ($total_bonus_available >= $withdrawal_setting->minimum_wd) {
                $return_data[] = [
                    'code' => $this->getWithdrawalCode($bonus->user_id),
                    'user_id' => $bonus->user_id,
                    'bonus_sponsor' => $bonus_sponsor,
                    'bonus_pairing' => $bonus_pairing,
                    'bonus_pairing_level' => $bonus_pairing_level,
                    'bonus_level' => $bonus_level,
                ];
            }
        }

        return $return_data;
    }

    public function deductBonus($user_bonus, $reward_setting, $withdrawal_setting)
    {
        $return_data = [];

        foreach ($user_bonus as $key => $bonus) {
            $deducted_bonuses = json_decode($reward_setting->deducted_bonuses);

            $bonus_sponsor = $bonus['bonus_sponsor'];
            $bonus_pairing = $bonus['bonus_pairing'];
            $bonus_pairing_level = $bonus['bonus_pairing_level'];
            $bonus_level = $bonus['bonus_level'];

            $reward_sponsor_saving = 0;
            if (in_array('bonus sponsor', $deducted_bonuses)) {
                if ($reward_setting->deduction_type == 1) {
                    $reward_sponsor_saving = ($bonus_sponsor * $reward_setting->deduction_value) / 100;
                }
            }

            $reward_pairing_saving = 0;
            if (in_array('bonus pairing', $deducted_bonuses)) {
                if ($reward_setting->deduction_type == 1) {
                    $reward_pairing_saving = $bonus_pairing * $reward_setting->deduction_value / 100;
                }
            }

            $reward_pairing_level_saving = 0;
            if (in_array('bonus pairing level', $deducted_bonuses)) {
                if ($reward_setting->deduction_type == 1) {
                    $reward_pairing_level_saving = $bonus_pairing_level * $reward_setting->deduction_value / 100;
                }
            }

            $reward_level_saving = 0;
            if (in_array('bonus level', $deducted_bonuses)) {
                if ($reward_setting->deduction_type == 1) {
                    $reward_level_saving = $bonus_level * $reward_setting->deduction_value / 100;
                }
            }

            $wd_amount = $bonus_sponsor + $bonus_pairing + $bonus_pairing_level + $bonus_level;
            $wd_index_deduction = $wd_amount * $withdrawal_setting->wd_index_deduction / 100;
            $my_saving = $this->member_reward_saving->getSum($bonus['user_id'], $reward_setting->id)->sum_amount;
            $my_saving = is_null($my_saving) ? 0 : $my_saving;

            if ($my_saving == $reward_setting->deduction_target) {
                $wd_reward_deduction = 0;
            } else {
                $wd_reward_deduction = $reward_sponsor_saving + $reward_pairing_saving + $reward_pairing_level_saving + $reward_level_saving;
                if (($wd_reward_deduction + $my_saving) > $reward_setting->deduction_target) {
                    $target_diff = ($wd_reward_deduction + $my_saving) - $reward_setting->deduction_target;
                    $wd_reward_deduction = $wd_reward_deduction - $target_diff;
                }
            }

            $wd_admin = $withdrawal_setting->wd_admin;

            $return_data[] = array_merge($user_bonus[$key], [
                'wd_amount' => $wd_amount,
                'wd_index_deduction' => $wd_index_deduction,
                'wd_reward_deduction' => $wd_reward_deduction,
                'wd_admin' => $wd_admin,
                'status' => 0
            ]);
        }

        return $return_data;
    }

    public function createWithdrawalAndSavings($deducted_bonus, $reward_setting)
    {
        foreach ($deducted_bonus as $data) {
            try {
                $wd = $this->withdrawal->create($data);

                if ($data['wd_reward_deduction'] > 0) {
                    $saving_data = [];
                    $saving_data['reward_setting_id'] = $reward_setting->id;
                    $saving_data['withdrawal_id'] = $wd->id;
                    $saving_data['user_id'] = $data['user_id'];
                    $saving_data['saving_amount'] = $data['wd_reward_deduction'];

                    $this->member_reward_saving->create($saving_data);
                }

                if (is_null($this->member_reward_status->getStatus($data['user_id'], $reward_setting->id))) {
                    if ($this->member_reward_saving->getSum($data['user_id'], $reward_setting->id)->sum_amount >= $reward_setting->deduction_target) {
                        $this->member_reward_status->create([
                            'user_id' => $data['user_id'],
                            'reward_setting_id' => $reward_setting->id
                        ]);
                    }
                }
            } catch (\Exception $e) {
                Log::error('Auto Withdrawal failed : '. $e);

                return false;
            }
        }

        return true;
    }

    public function getWithdrawalCode($userId)
    {
        $today = Carbon::now('Asia/Jakarta');
        $year = $today->format('y');
        $month = $today->format('m');
        $day = $today->format('d');

        switch (strlen($userId)) {
            case '1':
                $user = '0000000'. $userId;
                break;
            case '2':
                $user = '000000'. $userId;
                break;
            case '3':
                $user = '00000'. $userId;
                break;
            case 4:
                $user = '0000'. $userId;
                break;
            case 5:
                $user = '000'. $userId;
                break;
            case 6:
                $user = '00'. $userId;
                break;
            case 7:
                $user = '0'. $userId;
                break;
            default:
                $user = $userId;
                break;
        }
        $code = 'WD'. $year . $month . $day . $user;

        return $code;
    }
}
