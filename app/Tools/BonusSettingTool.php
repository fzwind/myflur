<?php

namespace App\Tools;

trait BonusSettingTool
{
    public function filterBonusSettingData($request)
    {
        $data_bonus_setting = [];
        $data_bonus_setting['plan_id'] = $request['plan_id'];
        $data_bonus_setting['active_at'] = $request['active_at'];
        if (isset($request['bonus_sponsor_amount'])) {
            $data_bonus_setting['bonus_sponsor_amount'] = $request['bonus_sponsor_amount'];
        }
        if (isset($request['bonus_pairing_amount'])) {
            $data_bonus_setting['bonus_pairing_amount'] = $request['bonus_pairing_amount'];
        }
        if (isset($request['bonus_pairing_level_amount'])) {
            $data_bonus_setting['bonus_pairing_level_amount'] = $request['bonus_pairing_level_amount'];
        }
        if (isset($request['max_pairing_by'])) {
            $data_bonus_setting['max_pairing_by'] = $request['max_pairing_by'];
        }
        if (isset($request['over_pairing_handle'])) {
            $data_bonus_setting['over_pairing_handle'] = $request['over_pairing_handle'];
        }
        if (isset($request['max_pairing_per_day'])) {
            $data_bonus_setting['max_pairing_per_day'] = $request['max_pairing_per_day'];
        }
        if (isset($request['max_pairing_amount_per_day'])) {
            $data_bonus_setting['max_pairing_amount_per_day'] = $request['max_pairing_amount_per_day'];
        }
        if (isset($request['pair_per_level'])) {
            $data_bonus_setting['pair_per_level'] = $request['pair_per_level'];
        }
        if (isset($request['max_level_bonus_level'])) {
            $data_bonus_setting['max_level_bonus_level'] = $request['max_level_bonus_level'];
        }

        return $data_bonus_setting;
    }
}
