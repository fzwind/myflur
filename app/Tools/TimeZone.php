<?php

namespace App\Tools;

use Carbon\Carbon;

class TimeZone
{
    // public function toUtc($date)
    // {
    //     return Carbon::parse($date, 'Asia/Jakarta')->setTimezone('UTC');
    // }
    //
    // public function fromUtc($date, $format = 'd-m-Y H:i:s (T P)')
    // {
    //     if (! is_null($date)) {
    //         return $date->setTimezone(auth_tz())->format($format);
    //     }
    // }

    public function toUtz($date, $format = 'd-m-Y H:i:s (T P)')
    {
        if (! is_null($date)) {
            return $date->setTimezone(auth_tz())->format($format);
        }
    }
}
