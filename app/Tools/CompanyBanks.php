<?php

namespace App\Tools;


class CompanyBanks {

	public function getCompanyBank()
	{
		$bank = new \App\Models\CompanyBank();
		return $bank->where('is_active',1)->get();
	}

	public function getCompanyBankList()
	{
		$bank = new \App\Models\CompanyBank();
		return $bank
			->where('is_active',1)
			->get()
			->pluck('name_with_account','id');
	}
}