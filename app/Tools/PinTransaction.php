<?php

namespace App\Tools;


class PinTransaction {

	public function getMyRunningTransaction()
	{
		$my_transaction = new \App\Models\PinTransaction();

		$id = (auth()->user()->hasRole('members')) ? auth()->id() : 11;

		return $my_transaction
			->with('fromuser')
			->with('touser')
			->whereIn('status',['ordered','paid'])
			->where(function ($q) use($id) {
				$q->where('from',$id)
				  ->orWhere('to',$id);
			})
			->orderBy('created_at', 'desc')
			->get();
	}
}