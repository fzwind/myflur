<?php

namespace App\Tools;

trait PinTransactionsTool
{
    public function strTransaction($transaction)
    {
        if ($transaction->transaction_type == 'buy' && $transaction->from == auth()->user()->id) {
            $str_transaction = '<span class="text-primary">Jual</span> - <small class="text-muted">ke '. $transaction->touser->username .'</small>';
        } elseif ($transaction->transaction_type == 'buy' && $transaction->to == auth()->user()->id) {
            $str_transaction = '<span class="text-success">Beli</span> - <small class="text-muted">dari '. $transaction->fromuser->username .'</small>';
        } elseif ($transaction->transaction_type == 'transfer' && $transaction->from == auth()->user()->id) {
            $str_transaction = '<span class="text-warning">Kirim</span>';
        } elseif ($transaction->transaction_type == 'transfer' && $transaction->to == auth()->user()->id) {
            $str_transaction = '<span class="text-info">Kirim</span>';
        } else {
            $str_transaction = '<span class="text-danger">-</span>';
        }

        return $str_transaction;
    }

    public function strTransactionCompany($transaction)
    {
        if ($transaction->transaction_type == 'buy' && $transaction->from == 11) {
            $str_transaction = '<span class="text-primary">Jual</span><pre class="text-muted">ke '. $transaction->touser->username .'</pre>';
        } else {
            $str_transaction = '<span class="text-danger">-</span>';
        }

        return $str_transaction;
    }

    public function actionButton($transaction)
    {
        if (
            system_settings()->proof_bank_transfer_upload == 1 &&
            $transaction->transaction_type == 'buy' &&
            $transaction->status == 'ordered' &&
            $transaction->to == auth()->user()->id) {
            $actionButton = '<a href="'. route('transaction.pin.invoice', ['pin_transaction' => $transaction]) .'" class="btn btn-sm btn-outline-secondary" data-toggle="modal" data-target="#invoice" aria-labelledby="invoiceModalLabel"><i class="fa fa-file"></i></a>&nbsp;<a href="'. route('transaction.pin.upload', ['pin_transaction' => $transaction]) .'" class="btn btn-sm btn-outline-primary" data-toggle="modal" data-target="#upload" aria-labelledby="uploadModalLabel"><i class="fe-upload-cloud"></i> Upload Bukti Transfer</a>';
        } elseif (
            system_settings()->proof_bank_transfer_upload == 0 &&
            $transaction->transaction_type == 'buy' &&
            $transaction->status == 'ordered' &&
            $transaction->to == auth()->user()->id) {
            $actionButton = '<form method="post" action="'. route('transaction-pin.update', ['transaction_pin' => $transaction]) .'">'. csrf_field() .'<input type="hidden" name="_method" value="PUT"><input type="hidden" name="status" value="paid"><a href="'. route('transaction.pin.invoice', ['pin_transaction' => $transaction]) .'" class="btn btn-sm btn-outline-secondary" data-toggle="modal" data-target="#invoice" aria-labelledby="invoiceModalLabel"><i class="fa fa-file"></i></a>&nbsp;<button type="submit" class="btn btn-sm btn-outline-primary">Selesai Transfer</button></form>';
        } elseif (
            $transaction->transaction_type == 'buy' &&
            $transaction->status == 'paid' &&
            $transaction->from == auth()->user()->id) {
            if (system_settings()->proof_bank_transfer_upload == 1) {
                $view_button = '&nbsp;&nbsp;<a href="'. route('transaction.pin.image', ['pin_transaction' => $transaction]) .'" class="btn btn-sm btn-outline-info waves-effect waves-light" data-toggle="modal" data-target="#image" aria-labelledby="imageModalLabel"><i class="fa fa-eye"></i></a>';
            } else {
                $view_button = '';
            }
            $reject_button = '&nbsp;&nbsp;<a href="'. $transaction->id .'" trans_id="'. $transaction->id .'" class="btn btn-sm btn-outline-warning waves-effect waves-light" data-toggle="modal" data-target="#reject" aria-labelledby="rejectModalLabel"><i class="fa fa-close"></i></a>';
            $actionButton = '<form method="post" action="'. route('transaction-pin.update', ['transaction_pin' => $transaction]) .'">'. csrf_field() .'<input type="hidden" name="_method" value="PUT"><input type="hidden" name="status" value="confirmed"><a href="'. route('transaction.pin.invoice', ['pin_transaction' => $transaction]) .'" class="btn btn-sm btn-outline-secondary" data-toggle="modal" data-target="#invoice" aria-labelledby="invoiceModalLabel"><i class="fa fa-file"></i></a>'. $view_button . $reject_button .'&nbsp;&nbsp;<button type="submit" class="btn btn-sm btn-outline-primary">Konfirmasi</button></form>';
        } elseif (
            $transaction->transaction_type == 'buy' &&
            $transaction->status == 'paid' &&
            $transaction->from == 11 &&
            auth()->user()->hasPermissionTo('admin pin approve')) {
            if (system_settings()->proof_bank_transfer_upload == 1) {
                $view_button = '&nbsp;&nbsp;<a href="'. route('transaction.pin.image', ['pin_transaction' => $transaction]) .'" class="btn btn-sm btn-outline-info waves-effect waves-light" data-toggle="modal" data-target="#image" aria-labelledby="imageModalLabel"><i class="fa fa-eye"></i></a>';
            } else {
                $view_button = '';
            }
            $reject_button = '&nbsp;&nbsp;<a href="'. $transaction->id .'" trans_id="" class="btn btn-sm btn-outline-warning waves-effect waves-light" data-toggle="modal" data-target="#reject" aria-labelledby="rejectModalLabel"><i class="fa fa-close"></i></a>';
            $actionButton = '<form method="post" action="'. route('transaction-pin.update', ['transaction_pin' => $transaction]) .'">'. csrf_field() .'<input type="hidden" name="_method" value="PUT"><input type="hidden" name="status" value="confirmed"><a href="'. route('transaction.pin.invoice', ['pin_transaction' => $transaction]) .'" class="btn btn-sm btn-outline-secondary" data-toggle="modal" data-target="#invoice" aria-labelledby="invoiceModalLabel"><i class="fa fa-file"></i></a>'. $view_button . $reject_button .'&nbsp;&nbsp;<button type="submit" class="btn btn-sm btn-outline-primary">Konfirmasi</button></form>';
        } elseif (
            $transaction->transaction_type == 'buy' &&
            $transaction->status == 'paid' &&
            $transaction->to == auth()->user()->id) {
            if (! is_null($transaction->paid_file)) {
                $actionButton = '<a href="'. route('transaction.pin.invoice', ['pin_transaction' => $transaction]) .'" class="btn btn-sm btn-outline-secondary" data-toggle="modal" data-target="#invoice" aria-labelledby="myModalLabel"><i class="fa fa-file"></i></a>&nbsp;&nbsp;<a href="'. route('transaction.pin.image', ['pin_transaction' => $transaction]) .'" class="btn btn-sm btn-outline-purple waves-effect waves-light" data-toggle="modal" data-target="#image" aria-labelledby="imageModalLabel"><i class="fe-clock"></i> Menunggu Konfirmasi</a>';
            } else {
                $actionButton = '<a href="'. route('transaction.pin.invoice', ['pin_transaction' => $transaction]) .'" class="btn btn-sm btn-outline-secondary" data-toggle="modal" data-target="#invoice" aria-labelledby="myModalLabel"><i class="fa fa-file"></i></a>&nbsp;&nbsp;<a href="javascript:void(0);" class="btn btn-sm btn-outline-purple waves-effect waves-light" onclick="javascript:void(0);"><i class="fe-clock"></i> Menunggu Konfirmasi</a>';
            }
        } else {
            $actionButton = '';
        }
        return $actionButton;
    }

    public function status_date($transaction)
    {
        if ($transaction->status == 'ordered') {
            $status_date = to_utz($transaction->ordered_at);
        } elseif ($transaction->status == 'paid') {
            $status_date = to_utz($transaction->paid_at);
        } elseif ($transaction->status == 'confirmed') {
            $status_date = to_utz($transaction->confirmed_at);
        } elseif ($transaction->status == 'rejected') {
            $status_date = to_utz($transaction->rejected_at);
        } else {
            $status_date = '';
        }
        return $status_date;
    }

    public function str_status($transaction)
    {
        if ($transaction->status == 'ordered') {
            $status = '<span class="text-muted">Order</span>';
        } elseif ($transaction->status == 'paid') {
            $status = '<span class="text-info">Bayar</span>';
        } elseif ($transaction->status == 'confirmed') {
            $status = '<span class="text-success">Selesai</span>';
        } elseif ($transaction->status == 'rejected') {
            $status = '<span class="text-danger">Ditolak</span><pre>alasan : '. $transaction->status_note .'</pre>';
        } else {
            $status = '';
        }
        return $status;
    }

    public function transaction_code($transaction)
    {
        return '<mark>'. $transaction->transaction_code .'</mark>';
    }

    public function invoice_modal($transaction)
    {
        $modal = '';

        return $modal;
    }
}
