<?php

namespace App\Tools;

use App\Models\SystemSetting;
use Illuminate\Support\Facades\Cache;

class SystemSettings {

	public function getSystemSettings() {
		$system_settings = new SystemSetting();

		return Cache::rememberForever('systemsettings', function () use ($system_settings) {
			return $system_settings->first();
		});
	}

}