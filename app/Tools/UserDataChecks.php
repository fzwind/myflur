<?php

namespace App\Tools;


use App\Models\UserBank;

class UserDataChecks {

	public function checkProfile()
	{
		$profile_incomplete = [];

		if (system_settings()->memdata_hp) {
			if (is_null(auth()->user()->no_hp))  $profile_incomplete[] = 'hp';
		}

		if (system_settings()->memdata_sex) {
			if (auth()->user()->sex != 'male' && auth()->user()->sex != 'female')  $profile_incomplete[] = 'sex';
		}

		if (system_settings()->memdata_birth_date) {
			if (is_null(auth()->user()->birth_date))  $profile_incomplete[] = 'birth_date';
		}

		return collect($profile_incomplete);
	}

	public function checkAddress()
	{
		$address_incomplete = [];

		if (system_settings()->memdata_address) {
			if (is_null(auth()->user()->address))  $address_incomplete[] = 'address';
		}

		if (system_settings()->memdata_province) {
			if (is_null(auth()->user()->province_id))  $address_incomplete[] = 'province';
		}

		if (system_settings()->memdata_regency) {
			if (is_null(auth()->user()->regency_id))  $address_incomplete[] = 'regency';
		}

		if (system_settings()->memdata_district) {
			if (is_null(auth()->user()->district_id))  $address_incomplete[] = 'district';
		}

		if (system_settings()->memdata_village) {
			if (is_null(auth()->user()->village_id))  $address_incomplete[] = 'village';
		}

		if (system_settings()->memdata_zip) {
			if (is_null(auth()->user()->postal_code))  $address_incomplete[] = 'zip';
		}

		return collect($address_incomplete);
	}

	public function checkId()
	{
		$id_incomplete = [];

		if (system_settings()->memdata_id_number) {
			if (is_null(auth()->user()->no_id))  $id_incomplete[] = 'ID Number';
		}

		if (system_settings()->memdata_tax_id_number) {
			if (is_null(auth()->user()->no_tax_id))  $id_incomplete[] = 'Tax ID Number';
		}

		return collect($id_incomplete);
	}

	public function checkBank()
	{
		$bank = new UserBank();

		$bank_count = $bank->where('user_id', auth()->id())->where('is_active', 1)->count();

		if ($bank_count < 1 || is_null($bank_count)) {
			return collect(['bank']);
		}

		return collect([]);
	}
}