<?php

namespace App\Tools;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;

class StockistSetting
{
    public function getStockistSetting($planId)
    {
        $stockist_settings = new \App\Models\StockistSetting();

        return Cache::rememberForever('stockistsettings-'. $planId, function () use ($stockist_settings, $planId) {
            return $stockist_settings
                ->with('plan')
                ->where('plan_id', '=', $planId)
                ->where('active_at', '<=', Carbon::now('Asia/Jakarta'))
                ->orderBy('active_at', 'desc')
                ->first();
        });
    }

    public function getStockistList($planId)
    {
        $usr = new User();

        $arr_code = explode('-', auth()->user()->memberstructure->code);
        $parents = [];
        for ($i = 1; $i < count($arr_code); $i++) {
            $parents[] = substr(auth()->user()->memberstructure->code, 0, -($i * 4));
        }

        if (auth()->user()->is_stockist == 0) {
            if (stockist_setting($planId)->member_buy_crossline == 1) {
                return $usr
                    ->with('province')
                    ->with('regency')
                    ->where('is_stockist', 1)
                    ->where('id', '<>', auth()->user()->id)
                    ->get()
                    ->pluck('name_with_province_regency', 'id');
            } else {
                return $usr
                    ->with('province')
                    ->with('regency')
                    ->whereHas('memberstructure', function ($query) use ($parents) {
                        $query
                            ->whereIn('code', $parents)
                            ->orWhere('code', 'like', auth()->user()->memberstructure->code .'%');
                    })
                    ->where('is_stockist', 1)
                    ->where('id', '<>', auth()->user()->id)
                    ->get()
                    ->pluck('name_with_province_regency', 'id');
            }
        } else {
            if (stockist_setting($planId)->member_buy_crossline == 1) {
                return $usr
                    ->with('province')
                    ->with('regency')
                    ->where('is_stockist', 1)
                    ->where('stockist_type', 1)
                    ->where('id', '<>', auth()->user()->id)
                    ->get()
                    ->pluck('name_with_province_regency', 'id')
                    ->prepend('Perusahaan', 11); // --> hardcoded id dari top company user, next dicari algoritma untuk mendapatkan ID ini
            } else {
                return $usr
                ->with('province')
                    ->with('regency')
                    ->whereHas('memberstructure', function ($query) use ($parents) {
                        $query
                            ->whereIn('code', $parents)
                            ->orWhere('code', 'like', auth()->user()->memberstructure->code .'%');
                    })
                    ->where('is_stockist', 1)
                    ->where('stockist_type', 1)
                    ->where('id', '<>', auth()->user()->id)
                    ->get()
                    ->pluck('name_with_province_regency', 'id')
                    ->prepend('Perusahaan', 11); // --> hardcoded id dari top company user, next dicari algoritma untuk mendapatkan ID ini
            }
        }
    }
}
