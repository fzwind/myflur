<?php

namespace App\Tools;

use App\Models\Bonus;
use Carbon\Carbon;

trait BonusTool
{
    public function getBonusCode($bonusModel)
    {
        $today = Carbon::now('Asia/Jakarta');
        $year = $today->year;
        $month = (strlen($today->month) == 1) ? '0'. $today->month : $today->month;
        $day = $today->day;
        $prefix = 'BNS'. $year . $month . $day .'-';

        return generate_unique_code($prefix, 16, $bonusModel, 'code');
    }

    public function maxPerDay($plan)
    {
        return bonus_setting($plan)->max_pairing_per_day;
    }

    public function getParents($userStructureCode)
    {
        $arr_upline = explode('-', $userStructureCode);

        $parents = [];
        $y = count($arr_upline)-1;
        $z = '';
        for ($i = 1; $i < count($arr_upline); $i++) {
            $z = '-'. $arr_upline[$y] . $z;
            $parents[] = str_replace($z, '', $userStructureCode);
            $y--;
        }

        return $parents;
    }

    public function isPairingBonus($receiverStructure, $userStructure, $user, $left, $right, $flushed_left, $flushed_right)
    {
        $pairing_bonus = false;

        if (bonus_setting($user->join_plan)->over_pairing_handle == 2) {
            $left = $left - $flushed_left;
            $right = $right - $flushed_right;
        }

        $new_member_code = $userStructure->code;
        $parent_left_code = $receiverStructure->code .'-'. ($receiverStructure->level + 1) .'.1';
        $parent_right_code = $receiverStructure->code .'-'. ($receiverStructure->level + 1) .'.2';
        $parent_code_length = strlen($parent_left_code);

        if (substr($new_member_code, 0, $parent_code_length) == $parent_left_code) {
            $pairing_bonus = ($left <= $right) ? true : false;
        }

        if (substr($new_member_code, 0, $parent_code_length) == $parent_right_code) {
            $pairing_bonus = ($right <= $left) ? true : false;
        }

        return $pairing_bonus;
    }

    public function countPairing($userId)
    {
        $pairing = new Bonus();
        return $pairing->where('user_id', $userId)
            ->where('is_bonus_pairing', 1)
            ->count();
    }

    public function countSponsor($userId)
    {
        $sponsor = new Bonus();
        return $sponsor->where('user_id', $userId)
            ->where('is_bonus_sponsor', 1)
            ->count();
    }

    public function countPairingLevel($userId, $level)
    {
        $pairing_level = new Bonus();
        return $pairing_level
            ->where('user_id', $userId)
            ->where('bonus_pairing_level', '>', 0)
            ->where('level', $level)
            ->count();
    }

    public function countAllPairingLevel($userId)
    {
        $pairing_level = new Bonus();
        return $pairing_level
            ->where('user_id', $userId)
            ->where('bonus_pairing_level', '>', 0)
            ->count();
    }
}
