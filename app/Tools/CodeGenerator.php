<?php

namespace App\Tools;


use Carbon\Carbon;

class CodeGenerator {

	public function getCode($prefix, $length = 14)
	{
		$length = (int) ($length > 1 ? $length : 14);
		$src = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$rand_code = $prefix;
		$prefix_length = strlen($prefix);

		for ($i = $prefix_length; $i < $length; $i++)
		{
			$char = $src[mt_rand(0, strlen($src)-1)];
			$rand_code .= $char;
		}
		return $rand_code;
	}

	public function getDateCode($prefix, $length = 14)
	{
		$length = (int) ($length > 1 ? $length : 14);
		$src = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$rand_code = $prefix;
		$rand_code .= Carbon::today(auth_tz())->format('y');
		$rand_code .= Carbon::today(auth_tz())->format('m');
		$rand_code .= Carbon::today(auth_tz())->format('d');
		$len = $length - strlen($rand_code);
		for ($i = 0; $i < $len; $i++)
		{
			$char = $src[mt_rand(0, strlen($src)-1)];
			$rand_code .= $char;
		}
		return $rand_code;
	}

	public function checkExistence($model, $field, $code)
	{
		if ($model->where($field, '=', $code)->count() > 0) {
			return true;
		}
		return false;
	}

}