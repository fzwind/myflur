<?php

namespace App\Tools;

use App\Models\BonusSetting;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;

class BonusSettings
{
    public function getBonusSetting($planId)
    {
        $bonus_settings = new BonusSetting();

        return Cache::rememberForever('bonussettings-'. $planId, function () use ($bonus_settings, $planId) {
            return $bonus_settings
                ->with('bonuslevel')
                ->with('plan')
                ->where('plan_id', '=', $planId)
                ->where('active_at', '<=', Carbon::now('Asia/Jakarta'))
                ->orderBy('active_at', 'desc')
                ->first();
        });
    }
}
