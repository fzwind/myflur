<?php

namespace App\Tools;

use App\Models\Plan;
use Illuminate\Support\Facades\Cache;

class PlanSettings {

	public function getPlanList()
	{
		$plans = new Plan();

		return Cache::rememberForever('planlist', function () use ($plans) {
			return $plans
				->selectRaw("id, code, name, CONCAT('(', code, ') ', name) AS codename")
				->where('is_active', 1)
				->pluck('codename','id');
		});
	}

	public function getActivePlan()
	{
		$plans = new Plan();

		return Cache::rememberForever('plans', function () use ($plans) {
			return $plans
				->select( 'id' )
				->where('is_active', 1)
				->get();
		});
	}

	public function getPlanPrice($planId)
	{
		$plans = new Plan();

		return Cache::rememberForever('planprice', function () use ($plans, $planId) {
			return $plans
				->with('activeprice')
				->where('id', $planId)
				->first();
		});
	}
}