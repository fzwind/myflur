<?php

namespace App\Tools;


use App\Models\UserBank;

class UserBanks {

	public function getUserBanks($userId)
	{
		$banks = new UserBank();
		return $banks->where('user_id',$userId)->where('is_active',1)->get();
	}

	public function getUserBankList($userId)
	{
		$bank = new UserBank();
		return $bank
			->where('user_id',$userId)
			->where('is_active',1)
			->get()
			->pluck('name_with_account','id');
	}
}