<?php

namespace App\Tools;


use App\Models\Pin;

class GeneratePin {

	public function getRandomCode($length = 12)
	{
		$length = (int) ($length > 1 ? $length : 12);
		$src = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$rand_code ='PKG';
		for ($i = 3; $i < $length; $i++)
		{
			$char = $src[mt_rand(0, strlen($src)-1)];
			$rand_code .= $char;
		}
		return $rand_code;
	}

	public function checkExistence($code)
	{
		if (Pin::where('code', '=', $code)->count() > 0) {
			return true;
		}
		return false;
	}

	public function insertNewPin($code, $planId)
	{
		$pin = new Pin();
		$pin['code'] = $code;
		$pin['plan_id'] = $planId;
		$pin['status'] = 0;
		$pin->save();
	}
}