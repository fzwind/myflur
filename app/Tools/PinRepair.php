<?php

namespace App\Tools;


use App\Models\Pin;
use App\Repositories\MemberStructureInterface;
use App\Repositories\PinInterface;
use App\Repositories\PinTransactionInterface;
use App\Repositories\UserPinInterface;
use Illuminate\Support\Facades\DB;

class PinRepair {

	public $pin;
	public $user_pin;
	public $pin_transaction;
	public $member_structure;

	public function __construct (
		PinInterface $pin,
		UserPinInterface $user_pin,
		PinTransactionInterface $pin_transaction,
		MemberStructureInterface $member_structure
	) {
		$this->pin = $pin;
		$this->user_pin = $user_pin;
		$this->pin_transaction = $pin_transaction;
		$this->member_structure = $member_structure;
	}

	public function repair()
	{
		// reset company PIN status to 0 (available)
		Pin::where('status',1)->update(['status' => 0]);

		// delete failed transactions
		$transaction_delete = $this->pin_transaction->getTransactionToDelete();
		$transaction_delete->delete();

		// truncate user_pins table
		DB::statement('SET FOREIGN_KEY_CHECKS=0;');
		DB::table('user_pins')->truncate();
		DB::statement('SET FOREIGN_KEY_CHECKS=1;');

		// update confirm date to earlier date, so pins can be sold to others
		$transaction_update = $this->pin_transaction->findByAttributes(['transaction_code' => 'PIN180904PRWS5']);
		$transaction_update->confirmed_at = '2018-09-02 06:25:42';
		$transaction_update->save();

		$all_transaction = $this->pin_transaction->getAllConfirmedTransaction();

		foreach ($all_transaction as $transaction) {

			if ($transaction->transaction_type == 'buy' && $transaction->from == 11) {

				DB::transaction(function () use($transaction) {
					$pins = $this->pin->getPinforUpdate($transaction)->get();

					if ($pins->count() < $transaction->amount) {
						echo "User : ". $transaction->from ." Kurang pin. \n";
						echo "Transaction Code : ". $transaction->transaction_code ."\n";
						echo "Jual ke : ". $transaction->to ."\n";
						echo "--- dibutuhkan : ". $transaction->amount .", dipunya : ". $pins->count() ."\n\n";
					}

					foreach ($pins as $pin) {
						$this->user_pin->create([
							'user_id' => $transaction->to,
							'pin_transaction_id' => $transaction->id,
							'pin_code' => $pin->code,
							'status' => 0,
						]);

						$pin->status = 1;
						$pin->save();
					}

				},3);

			} elseif ($transaction->transaction_type == 'buy' && $transaction->from != 11) {

				DB::transaction(function () use($transaction) {
					$pins = $this->user_pin->getPinforUpdate($transaction)->get();

					if ($pins->count() < $transaction->amount) {
						echo "User : ". $transaction->from ." Kurang pin. \n";
						echo "Transaction Code : ". $transaction->transaction_code ."\n";
						echo "Jual ke : ". $transaction->to ."\n";
						echo "--- dibutuhkan : ". $transaction->amount .", dipunya : ". $pins->count() ."\n\n";
					}

					foreach ($pins as $pin) {
						$this->user_pin->create([
							'user_id' => $transaction->to,
							'pin_transaction_id' => $transaction->id,
							'pin_code' => $pin->pin_code,
							'status' => 0,
						]);

						$pin->status = 1;
						$pin->save();
					}

				},3);

			}
		}

		$structures = $this->member_structure->getAllForRepair();

		foreach ($structures as $structure) {
			DB::transaction(function () use($structure) {
				$pin = $this->user_pin->getPinforUse($structure->user->sponsor_id,$structure->user)->first();

				$pin_code = (is_null($pin)) ? null : $pin->pin_code;

				if ($pin_code == null) {
					echo "Placement oleh : ". $structure->user->sponsor_id ." , PIN Lama : ". $structure->pin_code ." PIN Baru : ". $pin_code ."\n";
				}

				$structure->pin_code = $pin_code;
				$structure->save();

				if (! is_null($pin_code)) {
					$pin->status = 3;
					$pin->save();
				}
			},3);
		}

		echo "DONE \n";

	}
}