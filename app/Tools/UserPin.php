<?php

namespace App\Tools;


class UserPin {

	public function count_pin_all($userId)
	{
		$pin = new \App\Models\UserPin();

		return $pin->where('user_id', $userId)->count();
	}

	public function count_pin_available($userId)
	{
		$pin = new \App\Models\UserPin();

		return $pin->where('user_id', $userId)->where('status', 0)->count();
	}

	public function count_pin_used($userId)
	{
		$pin = new \App\Models\UserPin();

		return $pin->where('user_id', $userId)->where('status', 3)->count();
	}

	public function count_pin_sold($userId)
	{
		$pin = new \App\Models\UserPin();

		return $pin->where('user_id', $userId)->where('status', 1)->count();
	}

	public function count_pin_transferred($userId)
	{
		$pin = new \App\Models\UserPin();

		return $pin->where('user_id', $userId)->where('status', 2)->count();
	}

}