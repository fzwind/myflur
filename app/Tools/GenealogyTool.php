<?php

namespace App\Tools;

use App\Models\Memberstructure;

trait GenealogyTool
{
    public function fillEmpty($structure, $topLevel)
    {
        $cls_structure = new Memberstructure();

        $i = 0;
        foreach ($structure as $node) {
            if ($node->level < ($topLevel + 2)) {
                $child = $cls_structure->where('parent_id', $node->id)->get();
                if ($child->count() == 0) {
                    $item = new Memberstructure();
                    $item->id = $i;
                    $item->user_id = 0;
                    $item->parent_id = $node->id;
                    $item->code = 0;
                    $item->position = 1;
                    $item->level = 0;
                    $item->pin_code = 0;
                    $item->ic_company = 0;
                    $structure->push($item);
                    $i++;

                    $item = new Memberstructure();
                    $item->id = $i;
                    $item->user_id = 0;
                    $item->parent_id = $node->id;
                    $item->code = 0;
                    $item->position = 2;
                    $item->level = 0;
                    $item->pin_code = 0;
                    $item->ic_company = 0;
                    $structure->push($item);
                    $i++;
                }

                if ($child->count() == 1) {
                    $item = new Memberstructure();
                    $item->id = $i;
                    $item->user_id = 0;
                    $item->parent_id = $node->id;
                    $item->code = 0;
                    $item->position = ($child[0]->position == 1) ? 2 : 1;
                    $item->level = 0;
                    $item->pin_code = 0;
                    $item->ic_company = 0;
                    $structure->push($item);
                    $i++;
                }
            }
        }

        return $structure;
    }

    public function getEmpty($structure)
    {
    }
}
