<?php

namespace App\Tools;

use App\Models\UniqueDigit as UniqueCode;
use Carbon\Carbon;

class UniqueDigit {

	public function checkExistence($digit, $days)
	{
		$unique = new UniqueCode();

		if ($unique
			    ->where('unique_digit', '=', $digit)
			    ->where('created_at', '>', Carbon::now('Asia/Jakarta')->subDay($days))
			    ->count() > 0) {
			return true;
		}
		return false;
	}

	public function insertNewDigit($digit)
	{
		$unique = new UniqueCode();
		$unique['unique_digit'] = $digit;
		$unique->save();
	}
}