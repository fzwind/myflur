<?php

namespace App\Console;

use App\Console\Commands\Withdrawal;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
        Withdrawal::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();
        try {
            // $days = json_decode(DB::table('withdrawal_settings')->first()->wd_days);
            // $cron_days = '';
            // foreach ($days as $key => $day) {
            //     $cron_days .= ($key == 0) ? ($day+1) : ','. ($day+1);
            // }
            //
            // $cron_wd = '0 17 * * '. $cron_days .'';
            // $schedule->command('myflur:withdrawal')->cron($cron_wd);
            $schedule->command('myflur:withdrawal')->cron('0 17 * * 1-6');

            // $cron_reward = '07 17 * * '. $cron_days .'';
            // $schedule->command('myflur:reward')->cron($cron_reward);
        } catch (\Exception $e) {
            Log::error(
                'System Scheduler : '.$e
          );
        }
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
