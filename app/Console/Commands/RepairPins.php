<?php

namespace App\Console\Commands;

use App\Tools\PinRepair;
use Illuminate\Console\Command;

class RepairPins extends Command
{
    protected $signature = 'myflur:repairpins';

    protected $description = 'Repair pin data';

    public $pin_repair;

    public function __construct(PinRepair $pin_repair)
    {
        parent::__construct();

        $this->pin_repair = $pin_repair;
    }

    public function handle()
    {
        $this->pin_repair->repair();
    }
}
