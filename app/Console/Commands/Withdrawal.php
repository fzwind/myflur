<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Tools\WithdrawalService;

class Withdrawal extends Command
{
    protected $signature = 'myflur:withdrawal {date?}';

    protected $description = 'Run withdrawal calculation and distribution';

    public $withdrawal;

    public function __construct(
        WithdrawalService $withdrawal
    ) {
        parent::__construct();

        $this->withdrawal = $withdrawal;
    }

    public function handle()
    {
        $this->withdrawal->run($this->argument('date'));
    }
}
