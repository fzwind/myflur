<?php

namespace App\Console\Commands;

use App\Tools\SetRewardStatusService;
use Illuminate\Console\Command;

class SetRewardStatus extends Command
{
    protected $signature = 'myflur:reward {--mode=}';

    protected $description = 'Set reward status of each member.';

    public $set_reward_status;

    public function __construct(
        SetRewardStatusService $set_reward_status
    ) {
        parent::__construct();

        $this->set_reward_status = $set_reward_status;
    }

    public function handle()
    {
        $this->set_reward_status->run($this->option('mode'));
    }
}
