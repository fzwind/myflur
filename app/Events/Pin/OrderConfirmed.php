<?php

namespace App\Events\Pin;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class OrderConfirmed
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $pin_transaction;

    public function __construct($pin_transaction)
    {
    	$this->pin_transaction = $pin_transaction;
    }

    public function broadcastOn()
    {
        // return new PrivateChannel('channel-name');
    }
}
