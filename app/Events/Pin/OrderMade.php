<?php

namespace App\Events\Pin;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class OrderMade
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $pin_transaction;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($pin_transaction)
    {
        $this->pin_transaction = $pin_transaction;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
    	/*if ($this->pin_transaction->transaction_type == 'buy' && $this->pin_transaction->fromuser->id == 11) {
		    return new PrivateChannel('pin-order-admin');
	    } else if ($this->pin_transaction->transaction_type == 'buy' && $this->pin_transaction->fromuser->id != 11) {
		    return new PrivateChannel('pin-order-'. $this->pin_transaction->fromuser->id);
	    }*/
    }
}
