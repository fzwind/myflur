<?php

namespace App\Events\Members;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class UserPlaced
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $user;
    public $structure;

    public function __construct($user, $structure)
    {
        $this->user = $user;
        $this->structure = $structure;
    }

    public function broadcastOn()
    {
        //return new PrivateChannel('channel-name');
    }
}
