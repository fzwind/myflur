<?php

namespace App\Repositories\Repo;

use App\Models\FlushedPairing;
use App\Repositories\FlushedPairingInterface;
use Carbon\Carbon;

class FlushedPairingRepository extends BaseRepository implements FlushedPairingInterface
{
    public function __construct(FlushedPairing $model)
    {
        parent::__construct($model);
    }

    public function getFlushedPairing($userId = null)
    {
        $userId = (is_null($userId)) ? auth()->id() : $userId;

        return $this->model
            ->where('user_id', $userId)
            ->get();
    }

    public function updateFlushedPairing($leftRight, $userId, $placementId, $date = null)
    {
        if (is_null($date)) {
            $date = Carbon::now('Asia/Jakarta');
        }

        $flushed = $this->model
            ->where('user_id', $userId)
            ->whereDate('flushed_at', $date)
            ->first();

        if ($flushed) {
            if ($leftRight == 'left') {
                $flushed->update(['flushed_left' => ($flushed->flushed_left + 1)]);
            }
            if ($leftRight == 'right') {
                $flushed->update(['flushed_right' => ($flushed->flushed_left + 1)]);
            }
        } else {
            $data = [];
            $data['user_id'] = $userId;
            if ($leftRight == 'left') {
                $data['flushed_left'] = 1;
                $data['flushed_right'] = 0;
            }
            if ($leftRight == 'right') {
                $data['flushed_left'] = 0;
                $data['flushed_right'] = 1;
            }
            $data['placement_id'] = $placementId;
            $data['flushed_at'] = Carbon::now('Asia/Jakarta');
            $this->model->create($data);
        }
    }
}
