<?php

namespace App\Repositories\Repo;


use App\Models\StockistSetting;
use App\Repositories\StockistSettingInterface;

class StockistSettingRepository extends BaseRepository implements StockistSettingInterface {

	public function __construct ( StockistSetting $model ) {
		parent::__construct( $model );
	}

	public function getWithPlan ( $stockistSettingId = null ) {
		if (is_null($stockistSettingId)) {
			return $this->model->with('plan')->get();
		}

		return $this->model->where('id', '=', $stockistSettingId)->with('plan')->get();
	}
}