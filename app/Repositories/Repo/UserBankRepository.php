<?php

namespace App\Repositories\Repo;


use App\Models\UserBank;
use App\Repositories\UserBankInterface;

class UserBankRepository extends BaseRepository implements UserBankInterface {

	public function __construct ( UserBank $model ) {
		parent::__construct( $model );
	}

	public function getAllBanks ()
	{
		return $this->model
			->with('bank')
			->where('user_id', auth()->id())
			->get();
	}

	public function toggleBankStatus ( $bankId )
	{
		$bank = $this->model->find($bankId);

		if ($bank->is_active == 0) {
			$bank->update(['is_active' => 1]);
		} else {
			$bank->update(['is_active' => 0]);
		}

		return $bank;
	}


}