<?php

namespace App\Repositories\Repo;

use App\Models\MemberRewardStatus;
use App\Repositories\MemberRewardStatusInterface;

class MemberRewardStatusRepository extends BaseRepository implements MemberRewardStatusInterface
{
    public function __construct(MemberRewardStatus $model)
    {
        parent::__construct($model);
    }

    public function setStatus($userId, $rewardSettingId)
    {
        $data = [];
        $data['user_id'] = $userId;
        $data['reward_setting_id'] = $rewardSettingId;

        $this->model->updateOrCreate($data);
    }

    public function getStatus($rewardSettingId, $userId = null)
    {
        if (! is_null($userId)) {
            return $this->model
                ->where('user_id', $userId)
                ->where('reward_setting_id', $rewardSettingId)
                ->exists();
        }

        return $this->model
            ->where('user_id', auth()->id())
            ->where('reward_setting_id', $rewardSettingId)
            ->exists();
    }
}
