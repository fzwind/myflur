<?php

namespace App\Repositories\Repo;

use App\Events\Members\UserPlaced;
use App\Models\Memberstructure;
use App\Repositories\MemberStructureInterface;
use App\Repositories\UserInterface;
use App\Repositories\UserPinInterface;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class MemberStructureRepository extends BaseRepository implements MemberStructureInterface
{
    public $user;
    public $user_pin;

    public function __construct(
        Memberstructure $model,
        UserInterface $user,
        UserPinInterface $user_pin
    ) {
        parent::__construct($model);
        $this->user = $user;
        $this->user_pin = $user_pin;
    }

    public function getByUserId($userId)
    {
        return $this->model
            ->where('user_id', $userId)
            ->first();
    }

    public function getMyTree($topId = null)
    {
        if ($topId == null) {
            $topId = auth()->id();
        }

        // check if its on its own structure
        $check_length = strlen(auth()->user()->memberstructure->code);
        $check = $this->model->where('user_id', $topId)->first();

        // can not access structures above
        if (strlen($check->code) < $check_length) {
            return false;
        }

        // can not access structure across
        if (substr($check->code, 0, $check_length) != auth()->user()->memberstructure->code) {
            return false;
        }

        $top = $this->model->where('user_id', $topId)->first();
        return $this->model
            ->with('user')
            // ->with('memberReward')
            ->where('code', 'like', $top->code .'%')
            ->where('level', '<=', ($check->level + 2))
            ->selectRaw("*, CONCAT(code,'-',level+1,'.1%') as left_code, CONCAT(code,'-',level+1,'.2%') as right_code, (SELECT count(id) FROM memberstructures ls WHERE ls.code like left_code) as left_count, (SELECT count(id) FROM memberstructures ls WHERE ls.code like right_code) as right_count, (SELECT count(ls.id) FROM memberstructures ls join member_reward_statuses as st on ls.user_id = st.user_id where st.reward_setting_id = 1 AND ls.code like left_code ) as vleft_count, (SELECT count(ls.id) FROM memberstructures ls join member_reward_statuses as st on ls.user_id = st.user_id where st.reward_setting_id = 1 AND ls.code like right_code ) as vright_count")
            // ->orderBy('position','asc')
            // ->take(7)
            ->get();
    }

    public function getPlacementList()
    {
        return $this->user->getPlacementList(auth()->id());
    }

    public function doPlacement($parentId, $userId, $position, $mode = 'register')
    {
        $parent = $this->model->where('id', $parentId)->first();
        $user = $this->user->find($userId);

        $pin = $this->user_pin->getPinforUse(auth()->id(), $user)->first();

        if ($mode == 'register') {
            $data = [];
            $data['user_id'] = $userId;
            $data['parent_id'] = $parentId;
            $data['code'] = $parent->code .'-'. ($parent->level + 1) .'.'. $position;
            $data['position'] = $position;
            $data['level'] = $parent->level + 1;
            $data['pin_code'] = $pin->pin_code;

            try {
                DB::transaction(function () use ($data, $user, $pin) {
                    $structure = $this->model->create($data);

                    // $a = event(new UserPlaced($user, $structure));
                    // dd($a);

                    event(new UserPlaced($user, $structure));

                    $this->user->update($user, ['is_active' => 1, 'active_at' => Carbon::now()]);
                    $this->user_pin->update($pin, ['status' => 3]);
                }, 3);
            } catch (\Exception $e) {
                dd($e);
                return false;
            }
        }
        return true;
    }

    public function countMyLeftStructure($userStructure, $date = null, $rewardStart = null, $rewardEnd = null)
    {
        $leftCode = $userStructure->code .'-'. ($userStructure->level + 1) .'.1';
        if (! is_null($date)) {
            return $this->model
                ->where('code', 'like', $leftCode .'%')
                ->whereDate('created_at', $date)
                ->count();
        }

        if (! is_null($rewardStart) && ! is_null($rewardEnd)) {
            return $this->model
                ->where('code', 'like', $leftCode .'%')
                ->whereBetween('created_at', [$rewardStart, $rewardEnd])
                ->count();
        }

        return $this->model
            ->where('code', 'like', $leftCode .'%')
            ->count();
    }

    public function countMyRightStructure($userStructure, $date = null, $rewardStart = null, $rewardEnd = null)
    {
        $rightCode = $userStructure->code .'-'. ($userStructure->level + 1) .'.2';
        if (! is_null($date)) {
            return $this->model
                ->where('code', 'like', $rightCode . '%')
                ->whereDate('created_at', $date)
                ->count();
        }

        if (! is_null($rewardStart) && ! is_null($rewardEnd)) {
            return $this->model
                ->where('code', 'like', $rightCode . '%')
                ->whereBetween('created_at', [$rewardStart, $rewardEnd])
                ->count();
        }

        return $this->model
            ->where('code', 'like', $rightCode . '%')
            ->count();
    }

    public function countMyLeftPairTurnover($userStructure, $date = null)
    {
        // TODO: Implement countMyLeftTurnover() method.
    }

    public function countMyRightPairTurnover($userStructure, $date = null)
    {
        // TODO: Implement countMyRightTurnover() method.
    }

    public function countMyLeftReward($userStructure, $rewardSettingId, $date = null)
    {
        // $top = $this->model->where('user_id',$userId)->first();
        // $top_code_left = $top->code .'-'. ($top->level + 1) .'.1%';
        // return $this->model
        // 	->whereHas('memberReward', function ($query) use($rewardSettingId) {
        // 		$query->where('reward_setting_id', $rewardSettingId);
        // 	})
        // 	->where('code','like',$top_code_left)
        // 	->orderBy('position','asc')
        // 	->count();
        $leftCode = $userStructure->code .'-'. ($userStructure->level + 1) .'.1';
        return $this->model
            ->whereHas('memberReward', function ($query) use ($rewardSettingId) {
                $query->where('reward_setting_id', $rewardSettingId);
            })
            ->where('code', 'like', $leftCode .'%')
            ->orderBy('position', 'asc')
            ->count();
    }

    public function countMyRightReward($userStructure, $rewardSettingId, $date = null)
    {
        // $top = $this->model->where('user_id',$userId)->first();
        // $top_code_left = $top->code .'-'. ($top->level + 1) .'.2%';
        // return $this->model
        // 	->whereHas('memberReward', function ($query) use($rewardSettingId) {
        // 		$query->where('reward_setting_id', $rewardSettingId);
        // 	})
        // 	->where('code','like',$top_code_left)
        // 	->orderBy('position','asc')
        // 	->count();
        $rightCode = $userStructure->code .'-'. ($userStructure->level + 1) .'.2';
        return $this->model
            ->whereHas('memberReward', function ($query) use ($rewardSettingId) {
                $query->where('reward_setting_id', $rewardSettingId);
            })
            ->where('code', 'like', $rightCode .'%')
            ->orderBy('position', 'asc')
            ->count();
    }

    public function getAllParents($userId)
    {
        return $this->model
            ->with('AllParents')
            ->where('user_id', $userId)
            ->first();
    }

    public function getAllForRepair()
    {
        return $this->model
            ->with('user')
            ->where('id', '>', 16)
            ->lockForUpdate()
            ->get();
    }
}
