<?php

namespace App\Repositories\Repo;

use App\Models\Bonus;
use App\Repositories\BonusInterface;
use Carbon\Carbon;

class BonusRepository extends BaseRepository implements BonusInterface
{
    public function __construct(Bonus $model)
    {
        parent::__construct($model);
    }

    public function getMyBonus($userId = null)
    {
        $owner = (is_null($userId)) ? auth()->id() : $userId;

        return $this->model
            ->where('user_id', $owner)
            ->get();
    }

    public function getMySponsorBonus($userId = null)
    {
        $owner = (is_null($userId)) ? auth()->id() : $userId;

        return $this->model
            ->where('user_id', $owner)
            ->where('bonus_sponsor', '>', 0)
            ->get();
    }

    public function getMyPairingBonus($userId = null)
    {
        $owner = (is_null($userId)) ? auth()->id() : $userId;

        return $this->model
            ->where('user_id', $owner)
            ->where('bonus_pairing', '>', 0)
            ->get();
    }

    public function getMyPairingLevelBonus($userId = null)
    {
        $owner = (is_null($userId)) ? auth()->id() : $userId;

        return $this->model
            ->where('user_id', $owner)
            ->where('bonus_pairing_level', '>', 0)
            ->get();
    }

    public function getThisWeekSponsorBonus($userId = null)
    {
        $owner = (is_null($userId)) ? auth()->id() : $userId;

        return $this->model
            ->where('user_id', $owner)
            ->where('bonus_sponsor', '>', 0)
            ->whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])
            ->sum('bonus_sponsor');
    }

    public function getLastWeekSponsorBonus($userId = null)
    {
        $owner = (is_null($userId)) ? auth()->id() : $userId;

        return $this->model
            ->where('user_id', $owner)
            ->where('bonus_sponsor', '>', 0)
            ->whereBetween('created_at', [Carbon::now()->startOfWeek()->subWeek(), Carbon::now()->endOfWeek()->subWeek()])
            ->sum('bonus_sponsor');
    }

    public function getTotalBonusSponsor($userId = null)
    {
        $owner = (is_null($userId)) ? auth()->id() : $userId;

        return $this->model
            ->where('user_id', $owner)
            ->where('bonus_sponsor', '>', 0)
            ->sum('bonus_sponsor');
    }

    public function getThisWeekPairingBonus($userId = null)
    {
        $owner = (is_null($userId)) ? auth()->id() : $userId;

        return $this->model
            ->where('user_id', $owner)
            ->where('bonus_pairing', '>', 0)
            ->whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])
            ->sum('bonus_pairing');
    }

    public function getLastWeekPairingBonus($userId = null)
    {
        $owner = (is_null($userId)) ? auth()->id() : $userId;

        return $this->model
            ->where('user_id', $owner)
            ->where('bonus_pairing', '>', 0)
            ->whereBetween('created_at', [Carbon::now()->startOfWeek()->subWeek(), Carbon::now()->endOfWeek()->subWeek()])
            ->sum('bonus_pairing');
    }

    public function getTotalBonusPairing($userId = null)
    {
        $owner = (is_null($userId)) ? auth()->id() : $userId;

        return $this->model
            ->where('user_id', $owner)
            ->where('bonus_pairing', '>', 0)
            ->sum('bonus_pairing');
    }

    public function getThisWeekPairingLevelBonus($userId = null)
    {
        $owner = (is_null($userId)) ? auth()->id() : $userId;

        return $this->model
            ->where('user_id', $owner)
            ->where('bonus_pairing_level', '>', 0)
            ->whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])
            ->sum('bonus_pairing_level');
    }

    public function getLastWeekPairingLevelBonus($userId = null)
    {
        $owner = (is_null($userId)) ? auth()->id() : $userId;

        return $this->model
            ->where('user_id', $owner)
            ->where('bonus_pairing_level', '>', 0)
            ->whereBetween('created_at', [Carbon::now()->startOfWeek()->subWeek(), Carbon::now()->endOfWeek()->subWeek()])
            ->sum('bonus_pairing_level');
    }

    public function getTotalBonusPairingLevel($userId = null)
    {
        $owner = (is_null($userId)) ? auth()->id() : $userId;

        return $this->model
            ->where('user_id', $owner)
            ->where('bonus_pairing_level', '>', 0)
            ->sum('bonus_pairing_level');
    }

    public function getAllBonusForWithdrawal($date = null)
    {
        if (is_null($date)) {
            return $this->model
                ->groupBy('user_id')
                ->selectRaw('user_id, sum(bonus_sponsor) as sponsor, sum(bonus_pairing) as pairing, sum(bonus_pairing_level) as pairing_level, sum(bonus_level) as level')
                ->get();
        }

        return $this->model
            ->groupBy('user_id')
            ->selectRaw('user_id, sum(bonus_sponsor) as sponsor, sum(bonus_pairing) as pairing, sum(bonus_pairing_level) as pairing_level, sum(bonus_level) as level')
            ->where('created_at', '<=', $date)
            ->get();
    }
}
