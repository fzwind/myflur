<?php

namespace App\Repositories\Repo;


use App\Models\District;
use App\Repositories\DistrictInterface;

class DistrictRepository extends BaseRepository implements DistrictInterface {

	public function __construct ( District $model ) {
		parent::__construct( $model );
	}

	public function getDistrictList ( $regencyId = null ) {
		if (is_null($regencyId)) {
			return $this->model
				->pluck('name','id');
		}

		return $this->model
			->where('regency_id', $regencyId)
			->pluck('name','id');
	}
}