<?php

namespace App\Repositories\Repo;


use App\Models\Village;
use App\Repositories\VillageInterface;

class VillageRepository extends BaseRepository implements VillageInterface {

	public function __construct ( Village $model ) {
		parent::__construct( $model );
	}

	public function getVillageList ( $districtId = null ) {
		if (is_null($districtId)) {
			return $this->model
				->pluck('name','id');
		}

		return $this->model
			->where('district_id', $districtId)
			->pluck('name','id');
	}
}