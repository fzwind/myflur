<?php

namespace App\Repositories\Repo;

use App\Models\Reward;
use App\Repositories\RewardInterface;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class RewardRepository extends BaseRepository implements RewardInterface
{
    public function __construct(Reward $model)
    {
        parent::__construct($model);
    }

    public function getAllWithRelations()
    {
        return $this->model
            ->with('rewardsetting.plan')
            ->get();
    }

    public function getRewardAchievements($userId = null)
    {
        $user_id = is_null($userId) ? auth()->id() : $userId;
        $this->model->userId = $user_id;
        return $this->model
            ->with('rewardsetting.plan')
            ->doesntHave('authmemberreward')
            ->orWhereHas('authmemberreward')
            ->get();
    }

    public function create($data)
    {
        $data['active_at'] = $data['active_at'];
        if (! is_null($data['inactive_at'])) {
            $data['inactive_at'] = $data['inactive_at'];
        }

        return parent::create($data);
    }

    public function getBySetting($settingId)
    {
        return $this->model
            ->where('inactive_at', '>=', Carbon::now('Asia/Jakarta'))
            ->orWhere('inactive_at', null)
            ->where('reward_setting_id', $settingId)
            ->where('active_at', '<=', Carbon::now('Asia/Jakarta'))
            ->get();
    }

    public function claimReward($memberRewardId)
    {
        DB::transaction(function () use ($memberRewardId) {
        }, 3);
    }
}
