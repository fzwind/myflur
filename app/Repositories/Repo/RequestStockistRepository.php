<?php

namespace App\Repositories\Repo;

use App\Models\RequestStockist;
use App\Repositories\RequestStockistInterface;
use App\Repositories\UserInterface;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class RequestStockistRepository extends BaseRepository implements RequestStockistInterface
{
    public $user;

    public function __construct(
        RequestStockist $model,
        UserInterface $user
    ) {
        parent::__construct($model);
        $this->user = $user;
    }

    public function requestStockist($data)
    {
        try {
            $this->model->create(
                [
                    'user_id' => $data['user_id'],
                    'request_type' => $data['request_type'],
                    'status' => 0
                ]
            );
        } catch (\Exception $e) {
            return false;
        }

        return true;
    }

    public function myRequestStatus()
    {
        return $this->model
            ->where('user_id', auth()->id())
            ->orderBy('created_at', 'desc')
            ->first();
    }

    public function getUnApprovedRequests()
    {
        return $this->model
            ->with('user', 'user.province', 'user.regency')
            ->where('status', 0)
            ->get();
    }

    public function getApprovedAndRejectedRequests()
    {
        return $this->model
            ->with('user', 'user.province', 'user.regency')
            ->where('status', '>', 0)
            ->get();
    }

    public function approveRequest($request_stockist)
    {
        DB::beginTransaction();

        try {
            $user_requesting = $this->user->find($request_stockist->user_id);

            $user_data = [
                'is_stockist' => 1,
                'stockist_type' => $request_stockist->request_type,
                'stockist_at' => Carbon::now('Asia/Jakarta'),
            ];

            $this->user->update($user_requesting, $user_data);

            $request_stockist->update(
                [
                    'status' => 1
                ]
            );
        } catch (\Exception $e) {
            DB::rollBack();
            return false;
        }
        DB::commit();
        return true;
    }
}
