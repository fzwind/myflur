<?php

namespace App\Repositories\Repo;


use App\Models\MemberRewardSaving;
use App\Repositories\MemberRewardSavingInterface;

class MemberRewardSavingRepository extends BaseRepository implements MemberRewardSavingInterface {

	public function __construct ( MemberRewardSaving $model ) {
		parent::__construct( $model );
	}

	public function getSum ( $userId, $rewardSettingId ) {
		return $this->model
			->selectRaw('sum(saving_amount) as sum_amount')
			->where('user_id', $userId)
			->where('reward_setting_id', $rewardSettingId)
			->first();
	}

	public function getWithRelations ( $userId = null, $reward_setting_id ) {
		if (is_null($userId)) {
			return $this->model
				->with('reward_setting', 'withdrawal')
				->where('reward_setting_id', $reward_setting_id)
				->orderBy('created_at', 'desc')
				->get();
		}

		return $this->model
			->with('reward_setting', 'withdrawal')
			->where('user_id', $userId)
			->where('reward_setting_id', $reward_setting_id)
			->orderBy('created_at', 'desc')
			->get();
	}
}