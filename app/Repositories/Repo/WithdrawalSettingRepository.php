<?php

namespace App\Repositories\Repo;

use App\Models\WithdrawalSetting;
use App\Repositories\WithdrawalSettingInterface;
use Carbon\Carbon;

class WithdrawalSettingRepository extends BaseRepository implements WithdrawalSettingInterface
{
    public function __construct(WithdrawalSetting $model)
    {
        parent::__construct($model);
    }

    public function create($data)
    {
        $data['active_at'] = $data['active_at'];
        $data['wd_days'] = json_encode($data['wd_days']);

        return parent::create($data);
    }

    public function getActiveSetting()
    {
        return $this->model
            ->where('active_at', '<=', Carbon::now())
            ->orderBy('active_at', 'desc')
            ->first();
    }
}
