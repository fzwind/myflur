<?php

namespace App\Repositories\Repo;


use App\Models\Regency;
use App\Repositories\RegencyInterface;

class RegencyRepository extends BaseRepository implements RegencyInterface {

	public function __construct ( Regency $model ) {
		parent::__construct( $model );
	}

	public function getRegencyList ($provinceId = null) {
		if (is_null($provinceId)) {
			return $this->model
				->pluck('name','id');
		}

		return $this->model
			->where('province_id', $provinceId)
			->pluck('name','id');
	}
}