<?php

namespace App\Repositories\Repo;


use App\Models\BonusLevelSetting;
use App\Repositories\BonusLevelSettingInterface;

class BonusLevelSettingRepository extends BaseRepository implements BonusLevelSettingInterface {

	public function __construct ( BonusLevelSetting $model ) {
		parent::__construct( $model );
	}

}