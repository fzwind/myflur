<?php

namespace App\Repositories\Repo;


use App\Models\BonusSetting;
use App\Repositories\BonusSettingInterface;

class BonusSettingRepository extends BaseRepository implements BonusSettingInterface {

	public function __construct ( BonusSetting $model ) {
		parent::__construct( $model );
	}

	public function getWithPlan ( $bonusSettingId = null ) {
		if (is_null($bonusSettingId)) {
			return $this->model->with('plan')->get();
		}

		return $this->model->where('id', '=', $bonusSettingId)->with('plan')->get();
	}
}