<?php

namespace App\Repositories\Repo;


use App\Models\SystemSetting;
use App\Repositories\SystemSettingsInterface;

class SystemSettingsRepository extends BaseRepository implements SystemSettingsInterface {

	public function __construct ( SystemSetting $model ) {
		parent::__construct( $model );
	}
}