<?php

namespace App\Repositories\Repo;

use App\Models\Plan;
use App\Models\RewardSetting;
use App\Repositories\RewardSettingInterface;
use Carbon\Carbon;

class RewardSettingRepository extends BaseRepository implements RewardSettingInterface
{
    public $plan;

    public function __construct(
        RewardSetting $model,
        Plan $plan
    ) {
        parent::__construct($model);

        $this->plan = $plan;
    }

    public function getAllWithRelations()
    {
        return $this->model
            ->with('plan', 'bonussetting', 'prerequisite')
            ->get();
    }

    public function getAllActive()
    {
        return $this->model
            ->where('active_at', '<', Carbon::now('Asia/Jakarta'))
            ->where(function ($query) {
                $query->where('inactive_at', '>=', Carbon::now('Asia/Jakarta'))
                      ->orWhere('inactive_at', null);
            })
            ->get();
    }

    public function getRewardSettingList()
    {
        return $this->model
            ->where('active_at', '<', Carbon::now('Asia/Jakarta'))
            ->where(function ($query) {
                $query->where('inactive_at', '>=', Carbon::now('Asia/Jakarta'))
                      ->orWhere('inactive_at', null);
            })
            ->pluck('name', 'id');
    }

    public function create($data)
    {
        $bonus_setting = $this->plan->with('activebonussetting')->where('id', $data['plan_id'])->first();
        $bonus_setting_id = $bonus_setting->activebonussetting->id;

        $data['bonus_setting_id'] = $bonus_setting_id;
        $data['active_at'] = $data['active_at'];
        if (! is_null($data['inactive_at'])) {
            $data['inactive_at'] = $data['inactive_at'];
        }
        if (isset($data['deducted_bonuses'])) {
            $data['deducted_bonuses'] = json_encode($data['deducted_bonuses']);
        }

        return parent::create($data);
    }

    public function getRewardPotongLangsung($bonus)
    {
        return $this->model
            ->where('reward_type', 3)
            ->where('deducted_bonuses', 'like', '%'. $bonus .'%')
            ->where('active_at', '<=', Carbon::now('Asia/Jakarta'))
            ->where(function ($query) {
                $query->where('inactive_at', '>=', Carbon::now('Asia/Jakarta'))
                    ->orWhere('inactive_at', null);
            })
            ->get();
    }

    public function getRewardPotongBonus()
    {
        return $this->model
            ->where('reward_type', 2)
            ->where('active_at', '<=', Carbon::now('Asia/Jakarta'))
            ->where(function ($query) {
                $query->where('inactive_at', '>=', Carbon::now('Asia/Jakarta'))
                      ->orWhere('inactive_at', null);
            })
            ->get();
    }

    public function getRewardStructureOnly()
    {
        return $this->model
            ->where('reward_type', 1)
            ->where('active_at', '<=', Carbon::now('Asia/Jakarta'))
            ->where(function ($query) {
                $query->where('inactive_at', '>=', Carbon::now('Asia/Jakarta'))
                      ->orWhere('inactive_at', null);
            })
            ->get();
    }

    public function getRewardNonStructureOnly()
    {
        return $this->model
            ->where('reward_type', [2,3])
            ->where('active_at', '<=', Carbon::now('Asia/Jakarta'))
            ->where(function ($query) {
                $query->where('inactive_at', '>=', Carbon::now('Asia/Jakarta'))
                      ->orWhere('inactive_at', null);
            })
            ->get();
    }

    public function getById($rewardId)
    {
        return $this->model
            ->where('id', $rewardId)
            ->first();
    }
}
