<?php

namespace App\Repositories\Repo;


use App\Models\MemberPlanHistory;
use App\Repositories\MemberPlanHistoryInterface;

class MemberPlanHistoryRepository extends BaseRepository implements MemberPlanHistoryInterface {

	public function __construct ( MemberPlanHistory $model ) {
		parent::__construct( $model );
	}
}