<?php

namespace App\Repositories\Repo;

use App\Events\Members\UserRegistered;
use App\Models\User;
use App\Repositories\UserInterface;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserRepository extends BaseRepository implements UserInterface
{
    public function __construct(User $model)
    {
        parent::__construct($model);
    }

    public function getPlacementList($userId)
    {
        return $this->model
            ->doesntHave('memberstructure')
            ->where('sponsor_id', $userId)
            ->get();
    }

    public function getMySponsorList()
    {
        return $this->model
            ->where('sponsor_id', auth()->id())
            ->get();
    }

    public function getMyTreeList()
    {
        return $this->model
            ->with('memberstructure')
            ->whereHas('memberstructure', function ($query) {
                $query
                    ->where('code', 'like', auth()->user()->memberstructure->code .'%');
            })
            ->get()
            ->pluck('name_with_email_username', 'id');
    }


    public function registerNewUser($userData)
    {
        $data = [];
        $data['name'] = $userData['name'];
        $data['email'] = $userData['email'];
        $data['username'] = $userData['username'];
        $data['password'] = Hash::make($userData['password']);
        $data['sponsor_id'] = auth()->id();
        $data['join_plan'] = $userData['plan_id'];
        $data['current_plan'] = $userData['plan_id'];
        $data['current_plan_at'] = Carbon::now('Asia/Jakarta');

        DB::beginTransaction();

        $user = $this->model->create($data);

        if (! $user) {
            DB::rollBack();
            return false;
        }

        if (! $user->assignRole('members')) {
            DB::rollBack();
            return false;
        }

        if (! event(new UserRegistered($user))) {
            DB::rollBack();
            return false;
        }

        DB::commit();

        return $user;
    }

    public function getAllStockist()
    {
        return $this->model
            ->where('is_stockist', 1)
            ->where('is_active', 1)
            ->get();
    }
}
