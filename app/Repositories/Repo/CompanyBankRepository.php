<?php

namespace App\Repositories\Repo;


use App\Models\CompanyBank;
use App\Repositories\CompanyBankInterface;

class CompanyBankRepository extends BaseRepository implements CompanyBankInterface {

	public function __construct ( CompanyBank $model ) {
		parent::__construct( $model );
	}

	public function getAllBanks () {
		return $this->model->with('bank')->get();
	}
}