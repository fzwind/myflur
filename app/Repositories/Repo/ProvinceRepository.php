<?php

namespace App\Repositories\Repo;


use App\Models\Province;
use App\Repositories\ProvinceInterface;

class ProvinceRepository extends BaseRepository implements ProvinceInterface {

	public function __construct ( Province $model ) {
		parent::__construct( $model );
	}

	public function getProvinceList () {
		return $this->model
			->pluck('name','id');
	}

}