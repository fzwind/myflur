<?php

namespace App\Repositories\Repo;


use App\Models\UserPin;
use App\Repositories\UserPinInterface;

class UserPinRepository extends BaseRepository implements UserPinInterface {

	public function __construct ( UserPin $model ) {
		parent::__construct( $model );
	}

	public function getAll () {
		return $this->model->with('pin');
	}

	public function countForUse ( $userId ) {
		return $this->model
			->where('user_id', $userId)
			->where('status', 0)
			->count();
	}

	public function getAllWithTransaction ($userId) {
		return $this->model
			->with('pin')
			->with('pintransaction')
			->where('user_id', $userId)
			->get();
	}

	public function getAvailableWithTransaction ( $userId ) {
		return $this->model
			->with('pin')
			->with('pintransaction')
			->where('user_id', $userId)
			->where('status',0)
			->get();
	}

	public function getUsedWithTransaction ( $userId ) {
		return $this->model
			->with('pin')
			->with('pintransaction')
			->where('user_id', $userId)
			->where('status',3)
			->get();
	}

	public function getSoldWithTransaction ( $userId ) {
		return $this->model
			->with('pin')
			->with('pintransaction')
			->where('user_id', $userId)
			->where('status',1)
			->get();
	}

	public function getTransferredWithTransaction ( $userId ) {
		return $this->model
			->with('pin')
			->with('pintransaction')
			->where('user_id', $userId)
			->where('status',2)
			->get();
	}

	public function getPinforUpdate ( $pin_transaction ) {
		return $this->model
			->whereHas('pintransaction', function($q) use($pin_transaction) {
					$q->where('plan_id', $pin_transaction->plan_id);
				})
			->where('user_id', $pin_transaction->from)
			->where('status',0)
			->take($pin_transaction->amount)
			->lockForUpdate();
	}

	public function getPinforUse ( $ownerId, $newUser ) {
		return $this->model
			->whereHas('pintransaction', function($q) use($newUser) {
				$q->where('plan_id', $newUser->join_plan);
			})
			->where('user_id', $ownerId)
			->where('status', 0)
			->take(1)
			->lockForUpdate();
	}

	public function countMyPin ( $userId ) {
		return $this->model
			->where('user_id', $userId)
			->where('status', 0)
			->get()
			->count();
	}
}