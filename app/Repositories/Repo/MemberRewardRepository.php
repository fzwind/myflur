<?php

namespace App\Repositories\Repo;


use App\Models\MemberReward;
use App\Repositories\MemberRewardInterface;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class MemberRewardRepository extends BaseRepository implements MemberRewardInterface {

	public function __construct ( MemberReward $model ) {
		parent::__construct( $model );
	}

	public function getAllWithRelations () {
		// TODO: Implement getAllWithRelations() method.
	}

	public function setMemberReward ( $rewardId, $userId, $achievement ) {
		$member_reward = $this->model
			->where('reward_id',$rewardId)
			->where('user_id',$userId)
			->first();

		try {
            DB::transaction( function () use ($member_reward, $achievement, $userId, $rewardId) {
                if ($member_reward) {
                    $member_reward->left = $achievement->left;
                    $member_reward->right = $achievement->right;
                    if ($achievement->is_achieved && $member_reward->is_achieved == 0) {
                        $member_reward->is_achieved = $achievement->is_achieved;
                        $member_reward->achieved_at = $achievement->achieved_at;
                    }
                    $member_reward->save();
                } else {
                    $data['reward_id'] = $rewardId;
                    $data['user_id'] = $userId;
                    $data['left'] = $achievement->left;
                    $data['right'] = $achievement->right;
                    $data['is_achieved'] = $achievement->is_achieved;
                    $data['achieved_at'] = $achievement->achieved_at;

                    $this->model->create($data);
                }
            }, 3);
        } catch (\Exception $e) {
		    Log::error('ERROR : '. $e);
        }

	}

	public function getRewardClaim () {
		return $this->model
			->with('user','reward')
			->with('reward.rewardsetting','user.bank.bank')
			->where('is_claimed',1)
			->where('is_received',0)
			->orderBy('claimed_at','asc')
			->get();
	}

	public function getRewardClaimed () {
		return $this->model
			->with('user','reward')
			->with('reward.rewardsetting','user.bank.bank')
			->where('is_claimed',1)
			->where('is_received',1)
			->orderBy('claimed_at','asc')
			->get();
	}
}
