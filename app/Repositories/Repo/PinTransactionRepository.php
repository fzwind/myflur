<?php

namespace App\Repositories\Repo;

use App\Events\Pin\OrderConfirmed;
use App\Events\Pin\OrderMade;
use App\Events\Pin\OrderPaid;
use App\Models\PinTransaction;
use App\Repositories\PinTransactionInterface;
use App\Repositories\UserPinInterface;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class PinTransactionRepository extends BaseRepository implements PinTransactionInterface
{
    public $user_pin;

    public function __construct(
        PinTransaction $model,
        UserPinInterface $user_pin
    ) {
        parent::__construct($model);
        $this->user_pin = $user_pin;
    }

    public function processTransaction($from_to, $plan_id, $transaction_type, $amount)
    {
        $data = [];
        $data['plan_id'] = $plan_id;
        $data['from'] = ($transaction_type == 'buy') ? $from_to : auth()->user()->id;
        $data['to'] = ($transaction_type == 'buy') ? auth()->user()->id : $from_to;
        $data['transaction_type'] = $transaction_type;
        $data['amount'] = $amount;
        if ($data['from'] != 11) {
            $data['status'] = ($transaction_type == 'buy') ? 'paid' : 'confirmed';
            $data['paid_at'] = Carbon::now('Asia/Jakarta');
        } else {
            $data['status'] = ($transaction_type == 'buy') ? 'ordered' : 'confirmed';
        }
        $plan_price = plan_price($plan_id, stockist());
        $data['price'] = $amount * $plan_price;
        $data['unique_digit'] = generate_unique_digit();
        $data['transaction_code'] = generate_date_unique_code('PIN', 14, $this->model, 'transaction_code');
        $data['ordered_at'] = Carbon::now('Asia/Jakarta');

        if ($data['from'] != 11 && $this->user_pin->countMyPin($data['from']) < $amount) {
            return false;
        }

        DB::beginTransaction();

        if ($pin_transaction = $this->model->create($data)) {
            DB::commit();

            if (! event(new OrderMade($pin_transaction))) {
                return false;
            }

            return true;
        } else {
            DB::rollBack();
            return false;
        }
    }

    public function getMyTransaction()
    {
        return $this->model
            ->with('fromuser')
            ->with('touser')
            ->where('from', auth()->id())
            ->orWhere('to', auth()->id())
            ->orderBy('created_at', 'desc')
            ->get();
    }

    public function getMyTransactionHistory()
    {
        return $this->model
            ->with('fromuser')
            ->with('touser')
            ->whereIn('status', ['confirmed','rejected'])
            ->where(function ($q) {
                $q->where('from', auth()->id())
                      ->orWhere('to', auth()->id());
            })
            ->orderBy('created_at', 'desc')
            ->get();
    }

    public function getMyRunningTransaction()
    {
        return $this->model
            ->with('fromuser')
            ->with('touser')
            ->whereIn('status', ['ordered','paid'])
            ->where(function ($q) {
                $q->where('from', auth()->id())
                  ->orWhere('to', auth()->id());
            })
            ->orderBy('created_at', 'desc')
            ->get();
    }

    public function updateStatus($transaction, $status, $note = null)
    {
        $data = [];
        $data['status'] = $status;

        if ($status == 'paid') {
            $data['paid_at'] = Carbon::now('Asia/Jakarta');

            try {
                DB::transaction(function () use ($data, $transaction) {
                    $transaction->update($data);
                    event(new OrderPaid($transaction));
                }, 3);
            } catch (\Exception $e) {
                return false;
            }

            return true;
        }
        if ($status == 'confirmed') {
            $data['confirmed_at'] = Carbon::now('Asia/Jakarta');

            if ($transaction->from > 11 && $this->user_pin->countMyPin($transaction->from) < $transaction->amount) {
                return false;
            }

            if ($transaction->from <= 11) {
                generate_pin(12, $transaction->plan_id, $transaction->amount);

                if (system_settings()->logging) {
                    Log::info(
                        'Admin Store : '.
                        auth()->user()->name .' ('. auth()->user()->username .') generate '. number_format($transaction->amount, 0, ',', '.') .' new Pin for plan ID : '. $transaction->plan_id .' successfully'
                    );
                }
            }

            try {
                DB::transaction(function () use ($data, $transaction) {
                    $transaction->update($data);
                    event(new OrderConfirmed($transaction));
                }, 3);
            } catch (\Exception $e) {
                return false;
            }

            return true;
        }
        if ($status == 'rejected') {
            $data['rejected_at'] = Carbon::now('Asia/Jakarta');
            $data['status_note'] = $note;

            try {
                $transaction->update($data);
            } catch (\Exception $e) {
                return false;
            }

            return true;
        }
    }

    public function getCompanyPinTransaction()
    {
        return $this->model
            ->with('fromuser')
            ->with('touser')
            ->where('from', 11)
            ->where(function ($q) {
                $q->where('status', 'ordered')
                  ->orWhere('status', 'paid');
            })
            ->orderBy('created_at', 'desc')
            ->get();
    }

    public function getCompanyPinHistory()
    {
        return $this->model
            ->with('fromuser')
            ->with('touser')
            ->where('from', 11)
            ->where(function ($q) {
                $q->where('status', 'confirmed')
                  ->orWhere('status', 'rejected');
            })
            ->orderBy('confirmed_at', 'desc')
            ->orderBy('rejected_at', 'desc')
            ->get();
    }

    public function getAllConfirmedTransaction()
    {
        return $this->model
            ->where('status', 'confirmed')
            ->orderBy('confirmed_at', 'asc')
            ->get();
    }

    public function getTransactionToDelete()
    {
        return $this->model
            ->whereNotIn('id', function ($query) {
                $query->select('pin_transaction_id')->from('user_pins');
            });
    }
}
