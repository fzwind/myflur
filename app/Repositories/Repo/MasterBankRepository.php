<?php

namespace App\Repositories\Repo;


use App\Models\MasterBank;
use App\Repositories\MasterBankInterface;

class MasterBankRepository extends BaseRepository implements MasterBankInterface {

	public function __construct ( MasterBank $model ) {
		parent::__construct( $model );
	}

	public function getActiveMasterBank () {
		return $this->model
			->where('is_active',1)
			->get();
	}

	public function getActiveMasterBankList () {
		return $this->model
			->where('is_active',1)
			->pluck('name', 'id');
	}
}