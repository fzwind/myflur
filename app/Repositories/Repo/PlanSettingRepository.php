<?php

namespace App\Repositories\Repo;


use App\Models\Plan;
use App\Repositories\PlanPriceHistoryInterface;
use App\Repositories\PlanSettingInterface;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class PlanSettingRepository extends BaseRepository implements PlanSettingInterface
{

	private $plan_price_history;

	public function __construct ( Plan $model, PlanPriceHistoryInterface $plan_price_history ) {
		parent::__construct( $model );

		$this->plan_price_history = $plan_price_history;
	}

	public function getWithActivePrice ($planID = null)
	{
		if (is_null($planID)) {
			return $this->model
				->select( 'id', 'code', 'name', 'is_active' )
				->with( 'activeprice' )
				->get();
		}

		return $this->model
			->select( 'id', 'code', 'name', 'is_active' )
			->where('id', '=', $planID)->with( 'activeprice' )
			->get();
	}

	public function getWithPrices ($planId = null)
	{
		if (is_null($planId)) {
			return $this->model
				->select('id','code','name', 'is_active')
				->with('planprices')
				->get();
		}

		return $this->model
			->select('id','code','name', 'is_active')
			->where('id', '=', $planId)
			->with('planprices')
			->get();
	}

	public function getPlanList ()
	{
		return $this->model
			->selectRaw("id, code, name, CONCAT('(', code, ') ', name) AS codename")
			->where('is_active', 1)
			->pluck('codename','id');
	}

	public function deletePlan ( $planId )
	{
		$plan_price_history = $this->plan_price_history->findByAttributes(['plan_id' => $planId]);
		$plan = $this->model->find($planId);

		try {
			DB::transaction( function () use ($plan, $plan_price_history) {
				$this->plan_price_history->destroy($plan_price_history);
				$this->model->delete($plan);
			});
		} catch (QueryException $exception) {
			return false;
		}

		Log::info(
			'Admin Delete : '.
			Auth::user()->name .' ('. Auth::user()->username .') - 
		    delete plan data successfully'
		);
		return true;
	}

	public function getActivePlanId () {
		return $this->model
			->select( 'id' )
			->where('is_active', 1)
			->get();
	}
}