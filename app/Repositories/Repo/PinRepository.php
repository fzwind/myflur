<?php

namespace App\Repositories\Repo;


use App\Models\Pin;
use App\Repositories\PinInterface;

class PinRepository extends BaseRepository implements PinInterface {

	public function __construct ( Pin $model ) {
		parent::__construct( $model );
	}

	public function getWithPlan ( $pinCode = null ) {
		if (is_null($pinCode)) {
			return $this->model->with('plan')->get();
		}

		return $this->model->where('code', '=', $pinCode)->with('plan')->get();
	}

	public function getPinforUpdate ( $pin_transaction ) {
		return $this->model
			->where('plan_id',$pin_transaction->plan_id)
			->where('status',0)
			->take($pin_transaction->amount)
			->lockForUpdate();
	}
}