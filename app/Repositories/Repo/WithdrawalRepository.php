<?php

namespace App\Repositories\Repo;


use App\Models\Withdrawal;
use App\Repositories\BonusInterface;
use App\Repositories\RewardSettingInterface;
use App\Repositories\WithdrawalInterface;
use App\Repositories\WithdrawalSettingInterface;

class WithdrawalRepository extends BaseRepository implements WithdrawalInterface {

	public $withdrawal_setting;
	public $reward_setting;
	public $bonus;

	public function __construct (
		Withdrawal $model,
		WithdrawalSettingInterface $withdrawal_setting,
		RewardSettingInterface $reward_setting,
		BonusInterface $bonus
	) {
		parent::__construct( $model );

		$this->withdrawal_setting = $withdrawal_setting;
		$this->reward_setting = $reward_setting;
		$this->bonus = $bonus;
	}

	public function getWithdrawnBonus ( $userId = null ) {
		if (is_null($userId)) {
			return $this->model
				->groupBy('user_id')
				->selectRaw('sum(bonus_sponsor) as bonus_sponsor, sum(bonus_pairing) as bonus_pairing, sum(bonus_pairing_level) as bonus_pairing_level, sum(bonus_level) as bonus_level')
				->first();
		} else {
			return $this->model
				->selectRaw('sum(bonus_sponsor) as bonus_sponsor, sum(bonus_pairing) as bonus_pairing, sum(bonus_pairing_level) as bonus_pairing_level, sum(bonus_level) as bonus_level')
				->where('user_id', $userId)
				->first();
		}
	}

	public function getWithdrawalData ( $userId = null ) {
		if (is_null($userId)) {
			return $this->model
				->with('user','user.bank.bank')
				->where('status',0)
				->orderBy('created_at', 'desc')
				->get();
		}

		return $this->model
			->where('user_id', $userId)
			->orderBy('created_at', 'desc')
			->get();
	}

	public function getWithdrawalHistoryData () {
		return $this->model
			->where('status',1)
			->orderBy('created_at', 'desc')
			->get();
	}
}