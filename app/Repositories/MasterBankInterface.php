<?php

namespace App\Repositories;


interface MasterBankInterface extends BaseInterface {

	public function getActiveMasterBank();

	public function getActiveMasterBankList();

}