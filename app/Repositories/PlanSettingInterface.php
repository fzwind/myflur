<?php

namespace App\Repositories;


interface PlanSettingInterface extends BaseInterface {

	public function getWithActivePrice($planID = null);

	public function getWithPrices($planID = null);

	public function getPlanList();

	public function deletePlan($planId);

	public function getActivePlanId();

}