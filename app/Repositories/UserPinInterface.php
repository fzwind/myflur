<?php

namespace App\Repositories;


interface UserPinInterface extends BaseInterface {

	public function getAll();

	public function countForUse($userId);

	public function getAllWithTransaction($userId);

	public function getAvailableWithTransaction($userId);

	public function getUsedWithTransaction($userId);

	public function getSoldWithTransaction($userId);

	public function getTransferredWithTransaction($userId);

	public function getPinforUpdate($pin_transaction);

	public function getPinforUse($ownerId, $newUser);

	public function countMyPin($userId);
	
}