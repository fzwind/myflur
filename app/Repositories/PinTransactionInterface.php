<?php

namespace App\Repositories;


interface PinTransactionInterface extends BaseInterface {

	public function processTransaction($from_to, $plan_id, $transaction_type, $amount);

	public function getMyTransaction();

	public function getMyTransactionHistory();

	public function getMyRunningTransaction();

	public function updateStatus($transaction, $status, $note = null);

	public function getCompanyPinTransaction();

	public function getCompanyPinHistory();

	public function getAllConfirmedTransaction();

	public function getTransactionToDelete();
}