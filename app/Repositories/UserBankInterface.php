<?php

namespace App\Repositories;


interface UserBankInterface extends BaseInterface {

	public function getAllBanks();

	public function toggleBankStatus($bankId);

}