<?php

namespace App\Repositories;


interface StockistSettingInterface extends BaseInterface {

	public function getWithPlan($stockistSettingId = null);

}