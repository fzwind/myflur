<?php

namespace App\Repositories;


interface ProvinceInterface extends BaseInterface {

	public function getProvinceList();

}