<?php

namespace App\Repositories;


interface BonusInterface extends BaseInterface {

	public function getMyBonus($userId = null);

	public function getMySponsorBonus($userId = null);

	public function getMyPairingBonus($userId = null);

	public function getMyPairingLevelBonus($userId = null);

	public function getThisWeekSponsorBonus($userId = null);

	public function getLastWeekSponsorBonus($userId = null);

	public function getTotalBonusSponsor($userId = null);

	public function getThisWeekPairingBonus($userId = null);

	public function getLastWeekPairingBonus($userId = null);

	public function getTotalBonusPairing($userId = null);

	public function getThisWeekPairingLevelBonus($userId = null);

	public function getLastWeekPairingLevelBonus($userId = null);

	public function getTotalBonusPairingLevel($userId = null);

	public function getAllBonusForWithdrawal($date = null);

}