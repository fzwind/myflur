<?php

namespace App\Repositories;


interface PlanPriceHistoryInterface extends BaseInterface {

	public function getPriceHistory();

}