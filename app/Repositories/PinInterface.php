<?php

namespace App\Repositories;


interface PinInterface extends BaseInterface {

	public function getWithPlan($pinCode = null);

	public function getPinforUpdate($pin_transaction);

}