<?php

namespace App\Repositories;


interface RewardInterface extends BaseInterface {

	public function getAllWithRelations();

	public function getRewardAchievements($userId = null);

	public function create ( $data );

	public function getBySetting( $settingId );

	public function claimReward($memberRewardId);

}