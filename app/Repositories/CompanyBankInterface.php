<?php

namespace App\Repositories;


interface CompanyBankInterface extends BaseInterface {

	public function getAllBanks();

}