<?php

namespace App\Repositories;


interface MemberRewardSavingInterface extends BaseInterface {

	public function getSum($userId, $rewardSettingId);

	public function getWithRelations($userId = null, $reward_setting_id);

}