<?php

namespace App\Repositories;


interface UserInterface extends BaseInterface {

	public function getPlacementList($userId);

	public function getMyTreeList();

	public function registerNewUser($userData);

	public function getMySponsorList();

	public function getAllStockist();

}