<?php

namespace App\Repositories;


interface FlushedPairingInterface extends BaseInterface {

	public function getFlushedPairing($userId = null);

	public function updateFlushedPairing($leftRight, $userId, $placementId, $date = null);

}