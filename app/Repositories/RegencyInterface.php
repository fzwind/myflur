<?php

namespace App\Repositories;


interface RegencyInterface extends BaseInterface {

	public function getRegencyList($provinceId = null);

}