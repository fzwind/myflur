<?php

namespace App\Repositories;


interface MemberRewardStatusInterface extends BaseInterface {

	public function setStatus($userId, $rewardSettingId);

	public function getStatus($rewardSettingId, $userId = null);
}