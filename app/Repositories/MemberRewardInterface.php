<?php

namespace App\Repositories;


interface MemberRewardInterface extends BaseInterface {

	public function getAllWithRelations();

	public function setMemberReward($rewardId, $userId, $achievement);

	public function getRewardClaim();

	public function getRewardClaimed();

}