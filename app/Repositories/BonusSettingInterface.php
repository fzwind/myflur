<?php

namespace App\Repositories;


interface BonusSettingInterface extends BaseInterface {

	public function getWithPlan($bonusSettingId = null);
}