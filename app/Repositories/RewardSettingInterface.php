<?php

namespace App\Repositories;


interface RewardSettingInterface extends BaseInterface {

	public function getAllWithRelations();

	public function getAllActive();

	public function getRewardSettingList();

	public function create($data);

	public function getRewardPotongLangsung($bonus);

	public function getRewardPotongBonus();

	public function getById($rewardId);

	public function getRewardStructureOnly();

}