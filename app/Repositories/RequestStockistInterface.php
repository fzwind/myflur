<?php

namespace App\Repositories;


interface RequestStockistInterface extends BaseInterface {

	public function requestStockist($data);

	public function myRequestStatus();

	public function getUnApprovedRequests();

	public function getApprovedAndRejectedRequests();

	public function approveRequest($request_stockist);

}