<?php

namespace App\Repositories;


interface MemberStructureInterface extends BaseInterface {

	public function getByUserId($userId);

	public function getMyTree($topId = null);

	public function getPlacementList();

	public function doPlacement($parentId, $userId, $position, $mode = 'register');

	public function countMyLeftStructure($userStructure, $date = null, $rewardStart = null, $rewardEnd = null);

	public function countMyRightStructure($userStructure, $date = null, $rewardStart = null, $rewardEnd = null);

	public function countMyLeftPairTurnover($userStructure, $date = null);

	public function countMyRightPairTurnover($userStructure, $date = null);

	public function countMyLeftReward($userId, $rewardSettingId, $date = null);

	public function countMyRightReward($userId, $rewardSettingId, $date = null);

	public function getAllParents($userId);

	public function getAllForRepair();
}
