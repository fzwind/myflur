<?php

namespace App\Repositories;


interface VillageInterface extends BaseInterface {

	public function getVillageList($districtId = null);

}