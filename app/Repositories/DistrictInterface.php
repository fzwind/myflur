<?php

namespace App\Repositories;


interface DistrictInterface extends BaseInterface {

	public function getDistrictList($regencyId = null);

}