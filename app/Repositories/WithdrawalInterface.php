<?php

namespace App\Repositories;


interface WithdrawalInterface extends BaseInterface {

	public function getWithdrawnBonus($userId = null);

	public function getWithdrawalData($userId = null);

	public function getWithdrawalHistoryData();

}