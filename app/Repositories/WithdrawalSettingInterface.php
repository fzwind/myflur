<?php

namespace App\Repositories;


interface WithdrawalSettingInterface extends BaseInterface {

	public function create ( $data );

	public function getActiveSetting();

}