<?php

namespace App\Http\Controllers\Admin;

use App\Repositories\MemberRewardInterface;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;

class MemberRewardController extends Controller
{
    private $member_reward;

    public function __construct(
        MemberRewardInterface $member_reward
    ) {
        $this->member_reward = $member_reward;
    }

    public function index()
    {
        return view('admin.member-reward.index');
    }

    public function getRewardData()
    {
        $data = $this->member_reward->getRewardClaim();

        return DataTables::of($data)
            ->addColumn('member_bank', function ($data) {
                $str_bank = '';
                foreach ($data->user->bank as $key => $bank) {
                    if ($bank->is_active == 1) {
                        $str_bank .= $bank->bank->name .'<pre>no. rek. '. $bank->account_number .'<br>a.n '. $bank->account_name .'</pre>';
                    }
                }
                return $str_bank;
            })
            ->addColumn('str_reward', function ($data) {
                return $data->reward->reward .' ( '. $data->reward->rewardsetting->name .' )';
            })
            ->addColumn('str_member', function ($data) {
                return $data->user->name .' ( '. $data->user->username .' )';
            })
            ->addColumn('str_left', function ($data) {
                return $data->reward->left .' <span class="text-success">( '. $data->left .' )</span>';
            })
            ->addColumn('str_right', function ($data) {
                return $data->reward->right .' <span class="text-success">( '. $data->right .' )</span>';
            })
            ->addColumn('str_claimed_at', function ($data) {
                $date = to_utz(Carbon::parse($data->claimed_at));
                return $date;
            })
            ->addColumn('action', function ($data) {
                $proses = '<form method="post" action="'. route('admin.member-reward.process') .'" onsubmit="return confirmSubmit(this)"><input type="hidden" name="_method" value="PUT"><input type="hidden" name="id" value="'. $data->id .'"><input type="hidden" name="name" value="'. $data->user->name .'">'. csrf_field() .'<button type="submit" class="btn btn-sm btn-warning">proses</button></form>';
                return $proses;
            })
            ->rawColumns(['action','str_right','str_left','member_bank'])
            ->make(true);
    }

    public function process(Request $request)
    {
        $claimed_reward = $this->member_reward->find($request['id']);

        try {
            $this->member_reward->update(
                $claimed_reward,
                [
                    'is_received' => 1,
                    'received_at' => Carbon::now('Asia/Jakarta')
                ]
            );
        } catch (\Exception $e) {
            return redirect(
                route('admin.member-reward.index')
            )
                ->with(
                    'error',
                    'Klaim Reward ('. $request['name'] .') Gagal diproses.'
                );
        }

        return redirect(
            route('admin.member-reward.index')
        )
            ->with(
                'success',
                'Klaim Reward ('. $request['name'] .') BERHASIL diproses.'
            );
    }

    public function history()
    {
        return view('admin.member-reward.history');
    }

    public function getRewardHistoryData()
    {
        $data = $this->member_reward->getRewardClaimed();

        return DataTables::of($data)
            ->addColumn('member_bank', function ($data) {
                $str_bank = '';
                foreach ($data->user->bank as $key => $bank) {
                    if ($bank->is_active == 1) {
                        $str_bank .= $bank->bank->name .'<pre>no. rek. '. $bank->account_number .'<br>a.n '. $bank->account_name .'</pre>';
                    }
                }
                return $str_bank;
            })
            ->addColumn('str_reward', function ($data) {
                return $data->reward->reward .' ( '. $data->reward->rewardsetting->name .' )';
            })
            ->addColumn('str_member', function ($data) {
                return $data->user->name .' ( '. $data->user->username .' )';
            })
            ->addColumn('str_left', function ($data) {
                return $data->reward->left .' <span class="text-success">( '. $data->left .' )</span>';
            })
            ->addColumn('str_right', function ($data) {
                return $data->reward->right .' <span class="text-success">( '. $data->right .' )</span>';
            })
            ->addColumn('str_claimed_at', function ($data) {
                return to_utz(Carbon::parse($data->claimed_at));
            })
            ->addColumn('str_process_at', function ($data) {
                return to_utz(Carbon::parse($data->received_at));
            })
            ->rawColumns(['action','str_right','str_left','member_bank'])
            ->make(true);
    }
}
