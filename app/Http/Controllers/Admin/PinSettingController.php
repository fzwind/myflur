<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\PinSettingRequest;
use App\Repositories\PinInterface;
use App\Repositories\PlanSettingInterface;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Yajra\DataTables\DataTables;

class PinSettingController extends Controller
{
    private $pins;
    private $plans;

    public function __construct (PinInterface $pins, PlanSettingInterface $plans)
    {
        $this->pins = $pins;
        $this->plans = $plans;
    }

    public function index()
    {
	    $plans = $this->plans->getPlanList();

	    return view(
	    	'admin.pin.index',
		    compact('plans')
	    );
    }

	/**
	 * Get data from DB for datatables ajax request
	 *
	 * @return mixed
	 * @throws \Exception
	 */
	public function getPinSettingData() {
		$pins = $this->pins->getWithPlan();

		return DataTables::of($pins)
		                 ->addColumn('action', function ($pins) {
			                 return '';
		                 })
		                 ->rawColumns(['action'])
		                 ->make(true);
	}

	public function generatePinForm()
	{
		$plans = $this->plans->getPlanList();

		return view(
			'admin.pin.generate',
			compact('plans')
		);
	}

	public function generatePinPost(PinSettingRequest $request)
	{
		generate_pin(12,$request['plan_id'],$request['amount']);

		if (system_settings()->logging) {
			Log::info(
				'Admin Store : '.
				auth()->user()->name .' ('. auth()->user()->username .') generate '. number_format($request['amount'],0,',','.') .' new Pin for plan ID : '. $request['plan_id'] .' successfully'
			);
		}

		return redirect(
			route('pin-settings.index'))
			->with(
				'success',
				'Data Pin Baru BERHASIL Ditambahkan.'
			);
	}
}
