<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\PlanPriceHistoryInterface;
use App\Repositories\PlanSettingInterface;
use App\Http\Requests\Admin\PlanSettingRequest;
use App\Tools\PlanSettingTool;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Yajra\DataTables\Facades\DataTables;

class PlanSettingController extends Controller
{
	use PlanSettingTool;

	private $plan_setting;
	private $plan_price_history;

	/**
	 * PlanSettingController constructor.
	 *
	 * @param \App\Repositories\PlanSettingInterface $plan_setting
	 * @param \App\Repositories\PlanPriceHistoryInterface $plan_price_history
	 */
	public function __construct (
		PlanSettingInterface $plan_setting,
		PlanPriceHistoryInterface $plan_price_history
	) {
		$this->middleware('permission:admin plan setting create', ['only' => ['create','store']]);
		$this->middleware('permission:admin plan setting edit', ['only' => ['edit','update']]);
		$this->middleware('permission:admin plan setting delete', ['only' => ['destroy']]);
		$this->middleware('permission:admin plan setting list', ['only' => ['index', 'show']]);

		$this->plan_setting = $plan_setting;
		$this->plan_price_history = $plan_price_history;
	}

	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
		return view('admin.plan-setting.index');
    }

	/**
	 * Get data from DB for datatables ajax request
	 *
	 * @return mixed
	 * @throws \Exception
	 */
	public function getPlanSettingData() {
	    $plan_settings = $this->plan_setting->getWithActivePrice();

	    return DataTables::of($plan_settings)
			->addColumn('action', function ($plan_settings) {
				$editButton = (Auth::User()->hasPermissionTo('admin plan setting edit')) ? '<a href="'. route('plan-settings.edit', ['plan_setting' => $plan_settings]) .'" class="btn btn-info"><i class="fa fa-edit"></i></a>' : '' ;
				$deleteButton = (Auth::User()->hasPermissionTo('admin plan setting delete')) ? '<button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i></button>' : '' ;
				return '<form method="post" action="'. route('plan-settings.destroy', ['plan_setting' => $plan_settings]) .'" onsubmit="return confirmSubmit(this)"><input type="hidden" name="_method" value="DELETE"><input type="hidden" name="code" value="'. $plan_settings->code .'"><input type="hidden" name="name" value="'. $plan_settings->name .'">'. csrf_field() . $editButton .'&nbsp;'. $deleteButton .'</form>';
			})
			->rawColumns(['action'])
		    ->editColumn('price', function ($plan_settings) {
			    return optional($plan_settings->activeprice)->price ? with(number_format($plan_settings->activeprice->price,0,',','.')) : '0';
		    })
		    ->editColumn('stockist_price', function ($plan_settings) {
			    return optional($plan_settings->activeprice)->stockist_price ? with(number_format($plan_settings->activeprice->stockist_price,0,',','.')) : '0';
		    })
		    ->editColumn('master_stockist_price', function ($plan_settings) {
			    return optional($plan_settings->activeprice)->master_stockist_price ? with(number_format($plan_settings->activeprice->master_stockist_price,0,',','.')) : '0';
		    })
			->editColumn('active_at', function ($plan_settings) {
				return optional($plan_settings->activeprice)->active_at ? to_utz($plan_settings->activeprice->active_at,'d-m-Y') : '-';
			})
			->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.plan-setting.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\Admin\PlanSettingRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PlanSettingRequest $request)
    {
	    Cache::forget('planlist');
	    Cache::forget('plans');

	    $plan_data = $this->filterPlanSettingData($request->all());
        $plan = $this->plan_setting->create($plan_data);

        $price_data = $this->filterPlanPriceData($request->all(), $plan->id);
        $this->plan_price_history->create($price_data);

		if (system_settings()->logging) {
			Log::info(
				'Admin Store : '.
				Auth::user()->name .' ('. Auth::user()->username .') create new plan successfully'
			);
		}

	    return redirect(route('plan-settings.index'))
		    ->with(
		    	'success',
			    'Data PLAN Baru ('. $request['code'] .' / '. $request['name'] .') BERHASIL Ditambahkan.'
		    );

    }

    /**
     * Display the specified resource.
     *
     * @param  $planId
     *
     * @return \Illuminate\Http\Response
     */
    public function show($planId)
    {
    	//
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  $planId
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($planId)
    {
        $plan = $this->plan_setting->getWithActivePrice($planId)->first();

        $plan_prices = $this->plan_price_history->getByAttributes(['plan_id' => $planId], 'active_at', 'desc');

        $active_price_id = $plan->activeprice->id;

        return view(
        	'admin.plan-setting.edit',
	        compact('plan', 'plan_prices', 'active_price_id')
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\Admin\PlanSettingRequest  $request
     * @param  $planId
     *
     * @return \Illuminate\Http\Response
     */
    public function update ( PlanSettingRequest $request, $planId)
    {
	    Cache::forget('planlist');
	    Cache::forget('plans');

        $plan = $this->plan_setting->find($planId);
        $this->plan_setting->update($plan, $request->all());

	    if (system_settings()->logging) {
		    Log::info(
			    'Admin Update : ' .
			    Auth::user()->name . ' (' . Auth::user()->username . ') - update plan data successfully'
		    );
	    }

	    return redirect(
	    	route('plan-settings.index'))
		    ->with(
		    	'success',
			    'Data PLAN ('. $request['code'] .' / '. $request['name'] .') BERHASIL diubah.'
		    );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  $planId
     *
     * @return \Illuminate\Http\Response
     */
	public function destroy($planId)
    {
    	$plan_price_history = $this->plan_price_history->findByAttributes(['plan_id' => $planId]);
	    $plan = $this->plan_setting->find($planId);

	    try {
		    DB::transaction( function () use ($plan, $plan_price_history) {
			    $this->plan_price_history->destroy($plan_price_history);
			    $this->plan_setting->destroy($plan);
		    });
	    } catch (\Exception $exception) {
		    return redirect(route('plan-settings.index'))
			    ->with(
			        'error',
				    'Data PLAN ('. $plan->code .' / '. $plan->name .') Gagal Dihapus. Terdapat data yang mereferensikan data
				    PLAN ini. Anda bermaksud menonaktifkan PLAN ini? silahkan lakukan EDIT data.'
			    );
	    }

	    if (system_settings()->logging) {
		    Log::info(
			    'Admin Delete : ' .
			    Auth::user()->name . ' (' . Auth::user()->username . ') 
			    - delete plan data successfully'
		    );
	    }

	    return redirect(route('plan-settings.index'))->with('success', 'Data PLAN Berhasil Dihapus.');
    }
}
