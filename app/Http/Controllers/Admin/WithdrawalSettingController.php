<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\WithdrawalSettingRequest;
use App\Models\WithdrawalSetting;
use App\Repositories\WithdrawalSettingInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Yajra\DataTables\DataTables;

class WithdrawalSettingController extends Controller
{

	private $withdrawal_setting;

    public function __construct(
    	WithdrawalSettingInterface $withdrawal_setting
    )
    {
        $this->withdrawal_setting = $withdrawal_setting;
    }

    public function index()
    {
		return view(
			'admin.withdrawal-setting.index'
		);
    }

    public function getWithdrawalSettingData()
    {
		$data = $this->withdrawal_setting->all();

	    return DataTables::of($data)
			->addColumn('admin', function ($data) {
				return 'Rp '. number_format($data->wd_admin,0,',','.');
			})
			->addColumn('index', function ($data) {
				return $data->wd_index_deduction .' %';
			})
		    ->addColumn('str_mode', function ($data) {
		    	$mode = ($data->wd_mode == 1) ? 'auto' : 'manual';
		    	return $mode;
		    })
		    ->addColumn('str_wd_days', function ($data) {
		    	$days = str_replace('3','rabu',str_replace('2','selasa',str_replace('1','senin',$data->wd_days)));
		    	$days = str_replace('7','minggu',str_replace('6','sabtu',str_replace('5','jumat',str_replace('4','kamis', $days))));
		    	return $days;
		    })
			->addColumn('active_at_date', function ($data) {
				return to_utz($data->active_at);
			})
		    ->addColumn('setting_status', function ($data) {
			    return $data->active_at > today() ? 0 : 1;
		    })
			->addColumn('action', function ($data) {
				$editButton = (auth()->user()->hasPermissionTo('admin reward setting edit')) ? '<a href="'. route('withdrawal-settings.edit', ['withdrawal_setting' => $data]) .'" class="btn btn-info"><i class="fa fa-edit"></i></a>' : '' ;
				$deleteButton = (auth()->user()->hasPermissionTo('admin reward setting delete')) ? '<button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i></button>' : '' ;
				return '<form method="post" action="'. route('withdrawal-settings.destroy', ['withdrawal_setting' => $data]) .'" onsubmit="return confirmSubmit(this)"><input type="hidden" name="_method" value="DELETE"><input type="hidden" name="name" value="'. $data->id .'">'. csrf_field() . $editButton .'&nbsp;'. $deleteButton .'</form>';
			})
			->rawColumns(['action','index'])
			->make(true);
    }

    public function create()
    {
        return view(
        	'admin.withdrawal-setting.create'
        );
    }

    public function store(WithdrawalSettingRequest $request)
    {
	    if ($this->withdrawal_setting->create($request->all())) {
		    if (system_settings()->logging) {
			    Log::info(
				    'Admin Store : '.
				    auth()->user()->name .' ('. auth()->user()->username .') create new withdrawal setting successfully.'
			    );
		    }

		    return redirect(route('withdrawal-settings.index'))
			    ->with(
				    'success',
				    'Data withdrawal setting Berhasil Ditambahkan.'
			    );
	    }

	    if (system_settings()->logging) {
		    Log::info(
			    'Admin Store : '.
			    auth()->user()->name .' ('. auth()->user()->username .') failed to create new withdrawal setting.'
		    );
	    }

	    return redirect(route('withdrawal-settings.index'))
		    ->with(
			    'error',
			    'Data withdrawal setting GAGAL Ditambahkan.'
		    );

    }

    public function show(WithdrawalSetting $withdrawalSetting)
    {
        //
    }

    public function edit(WithdrawalSetting $withdrawalSetting)
    {
        //
    }

    public function update(Request $request, WithdrawalSetting $withdrawalSetting)
    {
        //
    }

    public function destroy(WithdrawalSetting $withdrawalSetting)
    {
        //
    }
}
