<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\SystemMemberMetaSettingRequest;
use App\Repositories\SystemSettingsInterface;
use App\Http\Requests\Admin\SystemBonusSettingRequest;
use App\Http\Requests\Admin\SystemGeneralSettingRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;

class SystemSettingController extends Controller
{
	private $system_settings;

	/**
	 * \App\Repositories\SystemSettingsInterface constructor.
	 *
	 * @param SystemSettingsInterface $system_settings
	 */
	public function __construct ( SystemSettingsInterface $system_settings ) {
		$this->middleware('role:super-administrator');

		$this->system_settings = $system_settings;
	}

	/**
	 * View system general setting form with existing data
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function general()
	{
		$settings = $this->system_settings->all()->first();

		return view('admin.system.general-settings', compact('settings'));
	}

	/**
	 * Update the system general setting
	 *
	 * @param \App\Http\Requests\Admin\SystemGeneralSettingRequest $request
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function generalUpdate(SystemGeneralSettingRequest $request) {
		$setting = $this->system_settings->all()->first();

		Log::info(
			'System Update : '.
			auth()->user()->name . ' (' . Auth::user()->username . ') - try to updating system general settings. 
			Old values : { 
			network_type = '. system_settings()->network_type .', 
			email_activation = '. system_settings()->email_activation .', 
			notification = '. system_settings()->notification .', 
			logging = '. system_settings()->logging .'}; 
			New Values : {
			network_type = '. $request['network_type'] .', 
			email_activation = '. $request['email_activation'] .', 
			notification = '. $request['notification'] .', 
			logging = '. $request['logging'] .',
			proof_bank_transfer_upload = '. $request['proof_bank_transfer_upload'] .'};'
		);

		Cache::forget('systemsettings');

		$this->system_settings->update($setting, $request->all());

		Log::info(
			'System Update : '.
			auth()->user()->name . ' (' . auth()->user()->username . ') - updating system general settings successfully'
		);

		return redirect(
			route('system.general.settings'))
			->with(
				'success',
				'General System Settings Updated Successfully.'
			);
	}

	/**
	 * View system general setting form with existing data
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function memberMeta()
	{
		$settings = $this->system_settings->all()->first();

		return view('admin.system.member-meta', compact('settings'));
	}

	/**
	 * Update the system general setting
	 *
	 * @param \App\Http\Requests\Admin\SystemMemberMetaSettingRequest $request
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function memberMetaUpdate(SystemMemberMetaSettingRequest $request) {
		$setting = $this->system_settings->all()->first();

		Log::info(
			'System Update : '.
			auth()->user()->name . ' (' . auth()->user()->username . ') - try to updating system member meta-data settings. 
			Old values : { 
			memdata_name = '. system_settings()->memdata_name .', 
			memdata_email = '. system_settings()->memdata_email .', 
			memdata_address = '. system_settings()->memdata_address .', 
			memdata_province = '. system_settings()->memdata_province .', 
			memdata_regency = '. system_settings()->memdata_regency .', 
			memdata_district = '. system_settings()->memdata_district .', 
			memdata_village = '. system_settings()->memdata_village .', 
			memdata_zip = '. system_settings()->memdata_zip .', 
			memdata_hp = '. system_settings()->memdata_hp .', 
			memdata_id_number = '. system_settings()->memdata_id_number .', 
			memdata_tax_id_number = '. system_settings()->memdata_tax_id_number.', 
			memdata_sex = '. system_settings()->memdata_sex .', 
			memdata_birth_date = '. system_settings()->memdata_birth_date .', 
			member_max_bank_account = '. system_settings()->member_max_bank_account .', 
			member_bank_name_match = '. system_settings()->member_bank_name_match .', 
			member_bank_unique = '. system_settings()->member_bank_unique .'}; 
			New Values : {
			memdata_name = '. $request['memdata_name'] .', 
			memdata_email = '. $request['memdata_email'] .', 
			memdata_address = '. $request['memdata_address'] .', 
			memdata_province = '. $request['memdata_province'] .', 
			memdata_regency = '. $request['memdata_regency'] .', 
			memdata_district = '. $request['memdata_district'] .', 
			memdata_village = '. $request['memdata_village'] .', 
			memdata_zip = '. $request['memdata_zip'] .', 
			memdata_hp = '. $request['memdata_hp'] .', 
			memdata_id_number = '. $request['memdata_id_number'] .', 
			memdata_tax_id_number = '. $request['memdata_tax_id_number'] .', 
			memdata_sex = '. $request['memdata_sex'] .', 
			memdata_birth_date = '. $request['memdata_birth_date'] .', 
			member_max_bank_account = '. $request['member_max_bank_account'] .', 
			member_bank_name_match = '. $request['member_bank_name_match'] .', 
			member_bank_unique = '. $request['member_bank_unique'] .'};'
		);

		Cache::forget('systemsettings');

		$this->system_settings->update($setting, $request->all());

		Log::info(
			'System Update : '.
			auth()->user()->name . ' (' . auth()->user()->username . ') - updating system member meta-data settings successfully'
		);

		return redirect(
			route('system.member-meta.settings'))
			->with(
				'success',
				'Member Meta-Data System Settings Updated Successfully.'
			);
	}

	/**
	 * View system bonus setting form with existing data
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function bonus() {
		$settings = $this->system_settings->all()->first();
		return view('admin.system.bonus-settings', compact('settings'));
	}

	/**
	 * Update the system bonus setting
	 *
	 * @param \App\Http\Requests\Admin\SystemBonusSettingRequest $request
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function bonusUpdate(SystemBonusSettingRequest $request) {
		$setting = $this->system_settings->all()->first();

		Log::info(
			'System Update : '.
			Auth::user()->name . ' (' . Auth::user()->username . ') - try to updating system bonus settings. 
			Old values : { 
			rewards = '. system_settings()->rewards .', 
			bonus_sponsor = '. system_settings()->bonus_sponsor .', 
			bonus_pairing = '. system_settings()->bonus_pairing .', 
			bonus_pairing_level = '. system_settings()->bonus_pairing_level .', 
			bonus_level = '. system_settings()->bonus_level .'}; 
			New Values : {
			rewards = '. $request['rewards'] .', 
			bonus_sponsor = '. $request['bonus_sponsor'] .', 
			bonus_pairing = '. $request['bonus_pairing'] .', 
			bonus_pairing_level = '. $request['bonus_pairing_level'] .', 
			bonus_level = '. $request['bonus_level'] .'};'
		);

		Cache::forget('systemsettings');

		$this->system_settings->update($setting, $request->all());

		Log::info(
			'System Update : '.
			auth()->user()->name . ' (' . auth()->user()->username . ') - updating system bonus settings successfully'
		);

		return redirect(
			route('system.bonus.settings'))
			->with(
				'success',
				'Bonus System Settings Updated Successfully.'
			);
	}

	public function cacheClear()
	{
		return view(
			'admin.system.cache'
		);
	}

	public function cacheClearPost($cache)
	{
		if ($cache == 'all') {
			Cache::flush();
		}

		if ($cache == 'systemsettings') {
			Cache::forget('systemsettings');
		}

		if ($cache == 'bonussettings') {

			foreach (active_plan() as $plan) {
				Cache::forget('bonussettings-'. $plan->id);
			}
		}

		if ($cache == 'stockistsettings') {
			foreach (active_plan() as $plan) {
				Cache::forget('stockistsettings-'. $plan->id);
			}
		}

		if ($cache == 'plansettings') {
			Cache::forget('planlist');
			Cache::forget('plans');
		}

		Log::info(
			'Admin Cache : '.
			auth()->user()->name .' ('. auth()->user()->username .') clear cache '. $cache .' successfully'
		);

		return redirect(
			route('system.cache.clear'))
			->with(
				'success',
				'Cache '. $cache .' berhasil di-reset'
			);
	}
}
