<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\RequestStockistInterface;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class RequestStockistController extends Controller
{
    private $request_stockist;

    public function __construct (
    	RequestStockistInterface $request_stockist
    ) {
    	$this->request_stockist = $request_stockist;
    }

    public function index()
    {
		return view(
			'admin.request-stockist.index'
		);
    }

    public function getRequestStockistData()
    {
    	$data = $this->request_stockist->getUnApprovedRequests();

	    return DataTables::of($data)
			->addColumn('str_user', function ($data) {
				return $data->user->name .' ('. $data->user->username .')';
			})
			->addColumn('str_location', function ($data) {
				return optional($data->user->province)->name .' - '. optional($data->user->regency)->name;
			})
		    ->addColumn('str_type', function ($data) {
			    return ($data->request_type==0) ? 'Stockist' : 'Master Stockist';
		    })
			->addColumn('str_date', function ($data) {
				return to_utz($data->created_at);
			})
		    ->addColumn('action', function ($data) {
			    $approve = '<form method="post" action="'. route('admin.request-stockist.process') .'" onsubmit="return confirmSubmit(this)"><input type="hidden" name="_method" value="PUT"><input type="hidden" name="id" value="'. $data->id .'"><input type="hidden" name="name" value="'. $data->user->name .'">'. csrf_field() .'<button type="submit" class="btn btn-sm btn-warning">setujui</button></form>';
			    return $approve;
		    })
		    ->rawColumns(['action'])
			->make(true);
    }

    public function process(Request $request)
    {
		$request_stockist = $this->request_stockist->find($request['id']);

		if (! $this->request_stockist->approveRequest($request_stockist)) {
			return redirect( route('admin.request-stockist.index') )
				->with(
					'error',
					'Approve Request Gagal! terjadi kesalahan.'
				);
		}

	    return redirect( route('admin.request-stockist.index') )
		    ->with(
			    'success',
			    'Approve Request Berhasil!'
		    );

    }

    public function history()
    {
	    return view(
		    'admin.request-stockist.history'
	    );
    }

    public function getRequestStockistHistoryData()
    {
	    $data = $this->request_stockist->getApprovedAndRejectedRequests();

	    return DataTables::of($data)
			->addColumn('str_user', function ($data) {
				return $data->user->name .' ('. $data->user->username .')';
			})
			->addColumn('str_location', function ($data) {
				return optional($data->user->province)->name .' - '. optional($data->user->regency)->name;
			})
		    ->addColumn('str_reject', function ($data) {
			    return $data->status_notes;
		    })
			->addColumn('str_date', function ($data) {
				return to_utz($data->updated_at);
			})
			->addColumn('action', function ($data) {
				if ($data->status == 1) {
					$status = '<a class="btn btn-sm btn-success text-white">disetujui</a>';
				} elseif ($data->status == 2) {
					$status = '<a class="btn btn-sm btn-danger text-white">ditolak</a>';
				}
				return $status;
			})
			->rawColumns(['str_reject','action'])
			->make(true);
    }
}
