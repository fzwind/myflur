<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StockistSettingRequest;
use App\Models\StockistSetting;
use App\Repositories\PlanSettingInterface;
use App\Repositories\StockistSettingInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use Yajra\DataTables\DataTables;

class StockistSettingController extends Controller
{
    private $stockist_setting;
    private $plan_setting;

    public function __construct(StockistSettingInterface $stockist_setting, PlanSettingInterface $plan_setting)
    {
        $this->middleware('permission:admin stockist setting create', ['only' => ['create','store']]);
        $this->middleware('permission:admin stockist setting edit', ['only' => ['edit','update']]);
        $this->middleware('permission:admin stockist setting delete', ['only' => ['destroy']]);
        $this->middleware('permission:admin stockist setting list', ['only' => ['index', 'show']]);

        $this->stockist_setting = $stockist_setting;
        $this->plan_setting = $plan_setting;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view(
            'admin.stockist-setting.index'
        );
    }

    /**
     * Get data from DB for datatables ajax request
     *
     * @return mixed
     * @throws \Exception
     */
    public function getStockistSettingData()
    {
        $stockist_settings = $this->stockist_setting->getWithPlan();

        return DataTables::of($stockist_settings)
            ->addColumn('status', function ($stockist_settings) {
                return $stockist_settings->active_at > today() ? 0 : 1;
            })
            ->addColumn('action', function ($stockist_settings) {
                $editButton = (auth()->User()->hasPermissionTo('admin stockist setting edit')) ? '<a href="'. route('stockist-settings.edit', ['stockist_setting' => $stockist_settings]) .'" class="btn btn-info"><div><i class="fa fa-edit"></i></div></a>' : '' ;
                $deleteButton = (auth()->User()->hasPermissionTo('admin stockist setting delete')) ? '<button type="submit" class="btn btn-danger"><div><i class="fa fa-trash"></i></div></button>' : '' ;
                return '<form method="post" action="'. route('stockist-settings.destroy', ['stockist_setting' => $stockist_settings]) .'" onsubmit="return confirmSubmit(this)"><input type="hidden" name="_method" value="DELETE"><input type="hidden" name="code" value="'. $stockist_settings->code .'"><input type="hidden" name="name" value="'. $stockist_settings->name .'">'. csrf_field() . $editButton .'&nbsp;'. $deleteButton .'</form>';
            })
            ->rawColumns(['action'])
            ->editColumn('active_at', function ($stockist_settings) {
                return $stockist_settings->active_at ? to_utz($stockist_settings->active_at) : '-';
            })
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $plans = $this->plan_setting->getPlanList();

        return view(
            'admin.stockist-setting.create',
            compact(
                'plans'
            )
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StockistSettingRequest $request)
    {
        foreach (active_plan() as $plan) {
            Cache::forget('stockistsetting-'. $plan->id);
        }

        $data = $request->all();
        $data['active_at'] = $request['active_at'];

        $this->stockist_setting->create($data);

        if (system_settings()->logging) {
            Log::info(
                'Admin Store : '.
                auth()->user()->name .' ('. auth()->user()->username .') create new stockist setting successfully'
            );
        }

        return redirect(
            route('stockist-settings.index')
        )
            ->with(
                'success',
                'Data Setting Stockist Baru BERHASIL Ditambahkan.'
            );
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Admin\StockistSetting  $stockistSetting
     * @return \Illuminate\Http\Response
     */
    public function show(StockistSetting $stockistSetting)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Admin\StockistSetting  $stockistSetting
     * @return \Illuminate\Http\Response
     */
    public function edit(StockistSetting $stockistSetting)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Admin\StockistSetting  $stockistSetting
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, StockistSetting $stockistSetting)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Admin\StockistSetting  $stockistSetting
     * @return \Illuminate\Http\Response
     */
    public function destroy(StockistSetting $stockistSetting)
    {
        //
    }
}
