<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\BonusInterface;
use App\Repositories\WithdrawalInterface;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class WithdrawalController extends Controller
{
	public $withdrawal;

	public function __construct (
		WithdrawalInterface $withdrawal
	) {
		$this->withdrawal = $withdrawal;
	}

	public function index()
    {
	    return view(
        	'admin.withdrawal.index'
        );
    }

    public function getWithdrawalData()
    {
	    $data = $this->withdrawal->getWithdrawalData();

	    return DataTables::of($data)
		    ->addColumn('member_bank', function ($data) {
			    $str_bank = '';
		    	foreach ($data->user->bank as $key => $bank) {
		    		if ($bank->is_active == 1) {
		    			$str_bank .= '<p>'. $bank->bank->name .'<pre>no. rek. '. $bank->account_number .'<br>a.n '. $bank->account_name .'</pre></p>';
				    }
			    }
			    return $str_bank;
		    })
			->addColumn('formatted_wd_amount', function ($data) {
				return number_format($data->wd_amount,0,',','.');
			})
		    ->addColumn('formatted_index', function ($data) {
			    return number_format($data->wd_index_deduction,0,',','.');
		    })
		    ->addColumn('formatted_reward_savings', function ($data) {
			    return number_format($data->wd_reward_deduction,0,',','.');
		    })
		    ->addColumn('formatted_wd_admin', function ($data) {
			    return number_format($data->wd_admin,0,',','.');
		    })
		    ->addColumn('formatted_transferred', function ($data) {
			    return number_format(($data->wd_amount - $data->wd_reward_deduction - $data->wd_index_deduction - $data->wd_admin),0,',','.');
		    })
		    ->addColumn('formatted_code', function ($data) {
			    return $data->code .'<br /><pre>'. $data->user->name .' ('. $data->user->username .')</pre>';
		    })
			->addColumn('wd_date', function ($data) {
				return to_utz($data->created_at);
			})
		    ->addColumn('formatted_status', function ($data) {
		    	$transfer = '<form method="post" action="'. route('admin.withdrawal.transfer') .'" onsubmit="return confirmSubmit(this)"><input type="hidden" name="_method" value="PUT"><input type="hidden" name="id" value="'. $data->id .'"><input type="hidden" name="code" value="'. $data->code .'">'. csrf_field() .'<button type="submit" class="btn btn-sm btn-warning">transfer</button></form>';
			    return $transfer;
		    })
			->rawColumns(['formatted_status','member_bank','formatted_code'])
			->make(true);
    }

    public function history()
    {
	    return view(
		    'admin.withdrawal.history'
	    );
    }

    public function getWithdrawalHistoryData()
    {
	    $data = $this->withdrawal->getWithdrawalHistoryData();

	    return DataTables::of($data)
			->addColumn('formatted_wd_amount', function ($data) {
				return number_format($data->wd_amount,0,',','.');
			})
			->addColumn('formatted_index', function ($data) {
				return number_format($data->wd_index_deduction,0,',','.');
			})
			->addColumn('formatted_reward_savings', function ($data) {
				return number_format($data->wd_reward_deduction,0,',','.');
			})
			->addColumn('formatted_wd_admin', function ($data) {
				return number_format($data->wd_admin,0,',','.');
			})
			->addColumn('formatted_transferred', function ($data) {
				return number_format(($data->wd_amount - $data->wd_reward_deduction - $data->wd_index_deduction - $data->wd_admin),0,',','.');
			})
		    ->addColumn('formatted_code', function ($data) {
			    return $data->code .'<br /><pre>'. $data->user->name .' ('. $data->user->username .')</pre>';
		    })
			->addColumn('wd_date', function ($data) {
				return to_utz($data->created_at);
			})
			->addColumn('formatted_status', function () {
				return '<a class="btn btn-sm btn-success text-white">selesai</a>';
			})
			->rawColumns(['formatted_status','formatted_code'])
			->make(true);
    }

    public function transfer(Request $request)
    {
    	$withdrawal = $this->withdrawal->find($request['id']);

    	try {
		    $this->withdrawal->update($withdrawal,['status' => 1]);
	    } catch (\Exception $e) {
		    return redirect(
			    route('admin.withdrawal.index'))
			    ->with(
				    'error',
				    'WD ('. $request['code'] .') Gagal ditransfer.'
			    );
	    }

	    return redirect(
		    route('admin.withdrawal.index'))
		    ->with(
			    'success',
			    'WD ('. $request['code'] .') BERHASIL ditransfer.'
		    );
    }
}
