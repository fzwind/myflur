<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\BonusLevelSettingInterface;
use App\Repositories\BonusSettingInterface;
use App\Repositories\PlanSettingInterface;
use App\Http\Requests\Admin\BonusSettingRequest;
use App\Tools\BonusSettingTool;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Yajra\DataTables\Facades\DataTables;

class BonusSettingController extends Controller
{
    use BonusSettingTool;

    private $bonus_settings;
    private $bonus_level_settings;
    private $plan;

    /**
     * BonusSettingController constructor.
     *
     * @param BonusSettingInterface $bonus_setting
     * @param PlanSettingInterface $plan
     * @param BonusLevelSettingInterface $bonus_level_setting
     */
    public function __construct(
        BonusSettingInterface $bonus_setting,
        PlanSettingInterface $plan,
        BonusLevelSettingInterface $bonus_level_setting
    ) {
        $this->middleware('permission:admin bonus setting create', ['only' => ['create','store']]);
        $this->middleware('permission:admin bonus setting edit', ['only' => ['edit','update']]);
        $this->middleware('permission:admin bonus setting delete', ['only' => ['destroy']]);
        $this->middleware('permission:admin bonus setting list', ['only' => ['index', 'show']]);

        $this->bonus_settings = $bonus_setting;
        $this->bonus_level_settings = $bonus_level_setting;
        $this->plan = $plan;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.bonus-setting.index');
    }

    /**
     * Get data from DB for datatables ajax request
     *
     * @return mixed
     * @throws \Exception
     */
    public function getBonusSettingData()
    {
        $bonus_settings = $this->bonus_settings->getWithPlan();

        return DataTables::of($bonus_settings)
            ->addColumn('status', function ($bonus_settings) {
                return $bonus_settings->active_at > today() ? 0 : 1;
            })
            ->addColumn('action', function ($bonus_settings) {
                $showButton = (auth()->User()->hasPermissionTo('admin bonus setting list') && $bonus_settings->active_at <= Carbon::now('Asia/Jakarta')) ? '<a href="'. route('bonus-settings.show', ['bonus_setting' => $bonus_settings]) .'" class="btn btn-outline-secondary btn-light"><div><i class="fa fa-eye"></i></div></a>' : '' ;
                $editButton = (auth()->User()->hasPermissionTo('admin bonus setting edit') && $bonus_settings->active_at > Carbon::now('Asia/Jakarta')) ? '<a href="'. route('bonus-settings.edit', ['bonus_setting' => $bonus_settings]) .'" class="btn btn-info"><div><i class="fa fa-edit"></i></div></a>' : '' ;
                $deleteButton = (Auth::User()->hasPermissionTo('admin bonus setting delete') && $bonus_settings->active_at > Carbon::now('Asia/Jakarta')) ? '<button type="submit" class="btn btn-danger"><div><i class="fa fa-trash"></i></div></button>' : '' ;
                return '<form method="post" action="'. route('bonus-settings.destroy', ['bonus_setting' => $bonus_settings]) .'" onsubmit="return confirmSubmit(this)"><input type="hidden" name="_method" value="DELETE"><input type="hidden" name="id" value="'. $bonus_settings->id .'">'. csrf_field() . $showButton .'&nbsp;'. $editButton .'&nbsp;'. $deleteButton .'</form>';
            })
            ->rawColumns(['action'])
            ->editColumn('active_at', function ($bonus_settings) {
                return $bonus_settings->active_at ? to_utz($bonus_settings->active_at) : '-';
            })
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $plans = $this->plan->getPlanList();

        return view(
            'admin.bonus-setting.create',
            compact('plans')
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\Admin\BonusSettingRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BonusSettingRequest $request)
    {
        foreach (active_plan() as $plan) {
            Cache::forget('bonussettings-'. $plan->id);
        }

        $data_bonus_setting = $this->filterBonusSettingData($request->all());

        try {
            $bonus_setting = $this->bonus_settings->create($data_bonus_setting);
        } catch (\Exception $e) {
            return redirect(
                route('bonus-settings.index')
            )
                ->with(
                    'error',
                    'Data Setting Bonus Baru GAGAL Ditambahkan.'
                );
        }

        for ($i = 1; $i <= 10; $i++) {
            if (isset($request['bonus_level_amount_'. $i])) {
                $data_bonus_level_setting = [];
                $data_bonus_level_setting['bonus_setting_id'] = $bonus_setting->id;
                $data_bonus_level_setting['level'] = $i;
                $data_bonus_level_setting['bonus_level_amount'] = $request['bonus_level_amount_'. $i];

                try {
                    $this->bonus_level_settings->create($data_bonus_level_setting);
                } catch (\Exception $e) {
                    return redirect(
                        route('bonus-settings.index')
                    )
                        ->with(
                            'error',
                            'Data Setting Bonus Baru GAGAL Ditambahkan.'
                        );
                }
            }
        }

        if (system_settings()->logging) {
            Log::info(
                'Admin Store : '.
                Auth::user()->name .' ('. Auth::user()->username .') create new bonus setting successfully'
            );
        }

        return redirect(
            route('bonus-settings.index')
        )
            ->with(
                'success',
                'Data Setting Bonus Baru BERHASIL Ditambahkan.'
            );
    }

    /**
     * Display the specified resource.
     *
     * @param  $bonusSettingId
     * @return \Illuminate\Http\Response
     */
    public function show($bonusSettingId)
    {
        $bonus_setting = $this->bonus_settings->getWithPlan($bonusSettingId)->first();

        $bonus_level_setting =$this->bonus_level_settings->findByAttributes(['bonus_setting_id' => $bonus_setting->id]);

        return view(
            'admin.bonus-setting.show',
            compact('bonus_setting', 'bonus_level_setting')
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  $bonusSettingId
     * @return \Illuminate\Http\Response
     */
    public function edit($bonusSettingId)
    {
        $bonus_setting = $this->bonus_settings->find($bonusSettingId);

        $plans = $this->plan->getPlanList();

        return view(
            'admin.bonus-setting.edit',
            compact('bonus_setting', 'plans')
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\Admin\BonusSettingRequest  $request
     * @param  $bonusSettingId
     * @return \Illuminate\Http\Response
     */
    public function update(BonusSettingRequest $request, $bonusSettingId)
    {
        foreach (active_plan() as $plan) {
            Cache::forget('bonussettings-'. $plan->id);
        }

        $bonus_setting = $this->bonus_settings->find($bonusSettingId);

        $data = $request->all();
        $data['active_at'] = $request['active_at'];

        $this->bonus_settings->update($bonus_setting, $data);

        if (system_settings()->logging) {
            Log::info(
                'Admin Update : ' .
                Auth::user()->name . ' (' . Auth::user()->username . ') - update bonus setting successfully'
            );
        }

        return redirect(
            route(
                'bonus-settings.index'
            )
        )->with(
            'success',
            'Data Bonus Setting BERHASIL Diperbaharui.'
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  $bonusSettingId
     * @return \Illuminate\Http\Response
     */
    public function destroy($bonusSettingId)
    {
        $bonus_setting = $this->bonus_settings->find($bonusSettingId);

        try {
            DB::transaction(function () use ($bonus_setting) {
                $this->bonus_settings->destroy($bonus_setting);
            });
        } catch (\Exception $exception) {
            return redirect(route('bonus-settings.index'))
                ->with(
                    'error',
                    'Bonus Setting Gagal Dihapus. Terdapat data yang mereferensikan data
				    PLAN ini, Atau Bonus Setting sedang dalam posisi aktif. Anda bermaksud mengubah bonus setting untuk kedepan? silahkan buat Bonus Setting baru.'
                );
        }

        if (system_settings()->logging) {
            Log::info(
                'Admin Delete : ' .
                Auth::user()->name . ' (' . Auth::user()->username . ') - delete bonus setting successfully'
            );
        }

        return redirect(route('bonus-settings.index'))->with('success', 'Data Bonus Setting Berhasil Dihapus.');
    }
}
