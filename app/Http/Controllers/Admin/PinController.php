<?php

namespace App\Http\Controllers\Admin;

use App\Repositories\PinTransactionInterface;
use App\Tools\PinTransactionsTool;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;

class PinController extends Controller
{
    use PinTransactionsTool;

    private $pin_transaction;

    public function __construct(
        PinTransactionInterface $pin_transaction
    ) {
        $this->pin_transaction = $pin_transaction;
    }

    public function index()
    {
        return view(
            'admin.pin.transaction-index'
        );
    }

    public function getPinData()
    {
        $pin_transaction = $this->pin_transaction->getCompanyPinTransaction();

        return DataTables::of($pin_transaction)
             ->addColumn('transaction', function ($pin_transaction) {
                 return $this->strTransactionCompany($pin_transaction);
             })
             ->addColumn('total_amount', function ($pin_transaction) {
                 return 'Rp '. number_format($pin_transaction->price + $pin_transaction->unique_digit, 0, ',', '.');
             })
             ->addColumn('action', function ($pin_transaction) {
                 return $this->actionButton($pin_transaction);
             })
             ->addColumn('status_date', function ($pin_transaction) {
                 return $this->status_date($pin_transaction);
             })
             ->editColumn('status', function ($pin_transaction) {
                 return $this->str_status($pin_transaction);
             })
             ->editColumn('transaction_code', function ($pin_transaction) {
                 return $this->transaction_code($pin_transaction);
             })
             ->rawColumns(['action', 'transaction', 'total_amount', 'transaction_code', 'status_date', 'status'])
             ->make(true);
    }

    public function history()
    {
        return view(
            'admin.pin.transaction-history'
        );
    }

    public function getPinHistoryData()
    {
        $pin_transaction = $this->pin_transaction->getCompanyPinHistory();

        return DataTables::of($pin_transaction)
            ->addColumn('transaction', function ($pin_transaction) {
                return $this->strTransactionCompany($pin_transaction);
            })
            ->addColumn('total_amount', function ($pin_transaction) {
                return 'Rp '. number_format($pin_transaction->price + $pin_transaction->unique_digit, 0, ',', '.');
            })
            ->addColumn('status_date', function ($pin_transaction) {
                return $this->status_date($pin_transaction);
            })
            ->editColumn('status', function ($pin_transaction) {
                return $this->str_status($pin_transaction);
            })
            ->editColumn('transaction_code', function ($pin_transaction) {
                return $this->transaction_code($pin_transaction);
            })
            ->rawColumns(['action', 'transaction', 'total_amount', 'transaction_code', 'status_date', 'status'])
            ->make(true);
    }

    public function reject(Request $request)
    {
        $transaction = $this->pin_transaction->find($request['id']);

        if ($transaction->from != 11) {
            return redirect(
                route(
                    'pin.index'
                )
            )->with(
                'error',
                'Terdeteksi manipulasi data transaksi!!!'
            );
        }

        try {
            $transaction->status = 'rejected';
            $transaction->status_note = $request['status_note'];
            $transaction->rejected_at = Carbon::now('Asia/Jakarta');
            $transaction->save();
        } catch (\Exception $e) {
            return redirect(
                route(
                    'pin.index'
                )
            )->with(
                'error',
                'Penolakan transaksi PIN Gagal!'
            );
        }

        return redirect(
            route(
                'pin.index'
            )
        )->with(
            'success',
            'Transaksi '. $transaction->transaction_code .' berhasil ditolak!'
        );
    }
}
