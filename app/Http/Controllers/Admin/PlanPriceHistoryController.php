<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\PlanPriceHistoryRequest;
use App\Repositories\PlanPriceHistoryInterface;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class PlanPriceHistoryController extends Controller
{
    private $plan_price_history;

    public function __construct(PlanPriceHistoryInterface $plan_price_history)
    {
        $this->middleware('permission:admin plan setting create', ['only' => ['create','store']]);
        $this->middleware('permission:admin plan setting edit', ['only' => ['edit','update']]);
        $this->middleware('permission:admin plan setting delete', ['only' => ['destroy']]);
        $this->middleware('permission:admin plan setting list', ['only' => ['index', 'show']]);

        $this->plan_price_history = $plan_price_history;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PlanPriceHistoryRequest $request)
    {
        Cache::forget('planlist');
        Cache::forget('plans');

        $planId = $request['plan_id'];

        $data = $request->all();
        $data['active_at'] = $request['active_at'];

        $this->plan_price_history->create($data);

        if (system_settings()->logging) {
            Log::info(
                'Admin Store : '.
                auth()->user()->name .' ('. auth()->user()->username .') create new plan price successfully'
            );
        }

        return redirect(
            route(
                'plan-settings.edit',
                ['plan_setting' => $planId]
            )
        )->with(
            'success',
            'Data Harga PLAN Baru BERHASIL Ditambahkan.'
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $plan_price_history = $this->plan_price_history->find($id);

        $plan_id = $plan_price_history->plan_id;

        try {
            DB::transaction(function () use ($plan_price_history) {
                $this->plan_price_history->destroy($plan_price_history);
            });
        } catch (\Exception $exception) {
            return redirect(route('plan-settings.edit', ['plan_setting' => $plan_id]))
                ->with(
                    'error',
                    'Data harga PLAN Gagal Dihapus. Terdapat data yang mereferensikan data harga PLAN ini'
                );
        }

        if (system_settings()->logging) {
            Log::info(
                'Admin Delete : ' .
                auth()->user()->name . ' (' . auth()->user()->username . ')
			    - delete plan price history data successfully'
            );
        }

        return redirect(route('plan-settings.edit', ['plan_setting' => $plan_id]))->with('success', 'Data history harga PLAN Berhasil Dihapus.');
    }
}
