<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\BonusLevelSetting;
use Illuminate\Http\Request;

class BonusLevelSettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\BonusLevelSetting  $bonusLevelSetting
     * @return \Illuminate\Http\Response
     */
    public function show(BonusLevelSetting $bonusLevelSetting)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\BonusLevelSetting  $bonusLevelSetting
     * @return \Illuminate\Http\Response
     */
    public function edit(BonusLevelSetting $bonusLevelSetting)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\BonusLevelSetting  $bonusLevelSetting
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BonusLevelSetting $bonusLevelSetting)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\BonusLevelSetting  $bonusLevelSetting
     * @return \Illuminate\Http\Response
     */
    public function destroy(BonusLevelSetting $bonusLevelSetting)
    {
        //
    }
}
