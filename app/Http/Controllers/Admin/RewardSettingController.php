<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\RewardSettingRequest;
use App\Models\RewardSetting;
use App\Repositories\PlanSettingInterface;
use App\Repositories\RewardSettingInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Yajra\DataTables\DataTables;

class RewardSettingController extends Controller
{
	private $reward_setting;
	private $plan;

	public function __construct (
		RewardSettingInterface $reward_setting,
		PlanSettingInterface $plan
	) {
		$this->reward_setting = $reward_setting;
		$this->plan = $plan;
	}

	public function index()
    {
        return view(
        	'admin.reward-setting.index'
        );
    }

    public function getRewardSettingData()
    {
		$data = $this->reward_setting->getAllWithRelations();

		return DataTables::of($data)
			->addColumn('reward_type_info', function ($data) {
				if ($data->reward_type == 1) {
					$reward_type_info = 'Kiri-Kanan Murni';
				} else {
					$reward_type_info = 'Potong Bonus<br><small>dari bonus : '. $data->deducted_bonuses .'. Sampai dengan Rp. '. number_format($data->deduction_target,0,',','.') .'</small>';
				}
				return $reward_type_info;
			})
			->addColumn('prerequisite', function ($data) {
				if (is_null($data->reward_prerequisite)) {
					return '-';
				}
				return $data->prerequisite->name;
			})
			->addColumn('active_at_date', function ($data) {
				return to_utz($data->active_at);
			})
			->addColumn('inactive_at_date', function ($data) {
				if (is_null($data->inactive_at)) {
					$date = '-';
				} else {
					$date = to_utz($data->inactive_at);
				}
				return $date;
			})
			->addColumn('action', function ($data) {
				$editButton = (auth()->user()->hasPermissionTo('admin reward setting edit')) ? '<a href="'. route('reward-settings.edit', ['reward_setting' => $data]) .'" class="btn btn-info"><i class="fa fa-edit"></i></a>' : '' ;
				$deleteButton = (auth()->user()->hasPermissionTo('admin reward setting delete')) ? '<button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i></button>' : '' ;
				return '<form method="post" action="'. route('reward-settings.destroy', ['reward_setting' => $data]) .'" onsubmit="return confirmSubmit(this)"><input type="hidden" name="_method" value="DELETE"><input type="hidden" name="name" value="'. $data->name .'">'. csrf_field() . $editButton .'&nbsp;'. $deleteButton .'</form>';
			})
			->rawColumns(['action','reward_type_info'])
			->make(true);
    }

    public function create()
    {
	    $plans = $this->plan->getPlanList();
	    $rewards = $this->reward_setting->getRewardSettingList();

        return view(
            'admin.reward-setting.create',
            compact('plans', 'rewards')
        );
    }

    public function store(RewardSettingRequest $request)
    {
        if ($this->reward_setting->create($request->all())) {
	        if (system_settings()->logging) {
		        Log::info(
			        'Admin Store : '.
			        auth()->user()->name .' ('. auth()->user()->username .') create new reward setting successfully.'
		        );
	        }

	        return redirect(route('reward-settings.index'))
		        ->with(
			        'success',
			        'Data reward setting ('. $request['name'] .') Berhasil Ditambahkan.'
		        );
        }

	    if (system_settings()->logging) {
		    Log::info(
			    'Admin Store : '.
			    auth()->user()->name .' ('. auth()->user()->username .') failed to create new reward setting.'
		    );
	    }

	    return redirect(route('reward-settings.index'))
		    ->with(
			    'error',
			    'Data reward setting ('. $request['name'] .') GAGAL Ditambahkan.'
		    );

    }

    public function show(RewardSetting $rewardSetting)
    {
        //
    }

    public function edit(RewardSetting $rewardSetting)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\RewardSetting  $rewardSetting
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RewardSetting $rewardSetting)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\RewardSetting  $rewardSetting
     * @return \Illuminate\Http\Response
     */
    public function destroy(RewardSetting $rewardSetting)
    {
        //
    }
}
