<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\CompanyBankRequest;
use App\Repositories\CompanyBankInterface;
use App\Repositories\MasterBankInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Yajra\DataTables\DataTables;

class CompanyBankController extends Controller
{
    private $company_bank;
    private $master_bank;

    public function __construct (CompanyBankInterface $company_bank, MasterBankInterface $master_bank) {
    	$this->company_bank = $company_bank;
    	$this->master_bank = $master_bank;
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		return view(
			'admin.bank.index'
		);
	}

	public function getCompanyBankData()
	{
		$company_bank = $this->company_bank->getAllBanks();

		return DataTables::of($company_bank)
             ->addColumn('action', function ($company_bank) {
                 $editButton = (auth()->user()->hasRole('administrators')) ? '<a href="'. route('system-bank.edit', ['system_bank' => $company_bank]) .'" class="btn btn-info"><div><i class="fa fa-edit"></i></div></a>' : '' ;
                 $deleteButton = (auth()->user()->hasRole('administrators')) ? '<button type="submit" class="btn btn-danger"><div><i class="fa fa-trash"></i></div></button>' : '' ;
                 return '<form method="post" action="'. route('company-bank.destroy', ['company_bank' => $company_bank]) .'" onsubmit="return confirmSubmit(this)"><input type="hidden" name="_method" value="DELETE"><input type="hidden" name="id" value="'. $company_bank->id .'">'. csrf_field() . $editButton .'&nbsp;'. $deleteButton .'</form>';
             })
			 ->editColumn('created_at', function ($bonus_settings) {
				return $bonus_settings->created_at ? to_utz($bonus_settings->created_at) : '-';
			 })
             ->rawColumns(['action'])
             ->make(true);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		$banks = $this->master_bank->getActiveMasterBankList();

		return view(
			'admin.bank.create',
			compact('banks')
		);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(CompanyBankRequest $request)
	{
		$this->company_bank->create($request->all());

		if (system_settings()->logging) {
			Log::info(
				'Admin Store : '.
				auth()->user()->name .' ('. auth()->user()->username .') create new company bank successfully'
			);
		}

		return redirect(
			route('company-bank.index'))
			->with(
				'success',
				'Data Bank Perusahaan Baru BERHASIL Ditambahkan.'
			);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  \App\District  $district
	 * @return \Illuminate\Http\Response
	 */
	public function show(District $district)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  \App\District  $district
	 * @return \Illuminate\Http\Response
	 */
	public function edit(District $district)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \App\District  $district
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, District $district)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  \App\District  $district
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(District $district)
	{
		//
	}

}
