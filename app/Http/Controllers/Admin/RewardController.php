<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\RewardsRequest;
use App\Models\Reward;
use App\Repositories\RewardInterface;
use App\Repositories\RewardSettingInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Yajra\DataTables\DataTables;

class RewardController extends Controller
{

	public $rewards;
	public $reward_setting;

	public function __construct (
		RewardInterface $reward,
		RewardSettingInterface $reward_setting
	) {
		$this->rewards = $reward;
		$this->reward_setting = $reward_setting;
	}

	public function index()
    {
        return view(
        	'admin.rewards.index'
        );
    }

    public function getRewardsData()
    {
	    $data = $this->rewards->getAllWithRelations();

	    return DataTables::of($data)
		    ->addColumn('plan', function ($data) {
		    	return $data->rewardsetting->plan->code .' - '. $data->rewardsetting->plan->name;
		    })
			->addColumn('kiri_kanan', function ($data) {
				return $data->left .' - '. $data->right;
			})
			->addColumn('reward_info', function ($data) {
				return $data->reward .'<br><small>'. number_format($data->reward_value,0,',','.') .'</small>';
			})
			->addColumn('active_at_date', function ($data) {
				return to_utz($data->active_at);
			})
			->addColumn('inactive_at_date', function ($data) {
				if (is_null($data->inactive_at)) {
					$date = '-';
				} else {
					$date = to_utz($data->inactive_at);
				}
				return $date;
			})
			->addColumn('action', function ($data) {
				$editButton = (auth()->user()->hasPermissionTo('admin reward setting edit')) ? '<a href="'. route('rewards.edit', ['reward' => $data]) .'" class="btn btn-info"><i class="fa fa-edit"></i></a>' : '' ;
				$deleteButton = (auth()->user()->hasPermissionTo('admin reward setting delete')) ? '<button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i></button>' : '' ;
				return '<form method="post" action="'. route('rewards.destroy', ['reward' => $data]) .'" onsubmit="return confirmSubmit(this)"><input type="hidden" name="_method" value="DELETE"><input type="hidden" name="name" value="'. $data->name .'">'. csrf_field() . $editButton .'&nbsp;'. $deleteButton .'</form>';
			})
			->rawColumns(['action','reward_info'])
			->make(true);
    }

    public function create()
    {
		$reward_settings = $this->reward_setting->getRewardSettingList();

        return view(
        	'admin.rewards.create',
	        compact('reward_settings')
        );
    }

    public function store(RewardsRequest $request)
    {
	    if ($this->rewards->create($request->all())) {
		    if (system_settings()->logging) {
			    Log::info(
				    'Admin Store : '.
				    auth()->user()->name .' ('. auth()->user()->username .') create new reward setting successfully.'
			    );
		    }

		    return redirect(route('rewards.index'))
			    ->with(
				    'success',
				    'Data rewards ('. $request['reward'] .') Berhasil Ditambahkan.'
			    );
	    }

	    if (system_settings()->logging) {
		    Log::info(
			    'Admin Store : '.
			    auth()->user()->name .' ('. auth()->user()->username .') failed to create new rewards.'
		    );
	    }

	    return redirect(route('rewards.index'))
		    ->with(
			    'error',
			    'Data rewards ('. $request['reward'] .') GAGAL Ditambahkan.'
		    );

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Reward  $reward
     * @return \Illuminate\Http\Response
     */
    public function show(Reward $reward)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Reward  $reward
     * @return \Illuminate\Http\Response
     */
    public function edit(Reward $reward)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Reward  $reward
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Reward $reward)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Reward  $reward
     * @return \Illuminate\Http\Response
     */
    public function destroy(Reward $reward)
    {
        //
    }
}
