<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\SystemMasterBankRequest;
use App\Repositories\MasterBankInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Yajra\DataTables\DataTables;

class MasterBankController extends Controller
{
    private $master_bank;

    public function __construct (MasterBankInterface $master_bank) {
    	$this->master_bank = $master_bank;
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		return view(
			'admin.system.bank-index'
		);
	}

	public function getMasterBankData()
	{
		$master_bank = $this->master_bank->all();

		return DataTables::of($master_bank)
	         ->addColumn('action', function ($master_bank) {
	             $editButton = (auth()->user()->hasRole('super-administrator')) ? '<a href="'. route('system-bank.edit', ['system_bank' => $master_bank]) .'" class="btn btn-info btn-sm"><div><i class="fa fa-edit"></i></div></a>' : '' ;
	             $deleteButton = (auth()->user()->hasRole('super-administrator')) ? '<button type="submit" class="btn btn-danger btn-sm"><div><i class="fa fa-trash"></i></div></button>' : '' ;
	             return '<form method="post" action="'. route('system-bank.destroy', ['system_bank' => $master_bank]) .'" onsubmit="return confirmSubmit(this)"><input type="hidden" name="_method" value="DELETE"><input type="hidden" name="id" value="'. $master_bank->id .'">'. csrf_field() . $editButton .'&nbsp;'. $deleteButton .'</form>';
	         })
	         ->rawColumns(['action'])
	         ->editColumn('created_at', function ($bonus_settings) {
	             return $bonus_settings->created_at ? to_utz($bonus_settings->created_at) : '-';
	         })
	         ->make(true);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		return view(
			'admin.system.bank-create'
		);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(SystemMasterBankRequest $request)
	{
		$this->master_bank->create($request->all());

		if (system_settings()->logging) {
			Log::info(
				'Admin Store : '.
				auth()->user()->name .' ('. auth()->user()->username .') create new master bank successfully'
			);
		}

		return redirect(
			route('system-bank.index'))
			->with(
				'success',
				'Data Bank Baru BERHASIL Ditambahkan.'
			);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  \App\District  $district
	 * @return \Illuminate\Http\Response
	 */
	public function show(District $district)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  \App\District  $district
	 * @return \Illuminate\Http\Response
	 */
	public function edit(District $district)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \App\District  $district
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, District $district)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  \App\District  $district
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(District $district)
	{
		//
	}
}
