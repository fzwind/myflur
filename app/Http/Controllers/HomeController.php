<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Bonus;
use App\Models\User;
use App\Models\News;
use App\Repositories\BonusInterface;
use App\Repositories\MemberStructureInterface;
use Carbon\Carbon;

class HomeController extends Controller
{
    private $member_structure;
    private $bonus;

    public function __construct(
        MemberStructureInterface $member_structure,
        BonusInterface $bonus
    ) {
        $this->middleware('auth');
        $this->member_structure = $member_structure;
        $this->bonus = $bonus;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if (auth()->user()->hasRole('members')) {
            $user_structure = $this->member_structure->findByAttributes(['user_id' => auth()->id()]);

            $mySumBonus = $this->bonus->getMyBonus(auth()->id())->sum('sum_of_all_bonuses');
            $myBonusSponsor = $this->bonus->getTotalBonusSponsor();
            $myBonusPairing = $this->bonus->getTotalBonusPairing();

            $myLeftStructureCount = $this->member_structure->countMyLeftStructure($user_structure);
            $myRightStructureCount = $this->member_structure->countMyRightStructure($user_structure);

            $news = News::where('is_active', 1)->get();

            return view('member.home', compact('myBonusSponsor', 'myBonusPairing', 'mySumBonus', 'news'));
        } elseif (auth()->user()->hasRole('administrators') || auth()->user()->hasRole('super-administrator')) {
            return view('admin.home');
        }
    }
}
