<?php

namespace App\Http\Controllers\Member;

use App\Models\MemberReward;
use App\Repositories\MemberRewardInterface;
use App\Repositories\MemberStructureInterface;
use App\Repositories\RewardInterface;
use App\Repositories\RewardSettingInterface;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class MemberRewardController extends Controller
{
    private $member_reward;
    private $reward_setting;
    private $reward;
    private $member_structure;

    public function __construct(
        RewardSettingInterface $reward_setting,
        RewardInterface $reward,
        MemberRewardInterface $member_reward,
        MemberStructureInterface $member_structure
    ) {
        $this->reward_setting = $reward_setting;
        $this->reward = $reward;
        $this->member_reward = $member_reward;
        $this->member_structure = $member_structure;
    }

    public function index()
    {
        $rewards = $this->reward->getRewardAchievements()->groupBy('reward_setting_id');

        return view(
            'member.reward.index',
            compact('rewards')
        );
    }

    public function claim($member_reward_id)
    {
        try {
            $member_reward = $this->member_reward->find($member_reward_id);
            $member_reward->is_claimed = 1;
            $member_reward->claimed_at = Carbon::now('Asia/Jakarta');
            $member_reward->save();
        } catch (\Exception $e) {
            if (system_settings()->logging) {
                Log::info(
                    'Reward Claim : ' .
                    auth()->user()->name . ' (' . auth()->user()->username . ') - FAILED to claim reward (member reward ID : '. $member_reward->id .')'
                );
            }

            return redirect(
                route(
                    'reward.achievement'
                )
            )->with(
                'error',
                'Reward gagal di-claim'
            );
        }

        if (system_settings()->logging) {
            Log::info(
                'Reward Claim : ' .
                auth()->user()->name . ' (' . auth()->user()->username . ') - claim reward (member reward ID : '. $member_reward->id .') successfully'
            );
        }

        return redirect(
            route(
                'reward.achievement'
            )
        )->with(
            'success',
            'Reward berhasil diclaim.'
        );
    }
}
