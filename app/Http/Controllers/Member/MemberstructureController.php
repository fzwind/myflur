<?php

namespace App\Http\Controllers\Member;

use App\Http\Requests\MemberPlacementRequest;
use App\Repositories\MemberStructureInterface;
use App\Repositories\UserInterface;
use App\Repositories\UserPinInterface;
use App\Tools\GenealogyTool;
use Carbon\Carbon;
use Yajra\DataTables\DataTables;

class MemberstructureController extends Controller
{
	use GenealogyTool;

	private $structure;
	private $user_pin;
	private $user;

	public function __construct (
		MemberStructureInterface $structure,
		UserPinInterface $user_pin,
		UserInterface $user
	) {
		$this->structure = $structure;
		$this->user_pin = $user_pin;
		$this->user = $user;
	}

	public function all()
	{
		return view(
			'member.network.index-all'
		);
	}

	public function allData()
	{
		$data = $this->structure->getMyTree();

		return DataTables::of($data)
             ->addColumn('status_date', function ($data) {
             	if ($data->user->is_active == 1) {
	                $date = '-';
             		if (! is_null($data->user->active_at)) {
		                $date = to_utz($data->user->active_at);
	                }
                } else {
             		if (is_null($data->user->inactive_at)) {
		                $date = to_utz($data->user->created_at);
	                } else {
		                $date = to_utz($data->user->inactive_at);
	                }
                }
                 return $date;
             })
             ->editColumn('status', function ($data) {
             	if ($data->user->is_active == 1) {
					$status = '<span class="text-success">aktif</span>';
                } else {
             		if (is_null($data->user->inactive_at)) {
             			$status = '<span class="text-warning">belum ditempatkan</span>';
	                } else {
             			$status = '<span class="text-danger">suspended</span>';
	                }
                }
                return $status;
             })
             ->rawColumns(['status_date', 'status'])
             ->make(true);
	}

    public function genealogy($topId = null)
    {
    	$topId = (is_null($topId)) ? auth()->id() : $topId;

	    // get the list of his/her structure for search / select
	    $my_tree_list = $this->user->getMyTreeList();

	    $structure = $this->structure->getMyTree($topId);
	    // dd($structure);

	    $topLevel = $this->structure->findByAttributes(['user_id' => $topId])->level;
	    $ok = true;

	    if (! $structure) {
	    	$ok = false;
		    return view(
			    'member.network.genealogy',
			    compact('structure', 'my_tree_list', 'ok')
		    );
	    }

		$structure = $this->fillEmpty($structure, $topLevel);
	    $structure = $structure->sortBy('position')->sortBy('parent_id');
	    //dd($structure);

    	return view(
    		'member.network.genealogy',
		    compact('structure', 'my_tree_list', 'ok')
	    );
    }

    public function getPlacementList($parentId, $position)
    {
		$placement_list = $this->structure->getPlacementList();

		return view(
			'member.network.placement-list',
			compact('placement_list', 'parentId', 'position')
		);
    }

    public function placement(MemberPlacementRequest $request)
    {
    	if ($this->user_pin->countForUse(auth()->id()) < 1) {
		    return redirect( route('genealogy') )
			    ->with(
				    'error',
				    'Anda tidak mempunyai cukup PIN!'
			    );
	    }

	    if (! $this->structure->doPlacement($request['parent_id'], $request['user_id'], $request['position'])) {
		    return redirect( route('genealogy') )
			    ->with(
				    'error',
				    'Placement member baru gagal dilakukan!'
			    );
	    }

	    return redirect( route('genealogy') )
		    ->with(
			    'success',
			    'Placement member baru berhasil dilakukan!'
		    );
    }
}
