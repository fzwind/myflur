<?php

namespace App\Http\Controllers\Member;

use App\Http\Requests\AccountRequest;
use App\Http\Requests\UserBankRequest;
use App\Models\User;
use App\Repositories\DistrictInterface;
use App\Repositories\MasterBankInterface;
use App\Repositories\ProvinceInterface;
use App\Repositories\RegencyInterface;
use App\Repositories\UserBankInterface;
use App\Repositories\VillageInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;

class AccountController extends Controller
{
    private $province;
    private $regency;
    private $district;
    private $village;
    private $user_bank;
    private $master_bank;

    public function __construct(ProvinceInterface $province, RegencyInterface $regency, DistrictInterface $district, VillageInterface $village, UserBankInterface $user_bank, MasterBankInterface $master_bank)
    {
        $this->province = $province;
        $this->regency = $regency;
        $this->district = $district;
        $this->village = $village;
        $this->user_bank = $user_bank;
        $this->master_bank = $master_bank;
    }

    public function profile()
    {
        $provinces = $this->province->getProvinceList();
        $regencies = [];
        if (auth()->user()->province_id > 0) {
            $regencies = $this->regency->getRegencyList(auth()->user()->province_id);
        }
        $districts = [];
        if (auth()->user()->regency_id > 0) {
            $districts = $this->district->getDistrictList(auth()->user()->regency_id);
        }
        $villages = [];
        if (auth()->user()->district_id > 0) {
            $villages = $this->village->getVillageList(auth()->user()->district_id);
        }



        $bank_list = $this->master_bank->getActiveMasterBankList();
        $banks = $this->user_bank->getAllBanks();

        return view(
            'member.account.my-account',
            compact('provinces', 'regencies', 'districts', 'villages', 'banks', 'bank_list')
        );
    }

    public function address()
    {
        return $this->profile();
    }

    public function identity()
    {
        return view(
            'member.account.my-account-id'
        );
    }

    public function changePassword()
    {
        return $this->profile();
    }

    public function store(AccountRequest $request)
    {
        $data = $request->all();
        $data['birth_date'] = $request['birth_date_at'];
        $back = 'my.account';
        $message = '';

        if (isset($data['password_confirmation'])) {
            unset($data['password_confirmation']);
            $data['password'] = Hash::make($request['password']);
            $back = 'my.account.changepw';
            $message = 'Password';
        }

        if (isset($data['name'])) {
            $back = 'my.account';
            $message = 'Data Profil';
        }

        if (isset($data['address'])) {
            $back = 'my.account.address';
            $message = 'Data Alamat';
        }

        if (isset($data['no_id'])) {
            $back = 'my.account.id';
            $message = 'Data Identitas';
        }

        if (auth()->user()->update($data)) {
            if (system_settings()->logging) {
                Log::info(
                    'Member Update : '.
                    auth()->user()->name .' ('. auth()->user()->username .') update account info successfully'
                );
            }

            return redirect(
                route($back)
            )
                ->with(
                    'success',
                    $message .' berhasil diperbaharui.'
                );
        }

        return redirect(
            route($back)
        )
            ->with(
                'error',
                $message. ' gagal diperbaharui.'
            );
    }

    public function bank()
    {
        return $this->profile();
    }

    public function storeBank(UserBankRequest $request)
    {
        $data = $request->all();
        $data['user_id'] = auth()->id();

        if ($this->user_bank->create($data)) {
            if (system_settings()->logging) {
                Log::info(
                    'Member Create : '.
                    auth()->user()->name .' ('. auth()->user()->username .') create new user bank successfully'
                );
            }

            return redirect(
                route('my.account.bank')
            )
                ->with(
                    'success',
                    'Data bank berhasil ditambahkan.'
                );
        }

        return redirect(
            route('my.account.bank')
        )
            ->with(
                'error',
                'Data bank gagal ditambahkan.'
            );
    }

    public function updateBank(UserBankRequest $request, $bankId)
    {
        $bank = $this->user_bank->find($bankId);
        $data = $request->all();

        if ($this->user_bank->update($bank, $data)) {
            if (system_settings()->logging) {
                Log::info(
                    'Member Update : '.
                    auth()->user()->name .' ('. auth()->user()->username .') update user bank successfully'
                );
            }
            return redirect(
                route('my.account.bank')
            )
                ->with(
                    'success',
                    'Data bank berhasil diperbaharui.'
                );
        }

        return redirect(
            route('my.account.bank')
        )
            ->with(
                'error',
                'Data bank gagal diperbaharui.'
            );
    }

    public function togleBankStatus($bankId)
    {
        if ($this->user_bank->toggleBankStatus($bankId)) {
            if (system_settings()->logging) {
                Log::info(
                    'Member Update : '.
                    auth()->user()->name .' ('. auth()->user()->username .') toggle bank status (id :'. $bankId .') successfully'
                );
            }

            return redirect(
                route('my.account.bank')
            )
                ->with(
                    'success',
                    'Status bank berhasil diperbaharui.'
                );
        }

        return redirect(
            route('my.account.bank')
        )
            ->with(
                'error',
                'Status bank gagal diperbaharui.'
            );
    }

    public function getRegency(Request $request)
    {
        return $this->regency->getRegencyList($request['id']);
    }

    public function getDistrict(Request $request)
    {
        return $this->district->getDistrictList($request['id']);
    }

    public function getVillage(Request $request)
    {
        return $this->village->getVillageList($request['id']);
    }
}
