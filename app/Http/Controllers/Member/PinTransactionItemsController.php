<?php

namespace App\Http\Controllers\Member;

use App\Models\PinTransactionItems;
use Illuminate\Http\Request;

class PinTransactionItemsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\PinTransactionItems  $pinTransactionItems
     * @return \Illuminate\Http\Response
     */
    public function show(PinTransactionItems $pinTransactionItems)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\PinTransactionItems  $pinTransactionItems
     * @return \Illuminate\Http\Response
     */
    public function edit(PinTransactionItems $pinTransactionItems)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\PinTransactionItems  $pinTransactionItems
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PinTransactionItems $pinTransactionItems)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\PinTransactionItems  $pinTransactionItems
     * @return \Illuminate\Http\Response
     */
    public function destroy(PinTransactionItems $pinTransactionItems)
    {
        //
    }
}
