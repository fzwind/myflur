<?php

namespace App\Http\Controllers\Member;

use App\Repositories\UserPinInterface;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class UserPinController extends Controller {

	private $user_pin;

	public function __construct (UserPinInterface $user_pin)
	{
		$this->user_pin = $user_pin;
	}

	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view(
        	'member.pin.index'
        );
    }

    public function getUserPinData()
    {
	    $user_pin = $this->user_pin->getAllWithTransaction(auth()->id());

	    return DataTables::of($user_pin)
             ->addColumn('action', function ($plan_settings) {
                 $detailButton = '';
                 return $detailButton;
             })
             ->rawColumns(['action'])
             ->make(true);
    }

    public function available()
    {
	    return view(
		    'member.pin.available'
	    );
    }

    public function getAvailableData()
    {
	    $user_pin = $this->user_pin->getAvailableWithTransaction(auth()->id());

	    return DataTables::of($user_pin)
	                     ->addColumn('action', function ($plan_settings) {
		                     $detailButton = '';
		                     return $detailButton;
	                     })
	                     ->rawColumns(['action'])
	                     ->make(true);
    }

	public function used()
	{
		return view(
			'member.pin.used'
		);
	}

	public function getUsedData()
	{
		$user_pin = $this->user_pin->getUsedWithTransaction(auth()->id());

		return DataTables::of($user_pin)
		                 ->addColumn('action', function ($plan_settings) {
			                 $detailButton = '';
			                 return $detailButton;
		                 })
		                 ->rawColumns(['action'])
		                 ->make(true);
	}

	public function sold()
	{
		return view(
			'member.pin.sold'
		);
	}

	public function getSoldData()
	{
		$user_pin = $this->user_pin->getSoldWithTransaction(auth()->id());

		return DataTables::of($user_pin)
		                 ->addColumn('action', function ($plan_settings) {
			                 $detailButton = '';
			                 return $detailButton;
		                 })
		                 ->rawColumns(['action'])
		                 ->make(true);
	}

	public function transferred()
	{
		return view(
			'member.pin.transferred'
		);
	}

	public function getTransferredData()
	{
		$user_pin = $this->user_pin->getTransferredWithTransaction(auth()->id());

		return DataTables::of($user_pin)
		                 ->addColumn('action', function ($plan_settings) {
			                 $detailButton = '';
			                 return $detailButton;
		                 })
		                 ->rawColumns(['action'])
		                 ->make(true);
	}

	/**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
