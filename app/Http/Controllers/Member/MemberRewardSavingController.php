<?php

namespace App\Http\Controllers\Member;

use App\Repositories\MemberRewardSavingInterface;
use App\Repositories\RewardSettingInterface;
use Yajra\DataTables\DataTables;

class MemberRewardSavingController extends Controller
{
	private $member_reward_saving;

	public function __construct (
		MemberRewardSavingInterface $member_reward_saving,
		RewardSettingInterface $reward_setting
	) {
		$this->member_reward_saving = $member_reward_saving;
		$this->reward_setting = $reward_setting;
	}

	public function index($reward_setting_id)
    {
	    $reward_setting = $this->reward_setting->getById($reward_setting_id);
	    $title = 'Reward Saving ('. $reward_setting->name .')';

        return view(
        	'member.reward.saving',
	        compact('title', 'reward_setting_id')
        );
    }

    public function getSavingData($reward_setting_id)
    {
	    $data = $this->member_reward_saving->getWithRelations(auth()->id(), $reward_setting_id);

	    return DataTables::of($data)
			->addColumn('withdrawal_code', function ($data) {
				return $data->withdrawal->code;
			})
			->addColumn('formatted_saving_amount', function ($data) {
				return number_format($data->saving_amount,0,',','.');
			})
			->addColumn('saving_date', function ($data) {
				return to_utz($data->created_at);
			})
			->make(true);
    }
}
