<?php

namespace App\Http\Controllers\Member;

use App\Models\PinTransaction;
use App\Repositories\PinInterface;
use App\Repositories\UserPinInterface;
use Illuminate\Support\Facades\DB;

class RepairPinDataController extends Controller
{
	private $pin;
	private $user_pin;

    public function __construct (
    	PinInterface $pin,
		UserPinInterface $user_pin
    ) {
    	$this->pin = $pin;
    	$this->user_pin = $user_pin;
    }

    public function index()
    {
    	$pin_transaction = new PinTransaction();

    	$all_transaction = $pin_transaction
		    ->where('status','confirmed')
		    ->orderBy('id', 'asc')
		    ->get();

	    foreach ($all_transaction as $transaction) {
		    if ($transaction->transaction_type == 'buy' && $transaction->from == 11) {

			    $pins = $this->pin->getPinforUpdate($transaction)->get();

			    foreach ($pins as $pin) {
				    $data = [
					    'user_id' => $transaction->to,
					    'pin_transaction_id' => $transaction->id,
					    'pin_code' => $pin->code,
					    'status' => 0,
					    'created_at' => $transaction->updated_at,
					    'updated_at' => $transaction->updated_at,
				    ];

				    $this->user_pin->create($data);
			    }

			    $this->pin->update($pins,['status' => 1]);

		    } elseif ($transaction->transaction_type == 'buy' && $transaction->from != 11) {

			    $pins = $this->user_pin->getPinforUpdate($transaction)->get();

			    dd($pins);

			    foreach ($pins->get() as $pin) {
				    $data = [
					    'user_id' => $transaction->to,
					    'pin_transaction_id' => $transaction->id,
					    'pin_code' => $pin->pin_code,
					    'status' => 0,
					    'created_at' => $transaction->updated_at,
					    'updated_at' => $transaction->updated_at,
				    ];

				    $this->user_pin->create($data);
			    }

			    $this->user_pin->update($pins,['status' => 1]);

		    }
	    }

	    echo "DONE";

    }
}
