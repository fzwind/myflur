<?php

namespace App\Http\Controllers\Member;

use App\Repositories\RequestStockistInterface;
use Illuminate\Http\Request;

class RequestStockistController extends Controller
{
    private $request_stockist;

    public function __construct (
    	RequestStockistInterface $request_stockist
    ) {
    	$this->request_stockist = $request_stockist;
    }

    public function index()
    {
		$my_request = $this->request_stockist->myRequestStatus();

		return view(
			'member.stockist.index',
			compact('my_request')
		);
    }

    public function postRequest(Request $request)
    {
    	if ($request['user_id'] != auth()->id() || $request['request_type'] > 1 || $request['request_type'] < 0) {
		    return redirect( route('stockist.request') )
			    ->with(
				    'error',
				    'Data tidak valid!'
			    );
	    }

	    if (!$this->request_stockist->requestStockist($request->all())) {
		    return redirect( route('stockist.request') )
			    ->with(
				    'error',
				    'Request Gagal! terjadi kesalahan.'
			    );
	    }

	    return redirect( route('stockist.request') )
		    ->with(
			    'success',
			    'Request stockist Berhasil diajukan.'
		    );
    }
}
