<?php

namespace App\Http\Controllers\Member;

use App\Http\Requests\UserRegisterRequest;
use App\Repositories\UserInterface;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class UserController extends Controller
{
    private $user;

    public function __construct(UserInterface $user)
    {
        $this->middleware('role:members');

        $this->user = $user;
    }

    public function create()
    {
        $plans = plan_list();

        return view(
            'member.network.register',
            compact('plans')
        );
    }

    public function store(UserRegisterRequest $request)
    {
        if ($this->user->registerNewUser($request->all())) {
            return redirect(route('register'))
                ->with(
                    'success',
                    'Registrasi member baru ('. $request['name'] .') berhasil dilakukan!'
                );
        }

        return redirect(route('register'))
            ->with(
                'error',
                'Registrasi member baru ('. $request['name'] .') gagal dilakukan!'
            );
    }

    public function sponsor()
    {
        return view(
            'member.network.index-sponsor'
        );
    }

    public function sponsorData()
    {
        $data = $this->user->getMySponsorList();

        return DataTables::of($data)
            ->addColumn('status_date', function ($data) {
                if ($data->is_active == 1) {
                    $date = '-';
                    if (! is_null($data->active_at)) {
                        $date = str_replace(' +07:00', '', to_utz($data->active_at));
                    }
                } else {
                    if (is_null($data->inactive_at)) {
                        $date = str_replace(' +07:00', '', to_utz($data->created_at));
                    } else {
                        $date = str_replace(' +07:00', '', to_utz($data->inactive_at));
                    }
                }
                return $date;
            })
            ->editColumn('status', function ($data) {
                if ($data->is_active == 1) {
                    $status = '<span class="text-success">aktif</span>';
                } else {
                    if (is_null($data->inactive_at)) {
                        $status = '<span class="text-warning">belum ditempatkan</span>';
                    } else {
                        $status = '<span class="text-danger">suspended</span>';
                    }
                }
                return $status;
            })
            ->rawColumns(['status_date', 'status'])
            ->make(true);
    }
}
