<?php

namespace App\Http\Controllers\Member;

use App\Events\Members\PlacingMemberInStructure;
use App\Events\Pin\OrderMade;
use App\Http\Requests\PinTransactionRequest;
use App\Http\Requests\PinTransactionUpdateRequest;
use App\Http\Requests\ProofTransferUploadRequest;
use App\Models\Memberstructure;
use App\Models\User;
use App\Repositories\FlushedPairingInterface;
use App\Repositories\PinTransactionInterface;
use App\Repositories\PlanSettingInterface;
use App\Repositories\UserInterface;
use App\Tools\PinTransactionsTool;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Yajra\DataTables\DataTables;

class PinTransactionController extends Controller
{
    use PinTransactionsTool;

    private $pin_transaction;
    private $plan_setting;

    public function __construct(
        PinTransactionInterface $pin_transaction,
        PlanSettingInterface $plan_setting
    ) {
        $this->pin_transaction = $pin_transaction;
        $this->plan_setting = $plan_setting;
    }

    public function index()
    {
        $page_title = 'Riwayat Transaksi PIN';
        $route_data = 'transaction.pin.data';

        return view(
            'member.pin.transaction-index',
            compact('page_title', 'route_data')
        );
    }

    public function getTransactionHistoryData()
    {
        $pin_transaction = $this->pin_transaction->getMyTransactionHistory();

        return DataTables::of($pin_transaction)
            ->addColumn('transaction', function ($pin_transaction) {
                return $this->strTransaction($pin_transaction);
            })
            ->addColumn('total_amount', function ($pin_transaction) {
                return 'Rp '. number_format($pin_transaction->price + $pin_transaction->unique_digit, 0, ',', '.');
            })
            ->addColumn('action', function ($pin_transaction) {
                return $this->actionButton($pin_transaction);
            })
            ->addColumn('status_date', function ($pin_transaction) {
                return $this->status_date($pin_transaction);
            })
            ->editColumn('status', function ($pin_transaction) {
                return $this->str_status($pin_transaction);
            })
            ->editColumn('transaction_code', function ($pin_transaction) {
                return $pin_transaction->transaction_code;
                // return $this->transaction_code($pin_transaction);
            })
            ->rawColumns(['action', 'transaction', 'total_amount', 'transaction_code', 'status_date', 'status'])
            ->make(true);
    }

    public function create()
    {
        $plans = plan_list();

        $planId = active_plan()->first()->id;
        $stockist_list = stockist_list($planId);

        return view(
            'member.pin.create',
            compact('plans', 'stockist_list')
        );
    }

    public function getStockistBasedOnPlan(Request $request)
    {
        return stockist_list($request['id']);
    }

    public function store(PinTransactionRequest $request)
    {
        $type = ($request['type'] == 'buy') ? 'pembelian' : 'transfer';

        if ($this->pin_transaction->processTransaction($request['stockist_id'], $request['plan_id'], $request['type'], $request['amount'])) {
            return redirect(
                route('transaction.pin.running')
            )
                ->with(
                    'success',
                    'Order '. $type .' PIN berhasil dibuat.'
                );
        }

        return redirect(
            route('transaction-pin.create')
        )
            ->with(
                'error',
                'Order '. $type .' PIN gagal dibuat.'
            );
    }

    public function update(PinTransactionUpdateRequest $request, $transaction)
    {
        $transaction = $this->pin_transaction->find($transaction);

        $code = $transaction->transaction_code;

        if (! $this->pin_transaction->updateStatus($transaction, $request['status'])) {
            if (system_settings()->logging) {
                Log::info(
                    'Member PIN error : ' .
                    auth()->user()->name . ' (' . auth()->user()->username . ') - '. $code
                );
            }

            return redirect(
                route(
                    'transaction.pin.running'
                )
            )->with(
                'error',
                'Status Transaksi '. $code .' gagal Diperbaharui.'
            );
        }

        if (system_settings()->logging) {
            Log::info(
                'Member PIN : ' .
                auth()->user()->name . ' (' . auth()->user()->username . ') - paid pin transaction '. $code .' successfully'
            );
        }

        return redirect(
            route(
                'transaction.pin.running'
            )
        )->with(
            'success',
            'Status Transaksi '. $code .' berhasil Diperbaharui.'
        );
    }

    public function invoice($transactionId)
    {
        $transaction = $this->pin_transaction->find($transactionId);

        if ($transaction->fromuser->id == 11) {
            $banks = company_banks();
        } else {
            $banks = user_banks($transaction->fromuser->id);
        }

        return view(
            'member.pin.invoice',
            compact('transaction', 'banks')
        );
    }

    public function runningTransaction()
    {
        $page_title = 'Transaksi PIN Berjalan';
        $route_data = 'transaction.running.pin.data';

        return view(
            'member.pin.transaction-index',
            compact('page_title', 'route_data')
        );
    }

    public function getRunningTransactionData()
    {
        $pin_transaction = $this->pin_transaction->getMyRunningTransaction();

        return DataTables::of($pin_transaction)
             ->addColumn('transaction', function ($pin_transaction) {
                 return $this->strTransaction($pin_transaction);
             })
             ->addColumn('total_amount', function ($pin_transaction) {
                 return 'Rp '. number_format($pin_transaction->price + $pin_transaction->unique_digit, 0, ',', '.');
             })
             ->addColumn('action', function ($pin_transaction) {
                 return $this->actionButton($pin_transaction);
             })
             ->addColumn('status_date', function ($pin_transaction) {
                 return $this->status_date($pin_transaction);
             })
             ->editColumn('status', function ($pin_transaction) {
                 return $this->str_status($pin_transaction);
             })
             ->editColumn('transaction_code', function ($pin_transaction) {
                 return $pin_transaction->transaction_code;
                 // return $this->transaction_code($pin_transaction);
             })
             ->rawColumns(['action', 'transaction', 'total_amount', 'transaction_code', 'status_date', 'status'])
             ->make(true);
    }

    public function upload($transactionId)
    {
        $transaction = $this->pin_transaction->find($transactionId);

        if ($transaction->fromuser->id == 11) {
            $company_banks = company_bank_list();
            $user_banks = null;
        } else {
            $company_banks = null;
            $user_banks = user_bank_list($transaction->fromuser->id);
        }

        return view(
            'member.pin.upload',
            compact('transaction', 'company_banks', 'user_banks')
        );
    }

    public function doUpload(ProofTransferUploadRequest $request, $transactionId)
    {
        $pin_transaction = $this->pin_transaction->find($transactionId);

        $file_name = uniqid('btransfer-', true) .'.'. request()->paid_file->getClientOriginalExtension();

        $data = $request->all();
        $data['paid_file'] = $file_name;
        $data['status'] = 'paid';
        $data['paid_at'] = Carbon::now('Asia/Jakarta');

        DB::beginTransaction();

        if ($this->pin_transaction->update($pin_transaction, $data)) {
            if ($request->file('paid_file')->storeAs('public/bukti-transfer', $file_name)) {
                DB::commit();

                if (system_settings()->logging) {
                    Log::info(
                        'Member PIN : ' .
                        auth()->user()->name . ' (' . auth()->user()->username . ') - paid pin transaction '. $pin_transaction->transaction_code .' successfully (uploaded proof of transfer : '. $file_name
                    );
                }

                return redirect(
                    route(
                        'transaction.pin.running'
                    )
                )->with(
                    'success',
                    'Status Transaksi '. $pin_transaction->transaction_code .' berhasil Diperbaharui, dan file bukti transfer berhasil diunggah.'
                );
            }
            DB::rollBack();

            return redirect(
                route(
                    'transaction.pin.running'
                        )
                    )->with(
                        'error',
                        'Status Transaksi '. $pin_transaction->transaction_code .' gagal Diperbaharui. Terjadi kesalahan saat mengunggah file bukti transfer.'
                    );
        }
        DB::rollBack();

        return redirect(
            route(
                'transaction.pin.running'
            )
        )->with(
            'error',
            'Status Transaksi '. $pin_transaction->transaction_code .' gagal Diperbaharui. Terjadi kesalahan saat menyimpan data.'
        );
    }

    public function image($transactionId)
    {
        $transaction = $this->pin_transaction->find($transactionId);

        if ($transaction->fromuser->id == 11) {
            $company_banks = company_bank_list();
            $user_banks = null;
        } else {
            $company_banks = null;
            $user_banks = user_bank_list($transaction->fromuser->id);
        }

        return view(
            'member.pin.bukti-transfer',
            compact('transaction', 'company_banks', 'user_banks')
        );
    }

    public function reject(Request $request)
    {
        $transaction = $this->pin_transaction->find($request['id']);

        if ($transaction->from != auth()->id()) {
            return redirect(
                route(
                    'transaction.pin.running'
                )
            )->with(
                'error',
                'Terdeteksi manipulasi data transaksi!!!'
            );
        }

        try {
            $transaction->status = 'rejected';
            $transaction->status_note = $request['status_note'];
            $transaction->rejected_at = Carbon::now('Asia/Jakarta');
            $transaction->save();
        } catch (\Exception $e) {
            return redirect(
                route(
                    'transaction.pin.running'
                )
            )->with(
                'error',
                'Penolakan transaksi PIN Gagal!'
            );
        }

        return redirect(
            route(
                'transaction.pin.running'
            )
        )->with(
            'success',
            'Transaksi '. $transaction->transaction_code .' berhasil ditolak!'
        );
    }
}
