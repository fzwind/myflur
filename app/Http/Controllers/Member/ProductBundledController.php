<?php

namespace App\Http\Controllers\Member;

use App\Models\ProductBundled;
use Illuminate\Http\Request;

class ProductBundledController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ProductBundled  $productBundled
     * @return \Illuminate\Http\Response
     */
    public function show(ProductBundled $productBundled)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ProductBundled  $productBundled
     * @return \Illuminate\Http\Response
     */
    public function edit(ProductBundled $productBundled)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ProductBundled  $productBundled
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProductBundled $productBundled)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ProductBundled  $productBundled
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProductBundled $productBundled)
    {
        //
    }
}
