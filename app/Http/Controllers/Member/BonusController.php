<?php

namespace App\Http\Controllers\Member;

use App\Models\Bonus;
use App\Repositories\BonusInterface;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class BonusController extends Controller
{
	private $bonus;

	public function __construct (BonusInterface $bonus) {
		$this->bonus = $bonus;
	}

	public function summary()
	{
		$sponsor = system_settings()->bonus_sponsor;
		$pairing = system_settings()->bonus_pairing;
		$level = system_settings()->bonus_level;
		$pairing_level = system_settings()->bonus_pairing_level;

		$i = 0;
		$i = ($sponsor) ? ($i + 1) : $i;
		$i = ($pairing) ? ($i + 1) : $i;
		$i = ($level) ? ($i + 1) : $i;
		$i = ($pairing_level) ? ($i + 1) : $i;

		$sponsor_total = $this->bonus->getTotalBonusSponsor();
		$pairing_total = $this->bonus->getTotalBonusPairing();
		$pairing_level_total = $this->bonus->getTotalBonusPairingLevel();

		return view(
			'member.bonus.summary',
			compact(
				'i',
				'sponsor_total',
				'pairing_total',
				'pairing_level_total'
			)
		);
	}

	public function summaryData()
	{
		$data = $this->bonus->getMyBonus();

		return DataTables::of($data)
			->addColumn('bonus_sponsor', function ($data) {
				return number_format($data->bonus_sponsor,0,',','.');
			})
			->addColumn('bonus_pairing', function ($data) {
				return number_format($data->bonus_pairing,0,',','.');
			})
			->addColumn('bonus_pairing_level', function ($data) {
				return number_format($data->bonus_pairing_level,0,',','.');
			})
			->addColumn('bonus_level', function ($data) {
				return number_format($data->bonus_level,0,',','.');
			})
			->addColumn('bonus_date', function ($data) {
				return str_replace(' +07:00','',to_utz($data->created_at));
			})
			->addColumn('from_user', function ($data) {
				return $data->from->name;
			})
			->rawColumns(['from_user'])
			->make(true);
	}

	public function sponsor()
	{
		return view(
			'member.bonus.sponsor'
		);
	}

	public function sponsorData()
	{
		$data = $this->bonus->getMySponsorBonus();

		return DataTables::of($data)
			->addColumn('bonus_amount', function ($data) {
				return number_format($data->bonus_sponsor,0,',','.');
			})
			->addColumn('bonus_date', function ($data) {
				return str_replace(' +07:00','',to_utz($data->created_at));
			})
			->addColumn('from_user', function ($data) {
				return $data->from->name .' ('. $data->from->username .')';
			})
			->rawColumns(['from_user'])
			->make(true);
	}

	public function pairing()
	{
		return view(
			'member.bonus.pairing'
		);
	}

	public function pairingData()
	{
		$data = $this->bonus->getMyPairingBonus();

		return DataTables::of($data)
			->addColumn('bonus_amount', function ($data) {
				return number_format($data->bonus_pairing,0,',','.');
			})
			->addColumn('bonus_date', function ($data) {
				return str_replace(' +07:00','',to_utz($data->created_at));
			})
			->addColumn('from_user', function ($data) {
				return $data->from->name .' ('. $data->from->username .')';
			})
			->rawColumns(['from_user'])
			->make(true);
	}

	public function pairingLevel()
	{
		return view(
			'member.bonus.pairing-level'
		);
	}

	public function pairingLevelData()
	{
		$data = $this->bonus->getMyPairingLevelBonus();

		return DataTables::of($data)
			->addColumn('bonus_amount', function ($data) {
				return number_format($data->bonus_pairing_level,0,',','.');
			})
			->addColumn('bonus_date', function ($data) {
				return str_replace(' +07:00','',to_utz($data->created_at));
			})
			->addColumn('from_user', function ($data) {
				return $data->from->name .' ('. $data->from->username .')';
			})
			->rawColumns(['from_user'])
			->make(true);
	}
}
