<?php

namespace App\Http\Controllers\Member;

use App\Repositories\BonusInterface;
use App\Repositories\WithdrawalInterface;
use Yajra\DataTables\DataTables;

class WithdrawalController extends Controller
{
	public $withdrawal;
	public $bonus;

	public function __construct (
		WithdrawalInterface $withdrawal,
		BonusInterface $bonus
	) {
		$this->withdrawal = $withdrawal;
		$this->bonus = $bonus;
	}

	public function index()
    {
        return view(
        	'member.bonus.withdrawal'
        );
    }

    public function getWithdrawalData()
    {
	    $data = $this->withdrawal->getWithdrawalData(auth()->id());

	    return DataTables::of($data)
			->addColumn('formatted_wd_amount', function ($data) {
				return number_format($data->wd_amount,0,',','.');
			})
		    ->addColumn('formatted_reward_savings', function ($data) {
			    return number_format($data->wd_reward_deduction,0,',','.');
		    })
		    ->addColumn('formatted_wd_admin', function ($data) {
			    return number_format($data->wd_admin,0,',','.');
		    })
		    ->addColumn('formatted_transferred', function ($data) {
			    return number_format(($data->wd_amount - $data->wd_reward_deduction - $data->wd_index_deduction - $data->wd_admin),0,',','.');
		    })
			->addColumn('wd_date', function ($data) {
				return str_replace(' +07:00','',to_utz($data->updated_at));
			})
		    ->addColumn('formatted_status', function ($data) {
		    	if ($data->status == 0) {
		    		$status = '<a class="btn btn-outline-warning btn-sm">Proses Transfer</a>';
			    } else {
				    $status = '<a class="btn btn-outline-success btn-sm">Selesai</a>';
			    }
			    return $status;
		    })
			->rawColumns(['formatted_status'])
			->make(true);
    }

}
