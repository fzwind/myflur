<?php

namespace App\Http\Controllers\Member;

use App\MemberPlanHistory;
use Illuminate\Http\Request;

class MemberPlanHistoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\MemberPlanHistory  $memberPlanHistory
     * @return \Illuminate\Http\Response
     */
    public function show(MemberPlanHistory $memberPlanHistory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\MemberPlanHistory  $memberPlanHistory
     * @return \Illuminate\Http\Response
     */
    public function edit(MemberPlanHistory $memberPlanHistory)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\MemberPlanHistory  $memberPlanHistory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MemberPlanHistory $memberPlanHistory)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\MemberPlanHistory  $memberPlanHistory
     * @return \Illuminate\Http\Response
     */
    public function destroy(MemberPlanHistory $memberPlanHistory)
    {
        //
    }
}
