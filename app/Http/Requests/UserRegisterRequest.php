<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRegisterRequest extends FormRequest
{

    public function authorize()
    {
	    if(auth()->user()->hasRole('members')) {
		    return true;
	    }

	    return false;
    }

    public function rules()
    {
        return [
            'name' => 'required|string|max:100',
	        'email' => 'required|string|email|max:100',
            'username' => 'required|string|max:30|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ];
    }

	public function messages()
	{
		return [
			'name.required' => 'Nama lengkap harus diisi!',
			'name.max' => 'Nama maksimum 100 karakter!',
			'email.required' => 'Email harus diisi!',
			'email.email' => 'Anda tidak memasukkan alamat email yang benar!',
			'email.max' => 'Email maksimum 100 karakter!',
			'username.required' => 'Username harus diisi!',
			'username.max' => 'Username maksimum 30 karakter!',
			'username.unique' => 'Username sudah digunakan!',
			'password.required' => 'Password harus diisi!',
			'password.min' => 'Password minimum 6 karakter!',
			'password.confirmed' => 'Konfirmasi password tidak cocok!',
		];
	}
}
