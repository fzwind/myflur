<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProofTransferUploadRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
    	if (auth()->user()->hasRole('members')) {
    		return true;
	    }
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'paid_company_to' => 'required_without:paid_to|exists:company_banks,id',
	        'paid_to' => 'required_without:paid_company_to|exists:user_banks,id',
	        'paid_file' => 'required|image|mimes:jpg,jpeg,bmp,png|max:1024'
        ];
    }

	/**
	 * Get the error messages for the defined validation rules.
	 *
	 * @return array
	 */
	public function messages()
	{
		return [
			'paid_company_to.required_without' => 'Anda harus memilih Bank tujuan transfer!',
			'paid_company_to.exists' => 'Bank tidak ada di database!',
			'paid_to.required_without' => 'Anda harus memilih Bank tujuan transfer!',
			'paid_to.exists' => 'Bank tidak ada di database!',
			'paid_file.required' => 'File bukti transfer tidak boleh kosong!',
			'paid_file.image' => 'File bukti transfer harus berupa image!',
			'paid_file.mimes' => 'Tipe image yang diperbolehkan : jpg, jpeg, bmp, png!',
			'paid_file.max' => 'Maksimum ukuran file : 1 MB!',
		];
	}

}
