<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PinTransactionUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
	    if(auth()->user()->hasRole('members') || auth()->user()->hasPermissionTo('admin pin approve')) {
		    return true;
	    }

	    return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
	    return [
		    'status' => 'required|in:paid,confirmed',
	    ];
    }

	/**
	 * Get the error messages for the defined validation rules.
	 *
	 * @return array
	 */
	public function messages() {
		return [
			'status.required' => 'Status null!',
			'status.in' => 'Data status tidak valid!',
		];
	}

}
