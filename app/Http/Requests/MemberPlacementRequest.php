<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MemberPlacementRequest extends FormRequest
{
    public function authorize()
    {
	    if(auth()->user()->hasRole('members')) {
		    return true;
	    }

	    return false;
    }

    public function rules()
    {
        return [
            'parent_id' => 'required|exists:memberstructures,id',
            'user_id' => 'required|unique:memberstructures,user_id',
        ];
    }

	public function messages()
	{
		return [
			'parent_id.required' => 'Data upline kosong!',
			'parent_id.exists' => 'Data upline tidak terdapat di database!',
			'user_id.required' => 'Data member yang akan diplacement kosong!',
			'user_id.unique' => 'Member sudah ada di dalam struktur!',
		];
	}
}
