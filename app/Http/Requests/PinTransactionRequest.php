<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PinTransactionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
    	if(auth()->user()->hasRole('members')) {
    		return true;
	    }

        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
    	$stockist_setting = stockist_setting($this->plan_id);

    	if(auth()->user()->is_stockist == 0) {
    		$min = 1;
	    }

	    if(auth()->user()->is_stockist == 1) {
		    (auth()->user()->stockist_type == 0) ? $min = $stockist_setting->min_order_stockist : $min = $stockist_setting->min_order_master_stockist;
	    }

        return [
        	'type' => 'required|in:buy,transfer',
	        'plan_id' => 'required|exists:plans,id',
	        'stockist_id' => 'required|exists:users,id',
	        'amount' => 'required|integer|min:'. $min
        ];
    }

	/**
	 * Get the error messages for the defined validation rules.
	 *
	 * @return array
	 */
	public function messages() {
		$stockist_setting = stockist_setting($this->plan_id);

		if(auth()->user()->is_stockist == 0) {
			$min = 1;
			$subject = 'Member';
		}

		if(auth()->user()->is_stockist == 1) {
			(auth()->user()->stockist_type == 0) ? $min = $stockist_setting->min_order_stockist : $min = $stockist_setting->min_order_master_stockist;
			(auth()->user()->stockist_type == 0) ? $subject = 'Stockist' : $subject = 'Master Stockist';
		}

		return [
			'type.required' => 'Transaction type null!',
			'type.in' => 'Transaction type invalid!',
			'plan_id.required' => 'ID Plan null!',
			'plan_id.exists' => 'ID Plan tidak ditemukan di database!',
			'stockist_id.required' => 'ID Stockist null!',
			'stockist.exist' => 'ID Stockist tidak ditemukan di database!',
			'amount.required' => 'Jumlah Pin tidak boleh kosong!',
			'amount.integer' => 'Jumlah Pin harus berupa angka!',
			'amount.min' => 'Minimal pembelian '. $subject .' : '. $min .' pin!',
		];
	}

}
