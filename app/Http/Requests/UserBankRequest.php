<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserBankRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
	    if(auth()->user()->hasRole('members')) {
		    return true;
	    }

	    return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'bank_id' => 'required|exists:master_banks,id',
	        'account_number' => 'required|string',
	        'account_name' => 'required|string'
        ];
    }

	/**
	 * Get the error messages for the defined validation rules.
	 *
	 * @return array
	 */
	public function messages()
	{
		return [
			'bank_id.required' => 'Anda harus memilih Bank!',
			'bank_id.exists' => 'Bank tidak ada di database!',
			'account_number.required' => 'Nomor rekening harus diisi!',
			'account_name.required' => 'Nama pemilik rekening harus diisi!',
		];
	}
}
