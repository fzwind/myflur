<?php

namespace App\Http\Requests;

use App\Rules\mobile_phone_number;
use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;

class AccountRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
	    return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
    	$validation = [];
    	if (system_settings()->memdata_name) {
		    $validation['name'] = ['sometimes', 'required', 'string'];
	    }

	    if (system_settings()->memdata_email) {
		    $validation['email'] = ['sometimes', 'required', 'email'];
	    }

	    if (system_settings()->memdata_hp) {
		    $validation['no_hp'] = ['sometimes', 'required', new mobile_phone_number];
	    }

	    if (system_settings()->memdata_sex) {
		    $validation['sex'] = ['sometimes', 'required', 'in:male,female'];
	    }

	    if (system_settings()->memdata_birth_date) {
		    $validation['birth_date_at'] = ['sometimes', 'required', 'date_format:Y-m-d', 'before:'. Carbon::today('Asia/Jakarta')->subYears(17)];
	    }

	    if (system_settings()->memdata_address) {
		    $validation['address'] = ['sometimes', 'required', 'string'];
	    }

	    if (system_settings()->memdata_province) {
		    $validation['province_id'] = ['sometimes', 'required', 'exists:provinces,id'];
	    }

	    if (system_settings()->memdata_regency) {
		    $validation['regency_id'] = ['sometimes', 'required', 'exists:regencies,id'];
	    }

	    if (system_settings()->memdata_district) {
		    $validation['district_id'] = ['sometimes', 'required', 'exists:districts,id'];
	    }

	    if (system_settings()->memdata_village) {
		    $validation['village_id'] = ['sometimes', 'required', 'exists:villages,id'];
	    }

	    if (system_settings()->memdata_zip) {
		    $validation['postal_code'] = ['sometimes', 'required', 'digits:5'];
	    }

	    if (system_settings()->memdata_id_number) {
		    $validation['no_id'] = ['sometimes', 'required', 'string'];
	    }

	    if (system_settings()->memdata_tax_id_number) {
		    $validation['no_tax_id'] = ['sometimes', 'required', 'string'];
	    }

	    $validation['password'] = ['sometimes', 'required', 'confirmed'];

	    return $validation;
    }
}
