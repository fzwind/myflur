<?php

namespace App\Http\Requests\Admin;

use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class PlanPriceHistoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
	    if (Auth::user()->hasPermissionTo('admin plan setting create')) {
		    return true;
	    }
	    return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        	'plan_id' => 'required|exists:plans,id',
            'price' => 'required|integer|min:0',
	        'stockist_price' => 'required|integer|min:0',
	        'master_stockist_price' => 'required|integer|min:0',
	        'active_at' => 'required|date_format:Y-m-d|after_or_equal:'. Carbon::today('Asia/Jakarta'),
        ];
    }

	/**
	 * Get the error messages for the defined validation rules.
	 *
	 * @return array
	 */
	public function messages()
	{
		return [
			'plan_id.required' => 'ID Plan null!',
			'plan_id.exists' => 'ID Plan tidak terdaftar di database!',
			'price.required' => 'Harga Member TIDAK boleh kosong!',
			'price.integer' => 'Harga Member harus berupa angka!',
			'price.min' => 'Ah yang bener aja! masa harga nya minus!',
			'stockist_price.required' => 'Harga Stockist TIDAK boleh kosong!',
			'stockist_price.integer' => 'Harga Stockist harus berupa angka!',
			'stockist_price.min' => 'Ah yang bener aja! masa harga nya minus!',
			'master_stockist_price.required' => 'Harga Master Stockist TIDAK boleh kosong!',
			'master_stockist_price.integer' => 'Harga Master Stockist harus berupa angka!',
			'master_stockist_price.min' => 'Ah yang bener aja! masa harga nya minus!',
			'active_at.required' => 'Tanggal Aktif TIDAK boleh kosong!',
			'active_at.date_format' => 'Format tangal : yyyy-mm-dd!',
			'active_at.after' => 'Tanggal aktif harus lebih dari hari ini!',
		];
	}
}
