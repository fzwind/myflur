<?php

namespace App\Http\Requests\Admin;

use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class BonusSettingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
	    $id = empty($this->all()) ? 0 : $this->bonus_setting;

	    if ($id == 0 && Auth::user()->hasPermissionTo('admin bonus setting create')) {
		    return true;
	    }

	    if ($id != 0 && Auth::user()->hasPermissionTo('admin bonus setting edit')) {
		    return true;
	    }

	    return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
	    $id = empty($this->all()) ? 0 : $this->bonus_setting;

        return [
            'plan_id' => 'required|exists:plans,id',
	        'active_at' => 'required|date_format:Y-m-d|after_or_equal:'. Carbon::today('Asia/Jakarta') .'|unique:bonus_settings,active_at,'. $id .',id,plan_id,'. $this->plan_id,
			'bonus_sponsor_amount' => 'sometimes|required|integer',
	        'bonus_pairing_amount' => 'sometimes|required|integer',
	        'bonus_pairing_level_amount' => 'sometimes|required|integer',
	        'max_pairing_by' => 'sometimes|required|integer|between:1,3',
	        'over_pairing_handle' => 'sometimes|required|integer|between:1,2',
	        'max_pairing_per_day' => 'sometimes|required|integer',
	        'max_pairing_amount_per_day' => 'sometimes|required|integer',
	        'pair_per_level' => 'sometimes|required|integer',
	        'max_level_bonus_level' => 'sometimes|required|integer|between:1,10',
	        'bonus_level_amount_1' => 'sometimes|required|integer',
            'bonus_level_amount_2' => 'sometimes|required|integer',
            'bonus_level_amount_3' => 'sometimes|required|integer',
            'bonus_level_amount_4' => 'sometimes|required|integer',
            'bonus_level_amount_5' => 'sometimes|required|integer',
            'bonus_level_amount_6' => 'sometimes|required|integer',
            'bonus_level_amount_7' => 'sometimes|required|integer',
            'bonus_level_amount_8' => 'sometimes|required|integer',
            'bonus_level_amount_9' => 'sometimes|required|integer',
            'bonus_level_amount_10' => 'sometimes|required|integer',
        ];
    }

	/**
	 * Get the error messages for the defined validation rules.
	 *
	 * @return array
	 */
	public function messages()
	{
		return [
			'plan_id.required' => 'ID Plan null!',
			'plan_id.exists' => 'ID Plan tidak terdaftar di database!',
			'active_at.required' => 'Tanggal Aktif TIDAK boleh kosong!',
			'active_at.date_format' => 'Format tangal : yyyy-mm-dd!',
			'active_at.unique' => 'Tanggal tersebut sudah terdapat di database untuk PLAN ini.',
			'bonus_sponsor_amount.required' => 'Nominal bonus sponsor TIDAK boleh kosong!',
			'bonus_sponsor_amount.integer' => 'Nominal bonus sponsor harus berupa angka!',
			'bonus_pairing_amount.required' => 'Nominal bonus pairing TIDAK boleh kosong!',
			'bonus_pairing_amount.integer' => 'Nominal bonus pairing harus berupa angka!',
			'bonus_pairing_level_amount.required' => 'Nominal bonus pairing level TIDAK boleh kosong!',
			'bonus_pairing_level_amount.integer' => 'Nominal bonus pairing level harus berupa angka!',
			'max_pairing_by.required' => 'Dasar pairing TIDAK boleh kosong!',
			'max_pairing_by.integer' => 'Dasar pairing harus berupa angka!',
			'max_pairing_by.between' => 'Dasar pairing harus dari pilihan yang tersedia!',
			'over_pairing_handle.required' => 'Over pairing TIDAK boleh kosong!',
			'over_pairing_handle.integer' => 'Over pairing harus berupa angka!',
			'over_pairing_handle.between' => 'Over pairing harus dari pilihan yang tersedia!',
			'max_pairing_per_day.required' => 'Maks. jumlah pairing TIDAK boleh kosong!',
			'max_pairing_per_day.integer' => 'Maks. jumlah pairing harus berupa angka!',
			'max_pairing_amount_per_day.required' => 'Maks. total nominal pairing TIDAK boleh kosong!',
			'max_pairing_amount_per_day.integer' => 'Maks.  total nominal pairing harus berupa angka!',
			'pair_per_level.required' => 'Jumlah pairing/level TIDAK boleh kosong!',
			'pair_per_level.integer' => 'Jumlah pairing/level harus berupa angka!',
			'max_level_bonus_level.required' => 'Jml. level bonus level TIDAK boleh kosong!',
			'max_level_bonus_level.integer' => 'Jml. level bonus level harus berupa angka!',
			'max_level_bonus_level.between' => 'Jml. level bonus level min:1 maks:10',
			'bonus_level_amount_1.required' => 'Nominal bonus level TIDAK boleh kosong!',
			'bonus_level_amount_1.integer' => 'Nominal bonus level harus berupa angka!',
			'bonus_level_amount_2.required' => 'Nominal bonus level TIDAK boleh kosong!',
			'bonus_level_amount_2.integer' => 'Nominal bonus level harus berupa angka!',
			'bonus_level_amount_3.required' => 'Nominal bonus level TIDAK boleh kosong!',
			'bonus_level_amount_3.integer' => 'Nominal bonus level harus berupa angka!',
			'bonus_level_amount_4.required' => 'Nominal bonus level TIDAK boleh kosong!',
			'bonus_level_amount_4.integer' => 'Nominal bonus level harus berupa angka!',
			'bonus_level_amount_5.required' => 'Nominal bonus level TIDAK boleh kosong!',
			'bonus_level_amount_5.integer' => 'Nominal bonus level harus berupa angka!',
			'bonus_level_amount_6.required' => 'Nominal bonus level TIDAK boleh kosong!',
			'bonus_level_amount_6.integer' => 'Nominal bonus level harus berupa angka!',
			'bonus_level_amount_7.required' => 'Nominal bonus level TIDAK boleh kosong!',
			'bonus_level_amount_7.integer' => 'Nominal bonus level harus berupa angka!',
			'bonus_level_amount_8.required' => 'Nominal bonus level TIDAK boleh kosong!',
			'bonus_level_amount_8.integer' => 'Nominal bonus level harus berupa angka!',
			'bonus_level_amount_9.required' => 'Nominal bonus level TIDAK boleh kosong!',
			'bonus_level_amount_9.integer' => 'Nominal bonus level harus berupa angka!',
			'bonus_level_amount_10.required' => 'Nominal bonus level TIDAK boleh kosong!',
			'bonus_level_amount_10.integer' => 'Nominal bonus level harus berupa angka!',
		];
	}
}
