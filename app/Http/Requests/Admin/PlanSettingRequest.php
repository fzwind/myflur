<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class PlanSettingRequest extends FormRequest
{
	/**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
	    $id = empty($this->all()) ? 0 : $this->plan_setting;

	    if ($id == 0 && Auth::user()->hasPermissionTo('admin plan setting create')) {
    		return true;
	    }

	    if ($id != 0 && Auth::user()->hasPermissionTo('admin plan setting edit')) {
		    return true;
	    }

        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
	    $id = empty($this->all()) ? 0 : $this->plan_setting;

	    if($id != 0) {
		    return [
			    'code' => 'required|max:5|unique:plans,code,'. $id,
			    'name' => 'required|max:100|unique:plans,name,'. $id,
		    ];
	    }
        return [
            'code' => 'required|max:5|unique:plans,code,'. $id,
	        'name' => 'required|max:100|unique:plans,name,'. $id,
	        'price' => 'required|integer|min:0',
            'stockist_price' => 'required|integer|min:0',
            'master_stockist_price' => 'required|integer|min:0',
        ];
    }

	/**
	 * Get the error messages for the defined validation rules.
	 *
	 * @return array
	 */
	public function messages()
	{
		$id = empty($this->all()) ? 0 : $this->plan_setting;

		if($id != 0) {
			return [
				'code.required' => 'Kode Plan TIDAK boleh kosong!',
				'code.max' => 'Kode Plan maksimum 5 karakter!',
				'code.unique' => 'Kode Plan sudah ada di database!',
				'name.required' => 'Nama Plan TIDAK boleh kosong!',
				'name.max' => 'Nama Plan maksimum 100 karakter!',
				'name.unique' => 'Nama Plan sudah ada di database!',
			];
		}

		return [
			'code.required' => 'Kode Plan TIDAK boleh kosong!',
			'code.max' => 'Kode Plan maksimum 5 karakter!',
			'code.unique' => 'Kode Plan sudah ada di database!',
			'name.required' => 'Nama Plan TIDAK boleh kosong!',
			'name.max' => 'Nama Plan maksimum 100 karakter!',
			'name.unique' => 'Nama Plan sudah ada di database!',
			'price.required' => 'Harga Plan TIDAK boleh kosong!',
			'price.integer' => 'Harga Plan harus berupa angka!',
			'price.min' => 'Ah yang bener aja! masa harga nya minus!',
			'stockist_price.required' => 'Harga Stockist TIDAK boleh kosong!',
			'stockist_price.integer' => 'Harga Stockist harus berupa angka!',
			'stockist_price.min' => 'Ah yang bener aja! masa harga nya minus!',
			'master_stockist_price.required' => 'Harga Master Stockist TIDAK boleh kosong!',
			'master_stockist_price.integer' => 'Harga Master Stockist harus berupa angka!',
			'master_stockist_price.min' => 'Ah yang bener aja! masa harga nya minus!',
		];
	}

}
