<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class PinSettingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
	    if (Auth::user()->hasRole('administrators') || Auth::user()->hasRole('super-administrator')) {
		    return true;
	    }
	    return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
	        'plan_id' => 'required|exists:plans,id',
	        'amount' => 'required|integer|min:0|max:10000',
        ];
    }

	/**
	 * Get the error messages for the defined validation rules.
	 *
	 * @return array
	 */
	public function messages()
	{
		return [
			'plan_id.required' => 'ID Plan null!',
			'plan_id.exists' => 'ID Plan tidak terdaftar di database!',
			'amount.required' => 'Jumlah Pin TIDAK boleh kosong!',
			'amount.integer' => 'Jumlah Pin harus berupa angka!',
			'amount.min' => 'Ah yang bener aja! masa minus!',
			'amount.max' => 'Maksimum generate Pin : 10.000!',
		];
	}
}
