<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class CompanyBankRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
	    if(auth()->user()->hasRole('administrators') || auth()->user()->hasRole('super-administrator')) {
		    return true;
	    } else {
		    return false;
	    }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
	        'bank_id' => 'required|exists:master_banks,id',
	        'account_number' => 'required',
	        'account_name' => 'required',
	        'is_active' => 'required|boolean'
        ];
    }

	/**
	 * Get the error messages for the defined validation rules.
	 *
	 * @return array
	 */
	public function messages()
	{
		return [
			'bank_id.required' => 'Bank can not empty!',
			'bank_id.exists' => 'Bank not listed in database!',
			'account_number.required' => 'Account number can not empty!',
			'account_name.required' => 'Account Name can not empty!',
			'is_active.required' => 'Active status can not empty!',
			'is_active.boolean' => 'Active status must be Yes or No!',
		];
	}

}
