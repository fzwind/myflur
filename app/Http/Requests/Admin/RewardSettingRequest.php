<?php

namespace App\Http\Requests\Admin;

use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;

class RewardSettingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'plan_id' => 'required|exists:plans,id',
	        'name' => 'required|string',
	        'reward_type' => 'required|in:1,2,3',
	        'deducted_bonuses' => 'required_if:reward_type,2,3',
	        'deduction_type' => 'required_if:reward_type,2|nullable|in:1,2',
	        'deduction_value' => 'required_if:reward_type,2|nullable|integer',
	        'deduction_target' => 'required_if:reward_type,2|nullable|integer',
	        'reward_prerequisite' => 'nullable|exists:reward_settings,id',
	        'active_at' => 'required|date_format:Y-m-d|after_or_equal:'. Carbon::today('Asia/Jakarta'),
            'inactive_at' => 'sometimes|nullable|date_format:Y-m-d|after:'. Carbon::today('Asia/Jakarta'),
        ];
    }

	public function messages()
	{
		return [
			'plan_id.required' => 'ID Plan null!',
			'plan_id.exists' => 'ID Plan tidak terdaftar di database!',
			'name.required' => 'Nama TIDAK boleh kosong!',
			'reward_type.required' => 'Jenis reward harus diisi!',
			'reward_type.in' => 'Pilih jenis reward sesuai pilihan yang ada!',
			'deducted_bonuses.required_if' => 'Tipe reward potong bonus harus memilih minimal satu bonus yang dipotong!',
			'deduction_type.required_if' => 'Tipe reward potong bonus harus memilih tipe potongan!',
			'deduction_type.in' => 'Pilih salah satu dari pilihan yang tersedia!',
			'deduction_value.required_if' => 'Besaran potongan harus diisi!',
			'deduction_value.integer' => 'Besaran potongan harus berupa angka!',
			'deduction_target.required_if' => 'Target jumlah potongan harus diisi!',
			'deduction_target.integer' => 'Target jumlah potongan harus berupa angka!',
			'reward_prerequisite.exists' => 'Reward pra-syarat tidak terdapat di database',
			'active_at.required' => 'Tanggal aktif harus diisi',
			'active_at.date_format' => 'Format : Y-m-d!',
			'inactive_at.date_format' => 'Format : Y-m-d!',
		];
	}
}
