<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class SystemGeneralSettingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
    	if(Auth::user()->hasRole('super-administrator')) {
    		return true;
	    } else {
		    return false;
	    }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'network_type' => 'required|in:binary,matrix',
	        'email_activation' => 'required|boolean',
	        'notification' => 'required|in:none,email,app,email-app',
	        'logging' => 'required|boolean',
	        'proof_bank_transfer_upload' => 'required|boolean'
        ];
    }

	/**
	 * Get the error messages for the defined validation rules.
	 *
	 * @return array
	 */
	public function messages()
	{
		return [
			'network_type.required' => 'Network Type can not Empty!',
			'network_type.in' => 'Network Type valid value are : Binary or Matrix!',
			'email_activation.required' => 'Email Activation can not Empty!',
			'email_activation.boolean' => 'Email Activation value Must be YES or NO!',
			'notification.required' => 'Notification can not Empty!',
			'notification.in' => 'Notification valid value are: none, email, app, email-app!',
			'logging.required' => 'Logging can not Empty!',
			'logging.boolean' => 'Logging value Must be YES or NO!',
			'proof_bank_transfer_upload.required' => 'Proof of Bank Transfer Upload can not Empty!',
			'proof_bank_transfer_upload.boolean' => 'Proof of Bank Transfer Upload value Must be YES or NO!'
		];
	}

}
