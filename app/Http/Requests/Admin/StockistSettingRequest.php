<?php

namespace App\Http\Requests\Admin;

use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class StockistSettingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
	    $id = empty($this->all()) ? 0 : $this->stockist_setting;

	    if ($id == 0 && Auth::user()->hasPermissionTo('admin stockist setting create')) {
		    return true;
	    }

	    if ($id != 0 && Auth::user()->hasPermissionTo('admin stockist setting edit')) {
		    return true;
	    }

	    return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
	    $id = empty($this->all()) ? 0 : $this->stockist_setting;

        return [
	        'plan_id' => 'required|exists:plans,id',
	        'active_at' => 'required|date_format:Y-m-d|after_or_equal:'. Carbon::today('Asia/Jakarta') .'|unique:stockist_settings,active_at,'. $id .',id,plan_id,'. $this->plan_id,
	        'min_order_stockist' => 'required|integer',
	        'min_order_master_stockist' => 'required|integer',
	        'member_buy_crossline' => 'required|boolean',
        ];
    }

	/**
	 * Get the error messages for the defined validation rules.
	 *
	 * @return array
	 */
	public function messages()
	{
		return [
			'plan_id.required' => 'ID Plan null!',
			'plan_id.exists' => 'ID Plan tidak terdaftar di database!',
			'active_at.required' => 'Tanggal Aktif TIDAK boleh kosong!',
			'active_at.date_format' => 'Format tangal : yyyy-mm-dd!',
			'active_at.unique' => 'Tanggal tersebut sudah terdapat di database untuk PLAN ini.',
			'active_at.after' => 'Tanggal aktif harus setelah hari ini.',
			'min_order_stockist.required' => 'Jumlah minimum order stockist TIDAK boleh kosong!',
			'min_order_stockist.integer' => 'Jumlah minimum order stockist harus berupa angka!',
			'min_order_master_stockist.required' => 'Jumlah minimum order master stockist TIDAK boleh kosong!',
			'min_order_master_stockist.integer' => 'Jumlah minimum order master stockist harus berupa angka!',
			'member_buy_crossline.required' => 'Member order crossline TIDAK boleh kosong!',
			'member_buy_crossline.boolean' => 'Member order crossline harus ALLOWED atau NOT ALLOWED!',
		];
	}
}
