<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class SystemBonusSettingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
	    if(Auth::user()->hasRole('super-administrator')) {
		    return true;
	    }

	    return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'rewards' => 'required|boolean',
            'bonus_sponsor' => 'required|boolean',
            'bonus_pairing' => 'required|boolean',
            'bonus_pairing_level' => 'required|boolean',
            'bonus_level' => 'required|boolean',
        ];
    }

	/**
	 * Get the error messages for the defined validation rules.
	 *
	 * @return array
	 */
	public function messages()
	{
		return [
			'rewards.required' => 'Rewards can not Empty!',
			'rewards.boolean' => 'Rewards value Must be YES or NO!',
			'bonus_sponsor.required' => 'Bonus Sponsor can not Empty!',
			'bonus_sponsor.boolean' => 'Bonus Sponsor value Must be YES or NO!',
			'bonus_pairing.required' => 'Bonus Pairing can not Empty!',
			'bonus_pairing.boolean' => 'Bonus Pairing value Must be YES or NO!',
			'bonus_pairing_level.required' => 'Bonus Pairing Level can not Empty!',
			'bonus_pairing_level.boolean' => 'Bonus Pairing Level value Must be YES or NO!',
			'bonus_level.required' => 'Bonus Level can not Empty!',
			'bonus_level.boolean' => 'Bonus Level value Must be YES or NO!',
		];
	}
}
