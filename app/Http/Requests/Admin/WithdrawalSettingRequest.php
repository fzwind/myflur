<?php

namespace App\Http\Requests\Admin;

use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;

class WithdrawalSettingRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
	        'wd_admin' => 'required|integer',
	        'wd_index_deduction' => 'required|integer|max:90',
	        'wd_mode' => 'required|in:1,2',
	        'wd_days' => 'required',
	        'active_at' => 'required|date_format:Y-m-d|after_or_equal:'. Carbon::today('Asia/Jakarta'),
        ];
    }

	public function messages()
	{
		return [
			'wd_admin.required' => 'Biaya admin tidak boleh kosong!',
			'wd_admin.integer' => 'Biaya admin harus berupa angka!',
			'wd_index_deduction.required' => 'Besaran index tidak boleh kosong!',
			'wd_index_deduction.integer' => 'Besaran index harus berupa angka!',
			'wd_index_deduction.max' => 'Maksimum index 90 %!',
			'wd_mode.required' => 'Mode WD tidak boleh kosong!',
			'wd_mode.in' => 'Mode WD harus memilih dari pilihan yang tersedia!',
			'wd_days.required' => 'Hari WD tidak boleh kosong!',
			'active_at.required' => 'Tanggal aktif harus diisi',
			'active_at.date_format' => 'Format : Y-m-d!',
		];
	}
}
