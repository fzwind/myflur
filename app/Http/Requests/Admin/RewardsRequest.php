<?php

namespace App\Http\Requests\Admin;

use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Session;

class RewardsRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
	        'reward_setting_id' => 'required|exists:reward_settings,id',
	        'reward_type' => 'required|in:1,2',
	        'can_claim_as_value' => 'required_if:reward_type,2|nullable',
	        'left' => 'required|integer',
	        'right' => 'required|integer',
	        'reward' => 'required|string|max:255',
	        'reward_value' => 'required|integer',
	        'reward_deduction_type' => 'required|in:0,1,2',
	        'reward_deduction_on_claim' => 'required_if:reward_deduction_type,1,2|nullable|integer',
	        'active_at' => 'required|date_format:Y-m-d|after_or_equal:'. Carbon::today('Asia/Jakarta'),
	        'inactive_at' => 'sometimes|nullable|date_format:Y-m-d|after:'. Carbon::today('Asia/Jakarta'),
        ];
    }

	public function messages()
	{
		return [
			'reward_setting_id.required' => 'ID Reward Setting null!',
			'reward_setting_id.exists' => 'ID Reward Setting tidak terdaftar di database!',
			'reward_type.required' => 'Jenis reward harus diisi!',
			'reward_type.in' => 'Pilih jenis reward sesuai pilihan yang ada!',
			'can_claim_as_value.required_if' => 'Kolom ini wajib diisi!',
			'left.required' => 'Target Kiri harus diisi!',
			'left.integer' => 'Target Kiri harus berupa angka!',
			'right.required' => 'Target Kanan harus diisi!',
			'right.integer' => 'Target Kanan harus berupa angka!',
			'reward.required' => 'Reward harus diisi!',
			'reward.string' => 'Reward harus berupa teks!',
			'reward.max' => 'Maksimum 255 karakter!',
			'reward_value.required' => 'Nominal reward harus diisi!',
			'reward_value.integer' => 'Nominal reward harus berupa angka!',
			'reward_deduction_type.required' => 'Jenis potongan harus diisi!',
			'reward_deduction_type.in' => 'Pilih jenis ppotongan sesuai pilihan yang tersedia!',
			'reward_deduction_on_claim.integer' => 'Nilai potongan harus berupa angka',
			'active_at.required' => 'Tanggal aktif harus diisi',
			'active_at.date_format' => 'Format : Y-m-d!',
			'inactive_at.date_format' => 'Format : Y-m-d!',
		];
	}

	public function withValidator($validator)
	{
		if ($validator->fails()) {
			Session::flash('error', 'Flash error!');
		} else {

		}

	}
}
