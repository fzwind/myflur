<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class SystemMemberMetaSettingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
	    if(Auth::user()->hasRole('super-administrator')) {
		    return true;
	    } else {
		    return false;
	    }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
	        'memdata_name' => 'required|boolean',
	        'memdata_email' => 'required|boolean',
	        'memdata_address' => 'required|boolean',
	        'memdata_province' => 'required|boolean',
	        'memdata_regency' => 'required|boolean',
	        'memdata_district' => 'required|boolean',
	        'memdata_village' => 'required|boolean',
	        'memdata_zip' => 'required|boolean',
	        'memdata_hp' => 'required|boolean',
	        'memdata_id_number' => 'required|boolean',
	        'memdata_tax_id_number' => 'required|boolean',
	        'memdata_sex' => 'required|boolean',
	        'memdata_birth_date' => 'required|boolean',
	        'member_max_bank_account' => 'required|integer|min:0',
	        'member_bank_name_match' => 'required|boolean',
	        'member_bank_unique' => 'required|boolean',
        ];
    }

	/**
	 * Get the error messages for the defined validation rules.
	 *
	 * @return array
	 */
	public function messages()
	{
		return [
			'memdata_name.required' => 'Name Option can not Empty!',
			'memdata_name.boolean' => 'Name Option value Must be YES or NO!',
			'memdata_email.required' => 'Email Option can not Empty!',
			'memdata_email.boolean' => 'Email Option value Must be YES or NO!',
			'memdata_address.required' => 'Address Option can not Empty!',
			'memdata_address.boolean' => 'Address Option value Must be YES or NO!',
			'memdata_province.required' => 'Province Option can not Empty!',
			'memdata_province.boolean' => 'Province Option value Must be YES or NO!',
			'memdata_regency.required' => 'Regency Option can not Empty!',
			'memdata_regency.boolean' => 'Regency Option value Must be YES or NO!',
			'memdata_district.required' => 'District Option can not Empty!',
			'memdata_district.boolean' => 'District Option value Must be YES or NO!',
			'memdata_village.required' => 'Village Option can not Empty!',
			'memdata_village.boolean' => 'Village Option value Must be YES or NO!',
			'memdata_zip.required' => 'ZIP Option can not Empty!',
			'memdata_zip.boolean' => 'ZIP Option value Must be YES or NO!',
			'memdata_hp.required' => 'Mobile Phone Option can not Empty!',
			'memdata_hp.boolean' => 'Mobile Phone Option value Must be YES or NO!',
			'memdata_id_number.required' => 'ID Number Option can not Empty!',
			'memdata_id_number.boolean' => 'ID Number Option value Must be YES or NO!',
			'memdata_tax_id_number.required' => 'Tax ID Number Option can not Empty!',
			'memdata_tax_id_number.boolean' => 'Tax ID Number Option value Must be YES or NO!',
			'memdata_sex.required' => 'Sex Option can not Empty!',
			'memdata_sex.boolean' => 'Sex Option value Must be YES or NO!',
			'memdata_birth_date.required' => 'Birth Date Option can not Empty!',
			'memdata_birth_date.boolean' => 'Birth Date Option value Must be YES or NO!',
			'member_max_bank_account.required' => 'Max. Bank Account can not Empty!',
			'memdata_name.integer' => 'Max. Bank Account value must be integer!',
			'memdata_name.min' => 'Max. Bank Account value min. 0!',
			'member_bank_name_match.required' => 'Bank Name Match Option can not Empty!',
			'member_bank_name_match.boolean' => 'Bank Name Match Option value Must be YES or NO!',
			'member_bank_unique.required' => 'Bank Unique Option can not Empty!',
			'member_bank_unique.boolean' => 'Bank Unique Option value Must be YES or NO!'
		];
	}
}
