<?php

// if (! function_exists('to_utc')) {
//     /**
//      * @param $date
//      *
//      * @return \App\Tools\TimeZone
//      */
//     function to_utc($date)
//     {
//         $tz = new \App\Tools\TimeZone();
//         return $tz->toUtc($date);
//     }
// }
//
// if (! function_exists('from_utc')) {
//     /**
//      * @param $date
//      * @param string $format
//      *
//      * @return mixed
//      */
//     function from_utc($date, $format = 'd-m-Y H:i:s (T P)')
//     {
//         $tz = new \App\Tools\TimeZone();
//         return $tz->fromUtc($date, $format);
//     }
// }

if (! function_exists('to_utz')) {
    /**
     * @param $date
     * @param string $format
     *
     * @return mixed
     */
    function to_utz($date, $format = 'd-m-Y H:i:s (T P)')
    {
        $tz = new \App\Tools\TimeZone();
        return $tz->toUtz($date, $format);
    }
}

if (! function_exists('auth_tz')) {
    function auth_tz()
    {
        return auth()->user()->timezone;
    }
}

if (! function_exists('system_settings')) {
    /**
     * @return mixed
     */
    function system_settings()
    {
        $sys = new \App\Tools\SystemSettings();
        return $sys->getSystemSettings();
    }
}

if (! function_exists('bonus_setting')) {
    /**
     * @param $planId
     *
     * @return mixed
     */
    function bonus_setting($planId)
    {
        $bns = new \App\Tools\BonusSettings();
        return $bns->getBonusSetting($planId);
    }
}

if (! function_exists('stockist_setting')) {
    /**
     * @param $planId
     *
     * @return mixed
     */
    function stockist_setting($planId)
    {
        $sts = new \App\Tools\StockistSetting();
        return $sts->getStockistSetting($planId);
    }
}

if (! function_exists('stockist_list')) {
    /**
     * @param $planId
     *
     * @return \App\Tools\StockistSetting
     */
    function stockist_list($planId)
    {
        $sts = new \App\Tools\StockistSetting();
        return $sts->getStockistList($planId);
    }
}

if (! function_exists('plan_list')) {
    /**
     * @return mixed
     */
    function plan_list()
    {
        $plan = new \App\Tools\PlanSettings();
        return $plan->getPlanList();
    }
}

if (! function_exists('active_plan')) {
    /**
     * @return mixed
     */
    function active_plan()
    {
        $plan = new \App\Tools\PlanSettings();
        return $plan->getActivePlan();
    }
}

if (! function_exists('stockist')) {
    function stockist()
    {
        if (auth()->user()->is_stockist == 0) {
            $stockist = 'member';
        } elseif (auth()->user()->is_stockist = 1 && auth()->user()->stockist_type == 0) {
            $stockist = 'stockist';
        } elseif (auth()->user()->is_stockist = 1 && auth()->user()->stockist_type == 1) {
            $stockist = 'master stockist';
        } else {
            $stockist = null;
        }

        return $stockist;
    }
}

if (! function_exists('plan_price')) {
    /**
     * @return mixed
     */
    function plan_price($planId, $subject)
    {
        $plan = new \App\Tools\PlanSettings();

        if ($subject == 'member') {
            $price = $plan->getPlanPrice($planId)->first()->activeprice->price;
        } elseif ($subject == 'stockist') {
            $price = $plan->getPlanPrice($planId)->first()->activeprice->stockist_price;
        } elseif ($subject == 'master stockist') {
            $price = $plan->getPlanPrice($planId)->first()->activeprice->master_stockist_price;
        } else {
            $price = null;
        }

        return $price;
    }
}

if (! function_exists('generate_unique_code')) {
    function generate_unique_code($prefix, $length, $model, $field)
    {
        $unique = new \App\Tools\CodeGenerator();

        $unique_code = $unique->getCode($prefix, $length);

        while ($unique->checkExistence($model, $field, $unique_code)) {
            $unique_code = $unique->getCode($prefix, $length);
            $unique->checkExistence($model, $field, $unique_code);
        }

        return $unique_code;
    }
}

if (! function_exists('generate_date_unique_code')) {
    function generate_date_unique_code($prefix, $length, $model, $field)
    {
        $unique = new \App\Tools\CodeGenerator();

        $unique_code = $unique->getDateCode($prefix, $length);

        while ($unique->checkExistence($model, $field, $unique_code)) {
            $unique_code = $unique->getDateCode($prefix, $length);
            $unique->checkExistence($model, $field, $unique_code);
        }

        return $unique_code;
    }
}

if (! function_exists('generate_unique_digit')) {
    /**
     * @param int $days
     * @param int $min
     * @param int $max
     *
     * @return int
     */
    function generate_unique_digit($days = 2, $min = 100, $max = 1800)
    {
        $unique = new \App\Tools\UniqueDigit();

        $unique_digit = rand($min, $max);

        while ($unique->checkExistence($unique_digit, $days) == true) {
            $unique_digit = rand($min, $max);
            $unique->checkExistence($unique_digit, $days);
        }

        $unique->insertNewDigit($unique_digit);

        return $unique_digit;
    }
}

if (! function_exists('company_banks')) {
    function company_banks()
    {
        $banks = new \App\Tools\CompanyBanks();
        return $banks->getCompanyBank();
    }
}

if (! function_exists('company_bank_list')) {
    function company_bank_list()
    {
        $banks = new \App\Tools\CompanyBanks();
        return $banks->getCompanyBankList();
    }
}

if (! function_exists('user_banks')) {
    function user_banks($userId)
    {
        $banks = new \App\Tools\UserBanks();
        return $banks->getUserBanks($userId);
    }
}

if (! function_exists('user_bank_list')) {
    function user_bank_list($userId)
    {
        $banks = new \App\Tools\UserBanks();
        return $banks->getUserBankList($userId);
    }
}

if (! function_exists('generate_pin')) {
    /**
     * @param int $length
     * @param $planId
     * @param $amount
     *
     * @return bool
     */
    function generate_pin($length = 12, $planId, $amount)
    {
        $code = new \App\Tools\GeneratePin();

        for ($i = 1; $i <= $amount; $i++) {
            $new_pin = $code->getRandomCode($length);

            while ($code->checkExistence($new_pin) == true) {
                $new_pin = $code->getRandomCode($length);
                $code->checkExistence($new_pin);
            }

            $code->insertNewPin($new_pin, $planId);
        }
        return true;
    }
}

if (! function_exists('check_user_data')) {
    function check_user_data()
    {
        $check = new \App\Tools\UserDataChecks();

        $return = collect();

        if ($check->checkProfile()->isNotEmpty()) {
            $return->put('profile', $check->checkProfile());
        }

        if ($check->checkAddress()->isNotEmpty()) {
            $return->put('address', $check->checkAddress());
        }

        if ($check->checkId()->isNotEmpty()) {
            $return->put('identity', $check->checkId());
        }

        if ($check->checkBank()->isNotEmpty()) {
            $return->put('bank', $check->checkBank());
        }

        return $return;
    }
}

if (! function_exists('count_my_transaction')) {
    function count_my_transaction()
    {
        $my_transaction = new \App\Tools\PinTransaction();

        return $my_transaction->getMyRunningTransaction()->count();
    }
}

if (! function_exists('count_pin_all')) {
    function count_pin_all($userId)
    {
        $pin = new \App\Tools\UserPin();

        return $pin->count_pin_all($userId);
    }
}

if (! function_exists('count_pin_available')) {
    function count_pin_available($userId)
    {
        $pin = new \App\Tools\UserPin();

        return $pin->count_pin_available($userId);
    }
}

if (! function_exists('count_pin_used')) {
    function count_pin_used($userId)
    {
        $pin = new \App\Tools\UserPin();

        return $pin->count_pin_used($userId);
    }
}

if (! function_exists('count_pin_sold')) {
    function count_pin_sold($userId)
    {
        $pin = new \App\Tools\UserPin();

        return $pin->count_pin_sold($userId);
    }
}

if (! function_exists('count_pin_transferred')) {
    function count_pin_transferred($userId)
    {
        $pin = new \App\Tools\UserPin();

        return $pin->count_pin_transferred($userId);
    }
}
