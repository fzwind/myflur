<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Village extends Model
{
    protected $guarded = [];

	protected $dates = ['created_at', 'updated_at'];

	public function district() {
		return $this->belongsTo(District::class);
	}
}
