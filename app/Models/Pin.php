<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pin extends Model
{
    protected $guarded = [];

    protected $primaryKey = ('code');

	public $incrementing = false;

    protected $dates = ['created_at', 'updated_at'];

    public function pintransaction()
    {
    	return $this->hasMany(PinTransaction::class, 'pin_code', 'code');
    }

    public function pinuser()
    {
    	return $this->hasMany(UserPin::class, 'pin_code', 'code');
    }

    public function plan()
    {
	    return $this->belongsTo(Plan::class);
    }
}
