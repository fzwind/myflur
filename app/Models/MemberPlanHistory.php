<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MemberPlanHistory extends Model
{
    protected $guarded = [];

    protected $dates = ['created_at', 'updated_at'];

    public function user() {
    	return $this->belongsTo(User::class);
    }

    public function plan() {
    	return $this->belongsTo(Plan::class);
    }
}
