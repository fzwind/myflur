<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Reward extends Model
{
    protected $guarded = [];

    protected $dates = ['created_at', 'updated_at', 'active_at', 'inactive_at'];

    public function rewardsetting()
    {
    	return $this->belongsTo(RewardSetting::class,'reward_setting_id','id');
    }

    public function memberreward()
    {
    	return $this->hasMany(MemberReward::class);
    }

    public function authmemberreward()
    {
    	return $this->hasMany(MemberReward::class,'reward_id','id')->where('user_id', auth()->id());
    }

    public function memberStructure()
    {
    	return $this->hasManyThrough(Memberstructure::class,MemberReward::class,'reward_id','user_id','id','user_id');
    }
}
