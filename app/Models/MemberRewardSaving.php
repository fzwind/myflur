<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MemberRewardSaving extends Model
{
    protected $guarded = [];

    protected $dates = ['created_at', 'updated_at'];

    public function reward_setting()
    {
    	return $this->belongsTo(RewardSetting::class);
    }

    public function withdrawal()
    {
    	return $this->belongsTo(Withdrawal::class);
    }

    public function user()
    {
    	return $this->belongsTo(User::class);
    }
}
