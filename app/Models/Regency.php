<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Regency extends Model
{
    protected $guarded = [];

    protected $dates = ['created_at', 'updated_at'];

    public function province() {
    	return $this->belongsTo(Province::class);
    }

    public function district() {
    	return $this->hasMany(District::class);
    }
}
