<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;
use App\Models\MemberRewardStatus;

class User extends Authenticatable
{
    use Notifiable, HasRoles;

    protected $guarded = [];

    protected $appends = ['premium'];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $dates = ['created_at', 'updated_at', 'birth_date', 'stockist_at', 'active_at', 'inactive_at', 'current_plan_at', 'qualified_at'];


    public function memberstructure()
    {
        return $this->hasOne(Memberstructure::class);
    }

    public function sponsor()
    {
        return $this->belongsTo(User::class, 'sponsor_id', 'id');
    }

    public function province()
    {
        return $this->belongsTo(Province::class);
    }

    public function regency()
    {
        return $this->belongsTo(Regency::class);
    }

    public function district()
    {
        return $this->belongsTo(District::class);
    }

    public function village()
    {
        return $this->belongsTo(Village::class);
    }

    public function bank()
    {
        return $this->hasMany(UserBank::class);
    }

    public function getNameWithProvinceRegencyAttribute()
    {
        return $this->name .' ('. $this->province->name .' - '. $this->regency->name .')';
    }

    public function getNameWithEmailUsernameAttribute()
    {
        return $this->name .' - '. $this->email .' - '. $this->username;
    }

    public function getPremiumAttribute()
    {
        $member = MemberRewardStatus::where('user_id', $this->id)->where('reward_setting_id', 1)->first();
        if ($member) {
            return true;
        } else {
            return false;
        }
    }
}
