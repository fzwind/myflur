<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WithdrawalSetting extends Model
{
    protected $guarded = [];

    protected $dates = ['created_at', 'updated_at', 'active_at'];
}
