<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BonusSetting extends Model
{
	protected $guarded = [];

    protected $dates = ['active_at', 'created_at', 'updated_at'];

    public function bonuslevel() {
    	return $this->hasMany(BonusLevelSetting::class);
    }

    public function plan() {
    	return $this->belongsTo(Plan::class);
    }
}
