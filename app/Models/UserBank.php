<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserBank extends Model
{
    protected $guarded = [];

    protected $dates = ['created_at', 'updated_at'];

    public function bank()
    {
    	return $this->belongsTo(MasterBank::class);
    }

    public function user()
    {
    	return $this->belongsTo(User::class);
    }

	public function getNameWithAccountAttribute()
	{
		return $this->bank->name .' (acc. no. '. $this->account_number .' - a.n '. $this->account_name .')';
	}

}
