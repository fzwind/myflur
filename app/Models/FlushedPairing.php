<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FlushedPairing extends Model
{
    protected $guarded = [];

    protected $dates = ['created_at', 'updated_at', 'flushed_at'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
