<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Memberstructure extends Model
{
    protected $guarded = [];
    protected $dates = ['created_at', 'updated_at'];

	public function user()
    {
    	return $this->belongsTo(User::class);
    }

    public function bonus()
    {
    	return $this->hasMany(Bonus::class,'user_id','user_id');
    }

    public function children()
    {
    	return $this->hasMany(Memberstructure::class, 'parent_id', 'id');
    }

    public function countChildren($node = null)
    {
	    $query = $this->children();
	    if (!empty($node))
	    {
		    $query = $query->where('position', $node);
	    }

	    $count = 0;
	    foreach ($query->get() as $child)
	    {
		    $count += $child->countChildren() + 1; // Plus 1 to count the direct child
	    }
	    return $count;
    }

    public function parent()
    {
    	return $this->belongsTo(Memberstructure::class,'parent_id','id');
    }

    public function allParents()
    {
    	return $this->parent()->with('AllParents');
    }

    public function memberReward()
    {
    	return $this->hasMany(MemberRewardStatus::class,'user_id','user_id');
    }
}
