<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BonusLevelSetting extends Model
{
    protected $guarded = [];

    protected $dates = ['created_at', 'updated_at'];

    public function bonussetting() {
    	return $this->belongsTo(BonusSetting::class);
    }
}
