<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RewardSetting extends Model
{
    protected $guarded = [];

    protected $dates = ['created_at', 'updated_at', 'active_at', 'inactive_at'];

    public function plan()
    {
    	return $this->belongsTo(Plan::class);
    }

    public function bonussetting()
    {
    	return $this->belongsTo(BonusSetting::class);
    }

    public function prerequisite()
    {
    	return $this->belongsTo(RewardSetting::class,'reward_prerequisite','id');
    }

    public function reward()
    {
    	return $this->hasMany(Reward::class);
    }
}