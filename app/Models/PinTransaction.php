<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PinTransaction extends Model
{
    protected $guarded = [];

    protected $dates = ['created_at', 'updated_at', 'ordered_at', 'paid_at', 'confirmed_at', 'rejected_at'];

    public function pin()
    {
    	return $this->belongsTo(Pin::class,'pin_code','code');
    }

    public function fromuser()
    {
    	return $this->belongsTo(User::class,'from');
    }

    public function touser()
    {
    	return $this->belongsTo(User::class,'to');
    }

    public function plan()
    {
    	return $this->belongsTo(Plan::class);
    }
}
