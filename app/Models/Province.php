<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
    protected $guarded = [];

    protected $dates = ['created_at', 'updated_at'];

    public function regency() {
    	return $this->hasMany(Regency::class);
    }

    public function getProvinceNameUserAttribute()
    {
    	return $this->for_user->name .' ('. $this->name;
    }
}
