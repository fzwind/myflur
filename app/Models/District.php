<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class District extends Model
{
    protected $guarded = [];

    protected $dates = ['created_at', 'updated_at'];

    public function regency() {
    	return $this->belongsTo(Regency::class);
    }

    public function village() {
    	return $this->hasMany(Village::class);
    }
}
