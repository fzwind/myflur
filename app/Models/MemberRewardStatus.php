<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MemberRewardStatus extends Model
{
    protected $guarded = [];
    protected $dates = ['created_at', 'updated_at'];

    public function user()
    {
    	return $this->belongsTo(User::class);
    }

    public function reward_setting()
    {
    	return $this->belongsTo(RewardSetting::class);
    }
}
