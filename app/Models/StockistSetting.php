<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StockistSetting extends Model {

    protected $guarded = [];

    protected $dates = ['created_at', 'updated_at', 'active_at'];

    public function plan()
    {
    	return $this->belongsTo(Plan::class);
    }
}
