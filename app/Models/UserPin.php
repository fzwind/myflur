<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserPin extends Model
{
    protected $guarded = [];

    protected $dates = ['created_at', 'updated_at'];

    public function pin()
    {
    	return $this->belongsTo(Pin::class, 'pin_code', 'code');
    }

    public function user()
    {
    	return $this->belongsTo(User::class);
    }

    public function pintransaction()
    {
    	return $this->hasMany(PinTransaction::class, 'id', 'pin_transaction_id');
    }
}
