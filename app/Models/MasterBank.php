<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MasterBank extends Model
{
    protected $guarded = [];

    protected $dates = ['created_at', 'updated_at'];

    public function user()
    {
    	return $this->hasMany(UserBank::class);
    }

    public function company()
    {
    	return $this->hasMany(CompanyBank::class);
    }
}
