<?php

namespace App\Models;

use App\Tools\TimeZone;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Plan extends Model
{
    protected $guarded = [];

    protected $date = ['created_at', 'updated_at'];

    public function bonussetting()
    {
        return $this->hasMany(BonusSetting::class);
    }

    public function memberplan()
    {
        return $this->hasMany(MemberPlanHistory::class);
    }

    public function planprices()
    {
        return $this->hasMany(PlanPriceHistory::class);
    }

    public function activeprice()
    {
        return $this->hasOne(PlanPriceHistory::class)
                    ->select('id', 'plan_id', 'price', 'stockist_price', 'master_stockist_price', 'active_at')
                    ->where('active_at', '<=', Carbon::now('Asia/Jakarta'))
                    ->orderBy('active_at', 'desc');
    }

    public function activebonussetting()
    {
        return $this->hasOne(BonusSetting::class)
            ->where('active_at', '<=', Carbon::now('Asia/Jakarta'))
            ->orderBy('active_at', 'desc');
    }
}
