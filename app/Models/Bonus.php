<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Bonus extends Model
{
    protected $guarded = [];

    protected $dates = ['created_at', 'updated_at'];

    public function user()
    {
    	return $this->belongsTo(User::class);
    }

    public function from()
    {
    	return $this->belongsTo(User::class,'bonus_from','id');
    }

    public function plan()
    {
    	return $this->belongsTo(Plan::class);
    }

    public function bonusSetting()
    {
    	return $this->belongsTo(BonusSetting::class,'bonus_setting_id','id');
    }

    public function getSumOfAllBonusesAttribute()
    {
    	return $this->bonus_sponsor + $this->bonus_pairing + $this->bonus_pairing_level + $this->bonus_level;
    }
}
