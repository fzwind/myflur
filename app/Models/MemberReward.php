<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MemberReward extends Model
{
    protected $guarded = [];

    protected $dates = ['created_at', 'updated_at'];

    public function reward()
    {
    	return $this->belongsTo(Reward::class);
    }

    public function user()
    {
    	return $this->belongsTo(User::class);
    }
}
