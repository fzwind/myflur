<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class MyFlurServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->app->bind('App\Repositories\BaseInterface', 'App\Repositories\Repo\BaseRepository');
        $this->app->bind('App\Repositories\SystemSettingsInterface', 'App\Repositories\Repo\SystemSettingsRepository');
        $this->app->bind('App\Repositories\PlanSettingInterface', 'App\Repositories\Repo\PlanSettingRepository');
        $this->app->bind('App\Repositories\PlanPriceHistoryInterface', 'App\Repositories\Repo\PlanPriceHistoryRepository');
        $this->app->bind('App\Repositories\BonusSettingInterface', 'App\Repositories\Repo\BonusSettingRepository');
        $this->app->bind('App\Repositories\BonusLevelSettingInterface', 'App\Repositories\Repo\BonusLevelSettingRepository');
        $this->app->bind('App\Repositories\StockistSettingInterface', 'App\Repositories\Repo\StockistSettingRepository');
        $this->app->bind('App\Repositories\PinInterface', 'App\Repositories\Repo\PinRepository');
        $this->app->bind('App\Repositories\UserPinInterface', 'App\Repositories\Repo\UserPinRepository');
        $this->app->bind('App\Repositories\PinTransactionInterface', 'App\Repositories\Repo\PinTransactionRepository');
        $this->app->bind('App\Repositories\MasterBankInterface', 'App\Repositories\Repo\MasterBankRepository');
        $this->app->bind('App\Repositories\CompanyBankInterface', 'App\Repositories\Repo\CompanyBankRepository');
        $this->app->bind('App\Repositories\UserBankInterface', 'App\Repositories\Repo\UserBankRepository');
        $this->app->bind('App\Repositories\ProvinceInterface', 'App\Repositories\Repo\ProvinceRepository');
        $this->app->bind('App\Repositories\RegencyInterface', 'App\Repositories\Repo\RegencyRepository');
        $this->app->bind('App\Repositories\DistrictInterface', 'App\Repositories\Repo\DistrictRepository');
        $this->app->bind('App\Repositories\VillageInterface', 'App\Repositories\Repo\VillageRepository');
        $this->app->bind('App\Repositories\UserInterface', 'App\Repositories\Repo\UserRepository');
        $this->app->bind('App\Repositories\MemberStructureInterface', 'App\Repositories\Repo\MemberStructureRepository');
        $this->app->bind('App\Repositories\MemberPlanHistoryInterface', 'App\Repositories\Repo\MemberPlanHistoryRepository');
        $this->app->bind('App\Repositories\BonusInterface', 'App\Repositories\Repo\BonusRepository');
        $this->app->bind('App\Repositories\FlushedPairingInterface', 'App\Repositories\Repo\FlushedPairingRepository');
        $this->app->bind('App\Repositories\RewardSettingInterface', 'App\Repositories\Repo\RewardSettingRepository');
        $this->app->bind('App\Repositories\RewardInterface', 'App\Repositories\Repo\RewardRepository');
        $this->app->bind('App\Repositories\MemberRewardStatusInterface', 'App\Repositories\Repo\MemberRewardStatusRepository');
        $this->app->bind('App\Repositories\MemberRewardSavingInterface', 'App\Repositories\Repo\MemberRewardSavingRepository');
        $this->app->bind('App\Repositories\MemberRewardInterface', 'App\Repositories\Repo\MemberRewardRepository');
        $this->app->bind('App\Repositories\WithdrawalSettingInterface', 'App\Repositories\Repo\WithdrawalSettingRepository');
        $this->app->bind('App\Repositories\WithdrawalInterface', 'App\Repositories\Repo\WithdrawalRepository');
        $this->app->bind('App\Repositories\RequestStockistInterface', 'App\Repositories\Repo\RequestStockistRepository');
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
