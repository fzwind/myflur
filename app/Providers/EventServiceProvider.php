<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        'App\Events\Members\UserRegistered' => [
            'App\Listeners\Members\MembersPlanHistoryUpdate',
            'App\Listeners\Members\SendWelcomeEmail',
        ],
        'App\Events\Members\UserPlaced' => [
            'App\Listeners\Members\DistributeBonus',
            'App\Listeners\Members\CalculateRewards',
        ],
        'App\Events\Pin\OrderMade'          => [
            'App\Listeners\Pin\LogOrder',
            'App\Listeners\Pin\SendOrderNotifications',
        ],
        'App\Events\Pin\OrderPaid'         => [
            'App\Listeners\Pin\LogPayment',
            'App\Listeners\Pin\SendPaymentNotifications',
        ],
        'App\Events\Pin\OrderConfirmed'     => [
            'App\Listeners\Pin\MoveOrderPinAccordingly',
            'App\Listeners\Pin\LogOrderMovement',
            'App\Listeners\Pin\SendOrderConfirmationNotifications',
        ],
        'App\Events\Pin\TransferMade' => [
            'App\Listeners\Pin\MoveTransferPinAccordingly',
            'App\Listeners\Pin\LogTransferMovement',
            'App\Listeners\Pin\SendTransferConfirmationNotifications',
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
