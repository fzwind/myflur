<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Schema;
use App\Tools\SystemSettings;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        Schema::defaultStringLength(191);

        Blade::if('system', function ($attribute) {
            $sys = new SystemSettings();
            return $sys->getSystemSettings()->$attribute;
        });

        Blade::if('nonstockist', function () {
            if (auth()->user()->is_stockist == 0) {
                return true;
            }
            return false;
        });

        Blade::if('stockist', function () {
            if (auth()->user()->is_stockist == 1 && auth()->user()->stockist_type == 0) {
                return true;
            }
            return false;
        });

        Blade::if('masterstockist', function () {
            if (auth()->user()->is_stockist == 1 && auth()->user()->stockist_type == 1) {
                return true;
            }
            return false;
        });
    }
}
