<?php

namespace App\Listeners\Members;

use App\Events\Members\UserRegistered;
use App\Repositories\MemberPlanHistoryInterface;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class MembersPlanHistoryUpdate
{
	public $member_plan_history;

    public function __construct(MemberPlanHistoryInterface $member_plan_history)
    {
        $this->member_plan_history = $member_plan_history;
    }

    public function handle ( UserRegistered $event)
    {
    	// $event is a User object
    	$data = [];
    	$data['user_id'] = $event->user->id;
    	$data['plan_id'] = $event->user->join_plan;

		$this->member_plan_history->create($data);
    }
}
