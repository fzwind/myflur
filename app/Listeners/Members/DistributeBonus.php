<?php

namespace App\Listeners\Members;

use App\Events\Members\UserPlaced;
use App\Models\Bonus;
use App\Repositories\BonusInterface;
use App\Repositories\FlushedPairingInterface;
use App\Repositories\MemberRewardStatusInterface;
use App\Repositories\MemberStructureInterface;
use App\Repositories\RewardSettingInterface;
use App\Tools\BonusTool;
use App\Tools\SetRewardStatusService;
use Carbon\Carbon;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class DistributeBonus
{
    use BonusTool;

    public $bonus;
    public $structure;
    public $flushed_pairing;
    public $reward_setting;
    public $member_reward_status;
    public $set_reward_status;

    public function __construct(
        BonusInterface $bonus,
        MemberStructureInterface $structure,
        FlushedPairingInterface $flushed_pairing,
        RewardSettingInterface $reward_setting,
        MemberRewardStatusInterface $member_reward_status,
        SetRewardStatusService $set_reward_status
    ) {
        $this->bonus = $bonus;
        $this->structure = $structure;
        $this->flushed_pairing = $flushed_pairing;
        $this->reward_setting = $reward_setting;
        $this->member_reward_status = $member_reward_status;
        $this->set_reward_status = $set_reward_status;
    }

    public function handle(UserPlaced $event)
    {
        // $event is contain a User object and a Structure object
        $user = $event->user;
        $user_structure = $event->structure;

        $parents = $this->getParents($user_structure->code);

        foreach ($parents as $parent) {
            $structure = $this->structure->findByAttributes(['code' => $parent]);
            $bonus_sponsor_amount = 0;
            $bonus_pairing_amount = 0;
            $bonus_pairing_level_amount = 0;

            $bonus = new Bonus();
            $bonus_code = $this->getBonusCode($bonus);

            if (system_settings()->bonus_sponsor) {
                $bonus_sponsor_amount = $this->calculateBonusSponsor($structure, $user);
            }

            if (system_settings()->bonus_pairing) {
                $bonus_pairing_amount = $this->calculateBonusPairing($structure, $user, $user_structure);
            }

            if (system_settings()->bonus_pairing_level) {
                $bonus_pairing_level_amount = $this->calculateBonusPairingLevel($structure, $user, $user_structure);
            }


            $is_bonus_sponsor = $bonus_sponsor_amount > 0 ? 1 : 0;
            $is_bonus_pairing = $bonus_pairing_amount > 0 ? 1 : 0;

            $bonus_sponsor_amount = $bonus_sponsor_amount == 1 ? 0 : $bonus_sponsor_amount;
            $bonus_pairing_amount = $bonus_pairing_amount == 1 ? 0 : $bonus_pairing_amount;

            $data = [];
            $data['code'] = $bonus_code;
            $data['plan_id'] = $user->join_plan;
            $data['bonus_setting_id'] = bonus_setting($user->join_plan)->id;
            $data['user_id'] = $structure->user_id;
            $data['bonus_sponsor'] = $bonus_sponsor_amount;
            $data['bonus_pairing'] = $bonus_pairing_amount;
            $data['bonus_pairing_level'] = $bonus_pairing_level_amount;
            $data['is_bonus_sponsor'] = $is_bonus_sponsor;
            $data['is_bonus_pairing'] = $is_bonus_pairing;
            $data['bonus_from'] = $user->id;
            $data['level'] = $user_structure->level;

            if ($is_bonus_sponsor == 1 || $is_bonus_pairing == 1 || $bonus_pairing_level_amount > 0) {
                try {
                    DB::transaction(function () use ($bonus, $data) {
                        $bonus->create($data);
                    }, 3);
                    $reward_settings = $this->reward_setting->getAllActive();
                    foreach ($reward_settings as $reward_setting) {
                        $this->set_reward_status->updateRewardStatus($structure, $reward_setting);

                        if ($reward_setting->reward_type == 1) {
                            $this->set_reward_status->updateStructureReward($structure, $reward_setting);
                        }
                    }
                } catch (\Exception $e) {
                    dd($e);
                }
            }
        }
    }

    private function calculateBonusSponsor($structure, $user)
    {
        $bonus_sponsor_amount = 0;

        if ($structure->user_id == $user->sponsor_id) {
            $bonus_sponsor_amount = (system_settings()->bonus_sponsor) ? bonus_setting($user->join_plan)->bonus_sponsor_amount : 0;
        }

        $reward_potong_langsung = $this->reward_setting->getRewardPotongLangsung('bonus sponsor');

        foreach ($reward_potong_langsung as $reward) {
            $amount_potongan = ($reward->deduction_type == 1) ? bonus_setting($user->join_plan)->bonus_sponsor_amount * $reward->deduction_value / 100 : $reward->deduction_value;
            $kali_potongan = ceil($reward->deduction_target / $amount_potongan);

            $count_sponsor = $this->countSponsor($structure->user_id);
            if ($count_sponsor < $kali_potongan) {
                if ($structure->user_id == $user->sponsor_id) {
                    $bonus_sponsor_amount = bonus_setting($user->join_plan)->bonus_sponsor_amount - $amount_potongan;
                    $bonus_sponsor_amount = $bonus_sponsor_amount > 0 ? $bonus_sponsor_amount : 1;
                }
            }
            // elseif ($count_sponsor == $kali_potongan) {
            //     if ($structure->user_id == $user->sponsor_id) {
            //         $this->member_reward_status->setStatus($structure->user_id, $reward->id);
            //     }
            // }
        }

        return $bonus_sponsor_amount;
    }

    private function calculateBonusPairing($structure, $user, $user_structure)
    {
        $bonus_pairing_amount = 0;

        $max_per_day = $this->maxPerDay($user->join_plan);

        $left_today = $this->structure->countMyLeftStructure($structure, Carbon::today('Asia/Jakarta'));
        $right_today = $this->structure->countMyRightStructure($structure, Carbon::today('Asia/Jakarta'));

        if ($left_today > $max_per_day && $right_today > $max_per_day) {
            if (bonus_setting($user->join_plan)->over_pairing_handle == 2) {
                $position = ($user_structure->position == 1) ? 'left' : 'right';
                $this->flushed_pairing->updateFlushedPairing($position, $structure->user_id, $user_structure->id);
            }
        } else {
            $left = $this->structure->countMyLeftStructure($structure);
            $right = $this->structure->countMyRightStructure($structure);
            $flushed_left = 0;
            $flushed_right = 0;

            if (bonus_setting($user->join_plan)->over_pairing_handle == 2) {
                $flushed_left = $this->flushed_pairing->getFlushedPairing()->sum('flushed_left');
                $flushed_right = $this->flushed_pairing->getFlushedPairing()->sum('flushed_right');
            }
            $pairing_bonus = $this->isPairingBonus($structure, $user_structure, $user, $left, $right, $flushed_left, $flushed_right);

            if ($pairing_bonus) {
                $bonus_pairing_amount = bonus_setting($user->join_plan)->bonus_pairing_amount;

                $reward_potong_langsung = $this->reward_setting->getRewardPotongLangsung('bonus pairing');

                foreach ($reward_potong_langsung as $reward) {
                    $amount_potongan = ($reward->deduction_type == 1) ? bonus_setting($user->join_plan)->bonus_pairing_amount * $reward->deduction_value / 100 : $reward->deduction_value;
                    $kali_potongan = ceil($reward->deduction_target / $amount_potongan);

                    $count_pairing = $this->countPairing($structure->user_id);
                    if ($count_pairing < $kali_potongan) {
                        $bonus_pairing_amount = bonus_setting($user->join_plan)->bonus_pairing_amount - $amount_potongan;
                        $bonus_pairing_amount = $bonus_pairing_amount > 0 ? $bonus_pairing_amount : 1;
                    }
                    // elseif ($count_pairing == $kali_potongan) {
                    //     $this->member_reward_status->setStatus($structure->user_id, $reward->id);
                    // }
                }
            }
        }

        return $bonus_pairing_amount;
    }

    private function calculateBonusPairingLevel($structure, $user, $user_structure)
    {
        $bonus_pairing_level_amount = 0;

        $left = $this->structure->countMyLeftStructure($structure);
        $right = $this->structure->countMyRightStructure($structure);
        $flushed_left = 0;
        $flushed_right = 0;

        if (bonus_setting($user->join_plan)->over_pairing_handle == 2) {
            $flushed_left = $this->flushed_pairing->getFlushedPairing()->sum('flushed_left');
            $flushed_right = $this->flushed_pairing->getFlushedPairing()->sum('flushed_right');
        }

        $pairing_bonus = $this->isPairingBonus($structure, $user_structure, $user, $left, $right, $flushed_left, $flushed_right);

        if ($pairing_bonus) {
            $count_pairing_level = $this->countPairingLevel($structure->user_id, $user_structure->level);

            if ($count_pairing_level < bonus_setting($user->join_plan)->pair_per_level) {
                $bonus_pairing_level_amount = bonus_setting($user->join_plan)->bonus_pairing_level_amount;
            }

            if ($count_pairing_level < bonus_setting($user->join_plan)->pair_per_level && $bonus_pairing_level_amount > 0) {
                $reward_potong_langsung = $this->reward_setting->getRewardPotongLangsung('bonus pairing level');

                foreach ($reward_potong_langsung as $reward) {
                    $amount_potongan = ($reward->deduction_type == 1) ? bonus_setting($user->join_plan)->bonus_pairing_level_amount * $reward->deduction_value / 100 : $reward->deduction_value;
                    $kali_potongan = ceil($reward->deduction_target / $amount_potongan);

                    $count_all_pairing_level = $this->countAllPairingLevel($structure->user_id);
                    if ($count_all_pairing_level < $kali_potongan) {
                        $bonus_pairing_level_amount = bonus_setting($user->join_plan)->bonus_pairing_level_amount - $amount_potongan;
                    }
                    // elseif ($count_all_pairing_level == $kali_potongan) {
                    //     $this->member_reward_status->setStatus($structure->user_id, $reward->id);
                    // }
                }
            }
        }

        return $bonus_pairing_level_amount;
    }
}
