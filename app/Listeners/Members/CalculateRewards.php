<?php

namespace App\Listeners\Members;

use App\Events\Members\UserPlaced;
use App\Repositories\MemberRewardInterface;
use App\Repositories\MemberStructureInterface;
use App\Repositories\RewardInterface;
use App\Repositories\RewardSettingInterface;
use App\Tools\BonusTool;
use Carbon\Carbon;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;

class CalculateRewards
{
    /*
     * Calculate pure left-right structure rewards
     */

    use BonusTool;

    public $reward_setting;
    public $member_reward;
    public $rewards;
    public $structure;

    public function __construct(
        RewardSettingInterface $reward_setting,
        MemberRewardInterface $member_reward,
        RewardInterface $rewards,
        MemberStructureInterface $structure
    ) {
        $this->reward_setting = $reward_setting;
        $this->member_reward = $member_reward;
        $this->rewards = $rewards;
        $this->structure = $structure;
    }

    public function handle(UserPlaced $event)
    {
        $user_structure = $event->structure;

        $parents = $this->getParents($user_structure->code);

        foreach ($parents as $parent) {
            $structure = $this->structure->findByAttributes([ 'code' => $parent ]);

            try {
                $this->calculateRewardStructure($structure);
            } catch (\Exception $e) {
                Log::error('error calculate pure left-right structure reward : '. $e);
                return false;
            }
        }
    }

    public function calculateRewardStructure($structure)
    {
        $reward_structure = $this->reward_setting->getRewardStructureOnly();

        foreach ($reward_structure as $reward) {
            $reward_item = $this->rewards->getBySetting($reward->id);

            foreach ($reward_item as $item) {
                $left_achievement = $this->structure->countMyLeftStructure($structure, null, $item->active_at, $item->inactive_at);
                $right_achievement = $this->structure->countMyRightStructure($structure, null, $item->active_at, $item->inactive_at);

                $achievement = [];
                $achievement['left'] = $left_achievement;
                $achievement['right'] = $right_achievement;
                if ($left_achievement >= $item->left && $right_achievement >= $item->right) {
                    $achievement['is_achieved'] = 1;
                    $achievement['achieved_at'] = Carbon::now('Asia/Jakarta');
                } else {
                    $achievement['is_achieved'] = 0;
                    $achievement['achieved_at'] = null;
                }

                $achievement = (object) $achievement;

                $this->member_reward->setMemberReward($item->id, $structure->user_id, $achievement);
            }
        }
    }

    public function calculateRewardStructureNonPure($structure)
    {
        $reward_structure = $this->reward_setting->getRewardNonStructureOnly();

        foreach ($reward_structure as $reward_setting) {
            $rewards = $this->rewards->getBySetting($reward_setting->id);

            $qualified = $this->member_reward_status->getStatus($reward_setting->id, $structure->user_id);

            $left_reward = $this->member_structure->countMyLeftReward($structure, $reward_setting->id);
            $right_reward = $this->member_structure->countMyRightReward($structure, $reward_setting->id);

            foreach ($rewards as $reward) {
                $member_reward = $this->member_reward->getByAttributes(
                    [
                        'reward_id' => $reward->id,
                        'user_id' => $structure->user_id
                    ]
                );

                if ($left_reward > 0 || $right_reward > 0) {
                    if ($member_reward->isEmpty() || $member_reward['is_achieved'] != 1) {
                        DB::transaction(function () use ($reward, $structure, $qualified, $left_reward, $right_reward) {
                            $is_achieved = 0;
                            $achieved_at = null;
                            if ($qualified && $left_reward >= $reward->left && $right_reward >= $reward->right) {
                                $is_achieved = 1;
                                $achieved_at = Carbon::now('Asia/Jakarta');
                            }

                            $this->member_reward->updateOrCreate(
                                [
                                    'reward_id' => $reward->id,
                                    'user_id' => $structure->user_id,
                                ],
                                [
                                    'left' => $left_reward,
                                    'right' => $right_reward,
                                    'is_achieved' => $is_achieved,
                                    'achieved_at' => $achieved_at,
                                ]
                            );
                        }, 3);
                    }
                }
            }
        }
    }
}
