<?php

namespace App\Listeners\Members;

use App\Events\Members\UserRegistered;
use App\Mail\WelcomeMessage;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

class SendWelcomeEmail implements ShouldQueue
{
    public function __construct()
    {
        //
    }

    public function handle(UserRegistered $event)
    {
        $user = $event->user;
        // $email = new WelcomeMessage($user);
        // Mail::to($user->email)->send($email);
    }
}
