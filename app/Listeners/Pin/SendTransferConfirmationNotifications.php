<?php

namespace App\Listeners\Pin;

use App\Events\Pin\TransferMade;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendTransferConfirmationNotifications
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  TransferMade  $event
     * @return void
     */
    public function handle(TransferMade $event)
    {
        //
    }
}
