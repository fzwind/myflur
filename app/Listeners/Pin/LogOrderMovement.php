<?php

namespace App\Listeners\Pin;

use App\Events\Pin\OrderConfirmed;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;

class LogOrderMovement
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  OrderConfirmed  $event
     * @return void
     */
    public function handle(OrderConfirmed $event)
    {
	    if (system_settings()->logging) {
		    Log::info(
			    'Pin Transaction : '.
			    auth()->user()->name .' ('. auth()->user()->username .') confirm pin transaction (code : '. $event->pin_transaction->transaction_code .') successfully'
		    );
	    }

    }
}
