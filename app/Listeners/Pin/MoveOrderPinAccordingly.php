<?php

namespace App\Listeners\Pin;

use App\Events\Pin\OrderConfirmed;
use App\Repositories\PinInterface;
use App\Repositories\UserPinInterface;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\DB;

class MoveOrderPinAccordingly
{

	public $pin;
	public $user_pin;

    public function __construct(PinInterface $pin, UserPinInterface $user_pin)
    {
        $this->pin = $pin;
        $this->user_pin = $user_pin;
    }

    public function handle(OrderConfirmed $event)
    {
    	$transaction = $event->pin_transaction;

	    if ($transaction->transaction_type == 'buy' && $transaction->from == 11) {

		    DB::transaction(function () use($transaction) {
			    $pins = $this->pin->getPinforUpdate($transaction)->get();

			    if ($pins->count() >= $transaction->amount) {
				    foreach ($pins as $pin) {
					    $this->user_pin->create([
						    'user_id' => $transaction->to,
						    'pin_transaction_id' => $transaction->id,
						    'pin_code' => $pin->code,
						    'status' => 0,
					    ]);

					    $pin->status = 1;
					    $pin->save();
				    }
			    } else {
			    	return false;
			    }

		    },3);

	    } elseif ($transaction->transaction_type == 'buy' && $transaction->from != 11) {

		    DB::transaction(function () use($transaction) {
			    $pins = $this->user_pin->getPinforUpdate($transaction)->get();

			    if ($pins->count() >= $transaction->amount) {
				    foreach ($pins as $pin) {
					    $this->user_pin->create([
						    'user_id' => $transaction->to,
						    'pin_transaction_id' => $transaction->id,
						    'pin_code' => $pin->pin_code,
						    'status' => 0,
					    ]);

					    $pin->status = 1;
					    $pin->save();
				    }
			    } else {
			    	return false;
			    }

		    },3);

	    }
    }
}
