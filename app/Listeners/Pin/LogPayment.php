<?php

namespace App\Listeners\Pin;

use App\Events\Pin\OrderPaid;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;

class LogPayment
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  OrderPaid  $event
     * @return void
     */
    public function handle(OrderPaid $event)
    {
	    if (system_settings()->logging) {
		    Log::info(
			    'Pin Transaction : '.
			    auth()->user()->name .' ('. auth()->user()->username .') made a paymen for transaction code : '. $event->pin_transaction->transaction_code .' successfully'
		    );
	    }
    }
}
