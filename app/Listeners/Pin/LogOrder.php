<?php

namespace App\Listeners\Pin;

use App\Events\Pin\OrderMade;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;

class LogOrder
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  OrderMade  $event
     * @return void
     */
    public function handle(OrderMade $event)
    {
	    if (system_settings()->logging) {
			Log::info(
				'Pin Transaction : '.
				auth()->user()->name .' ('. auth()->user()->username .') made a pin '. $event->pin_transaction->transaction_type .' order successfully'
			);
		}
    }
}
