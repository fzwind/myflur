<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['namespace' => 'Member','middleware' => 'auth'], function () {
    // networks routes
    Route::prefix('networks')->group(function () {
        // Registration Routes...
        Route::get('register', 'UserController@create')->name('register');
        Route::post('registerpost', 'UserController@store')->name('register.store');

        Route::get('genealogy/{top_id?}', 'MemberstructureController@genealogy')->name('genealogy');
        Route::get('placement-list/{parent_id}/{position}', 'MemberstructureController@getPlacementList')->name('placement.list');
        Route::post('placement', 'MemberstructureController@placement')->name('placement');
        Route::get('all', 'MemberstructureController@all')->name('networks.all');
        Route::get('all-data', 'MemberstructureController@allData')->name('networks.all.data');
        Route::get('sponsor', 'UserController@sponsor')->name('networks.sponsor');
        Route::get('sponsor-data', 'UserController@sponsorData')->name('networks.sponsor.data');
    });

    // bonus routes
    Route::prefix('bonus')->group(function () {
        Route::get('summary', 'BonusController@summary')->name('bonus.summary');
        Route::get('summary-data', 'BonusController@summaryData')->name('bonus.summary.data');
        Route::get('sponsor', 'BonusController@sponsor')->name('bonus.sponsor');
        Route::get('sponsor-data', 'BonusController@sponsorData')->name('bonus.sponsor.data');
        Route::get('pairing', 'BonusController@pairing')->name('bonus.pairing');
        Route::get('pairing-data', 'BonusController@pairingData')->name('bonus.pairing.data');
        Route::get('pairing-level', 'BonusController@pairingLevel')->name('bonus.pairing-level');
        Route::get('pairing-level-data', 'BonusController@pairingLevelData')->name('bonus.pairing-level.data');
    });

    // reward routes
    Route::prefix('reward')->group(function () {
        Route::get('my-achievement', 'MemberRewardController@index')->name('reward.achievement');
        Route::get('my-savings/{reward_setting}', 'MemberRewardSavingController@index')->name('reward.saving');
        Route::get('my-savings-data/{reward_setting}', 'MemberRewardSavingController@getSavingData')->name('reward.saving.data');
        Route::get('reward-claim/{member_reward_id}', 'MemberRewardController@claim')->name('reward.claim');
    });

    // withdrawal routes
    Route::prefix('withdrawal')->group(function () {
        Route::get('withdrawal', 'WithdrawalController@index')->name('withdrawal.index');
        Route::get('withdrawal-data', 'WithdrawalController@getWithdrawalData')->name('withdrawal.data');
    });

    //user My Account routes
    Route::prefix('my-account')->group(function () {
        Route::get('/', 'AccountController@profile')->name('my.account');
        Route::get('address', 'AccountController@address')->name('my.account.address');
        Route::get('id', 'AccountController@identity')->name('my.account.id');
        Route::put('/{user}', 'AccountController@store')->name('my.account.store');
        Route::put('bank/{bank}', 'AccountController@updateBank')->name('my.account.bank.update');
        Route::get('bank/{bank}', 'AccountController@togleBankStatus')->name('my.account.bank.status');
        Route::get('bank', 'AccountController@bank')->name('my.account.bank');
        Route::post('bank', 'AccountController@storeBank')->name('my.account.bank.store');
        Route::get('change-password', 'AccountController@changePassword')->name('my.account.changepw');
    });

    // user PIN
    Route::resources([
        'user-pin' => 'UserPinController',
    ]);
    Route::get('pin-data', 'UserPinController@getUserPinData')->name('user.pin.data');
    Route::get('pin-available', 'UserPinController@available')->name('user.pin.available');
    Route::get('pin-available-data', 'UserPinController@getAvailableData')->name('user.pin.available.data');
    Route::get('pin-used', 'UserPinController@used')->name('user.pin.used');
    Route::get('pin-used-data', 'UserPinController@getUsedData')->name('user.pin.used.data');
    Route::get('pin-sold', 'UserPinController@sold')->name('user.pin.sold');
    Route::get('pin-sold-data', 'UserPinController@getSoldData')->name('user.pin.sold.data');
    Route::get('pin-transferred', 'UserPinController@transferred')->name('user.pin.transferred');
    Route::get('pin-transferred-data', 'UserPinController@getTransferredData')->name('user.pin.transferred.data');

    // user PIN Transaction
    Route::resources([
        'transaction-pin' => 'PinTransactionController',
    ]);

    Route::prefix('transactionpin')->group(function () {
        Route::get('data', 'PinTransactionController@getTransactionHistoryData')->name('transaction.pin.data');
        Route::post('stockist-based-on-plan', 'PinTransactionController@getStockistBasedOnPlan')->name('transaction.get.stockist');
        Route::get('running', 'PinTransactionController@runningTransaction')->name('transaction.pin.running');
        Route::get('running-data', 'PinTransactionController@getRunningTransactionData')->name('transaction.running.pin.data');
        Route::get('invoice/{pin_transaction}', 'PinTransactionController@invoice')->name('transaction.pin.invoice');
        Route::get('upload/{pin_transaction}', 'PinTransactionController@upload')->name('transaction.pin.upload');
        Route::post('upload/{pin_transaction}', 'PinTransactionController@doUpload')->name('transaction.pin.doupload');
        Route::get('image/{pin_transaction}', 'PinTransactionController@image')->name('transaction.pin.image');
        Route::post('reject', 'PinTransactionController@reject')->name('transaction.pin.reject');
    });

    // user Request Stockist
    Route::prefix('stockist')->group(function () {
        Route::get('request', 'RequestStockistController@index')->name('stockist.request');
        Route::post('post-request', 'RequestStockistController@postRequest')->name('stockist.request.post');
    });

    Route::post('get-regency', 'AccountController@getRegency')->name('get.regency');
    Route::post('get-district', 'AccountController@getDistrict')->name('get.district');
    Route::post('get-village', 'AccountController@getVillage')->name('get.village');
});

Route::middleware(['auth'])->group(function () {
    //** ADMIN MENU GROUP ROUTES
    Route::group(['prefix' => 'admin', 'middleware' => ['role:super-administrator|administrators']], function () {

        //PIN routes
        Route::resources([
            'pin' => 'Admin\PinController',
        ]);
        Route::get('pin-data', 'Admin\PinController@getPinData')->name('pin.data');
        Route::get('pin-history', 'Admin\PinController@history')->name('pin.history');
        Route::get('pin-history-data', 'Admin\PinController@getPinHistoryData')->name('pin.history.data');
        Route::post('pin-reject', 'Admin\PinController@reject')->name('pin.reject');

        //Withdrawal route
        Route::get('withdrawal', 'Admin\WithdrawalController@index')->name('admin.withdrawal.index');
        Route::get('withdrawal-data', 'Admin\WithdrawalController@getWithdrawalData')->name('admin.withdrawal.index.data');
        Route::get('withdrawal-history', 'Admin\WithdrawalController@history')->name('admin.withdrawal.history');
        Route::get('withdrawal-history-data', 'Admin\WithdrawalController@getWithdrawalHistoryData')->name('admin.withdrawal.history.data');
        Route::put('withdrawal-transfer', 'Admin\WithdrawalController@transfer')->name('admin.withdrawal.transfer');

        //Reward route
        Route::get('reward', 'Admin\MemberRewardController@index')->name('admin.member-reward.index');
        Route::get('reward-data', 'Admin\MemberRewardController@getRewardData')->name('admin.member-reward.index.data');
        Route::get('reward-history', 'Admin\MemberRewardController@history')->name('admin.member-reward.history');
        Route::get('reward-history-data', 'Admin\MemberRewardController@getRewardHistoryData')->name('admin.member-reward.history.data');
        Route::put('reward-process', 'Admin\MemberRewardController@process')->name('admin.member-reward.process');

        //Request Stockist route
        Route::get('request-stockist', 'Admin\RequestStockistController@index')->name('admin.request-stockist.index');
        Route::get('request-stockist-data', 'Admin\RequestStockistController@getRequestStockistData')->name('admin.request-stockist.index.data');
        Route::get('request-stockist-history', 'Admin\RequestStockistController@history')->name('admin.request-stockist.history');
        Route::get('request-stockist-history-data', 'Admin\RequestStockistController@getRequestStockistHistoryData')->name('admin.request-stockist.history.data');
        Route::put('request-stockist-process', 'Admin\RequestStockistController@process')->name('admin.request-stockist.process');
        Route::get('stockist', 'Admin\RequestStockistController@stockistIndex')->name('admin.stockist.index');

        // Plan Setting routes
        Route::resources([
            'plan-settings' => 'Admin\PlanSettingController',
        ]);
        Route::resources([
            'plan-price' => 'Admin\PlanPriceHistoryController',
        ]);
        Route::get('plan-settings-data', 'Admin\PlanSettingController@getPlanSettingData')->name('plan.setting.data');

        // Bonus Setting routes
        Route::resources([
            'bonus-settings' => 'Admin\BonusSettingController',
        ]);
        Route::get('bonus-settings-data', 'Admin\BonusSettingController@getBonusSettingData')->name('bonus.setting.data');

        // Reward Setting routes
        Route::resources([
            'reward-settings' => 'Admin\RewardSettingController',
        ]);
        Route::get('reward-settings-data', 'Admin\RewardSettingController@getRewardSettingData')->name('reward.setting.data');

        // Rewards routes
        Route::resources([
            'rewards' => 'Admin\RewardController',
        ]);
        Route::get('rewards-data', 'Admin\RewardController@getRewardsData')->name('rewards.data');

        // WithdrawalService Setting routes
        Route::resources([
            'withdrawal-settings' => 'Admin\WithdrawalSettingController',
        ]);
        Route::get('withdrawal-settings-data', 'Admin\WithdrawalSettingController@getWithdrawalSettingData')->name('withdrawal.setting.data');

        // Pin Setting routes
        Route::prefix('pin-settings')->group(function () {
            Route::get('/', 'Admin\PinSettingController@index')->name('pin-settings.index');
            Route::get('data', 'Admin\PinSettingController@getPinSettingData')->name('pin.setting.data');
            Route::get('generate', 'Admin\PinSettingController@generatePinForm')->name('pin.generate');
            Route::post('generate', 'Admin\PinSettingController@generatePinPost')->name('pin.generate.post');
        });

        // Stockist Setting routes
        Route::resources([
            'stockist-settings' => 'Admin\StockistSettingController',
        ]);
        Route::get('stockist-settings-data', 'Admin\StockistSettingController@getStockistSettingData')->name('stockist.setting.data');

        // Company Bank routes
        Route::resources([
            'company-bank' => 'Admin\CompanyBankController',
        ]);
        Route::get('company-bank-data', 'Admin\CompanyBankController@getCompanyBankData')->name('company.bank.data');

        //** SUPER-ADMIN MENU GROUP ROUTES
        Route::group(['prefix' => 'system', 'middleware' => ['role:super-administrator']], function () {
            // System General Setting routes
            Route::get('general-settings', 'Admin\SystemSettingController@general')->name('system.general.settings');
            Route::post('general-settings', 'Admin\SystemSettingController@generalUpdate')->name('system.general.settings.post');
            // System Member meta-data Setting routes
            Route::get('member-meta-settings', 'Admin\SystemSettingController@memberMeta')->name('system.member-meta.settings');
            Route::post('member-meta-settings', 'Admin\SystemSettingController@memberMetaUpdate')->name('system.member-meta.settings.post');
            // System Bonus Setting routes
            Route::get('bonus-settings', 'Admin\SystemSettingController@bonus')->name('system.bonus.settings');
            Route::post('bonus-settings', 'Admin\SystemSettingController@bonusUpdate')->name('system.bonus.settings.post');
            // System Cache Clear routes
            Route::get('cache-clear', 'Admin\SystemSettingController@cacheClear')->name('system.cache.clear');
            Route::post('cache-clear/{cache}', 'Admin\SystemSettingController@cacheClearPost')->name('system.cache.clear.post');
            Route::resources([
                'system-bank' => 'Admin\MasterBankController',
            ]);
            Route::get('bank-data', 'Admin\MasterBankController@getMasterBankData')->name('system.bank.data');
        });
    });
});
